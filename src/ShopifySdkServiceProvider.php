<?php
namespace Thainph\ShopifySdk;

use Illuminate\Support\ServiceProvider;

class ShopifySdkServiceProvider extends ServiceProvider
{
    public function register()
    {
        foreach (Shopify::REST_SERVICES as $serviceClass) {
            $this->app->singleton($serviceClass, function ($app) use ($serviceClass) {
                return new $serviceClass();
            });
        }

        foreach (Shopify::GRAPH_SERVICES as $serviceClass) {
            $this->app->singleton($serviceClass, function ($app) use ($serviceClass) {
                return new $serviceClass();
            });
        }
    }
}
