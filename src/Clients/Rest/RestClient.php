<?php

namespace Thainph\ShopifySdk\Clients\Rest;

use Illuminate\Http\Client\PendingRequest;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Facades\Http;

class RestClient
{

    private PendingRequest $httpClient;
    private string $baseUrl;
    private string $unstableUrl;
    private string $paymentUrl;


    public function __construct($shopDomain, $accessToken, $apiVersion)
    {
        $this->baseUrl = 'https://' . $shopDomain . '/admin/api/' . $apiVersion;
        $this->unstableUrl = 'https://' . $shopDomain . '/admin/api/unstable';
        $this->paymentUrl = 'https://' . $shopDomain . '/payments_apps/api/' . $apiVersion;

        $this->httpClient = Http::withHeaders([
            'X-Shopify-Access-Token' => $accessToken,
            'Content-Type' => 'application/json',
        ]);
    }

    protected function getPaginationInHeaders(array $headers): ?array
    {
        $nextLink = null;
        $prevLink = null;

        if (empty($headers['link']) && empty($headers['Link'])) {
            return null;
        }

        $linkInHeader = $headers['link'] ?? $headers['Link'];

        foreach (explode(', ', ($linkInHeader[0])) as $link) {
            if (in_array('rel="previous"', explode(' ', $link))) {
                $prevLink = str_replace(
                    ['>', '<', ';', ' ', 'rel="previous"'],
                    '',
                    explode(' ', $link)[0]
                );
            }
            if (in_array('rel="next"', explode(' ', $link))) {
                $nextLink = str_replace(
                    ['>', '<', ';', ' ', 'rel="next"'],
                    '',
                    explode(' ', $link)[0]
                );
            }
        }

        if ($nextLink) {
            parse_str(parse_url($nextLink)['query'], $queryNext);
        }

        if ($prevLink) {
            parse_str(parse_url($prevLink)['query'], $queryPrevious);
        }

        return [
            'next' => isset($queryNext) ? $queryNext['page_info'] : null,
            'previous' => isset($queryPrevious) ? $queryPrevious['page_info'] : null,
        ];
    }
    public function getEndpoint($path): string
    {
        $path = '/' . ltrim($path, '/');
        return $this->baseUrl . $path;
    }

    public function getUnstableEndpoint($path): string
    {
        $path = '/' . ltrim($path, '/');
        return $this->unstableUrl . $path;
    }

    public function getPaymentEndpoint($path): string
    {
        $path = '/' . ltrim($path, '/');
        return $this->paymentUrl . $path;
    }

    protected function response(Response $response): array
    {
        $result = [
            'status' => $response->status(),
            'data' => $response->json()
        ];
        $paginator = self::getPaginationInHeaders($response->headers());

        if (!empty($paginator)) {
            $result['paginator'] = $paginator;
        }

        return $result;
    }

    public function get($endpoint, $query = null): array
    {
        return self::response($this->httpClient->get($this->getEndpoint($endpoint), $query));
    }

    public function post($endpoint, $payload = []): array
    {
        return self::response($this->httpClient->post($this->getEndpoint($endpoint), $payload));
    }

    public function put($endpoint, $payload = []): array
    {
        return self::response($this->httpClient->put($this->getEndpoint($endpoint), $payload));
    }

    public function delete($endpoint, $payload = []): array
    {
        return self::response($this->httpClient->delete($this->getEndpoint($endpoint), $payload));
    }
}
