<?php

namespace Thainph\ShopifySdk\Clients\Graphql;

use Illuminate\Support\Facades\Http;

class GraphClient
{

    private $httpClient;
    private string $baseUrl;
    private string $unstableUrl;
    private string $paymentUrl;

    public function __construct($shopDomain, $accessToken, $apiVersion)
    {
        $this->baseUrl = 'https://' . $shopDomain . '/admin/api/' . $apiVersion . '/graphql.json';
        $this->unstableUrl = 'https://' . $shopDomain . '/admin/api/unstable';
        $this->paymentUrl = 'https://' . $shopDomain . '/payments_apps/api/' . $apiVersion . '/graphql.json';

        $this->httpClient = Http::withHeaders([
            'X-Shopify-Access-Token' => $accessToken,
            'Content-Type' => 'application/json',
        ]);
    }

    public function getEndpoint(): string
    {
        return $this->baseUrl;
    }

    public function getUnstableEndpoint(): string
    {
        return $this->unstableUrl;
    }

    public function getPaymentEndpoint(): string
    {
        return $this->paymentUrl;
    }
    public function request($query, $variables, $endpoint = null)
    {
        $endpoint = $endpoint ?? $this->getEndpoint();

        $payload = [
            'query' => $query,
            'variables' => $variables,
        ];
        
        $response = $this->httpClient->post($endpoint, $payload);

        return $response->json();
    }
}
