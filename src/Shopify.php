<?php

namespace Thainph\ShopifySdk;

use Thainph\ShopifySdk\Enums\ServiceAlias;
use Thainph\ShopifySdk\Clients\Rest\RestClient;
use Thainph\ShopifySdk\Clients\Graphql\GraphClient;

use Thainph\ShopifySdk\Exceptions\ServiceNotFound;

use Thainph\ShopifySdk\Services\Rest\RestBlogService;
use Thainph\ShopifySdk\Services\Rest\RestOrderService;
use Thainph\ShopifySdk\Services\Rest\RestThemeService;
use Thainph\ShopifySdk\Services\Rest\RestPaymentService;
use Thainph\ShopifySdk\Services\Rest\RestProductService;
use Thainph\ShopifySdk\Services\Rest\RestVariantService;
use Thainph\ShopifySdk\Services\Rest\RestWebhookService;
use Thainph\ShopifySdk\Services\Rest\RestCustomerService;
use Thainph\ShopifySdk\Services\Rest\RestMetafieldService;
use Thainph\ShopifySdk\Services\Rest\RestPriceRuleService;
use Thainph\ShopifySdk\Services\Rest\RestCollectionService;
use Thainph\ShopifySdk\Services\Rest\RestCarrierServiceService;
use Thainph\ShopifySdk\Services\Rest\RestCustomerAddressService;
use Thainph\ShopifySdk\Services\Rest\RestArticleService;
use Thainph\ShopifySdk\Services\Rest\RestRedirectService;
use Thainph\ShopifySdk\Services\Rest\RestPageService;
use Thainph\ShopifySdk\Services\Rest\RestScriptTagService;
use Thainph\ShopifySdk\Services\Rest\RestLocationService;
use Thainph\ShopifySdk\Services\Rest\RestDiscountService;
use Thainph\ShopifySdk\Services\Rest\RestDraftOrderService;
use Thainph\ShopifySdk\Services\Rest\RestTransactionService;
use Thainph\ShopifySdk\Services\Rest\RestRecurringApplicationChargeService;
use Thainph\ShopifySdk\Services\Rest\RestInventoryItemService;
use Thainph\ShopifySdk\Services\Rest\RestInventoryLevelService;
use Thainph\ShopifySdk\Services\Rest\RestCollectService;
use Thainph\ShopifySdk\Services\Rest\RestProductImageService;

use Thainph\ShopifySdk\Services\Graphql\GraphOrderService;
use Thainph\ShopifySdk\Services\Graphql\GraphPaymentService;
use Thainph\ShopifySdk\Services\Graphql\GraphProductService;
use Thainph\ShopifySdk\Services\Graphql\GraphCompanyService;
use Thainph\ShopifySdk\Services\Graphql\GraphCustomerService;
use Thainph\ShopifySdk\Services\Graphql\GraphMetafieldService;
use Thainph\ShopifySdk\Services\Graphql\GraphPriceRuleService;
use Thainph\ShopifySdk\Services\Graphql\GraphCollectionService;
use Thainph\ShopifySdk\Services\Graphql\GraphCarrierServiceService;
use Thainph\ShopifySdk\Services\Graphql\GraphVariantService;
use Thainph\ShopifySdk\Services\Graphql\GraphRedirectService;
use Thainph\ShopifySdk\Services\Graphql\GraphScriptTagService;
use Thainph\ShopifySdk\Services\Graphql\GraphLocationService;
use Thainph\ShopifySdk\Services\Graphql\GraphDiscountService;
use Thainph\ShopifySdk\Services\Graphql\GraphMetafieldDefinitionService;
use Thainph\ShopifySdk\Services\Graphql\GraphMetaobjectDefinitionService;
use Thainph\ShopifySdk\Services\Graphql\GraphMetaobjectService;
use Thainph\ShopifySdk\Services\Graphql\GraphStagedUploadService;
use Thainph\ShopifySdk\Services\Graphql\GraphBulkOperationService;
use Thainph\ShopifySdk\Services\Graphql\GraphDraftOrderService;
use Thainph\ShopifySdk\Services\Graphql\GraphInventoryItemService;
use Thainph\ShopifySdk\Services\Graphql\GraphInventoryLevelService;
use Thainph\ShopifySdk\Services\Graphql\GraphPaymentCustomizationService;
use Thainph\ShopifySdk\Services\Graphql\GraphValidationService;
use Thainph\ShopifySdk\Services\Graphql\GraphCartTransformService;
use Thainph\ShopifySdk\Services\Graphql\GraphDeliveryCustomizationService;
use Thainph\ShopifySdk\Services\Graphql\GraphFulfillmentConstraintService;
use Thainph\ShopifySdk\Services\Graphql\GraphNodeService;
use Thainph\ShopifySdk\Services\Graphql\GraphShopService;

class Shopify
{
    public const API_VERSION = '2024-01';
    
    public const REST_SERVICES = [
        ServiceAlias::CUSTOMER                  => RestCustomerService::class,
        ServiceAlias::METAFIELD                 => RestMetafieldService::class,
        ServiceAlias::ORDER                     => RestOrderService::class,
        ServiceAlias::PAYMENT                   => RestPaymentService::class,
        ServiceAlias::PRODUCT                   => RestProductService::class,
        ServiceAlias::WEBHOOK                   => RestWebhookService::class,
        ServiceAlias::COLLECTION                => RestCollectionService::class,
        ServiceAlias::THEME                     => RestThemeService::class,
        ServiceAlias::CUSTOMER_ADDRESS          => RestCustomerAddressService::class,
        ServiceAlias::CARRIER_SERVICE           => RestCarrierServiceService::class,
        ServiceAlias::PRICE_RULE                => RestPriceRuleService::class,
        ServiceAlias::BLOG                      => RestBlogService::class,
        ServiceAlias::VARIANT                   => RestVariantService::class,
        ServiceAlias::ARTICLE                   => RestArticleService::class,
        ServiceAlias::REDIRECT                  => RestRedirectService::class,
        ServiceAlias::PAGE                      => RestPageService::class,
        ServiceAlias::SCRIPT_TAG                => RestScriptTagService::class,
        ServiceAlias::LOCATION                  => RestLocationService::class,
        ServiceAlias::DRAFT_ORDER               => RestDraftOrderService::class,
        ServiceAlias::TRANSACTION               => RestTransactionService::class,
        ServiceAlias::RECURRING_CHARGE          => RestRecurringApplicationChargeService::class,
        ServiceAlias::INVENTORY_ITEM            => RestInventoryItemService::class,
        ServiceAlias::INVENTORY_LEVEL           => RestInventoryLevelService::class,
        ServiceAlias::COLLECT                   => RestCollectService::class,
        ServiceAlias::PRODUCT_IMAGE             => RestProductImageService::class,
        ServiceAlias::DISCOUNT                  => RestDiscountService::class,
    ];

    public const GRAPH_SERVICES = [
        ServiceAlias::CUSTOMER                  => GraphCustomerService::class,
        ServiceAlias::METAFIELD                 => GraphMetafieldService::class,
        ServiceAlias::ORDER                     => GraphOrderService::class,
        ServiceAlias::PAYMENT                   => GraphPaymentService::class,
        ServiceAlias::PRODUCT                   => GraphProductService::class,
        ServiceAlias::COLLECTION                => GraphCollectionService::class,
        ServiceAlias::CARRIER_SERVICE           => GraphCarrierServiceService::class,
        ServiceAlias::PRICE_RULE                => GraphPriceRuleService::class,
        ServiceAlias::COMPANY                   => GraphCompanyService::class,
        ServiceAlias::VARIANT                   => GraphVariantService::class,
        ServiceAlias::REDIRECT                  => GraphRedirectService::class,
        ServiceAlias::SCRIPT_TAG                => GraphScriptTagService::class,
        ServiceAlias::LOCATION                  => GraphLocationService::class,
        ServiceAlias::METAFIELD_DEFINITION      => GraphMetafieldDefinitionService::class,
        ServiceAlias::METAOBJECT_DEFINITION     => GraphMetaobjectDefinitionService::class,
        ServiceAlias::METAOBJECT                => GraphMetaobjectService::class,
        ServiceAlias::STAGED_UPLOAD             => GraphStagedUploadService::class,
        ServiceAlias::BULK_OPERATION            => GraphBulkOperationService::class,
        ServiceAlias::DRAFT_ORDER               => GraphDraftOrderService::class,
        ServiceAlias::INVENTORY_ITEM            => GraphInventoryItemService::class,
        ServiceAlias::INVENTORY_LEVEL           => GraphInventoryLevelService::class,
        ServiceAlias::PAYMENT_CUSTOMIZATION     => GraphPaymentCustomizationService::class,
        ServiceAlias::DISCOUNT                  => GraphDiscountService::class,
        ServiceAlias::VALIDATION                => GraphValidationService::class,
        ServiceAlias::CART_TRANSFORM            => GraphCartTransformService::class,
        ServiceAlias::DELIVERY_CUSTOMIZATION    => GraphDeliveryCustomizationService::class,
        ServiceAlias::FULFILLMENT_CONSTRAINT    => GraphFulfillmentConstraintService::class,
        ServiceAlias::NODE                      => GraphNodeService::class,
        ServiceAlias::SHOP                      => GraphShopService::class,
    ];

    protected RestClient $restClient;
    protected GraphClient $graphClient;

    public function __construct($shopDomain, $accessToken)
    {
        $this->restClient = new RestClient($shopDomain, $accessToken, self::API_VERSION);
        $this->graphClient = new GraphClient($shopDomain, $accessToken, self::API_VERSION);
    }

    /**
     * @throws ServiceNotFound
     */
    public function restService($alias)
    {
        if (!array_key_exists($alias, self::REST_SERVICES)) {
            throw new ServiceNotFound('Not found any service for alias ' . $alias);
        }
        $serviceClass = self::REST_SERVICES[$alias];

        return new $serviceClass($this->restClient);
    }

    /**
     * @throws ServiceNotFound
     */
    public function graphService($alias)
    {
        if (!array_key_exists($alias, self::GRAPH_SERVICES)) {
            throw new ServiceNotFound('Not found any service for alias ' . $alias);
        }
        $serviceClass = self::GRAPH_SERVICES[$alias];

        return new $serviceClass($this->graphClient);
    }
}
