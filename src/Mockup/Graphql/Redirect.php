<?php

namespace Thainph\ShopifySdk\Mockup\Graphql;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Mockup\Mocker;

class Redirect extends Mocker
{
    protected string $objectType = ShopifyObject::REDIRECT;

    protected array $fillable = [
        'id',
        'path',
        'target',
    ];

    protected array $graphMapping = [
        'id',
        'path',
        'target',
    ];

    protected array $restMapping = [];
}
