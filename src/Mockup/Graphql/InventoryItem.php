<?php

namespace Thainph\ShopifySdk\Mockup\Graphql;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Mockup\Mocker;

class InventoryItem extends Mocker
{
    protected string $objectType = ShopifyObject::INVENTORY_ITEM;

    protected array $fillable = [
        'id',
        'countryCodeOfOrigin',
        'createdAt',
        'duplicateSkuCount',
        'harmonizedSystemCode',
        'inventoryHistoryUrl',
        'inventoryLevel',
        'legacyResourceId',
        'locationsCount',
        'provinceCodeOfOrigin',
        'requiresShipping',
        'sku',
        'tracked',
        'trackedEditable',
        'unitCost',
        'updatedAt',
        'variant'
    ];

    protected array $graphMapping = [
        'id',
        'countryCodeOfOrigin',
        'createdAt',
        'duplicateSkuCount',
        'harmonizedSystemCode',
        'inventoryHistoryUrl',
        'inventoryLevel',
        'legacyResourceId',
        'locationsCount',
        'provinceCodeOfOrigin',
        'requiresShipping',
        'sku',
        'tracked',
        'trackedEditable',
        'unitCost',
        'updatedAt',
        'variant'
    ];

    protected array $restMapping = [];
}
