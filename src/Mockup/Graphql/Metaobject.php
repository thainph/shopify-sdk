<?php

namespace Thainph\ShopifySdk\Mockup\Graphql;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Mockup\Mocker;

class Metaobject extends Mocker
{
    protected string $objectType = ShopifyObject::METAOBJECT;

    protected array $fillable = [
        'id',
        'capabilities',
        'createdBy',
        'createdByApp',
        'createdByStaff',
        'definition',
        'displayName',
        'field',
        'fields',
        'handle',
        'thumbnailField',
        'type',
        'updatedAt'
    ];

    protected array $graphMapping = [
        'id',
        'capabilities',
        'createdBy',
        'createdByApp',
        'createdByStaff',
        'definition',
        'displayName',
        'field',
        'fields',
        'handle',
        'thumbnailField',
        'type',
        'updatedAt'
    ];

    protected array $restMapping = [];
}
