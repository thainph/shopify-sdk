<?php

namespace Thainph\ShopifySdk\Mockup\Graphql;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Mockup\Mocker;

class Variant extends Mocker
{
    protected string $objectType = ShopifyObject::PRODUCT_VARIANT;

    protected array $fillable = [
        'barcode', // String
        'compareAtPrice', // Money
        'harmonizedSystemCode', // String
        'id', // String
        'inventoryItem', // Object
        'inventoryPolicy', // Object
        'inventoryQuantities', // Object
        'mediaId',
        'mediaSrc',
        'metafields',
        'options',
        'position',
        'price',
        'productId',
        'requiresComponents',
        'requiresShipping',
        'sku',
        'taxCode',
        'taxable',
        'title',
        'weight',
        'weightUnit',
    ];

    protected array $graphMapping = [
        'barcode', // String
        'compareAtPrice', // Money
        'harmonizedSystemCode', // String
        'id', // String
        'inventoryItem', // Object
        'inventoryPolicy', // Object
        'inventoryQuantities', // Object
        'mediaId',
        'mediaSrc',
        'metafields',
        'options',
        'position',
        'price',
        'productId',
        'requiresComponents',
        'requiresShipping',
        'sku',
        'taxCode',
        'taxable',
        'title',
        'weight',
        'weightUnit',
    ];

    protected array $restMapping = [];

}
