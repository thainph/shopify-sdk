<?php

namespace Thainph\ShopifySdk\Mockup\Graphql;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Mockup\Mocker;

class Collection extends Mocker
{
    protected string $objectType = ShopifyObject::COLLECTION;

    protected array $fillable = [
        'id',
        'descriptionHtml',
        'handle',
        'image',
        'sortOrder',
        'templateSuffix',
        'title',
        'metafields',
    ];

    protected array $graphMapping = [
        'id',
        'descriptionHtml',
        'handle',
        'image',
        'sortOrder',
        'templateSuffix',
        'title',
        'metafields',
    ];

    protected array $restMapping = [];
}
