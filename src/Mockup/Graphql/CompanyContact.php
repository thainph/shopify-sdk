<?php

namespace Thainph\ShopifySdk\Mockup\Graphql;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Mockup\Mocker;

class CompanyContact extends Mocker
{
    protected string $objectType = ShopifyObject::COMPANY_CONTACT;

    protected array $fillable = [
        'id',
        'email',
        'firstName',
        'lastName',
        'phone',
        'title',
        'locale',
    ];

    protected array $graphMapping = [
        'id',
        'email',
        'firstName',
        'lastName',
        'phone',
        'title',
        'locale',
    ];

    protected array $restMapping = [];
}
