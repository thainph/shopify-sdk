<?php

namespace Thainph\ShopifySdk\Mockup\Graphql;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Mockup\Mocker;

class DraftOrder extends Mocker
{
    protected string $objectType = ShopifyObject::DRAFT_ORDER;

    protected array $fillable = [
        'id',
        'email',
        'marketRegionCountryCode',
        'note',
        'phone',
        'poNumber',
        'presentmentCurrencyCode',
        'customAttributes',
        'appliedDiscount',
        'shippingAddress',
        'billingAddress',
        'reserveInventoryUntil',
        'shippingLine',
        'sourceName',
        'tags',
        'taxExempt',
        'useCustomerDefaultAddress',
        'visibleToCustomer',
        'lineItems',
        'localizationExtensions',
        'metafields',
        'paymentTerms',
        'purchasingEntity',
        'paymentGatewayId',
        'paymentPending',
    ];

    protected array $graphMapping = [
        'id',
        'email',
        'marketRegionCountryCode',
        'note',
        'phone',
        'poNumber',
        'presentmentCurrencyCode',
        'customAttributes',
        'appliedDiscount',
        'shippingAddress',
        'billingAddress',
        'reserveInventoryUntil',
        'shippingLine',
        'sourceName',
        'tags',
        'taxExempt',
        'useCustomerDefaultAddress',
        'visibleToCustomer',
        'lineItems',
        'localizationExtensions',
        'metafields',
        'paymentTerms',
        'purchasingEntity',
        'paymentGatewayId',
        'paymentPending',
    ];

    protected array $restMapping = [];
}
