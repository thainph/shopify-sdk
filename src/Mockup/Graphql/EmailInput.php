<?php

namespace Thainph\ShopifySdk\Mockup\Graphql;

use Thainph\ShopifySdk\Mockup\Mocker;

class EmailInput extends Mocker
{
    protected array $fillable = [
        'bcc',
        'body',
        'customMessage',
        'from',
        'subject',
        'to',
    ];

    protected array $graphMapping = [
        'bcc',
        'body',
        'customMessage',
        'from',
        'subject',
        'to',
    ];

    protected array $restMapping = [];
}
