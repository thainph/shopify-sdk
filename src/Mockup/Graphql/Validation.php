<?php

namespace Thainph\ShopifySdk\Mockup\Graphql;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Mockup\Mocker;

class Validation extends Mocker
{
    protected string $objectType = ShopifyObject::VALIDATION;

    protected array $fillable = [
        'id',
        'blockOnFailure',
        'enable',
        'errorHistory',
        'metafields',
        'shopifyFunction',
        'functionId',
        'title',
    ];

    protected array $graphMapping = [
        'id',
        'blockOnFailure',
        'enable',
        'errorHistory',
        'metafields',
        'shopifyFunction',
        'functionId',
        'title',
    ];

    protected array $restMapping = [];
}
