<?php

namespace Thainph\ShopifySdk\Mockup\Graphql;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Mockup\Mocker;

class Customer extends Mocker
{
    protected string $objectType = ShopifyObject::CUSTOMER;

    protected array $fillable = [
        'id',
        'firstName',
        'lastName',
        'email',
        'phone',
        'tags',
        'note',
        'locale',
        'metafields',
        'taxExempt'
    ];

    protected array $graphMapping = [
        'id',
        'firstName',
        'lastName',
        'email',
        'phone',
        'tags',
        'note',
        'locale',
        'metafields',
        'taxExempt'
    ];

    protected array $restMapping = [];
}
