<?php

namespace Thainph\ShopifySdk\Mockup\Graphql;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Mockup\Mocker;

class Location extends Mocker
{
    protected string $objectType = ShopifyObject::LOCATION;

    protected array $fillable = [
        'id',
        'name',
        'fulfillsOnlineOrders',
        'address',
        'metafields',
    ];

    protected array $graphMapping = [
        'id',
        'name',
        'fulfillsOnlineOrders',
        'address',
        'metafields',
    ];

    protected array $restMapping = [];
}
