<?php

namespace Thainph\ShopifySdk\Mockup\Graphql;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Mockup\Mocker;

class MetaobjectDefinition extends Mocker
{
    protected string $objectType = ShopifyObject::METAOBJECT_DEFINITION;

    protected array $fillable = [
        'id',
        'access',
        'capabilities',
        'createdByApp',
        'createdByStaff',
        'description',
        'displayNameKey',
        'fieldDefinitions',
        'hasThumbnailField',
        'metaobjectsCount',
        'name',
        'type'
    ];

    protected array $graphMapping = [
        'id',
        'access',
        'capabilities',
        'createdByApp',
        'createdByStaff',
        'description',
        'displayNameKey',
        'fieldDefinitions',
        'hasThumbnailField',
        'metaobjectsCount',
        'name',
        'type'
    ];

    protected array $restMapping = [];
}
