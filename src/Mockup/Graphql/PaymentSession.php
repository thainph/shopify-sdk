<?php

namespace Thainph\ShopifySdk\Mockup\Graphql;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Mockup\Mocker;

class PaymentSession extends Mocker
{
    protected string $objectType = ShopifyObject::PAYMENT_SESSION;

    protected array $fillable = [
        'id',
        'reason',
        'pendingExpiresAt',
        'authorizationExpiresAt',
        'redirectUrl',
        'networkTransactionId',
    ];

    protected array $graphMapping = [
        'id',
        'reason',
        'pendingExpiresAt',
        'authorizationExpiresAt',
        'redirectUrl',
        'networkTransactionId',
    ];

    protected array $restMapping = [];
}
