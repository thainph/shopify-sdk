<?php

namespace Thainph\ShopifySdk\Mockup\Graphql;

use Thainph\ShopifySdk\Mockup\Mocker;

class StagedUpload extends Mocker
{
    protected array $fillable = [
        'fileSize',
        'filename',
        'httpMethod',
        'mimeType',
        'resource',
    ];

    protected array $graphMapping = [
        'fileSize',
        'filename',
        'httpMethod',
        'mimeType',
        'resource',
    ];

    protected array $restMapping = [];
}
