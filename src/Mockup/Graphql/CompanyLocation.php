<?php

namespace Thainph\ShopifySdk\Mockup\Graphql;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Mockup\Mocker;

class CompanyLocation extends Mocker
{
    protected string $objectType = ShopifyObject::COMPANY_LOCATION;

    protected array $fillable = [
        'id',
        'billingAddress',
        'billingSameAsShipping',
        'buyerExperienceConfiguration',
        'externalId',
        'locale',
        'name',
        'note',
        'phone',
        'shippingAddress',
        'taxExemptions',
        'taxRegistrationId',
    ];

    protected array $graphMapping = [
        'id',
        'billingAddress',
        'billingSameAsShipping',
        'buyerExperienceConfiguration',
        'externalId',
        'locale',
        'name',
        'note',
        'phone',
        'shippingAddress',
        'taxExemptions',
        'taxRegistrationId',
    ];

    protected array $restMapping = [];
}
