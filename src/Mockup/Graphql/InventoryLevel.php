<?php

namespace Thainph\ShopifySdk\Mockup\Graphql;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Mockup\Mocker;

class InventoryLevel extends Mocker
{
    protected string $objectType = ShopifyObject::INVENTORY_LEVEL;

    protected array $fillable = [
        'id',
        'canDeactivate',
        'createdAt',
        'deactivationAlert',
        'item',
        'location',
        'quantities',
        'updatedAt',
        'available',
        'onHand',
        'inventoryItemId',
        'locationId'
    ];

    protected array $graphMapping = [
        'id',
        'canDeactivate',
        'createdAt',
        'deactivationAlert',
        'item',
        'location',
        'quantities',
        'updatedAt',
        'available',
        'onHand',
        'inventoryItemId',
        'locationId'
    ];

    protected array $restMapping = [];
}
