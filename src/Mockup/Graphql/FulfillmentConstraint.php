<?php

namespace Thainph\ShopifySdk\Mockup\Graphql;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Mockup\Mocker;

class FulfillmentConstraint extends Mocker
{
    protected string $objectType = ShopifyObject::FULFILLMENT_CONSTRAINT;

    protected array $fillable = [
        'id',
        'functionId',
        'metafields',
    ];

    protected array $graphMapping = [
        'id',
        'functionId',
        'metafields',
    ];

    protected array $restMapping = [];
}
