<?php

namespace Thainph\ShopifySdk\Mockup\Graphql;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Mockup\Mocker;

class DiscountAutomatic extends Mocker
{
    protected string $objectType = ShopifyObject::DISCOUNT_AUTOMATIC;

    protected array $fillable = [
        'id',
        'title',
        'combinesWith',
        'functionId',
        'metafields',
        'startsAt',
        'endsAt',
        'recurringCycleLimit',
        'usesPerOrderLimit',
        'minimumRequirement',
        'customerGets',
        'customerBuys',
        'destination',
        'appliesOnOneTimePurchase',
        'appliesOnSubscription',
        'maximumShippingPrice',
        'customerSelection'
    ];

    protected array $graphMapping = [
        'id',
        'title',
        'combinesWith',
        'functionId',
        'metafields',
        'startsAt',
        'endsAt',
        'recurringCycleLimit',
        'usesPerOrderLimit',
        'minimumRequirement',
        'customerGets',
        'customerBuys',
        'destination',
        'appliesOnOneTimePurchase',
        'appliesOnSubscription',
        'maximumShippingPrice'
    ];

    protected array $restMapping = [];
}
