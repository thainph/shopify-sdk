<?php

namespace Thainph\ShopifySdk\Mockup\Graphql;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Mockup\Mocker;

class DeliveryCustomization extends Mocker
{
    protected string $objectType = ShopifyObject::COLLECTION;

    protected array $fillable = [
        'id',
        'enabled',
        'functionId',
        'title',
        'metafields',
    ];

    protected array $graphMapping = [
        'id',
        'enabled',
        'functionId',
        'title',
        'metafields',
    ];

    protected array $restMapping = [];
}
