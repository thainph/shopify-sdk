<?php

namespace Thainph\ShopifySdk\Mockup\Graphql;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Mockup\Mocker;

class Product extends Mocker
{
    protected string $objectType = ShopifyObject::PRODUCT;

    protected array $fillable = [
        'claimOwnership',
        'collectionsToJoin',
        'collectionsToLeave',
        'customProductType',
        'descriptionHtml',
        'giftCard',
        'giftCardTemplateSuffix',
        'handle',
        'id',
        'metafields',
        'options',
        'productCategory',
        'productType',
        'redirectNewHandle',
        'requiresSellingPlan',
        'seo',
        'standardizedProductType',
        'tags',
        'templateSuffix',
        'title',
        'variants',
        'vendor',
    ];

    protected array $graphMapping = [
        'claimOwnership',
        'collectionsToJoin',
        'collectionsToLeave',
        'customProductType',
        'descriptionHtml',
        'giftCard',
        'giftCardTemplateSuffix',
        'handle',
        'id',
        'metafields',
        'options',
        'productCategory',
        'productType',
        'redirectNewHandle',
        'requiresSellingPlan',
        'seo',
        'standardizedProductType',
        'tags',
        'templateSuffix',
        'title',
        'variants',
        'vendor',
    ];
}
