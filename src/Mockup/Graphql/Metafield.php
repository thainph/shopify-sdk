<?php

namespace Thainph\ShopifySdk\Mockup\Graphql;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Mockup\Mocker;

class Metafield extends Mocker
{
    protected string $objectType = ShopifyObject::METAFIELD;

    protected array $fillable = [
        'id',
        'namespace',
        'key',
        'type',
        'value',
        'ownerId'
    ];

    protected array $graphMapping = [
        'id',
        'namespace',
        'key',
        'type',
        'value',
        'ownerId'
    ];

    protected array $restMapping = [];
}
