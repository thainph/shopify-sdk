<?php

namespace Thainph\ShopifySdk\Mockup\Graphql;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Mockup\Mocker;

class Transaction extends Mocker
{
    protected string $objectType = ShopifyObject::TRANSACTION;

    protected array $fillable = [
        'id',
        'amount',
        'currency',
        'parentTransactionId'
    ];

    protected array $graphMapping = [
        'id',
        'amount',
        'currency',
        'parentTransactionId'
    ];

    protected array $restMapping = [];
}
