<?php

namespace Thainph\ShopifySdk\Mockup\Graphql;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Mockup\Mocker;

class ScriptTag extends Mocker
{
    protected string $objectType = ShopifyObject::SCRIPT_TAG;

    protected array $fillable = [
        'id',
        'src',
        'displayScope',
        'cache'
    ];

    protected array $graphMapping = [
        'id',
        'src',
        'displayScope',
        'cache'
    ];

    protected array $restMapping = [];
}
