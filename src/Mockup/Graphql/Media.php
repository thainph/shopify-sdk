<?php

namespace Thainph\ShopifySdk\Mockup\Graphql;

use Thainph\ShopifySdk\Mockup\Mocker;

class Media extends Mocker
{

    protected array $fillable = [
        'id',
        'alt',
        'mediaContentType',
        'originalSource',
        'previewImageSource'
    ];

    protected array $graphMapping = [
        'id',
        'alt',
        'mediaContentType',
        'originalSource',
        'previewImageSource'
    ];

    protected array $restMapping = [];

}
