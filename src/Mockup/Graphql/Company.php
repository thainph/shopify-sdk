<?php

namespace Thainph\ShopifySdk\Mockup\Graphql;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Mockup\Mocker;

class Company extends Mocker
{
    protected string $objectType = ShopifyObject::COMPANY;

    protected array $fillable = [
        'id',
        'customerSince',
        'externalId',
        'name',
        'note',
    ];

    protected array $graphMapping = [
        'id',
        'customerSince',
        'externalId',
        'name',
        'note',
    ];

    protected array $restMapping = [];
}
