<?php

namespace Thainph\ShopifySdk\Mockup\Graphql;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Mockup\Mocker;

class PaymentCustomization extends Mocker
{
    protected string $objectType = ShopifyObject::PAYMENT_CUSTOMIZATION;

    protected array $fillable = [
        'id',
        'enabled',
        'errorHistory',
        'functionId',
        'metafield',
        'shopifyFunction',
        'title',
        'metafields'
    ];

    protected array $graphMapping = [
        'id',
        'enabled',
        'errorHistory',
        'functionId',
        'metafield',
        'shopifyFunction',
        'title',
        'metafields'
    ];

    protected array $restMapping = [];
}
