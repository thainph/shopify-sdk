<?php

namespace Thainph\ShopifySdk\Mockup\Graphql;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Mockup\Mocker;

class CartTransform extends Mocker
{
    protected string $objectType = ShopifyObject::CART_TRANSFORM;

    protected array $fillable = [
        'id',
        'blockOnFailure',
        'functionId',
    ];

    protected array $graphMapping = [
        'id',
        'blockOnFailure',
        'functionId',
    ];

    protected array $restMapping = [];
}
