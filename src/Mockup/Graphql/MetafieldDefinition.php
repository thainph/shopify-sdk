<?php

namespace Thainph\ShopifySdk\Mockup\Graphql;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Mockup\Mocker;

class MetafieldDefinition extends Mocker
{
    protected string $objectType = ShopifyObject::METAFIELD_DEFINITION;

    protected array $fillable = [
        'id',
        'access',
        'name',
        'description',
        'namespace',
        'key',
        'ownerType',
        'pinnedPosition',
        'standardTemplate',
        'type',
        'useAsCollectionCondition',
        'validationStatus',
        'validations'
    ];

    protected array $graphMapping = [
        'id',
        'access',
        'name',
        'description',
        'namespace',
        'key',
        'ownerType',
        'pinnedPosition',
        'standardTemplate',
        'type',
        'useAsCollectionCondition',
        'validationStatus',
        'validations'
    ];

    protected array $restMapping = [];
}
