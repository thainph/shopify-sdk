<?php

namespace Thainph\ShopifySdk\Mockup\Graphql;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Mockup\Mocker;

class DiscountCode extends Mocker
{
    protected string $objectType = ShopifyObject::DISCOUNT_CODE;

    protected array $fillable = [
        'id',
        'title',
        'combinesWith',
        'functionId',
        'metafields',
        'startsAt',
        'endsAt',
        'recurringCycleLimit',
        'usesPerOrderLimit',
        'minimumRequirement',
        'customerGets',
        'customerBuys',
        'destination',
        'appliesOnOneTimePurchase',
        'appliesOnSubscription',
        'maximumShippingPrice',
        'appliesOncePerCustomer',
        'code',
        'usageLimit',
        'customerSelection'
    ];

    protected array $graphMapping = [
        'id',
        'title',
        'combinesWith',
        'functionId',
        'metafields',
        'startsAt',
        'endsAt',
        'recurringCycleLimit',
        'usesPerOrderLimit',
        'minimumRequirement',
        'customerGets',
        'customerBuys',
        'destination',
        'appliesOnOneTimePurchase',
        'appliesOnSubscription',
        'maximumShippingPrice',
        'appliesOncePerCustomer',
        'code',
        'usageLimit',
        'customerSelection'
    ];

    protected array $restMapping = [];
}
