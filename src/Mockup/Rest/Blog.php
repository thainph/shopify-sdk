<?php

namespace Thainph\ShopifySdk\Mockup\Rest;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Mockup\Mocker;

class Blog extends Mocker
{
    protected string $objectType = ShopifyObject::BLOG;

    protected array $fillable = [
        'id',
        'commentable',
        'feedburner',
        'feedburner_location',
        'handle',
        'metafields',
        'template_suffix',
        'title',
    ];

    protected array $graphMapping = [];

    protected array $restMapping = [
        'id',
        'commentable',
        'feedburner',
        'feedburner_location',
        'handle',
        'metafields',
        'template_suffix',
        'title',
    ];
}
