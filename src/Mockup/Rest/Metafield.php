<?php

namespace Thainph\ShopifySdk\Mockup\Rest;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Mockup\Mocker;

class Metafield extends Mocker
{
    protected string $objectType = ShopifyObject::METAFIELD;

    protected array $fillable = [
        'id',
        'namespace',
        'key',
        'type',
        'value',
        'owner_id'
    ];

    protected array $graphMapping = [];

    protected array $restMapping = [
        'namespace' => 'namespace',
        'key'       => 'key',
        'type'      => 'type',
        'value'     => 'value',
        'id'        => 'id',
    ];
}
