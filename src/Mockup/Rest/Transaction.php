<?php

namespace Thainph\ShopifySdk\Mockup\Rest;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Mockup\Mocker;

class Transaction extends Mocker
{
    protected string $objectType = ShopifyObject::TRANSACTION;

    protected array $fillable = [
        'id',
        'amount',
        'authorization',
        'authorization_expires_at',
        'currency',
        'extended_authorization_attributes',
        'gateway',
        'kind',
        'order_id',
        'parent_id',
        'payments_refund_attributes',
        'processed_at',
        'status',
        'test',
        'user_id',
        'currency_exchange_adjustment'
    ];

    protected array $graphMapping = [];

    protected array $restMapping = [
        'id',
        'amount',
        'authorization',
        'authorization_expires_at',
        'currency',
        'extended_authorization_attributes',
        'gateway',
        'kind',
        'order_id',
        'parent_id',
        'payments_refund_attributes',
        'processed_at',
        'status',
        'test',
        'user_id',
        'currency_exchange_adjustment'
    ];
}
