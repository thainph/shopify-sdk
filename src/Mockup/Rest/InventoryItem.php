<?php

namespace Thainph\ShopifySdk\Mockup\Rest;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Mockup\Mocker;

class InventoryItem extends Mocker
{
    protected string $objectType = ShopifyObject::INVENTORY_ITEM;

    protected array $fillable = [
        'id',
        'cost',
        'country_code_of_origin',
        'country_harmonized_system_codes',
        'created_at',
        'harmonized_system_code',
        'province_code_of_origin',
        'sku',
        'tracked',
        'updated_at',
        'requires_shipping',
    ];

    protected array $graphMapping = [];

    protected array $restMapping = [
        'id',
        'cost',
        'country_code_of_origin',
        'country_harmonized_system_codes',
        'created_at',
        'harmonized_system_code',
        'province_code_of_origin',
        'sku',
        'tracked',
        'updated_at',
        'requires_shipping',
    ];
}
