<?php
namespace Thainph\ShopifySdk\Mockup\Rest;

use Thainph\ShopifySdk\Mockup\Mocker;

class Webhook extends Mocker
{
    protected array $fillable = [
        'id',
        'gid',
        'topic',
        'address',
        'format'
    ];

    protected array $restMapping = [
        'id',
        'gid',
        'topic',
        'address',
        'format'
    ];
}
