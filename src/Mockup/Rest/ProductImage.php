<?php

namespace Thainph\ShopifySdk\Mockup\Rest;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Mockup\Mocker;

class ProductImage extends Mocker
{
    protected string $objectType = ShopifyObject::PRODUCT_IMAGE;

    protected array $fillable = [
        'id',
        'position',
        'product_id',
        'variant_ids',
        'src',
        'attachment',
        'filename',
        'alt',
        'width',
        'height',
        'metafields',
        'created_at',
        'updated_at'
    ];

    protected array $restMapping = [
        'id',
        'position',
        'product_id',
        'variant_ids',
        'src',
        'attachment',
        'filename',
        'alt',
        'width',
        'height',
        'metafields',
        'created_at',
        'updated_at'
    ];
}
