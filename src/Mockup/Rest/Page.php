<?php

namespace Thainph\ShopifySdk\Mockup\Rest;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Mockup\Mocker;

class Page extends Mocker
{
    protected string $objectType = ShopifyObject::PAGE;

    protected array $fillable = [
        'id',
        'author',
        'body_html',
        'handle',
        'metafields',
        'published_at',
        'template_suffix',
        'title'
    ];

    protected array $graphMapping = [];

    protected array $restMapping = [
        'id',
        'author',
        'body_html',
        'handle',
        'metafields',
        'published_at',
        'template_suffix',
        'title'
    ];
}
