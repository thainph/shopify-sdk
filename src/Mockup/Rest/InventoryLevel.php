<?php

namespace Thainph\ShopifySdk\Mockup\Rest;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Mockup\Mocker;

class InventoryLevel extends Mocker
{
    protected string $objectType = ShopifyObject::INVENTORY_LEVEL;

    protected array $fillable = [
        'available',
        'inventory_item_id',
        'available_adjustment',
        'location_id',
        'updated_at',
    ];

    protected array $graphMapping = [];

    protected array $restMapping = [
        'available',
        'inventory_item_id',
        'available_adjustment',
        'location_id',
        'updated_at',
    ];
}
