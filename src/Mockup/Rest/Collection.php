<?php

namespace Thainph\ShopifySdk\Mockup\Rest;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Mockup\Mocker;

class Collection extends Mocker
{
    protected string $objectType = ShopifyObject::COLLECTION;

    protected array $fillable = [
        'id',
        'body_html',
        'handle',
        'image',
        'published',
        'sort_order',
        'template_suffix',
        'title',
        'metafields',

        // Custom
        'collects',

        // Smart
        'rules',
        'disjunctive'
    ];

    protected array $graphMapping = [];

    protected array $restMapping = [
        'id',
        'body_html',
        'handle',
        'image',
        'published',
        'sort_order',
        'template_suffix',
        'title',
        'metafields',
        'collects',
        'rules',
        'disjunctive'
    ];
}
