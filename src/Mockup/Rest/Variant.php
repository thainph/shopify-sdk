<?php

namespace Thainph\ShopifySdk\Mockup\Rest;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Mockup\Mocker;

class Variant extends Mocker
{
    protected string $objectType = ShopifyObject::PRODUCT_VARIANT;

    protected array $fillable = [
        'barcode',
        'compare_at_price',
        'created_at',
        'fulfillment_service',
        'grams',
        'id',
        'image_id',
        'inventory_item_id',
        'inventory_management',
        'inventory_policy',
        'option1',
        'option2',
        'option3',
        'price',
        'product_id',
        'sku',
        'taxable',
        'tax_code',
        'title',
        'updated_at',
        'weight',
        'weight_unit',
        'metafields'
    ];

    protected array $graphMapping = [];

    protected array $restMapping = [
        'barcode',
        'compare_at_price',
        'created_at',
        'fulfillment_service',
        'grams',
        'id',
        'image_id',
        'inventory_item_id',
        'inventory_management',
        'inventory_policy',
        'option1',
        'option2',
        'option3',
        'price',
        'product_id',
        'sku',
        'taxable',
        'tax_code',
        'title',
        'updated_at',
        'weight',
        'weight_unit',
        'metafields'
    ];

}
