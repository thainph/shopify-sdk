<?php

namespace Thainph\ShopifySdk\Mockup\Rest;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Mockup\Mocker;

class ScriptTag extends Mocker
{
    protected string $objectType = ShopifyObject::SCRIPT_TAG;

    protected array $fillable = [
        'id',
        'event',
        'src',
        'display_scope',
        'cache'
    ];

    protected array $graphMapping = [];

    protected array $restMapping = [
        'id',
        'event',
        'src',
        'display_scope',
        'cache'
    ];
}
