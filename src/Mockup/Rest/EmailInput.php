<?php

namespace Thainph\ShopifySdk\Mockup\Rest;

use Thainph\ShopifySdk\Mockup\Mocker;

class EmailInput extends Mocker
{
    protected array $fillable = [
        'to',
        'from',
        'subject',
        'custom_message',
        'bcc',
    ];

    protected array $graphMapping = [];

    protected array $restMapping = [
        'to',
        'from',
        'subject',
        'custom_message',
        'bcc',
    ];
}
