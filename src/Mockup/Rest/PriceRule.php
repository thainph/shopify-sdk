<?php

namespace Thainph\ShopifySdk\Mockup\Rest;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Mockup\Mocker;

class PriceRule extends Mocker
{
    protected string $objectType = ShopifyObject::PRICE_RULE;

    protected array $fillable = [
        'id',
        'value_type',
        'value',
        'customer_selection',
        'target_type',
        'target_selection',
        'allocation_method',
        'title',
        'starts_at',
        'ends_at',
        'prerequisite_collection_ids',
        'entitled_collection_ids',
        'entitled_country_ids',
        'entitled_product_ids',
        'prerequisite_product_ids',
        'entitled_variant_ids',
        'prerequisite_variant_ids',
        'once_per_customer',
        'prerequisite_customer_ids',
        'customer_segment_prerequisite_ids',
        'prerequisite_to_entitlement_quantity_ratio',
        'allocation_limit',
        'prerequisite_quantity_range',
        'prerequisite_shipping_price_range',
        'prerequisite_subtotal_range',
        'prerequisite_to_entitlement_purchase',
        'prerequisite_subtotal_range',
        'prerequisite_to_entitlement_purchase',
        'usage_limit'
    ];

    protected array $graphMapping = [];

    protected array $restMapping = [
        'id',
        'value_type',
        'value',
        'customer_selection',
        'target_type',
        'target_selection',
        'allocation_method',
        'title',
        'starts_at',
        'ends_at',
        'prerequisite_collection_ids',
        'entitled_collection_ids',
        'entitled_country_ids',
        'entitled_product_ids',
        'prerequisite_product_ids',
        'entitled_variant_ids',
        'prerequisite_variant_ids',
        'once_per_customer',
        'prerequisite_customer_ids',
        'customer_segment_prerequisite_ids',
        'prerequisite_to_entitlement_quantity_ratio',
        'allocation_limit',
        'prerequisite_quantity_range',
        'prerequisite_shipping_price_range',
        'prerequisite_subtotal_range',
        'prerequisite_to_entitlement_purchase',
        'prerequisite_subtotal_range',
        'prerequisite_to_entitlement_purchase',
        'usage_limit'
    ];
}
