<?php

namespace Thainph\ShopifySdk\Mockup\Rest;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Mockup\Mocker;

class Article extends Mocker
{
    protected string $objectType = ShopifyObject::ARTICLE;

    protected array $fillable = [
        'id',
        'author',
        'body_html',
        'handle',
        'image',
        'metafields',
        'published',
        'published_at',
        'summary_html',
        'tags',
        'template_suffix',
        'title'
    ];

    protected array $graphMapping = [];

    protected array $restMapping = [
        'id',
        'author',
        'body_html',
        'handle',
        'image',
        'metafields',
        'published',
        'published_at',
        'summary_html',
        'tags',
        'template_suffix',
        'title'
    ];
}
