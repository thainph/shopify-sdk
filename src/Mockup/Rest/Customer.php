<?php

namespace Thainph\ShopifySdk\Mockup\Rest;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Mockup\Mocker;

class Customer extends Mocker
{
    protected string $objectType = ShopifyObject::CUSTOMER;

    protected array $fillable = [
        'id',
        'first_name',
        'last_name',
        'email',
        'phone',
        'tags',
        'note',
        'locale',
        'metafields',
        'tax_exempt'
    ];

    protected array $graphMapping = [];

    protected array $restMapping = [
        'id',
        'first_name',
        'last_name',
        'email',
        'phone',
        'tags',
        'note',
        'locale',
        'metafields',
        'tax_exempt'
    ];
}
