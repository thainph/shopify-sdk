<?php

namespace Thainph\ShopifySdk\Mockup\Rest;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Mockup\Mocker;

class Product extends Mocker
{
    protected string $objectType = ShopifyObject::PRODUCT;

    protected array $fillable = [
        'id',
        'body_html',
        'handle',
        'images',
        'options',
        'product_type',
        'published_at',
        'published_scope',
        'status',
        'tags',
        'template_suffix',
        'title',
        'updated_at',
        'variants',
        'metafields',
        'vendor',
    ];

    protected array $restMapping = [
        'id',
        'body_html',
        'handle',
        'images',
        'options',
        'product_type',
        'published_at',
        'published_scope',
        'status',
        'tags',
        'template_suffix',
        'title',
        'updated_at',
        'variants',
        'metafields',
        'vendor',
    ];
}
