<?php

namespace Thainph\ShopifySdk\Mockup\Rest;

use Thainph\ShopifySdk\Mockup\Mocker;
use Thainph\ShopifySdk\Enums\ShopifyObject;

class CarrierService extends Mocker
{
    protected string $objectType = ShopifyObject::CARRIER_SERVICE;

    protected array $fillable = [
        'id',
        'active',
        'callback_url',
        'carrier_service_type',
        'format',
        'name',
        'service_discovery',
    ];

    protected array $graphMapping = [];

    protected array $restMapping = [
        'id',
        'active',
        'callback_url',
        'carrier_service_type',
        'format',
        'name',
        'service_discovery',
    ];
}
