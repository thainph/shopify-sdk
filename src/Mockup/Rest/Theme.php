<?php

namespace Thainph\ShopifySdk\Mockup\Rest;

use Thainph\ShopifySdk\Mockup\Mocker;

class Theme extends Mocker
{
    protected array $fillable = [
        'id',
        'name',
        'src',
        'role',
    ];

    protected array $graphMapping = [];

    protected array $restMapping = [
        'id',
        'name',
        'src',
        'role',
    ];
}
