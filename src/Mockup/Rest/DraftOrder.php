<?php

namespace Thainph\ShopifySdk\Mockup\Rest;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Mockup\Mocker;

class DraftOrder extends Mocker
{
    protected string $objectType = ShopifyObject::DRAFT_ORDER;

    protected array $fillable = [
        'id',
        'order_id',
        'customer',
        'shipping_address',
        'billing_address',
        'note',
        'note_attributes',
        'email',
        'currency',
        'invoice_sent_at',
        'invoice_url',
        'line_items',
        'payment_terms',
        'shipping_line',
        'tags',
        'tax_exempt',
        'tax_exemptions',
        'tax_lines',
        'applied_discount',
        'taxes_included',
        'total_tax',
        'subtotal_price',
        'total_price',
        'completed_at',
        'status',
        'payment_gateway_id',
        'payment_pending'
    ];

    protected array $graphMapping = [];

    protected array $restMapping = [
        'id',
        'order_id',
        'customer',
        'shipping_address',
        'billing_address',
        'note',
        'note_attributes',
        'email',
        'currency',
        'invoice_sent_at',
        'invoice_url',
        'line_items',
        'payment_terms',
        'shipping_line',
        'tags',
        'tax_exempt',
        'tax_exemptions',
        'tax_lines',
        'applied_discount',
        'taxes_included',
        'total_tax',
        'subtotal_price',
        'total_price',
        'completed_at',
        'status',
        'payment_gateway_id',
        'payment_pending'
    ];
}
