<?php

namespace Thainph\ShopifySdk\Mockup\Rest;

use Thainph\ShopifySdk\Mockup\Mocker;

class RecurringApplicationCharge extends Mocker
{
    protected array $fillable = [
        'id',
        'activated_on',
        'billing_on',
        'cancelled_on',
        'capped_amount',
        'confirmation_url',
        'created_at',
        'name',
        'price',
        'return_url',
        'status',
        'terms',
        'test',
        'trial_days',
        'trial_ends_on',
        'updated_at',
        'currency'
    ];

    protected array $graphMapping = [];

    protected array $restMapping = [
        'id',
        'activated_on',
        'billing_on',
        'cancelled_on',
        'capped_amount',
        'confirmation_url',
        'created_at',
        'name',
        'price',
        'return_url',
        'status',
        'terms',
        'test',
        'trial_days',
        'trial_ends_on',
        'updated_at',
        'currency'
    ];
}
