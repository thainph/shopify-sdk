<?php

namespace Thainph\ShopifySdk\Mockup\Rest;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Mockup\Mocker;

class Order extends Mocker
{
    protected string $objectType = ShopifyObject::ORDER;

    protected array $fillable = [
        'id',
        'line_items',
        'transactions',
        'total_tax',
        'currency',
        'customer',
        'billing_address',
        'shipping_address',
        'email',
        'phone',
        'financial_status',
        'fulfillment_status',
        'fulfillments',
        'discount_codes',
        'tax_lines',
        'total_tax',
        'metafields'
    ];

    protected array $graphMapping = [];

    protected array $restMapping = [
        'id',
        'line_items',
        'transactions',
        'total_tax',
        'currency',
        'customer',
        'billing_address',
        'shipping_address',
        'email',
        'phone',
        'financial_status',
        'fulfillment_status',
        'fulfillments',
        'discount_codes',
        'tax_lines',
        'total_tax',
        'metafields'
    ];
}
