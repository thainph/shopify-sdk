<?php

namespace Thainph\ShopifySdk\Mockup\Rest;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Mockup\Mocker;

class CustomerAddress extends Mocker
{
    protected string $objectType = ShopifyObject::CUSTOMER_ADDRESS;

    protected array $fillable = [
        'id',
        'address1',
        'address2',
        'city',
        'country',
        'country_name',
        'country_code',
        'company',
        'first_name',
        'last_name',
        'name',
        'phone',
        'province',
        'zip',
    ];

    protected array $graphMapping = [];

    protected array $restMapping = [
        'id',
        'address1',
        'address2',
        'city',
        'country',
        'country_name',
        'company',
        'first_name',
        'last_name',
        'name',
        'phone',
        'province',
        'zip',
    ];
}
