<?php

namespace Thainph\ShopifySdk\Mockup;

use Thainph\ShopifySdk\Exceptions\AttributeNotExists;

class Mocker
{
    protected string $objectType = '';

    protected array $fillable = [];
    protected array $restMapping = [];
    protected array $graphMapping = [];

    protected array $attributes = [];

    /**
     * @param array|null $attributes
     * @throws AttributeNotExists
     */
    public function __construct(?array $attributes = [])
    {
        if (!empty($attributes)) {
            $this->setAttributes($attributes);
        }
    }

    /**
     * @throws AttributeNotExists
     */
    public function set($key, $value): Mocker
    {
        if (in_array($key, $this->fillable)) {
            $this->attributes[$key] = $value;
        } else {
            throw new AttributeNotExists($key . ' is not defined');
        }

        return $this;
    }

    /**
     * @throws AttributeNotExists
     */
    public function get($key)
    {
        if (in_array($key, $this->fillable)) {
            return $this->attributes[$key] ?? null;
        } else {
            throw new AttributeNotExists($key . ' is not defined');
        }
    }

    public function toArray(): array
    {
        return $this->attributes;
    }

    /**
     * @throws AttributeNotExists
     */
    public function setAttributes(array $attributes): Mocker
    {
        foreach ($attributes as $key => $value) {
            $this->set($key, $value);
        }

        return $this;
    }

    /**
     * @throws AttributeNotExists
     */
    public function bindingGraphData(): array
    {
        $data = [];

        foreach ($this->graphMapping as $fillableKey => $graphKey) {
            $value = is_numeric($fillableKey) ? $this->get($graphKey) : $this->get($fillableKey);

            if (!is_null($value)) {
                $data[$graphKey] = $value;
            }
        }

        return $data;
    }

    /**
     * @throws AttributeNotExists
     */
    public function bindingRestData(): array
    {
        $data = [];

        foreach ($this->restMapping as $fillableKey => $restKey) {
            $value = is_numeric($fillableKey) ? $this->get($restKey) : $this->get($fillableKey);

            if (!is_null($value)) {
                $data[$restKey] = $value;
            }
        }

        return $data;
    }
}
