<?php

namespace Thainph\ShopifySdk\Enums;

use MyCLabs\Enum\Enum;

class MetafieldType extends Enum
{
    const BOOLEAN = 'boolean';
    const COLOR = 'color';
    const DATE = 'date';
    const DATETIME = 'date_time';
    const DIMENSION = 'dimension';
    const JSON = 'json';
    const MONEY = 'money';
    const MULTI_LINE_TEXT_FIELD = 'multi_line_text_field';
    const NUMBER_DECIMAL = 'number_decimal';
    const NUMBER_INTEGER = 'number_integer';
    const RATING = 'rating';
    const RICH_TEXT_FIELD = 'rich_text_field';
    const SINGLE_LINE_TEXT_FIELD = 'single_line_text_field';
    const URL = 'url';
    const VOLUME = 'volume';
    const WEIGHT = 'weight';

    const COLLECTION = 'collection_reference';
    const FILE = 'file_reference';
    const METAOBJECT = 'metaobject_reference';
    const MIXED = 'mixed_reference';
    const PAGE = 'page_reference';
    const PRODUCT = 'product_reference';
    const VARIANT = 'variant_reference';

    const LIST_COLLECTION = 'list.collection_reference';
    const LIST_COLOR = 'list.color';
    const LIST_DATE = 'list.date';
    const LIST_DATETIME = 'list.date_time';
    const LIST_DIMENSION = 'list.dimension';
    const LIST_FILE = 'list.file_reference';
    const LIST_METAOBJECT = 'list.metaobject_reference';
    const LIST_MIXED = 'list.mixed_reference';
    const LIST_NUMBER_DECIMAL = 'list.number_decimal';
    const LIST_NUMBER_INTEGER = 'list.number_integer';
    const LIST_PAGE = 'list.page_reference';
    const LIST_PRODUCT = 'list.product_reference';
    const LIST_RATING = 'list.rating';
    const LIST_SINGLE_LINE_TEXT_FIELD = 'list.single_line_text_field';
    const LIST_URL = 'list.url';
    const LIST_VARIANT = 'list.variant_reference';
    const LIST_VOLUME = 'list.volume';
    const LIST_WEIGHT = 'list.weight';
}
