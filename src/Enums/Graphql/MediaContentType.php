<?php

namespace Thainph\ShopifySdk\Enums\Graphql;

use MyCLabs\Enum\Enum;

class MediaContentType extends Enum
{
    const EXTERNAL_VIDEO = 'EXTERNAL_VIDEO';
    const IMAGE = 'IMAGE';
    const MODEL_3D = 'MODEL_3D';
    const VIDEO = 'VIDEO';
}
