<?php

namespace Thainph\ShopifySdk\Enums\Graphql\Payment;

use MyCLabs\Enum\Enum;

class PartnerError extends Enum
{
    const PROCESSING_ERROR = 'PROCESSING_ERROR';
}
