<?php

namespace Thainph\ShopifySdk\Enums\Graphql\Payment;

use MyCLabs\Enum\Enum;

class ChargebackLiability extends Enum
{
    const MERCHANT = 'MERCHANT';
    const UNKNOWN = 'UNKNOWN';
}
