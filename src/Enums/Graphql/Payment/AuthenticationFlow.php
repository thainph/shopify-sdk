<?php

namespace Thainph\ShopifySdk\Enums\Graphql\Payment;

use MyCLabs\Enum\Enum;

class AuthenticationFlow extends Enum
{
    const CHALLENGE = 'CHALLENGE';
    const FRICTIONLESS = 'FRICTIONLESS';
    const UNKNOWN = 'UNKNOWN';
}
