<?php

namespace Thainph\ShopifySdk\Enums\Graphql\Payment;

use MyCLabs\Enum\Enum;

class Version extends Enum
{
    const UNKNOWN = 'UNKNOWN';
    const V1_0 = 'V1_0';
    const V2_1 = 'V2_1';
    const V2_2 = 'V2_2';
    const V2_3 = 'V2_3';
}
