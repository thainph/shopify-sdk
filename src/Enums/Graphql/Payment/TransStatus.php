<?php

namespace Thainph\ShopifySdk\Enums\Graphql\Payment;

use MyCLabs\Enum\Enum;

class TransStatus extends Enum
{
    const A = 'A';
    const I = 'I';
    const N = 'N';
    const R = 'R';
    const U = 'U';
    const Y = 'Y';
}
