<?php
namespace Thainph\ShopifySdk\Enums\Graphql\Payment;

use MyCLabs\Enum\Enum;

class PaymentReasonCode extends Enum
{
    const PROCESSING_ERROR = 'PROCESSING_ERROR';
    const AUTHENTICATION_FAILED = 'AUTHENTICATION_FAILED';
    const CARD_DECLINED = 'CARD_DECLINED';
    const CONFIRMATION_REJECTED = 'CONFIRMATION_REJECTED';
    const EXPIRED_CARD = 'EXPIRED_CARD';
    const INCORRECT_ADDRESS = 'INCORRECT_ADDRESS';
    const INCORRECT_CVC = 'INCORRECT_CVC';
    const INCORRECT_NUMBER = 'INCORRECT_NUMBER';
    const INCORRECT_PIN = 'INCORRECT_PIN';
    const INCORRECT_ZIP = 'INCORRECT_ZIP';
    const INVALID_CVC = 'INVALID_CVC';
    const INVALID_EXPIRY_DATE = 'INVALID_EXPIRY_DATE';
    const INVALID_NUMBER = 'INVALID_NUMBER';
    const RISKY = 'RISKY';
    const AUTHORIZATION_EXPIRED = 'AUTHORIZATION_EXPIRED';
    const BUYER_ACTION_REQUIRED = 'BUYER_ACTION_REQUIRED';
    const NETWORK_ACTION_REQUIRED = 'NETWORK_ACTION_REQUIRED';
    const PARTNER_ACTION_REQUIRED = 'PARTNER_ACTION_REQUIRED';
    const GENERIC_ERROR = 'GENERIC_ERROR';
    const REQUIRED_3DS_CHALLENGE = 'REQUIRED_3DS_CHALLENGE';

    public static function captureSessionReject(): array
    {
        return [
            self::AUTHORIZATION_EXPIRED,
            self::PROCESSING_ERROR
        ];
    }

    public static function paymentSessionPending(): array
    {
        return [
            self::BUYER_ACTION_REQUIRED,
            self::NETWORK_ACTION_REQUIRED,
            self::PARTNER_ACTION_REQUIRED
        ];
    }

    public static function paymentSessionReject(): array
    {
        return [
            self::AUTHENTICATION_FAILED,
            self::CARD_DECLINED,
            self::CONFIRMATION_REJECTED,
            self::EXPIRED_CARD,
            self::INCORRECT_ADDRESS,
            self::INCORRECT_CVC,
            self::INCORRECT_NUMBER,
            self::INCORRECT_PIN,
            self::INCORRECT_ZIP,
            self::INVALID_CVC,
            self::INVALID_EXPIRY_DATE,
            self::INVALID_NUMBER,
            self::PROCESSING_ERROR,
            self::RISKY
        ];
    }

    public static function refundSessionReject(): array
    {
        return [
            self::PROCESSING_ERROR
        ];
    }

    public static function verificationSessionReject(): array
    {
        return [
            self::CARD_DECLINED,
            self::EXPIRED_CARD,
            self::GENERIC_ERROR,
            self::INCORRECT_ADDRESS,
            self::INCORRECT_CVC,
            self::INCORRECT_ZIP,
            self::INVALID_CVC,
            self::INVALID_EXPIRY_DATE,
            self::INVALID_NUMBER,
            self::REQUIRED_3DS_CHALLENGE
        ];
    }

    public static function voidSessionReject(): array
    {
        return [
            self::PROCESSING_ERROR
        ];
    }
}
