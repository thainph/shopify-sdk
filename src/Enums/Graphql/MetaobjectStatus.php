<?php

namespace Thainph\ShopifySdk\Enums\Graphql;

use MyCLabs\Enum\Enum;

class MetaobjectStatus extends Enum
{
    const ACTIVE = 'ACTIVE';
    const DRAFT = 'DRAFT';
}
