<?php

namespace Thainph\ShopifySdk\Enums\Graphql;

use MyCLabs\Enum\Enum;

class BulkOperationType extends Enum
{
    const MUTATION = 'MUTATION';
    const QUERY = 'QUERY';
}
