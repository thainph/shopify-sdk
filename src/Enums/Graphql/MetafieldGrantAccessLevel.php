<?php

namespace Thainph\ShopifySdk\Enums\Graphql;

use MyCLabs\Enum\Enum;

class MetafieldGrantAccessLevel extends Enum
{
    const READ = 'READ';
    const READ_WRITE = 'READ_WRITE';
}
