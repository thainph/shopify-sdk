<?php

namespace Thainph\ShopifySdk\Enums\Graphql;

use MyCLabs\Enum\Enum;

class MetaobjectAdminAccess extends Enum
{
    const MERCHANT_READ = 'MERCHANT_READ';
    const MERCHANT_READ_WRITE = 'MERCHANT_READ_WRITE';
    const PRIVATE = 'PRIVATE';
    const PUBLIC_READ = 'PUBLIC_READ';
    const PUBLIC_READ_WRITE = 'PUBLIC_READ_WRITE';
}
