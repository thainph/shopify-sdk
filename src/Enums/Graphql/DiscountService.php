<?php

namespace Thainph\ShopifySdk\Enums\Graphql;

use MyCLabs\Enum\Enum;

class DiscountService extends Enum
{
    const ALL       = 'ALL';
    const AUTOMATIC = 'AUTOMATIC';
    const CODE      = 'CODE';
}
