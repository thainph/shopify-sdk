<?php

namespace Thainph\ShopifySdk\Enums\Graphql;

use MyCLabs\Enum\Enum;

class MetafieldDefinitionPinnedStatus extends Enum
{
    const ANY = 'ANY';
    const PINNED = 'PINNED';
    const UNPINNED = 'UNPINNED';
}
