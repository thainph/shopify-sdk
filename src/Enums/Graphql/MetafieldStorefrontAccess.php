<?php

namespace Thainph\ShopifySdk\Enums\Graphql;

use MyCLabs\Enum\Enum;

class MetafieldStorefrontAccess extends Enum
{
    const NONE = 'NONE';
    const PUBLIC_READ = 'PUBLIC_READ';
}
