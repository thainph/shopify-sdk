<?php

namespace Thainph\ShopifySdk\Enums\Graphql;

use MyCLabs\Enum\Enum;

class InventoryPolicy extends Enum
{
    const CONTINUE = 'CONTINUE';
    const DENY = 'DENY';
}
