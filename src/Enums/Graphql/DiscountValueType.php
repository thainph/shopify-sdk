<?php

namespace Thainph\ShopifySdk\Enums\Graphql;

use MyCLabs\Enum\Enum;

class DiscountValueType extends Enum
{
    const FIXED_AMOUNT = 'FIXED_AMOUNT';
    const PERCENTAGE = 'PERCENTAGE';
    const SHIPPING = 'SHIPPING';
}
