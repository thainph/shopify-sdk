<?php

namespace Thainph\ShopifySdk\Enums\Graphql;

use MyCLabs\Enum\Enum;

class LocalizationExtensionKey extends Enum
{
    const SHIPPING_CREDENTIAL_BR = 'SHIPPING_CREDENTIAL_BR';
    const SHIPPING_CREDENTIAL_CN = 'SHIPPING_CREDENTIAL_CN';
    const SHIPPING_CREDENTIAL_KR = 'SHIPPING_CREDENTIAL_KR';
    const TAX_CREDENTIAL_BR = 'TAX_CREDENTIAL_BR';
    const TAX_CREDENTIAL_IT = 'TAX_CREDENTIAL_IT';
    const TAX_EMAIL_IT = 'TAX_EMAIL_IT';
}
