<?php

namespace Thainph\ShopifySdk\Enums\Graphql;

use MyCLabs\Enum\Enum;

class WeightUnit extends Enum
{
    const GRAMS = 'GRAMS';
    const KILOGRAMS = 'KILOGRAMS';
    const OUNCES = 'OUNCES';
    const POUNDS = 'POUNDS';
}
