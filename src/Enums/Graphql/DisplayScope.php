<?php

namespace Thainph\ShopifySdk\Enums\Graphql;

use MyCLabs\Enum\Enum;

class DisplayScope extends Enum
{
    const ONLINE_STORE = 'ONLINE_STORE';
    const ORDER_STATUS = 'ORDER_STATUS';
    const ALL          = 'ALL';
}
