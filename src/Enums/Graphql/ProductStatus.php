<?php

namespace Thainph\ShopifySdk\Enums\Graphql;

use MyCLabs\Enum\Enum;

class ProductStatus extends Enum
{
    const ACTIVE = 'ACTIVE';
    const DRAFT = 'DRAFT';
    const ARCHIVED = 'ARCHIVED';
}
