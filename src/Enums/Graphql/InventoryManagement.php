<?php

namespace Thainph\ShopifySdk\Enums\Graphql;

use MyCLabs\Enum\Enum;

class InventoryManagement extends Enum
{
    const FULFILLMENT_SERVICE = 'FULFILLMENT_SERVICE';
    const NOT_MANAGED = 'NOT_MANAGED';
    const SHOPIFY = 'SHOPIFY';
}
