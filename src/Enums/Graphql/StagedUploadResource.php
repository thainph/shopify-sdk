<?php

namespace Thainph\ShopifySdk\Enums\Graphql;

use MyCLabs\Enum\Enum;

class StagedUploadResource extends Enum
{
    const BULK_MUTATION_VARIABLES = 'BULK_MUTATION_VARIABLES';
    const COLLECTION_IMAGE = 'COLLECTION_IMAGE';
    const FILE = 'FILE';
    const IMAGE = 'IMAGE';
    const MODEL_3D = 'MODEL_3D';
    const PRODUCT_IMAGE = 'PRODUCT_IMAGE';
    const RETURN_LABEL = 'RETURN_LABEL';
    const SHOP_IMAGE = 'SHOP_IMAGE';
    const URL_REDIRECT_IMPORT = 'URL_REDIRECT_IMPORT';
    const VIDEO = 'VIDEO';
}
