<?php

namespace Thainph\ShopifySdk\Enums\Graphql;

use MyCLabs\Enum\Enum;

class MetafieldAdminAccess extends Enum
{
    const MERCHANT_READ = 'MERCHANT_READ';
    const MERCHANT_READ_WRITE = 'MERCHANT_READ_WRITE';
    const PUBLIC_READ = 'PUBLIC_READ';
    const PRIVATE = 'PRIVATE';
}
