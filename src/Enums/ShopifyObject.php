<?php

namespace Thainph\ShopifySdk\Enums;

use MyCLabs\Enum\Enum;

class ShopifyObject extends Enum
{
    const ARTICLE                               = 'OnlineStoreArticle';
    const BLOG                                  = 'OnlineStoreBlog';
    const COLLECTION                            = 'Collection';
    const SMART_COLLECTION                      = 'Collection';
    const CUSTOMER                              = 'Customer';
    const DRAFT_ORDER                           = 'DraftOrder';
    const LOCATION                              = 'Location';
    const ORDER                                 = 'Order';
    const MEDIA_IMAGE                           = 'MediaImage';
    const MEDIA_MODEL_3D                        = 'Model3d';
    const MEDIA_VIDEO                           = 'Video';
    const MEDIA_EXTERNAL_VIDEO                  = 'ExternalVideo';
    const PAGE                                  = 'OnlineStorePage';
    const PRODUCT                               = 'Product';
    const PRODUCT_IMAGE                         = 'ProductImage';
    const PRODUCT_VARIANT                       = 'ProductVariant';
    const SHOP                                  = 'Shop';
    const METAFIELD                             = 'Metafield';
    const SAVED_SEARCH                          = 'SavedSearch';
    const FULFILL_SERVICE                       = 'ApiFulfillmentService';
    const CUSTOMER_ADDRESS                      = 'MailingAddress';  // model_name=CustomerAddress
    const CARRIER_SERVICE                       = 'DeliveryCarrierService';
    const PRICE_RULE                            = 'PriceRule';
    const COMPANY                               = 'Company';
    const PAYMENT_TERMS_TEMPLATE                = 'PaymentTermsTemplate';
    const COMPANY_LOCATION                      = 'CompanyLocation';
    const COMPANY_CONTACT                       = 'CompanyContact';
    const REDIRECT                              = 'UrlRedirect';
    const SCRIPT_TAG                            = 'ScriptTag';
    const METAFIELD_DEFINITION                  = 'MetafieldDefinition';
    const METAOBJECT_DEFINITION                 = 'MetaobjectDefinition';
    const METAOBJECT                            = 'Metaobject';
    const BULK_OPERATION                        = 'BulkOperation';
    const TRANSACTION                           = 'OrderTransaction';
    const INVENTORY_ITEM                        = 'InventoryItem';
    const INVENTORY_LEVEL                       = 'InventoryLevel';
    const PAYMENT_CUSTOMIZATION                 = 'PaymentCustomization';
    const DISCOUNT_AUTOMATIC                    = 'DiscountAutomaticNode';
    const DISCOUNT_CODE                         = 'DiscountCodeNode';
    const DISCOUNT_REDEEM_CODE_BULK_CREATION    = 'DiscountRedeemCodeBulkCreation';
    const DISCOUNT_REDEEM_CODE                  = 'DiscountRedeemCode';
    const CUSTOMER_SEGMENT                      = 'CustomerSegmentMember';
    const VALIDATION                            = 'Validation';
    const CART_TRANSFORM                        = 'CartTransform';
    const DELIVERY_CUSTOMIZATION                = 'DeliveryCustomization';
    const FULFILLMENT_CONSTRAINT                = 'FulfillmentConstraintRule';
    const PAYMENT_SESSION                       = 'PaymentSession';
    const PAYMENT_GATEWAY                       = 'PaymentGateway';
    const CALCULATED_ORDER                      = 'CalculatedOrder';
}
