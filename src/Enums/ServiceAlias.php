<?php

namespace Thainph\ShopifySdk\Enums;

use MyCLabs\Enum\Enum;

class ServiceAlias extends Enum
{
    const WEBHOOK                   = 'webhook';
    const PRODUCT                   = 'product';
    const PAYMENT                   = 'payment';
    const ORDER                     = 'order';
    const METAFIELD                 = 'metafield';
    const CUSTOMER                  = 'customer';
    const COLLECTION                = 'collection';
    const THEME                     = 'theme';
    const CUSTOMER_ADDRESS          = 'customer_address';
    const CARRIER_SERVICE           = 'carrier_service';
    const PRICE_RULE                = 'price_rule';
    const COMPANY                   = 'company';
    const BLOG                      = 'blog';
    const VARIANT                   = 'variant';
    const ARTICLE                   = 'article';
    const REDIRECT                  = 'redirect';
    const PAGE                      = 'page';
    const SCRIPT_TAG                = 'script_tag';
    const LOCATION                  = 'location';
    const METAFIELD_DEFINITION      = 'metafield_definition';
    const METAOBJECT_DEFINITION     = 'metaobject_definition';
    const METAOBJECT                = 'metaobject';
    const STAGED_UPLOAD             = 'staged_upload';
    const BULK_OPERATION            = 'BulkOperation';
    const DRAFT_ORDER               = 'draft_order';
    const TRANSACTION               = 'transaction';
    const RECURRING_CHARGE          = 'recurring_application_charge';
    const INVENTORY_ITEM            = 'inventory_item';
    const INVENTORY_LEVEL           = 'inventory_level';
    const COLLECT                   = 'collect';
    const PRODUCT_IMAGE             = 'product_image';
    const PAYMENT_CUSTOMIZATION     = 'payment_customization';
    const DISCOUNT                  = 'discount';
    const VALIDATION                = 'validation';
    const CART_TRANSFORM            = 'cart_transform';
    const DELIVERY_CUSTOMIZATION    = 'delivery_customization';
    const FULFILLMENT_CONSTRAINT    = 'fulfillment_constraint';
    const NODE                      = 'node';
    const SHOP                      = 'shop';
}
