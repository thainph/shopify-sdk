<?php

namespace Thainph\ShopifySdk\Enums\Rest;

use MyCLabs\Enum\Enum;

class AllocationMethod extends Enum
{
    const EACH          = 'each';
    const ACROSS        = 'across';
}
