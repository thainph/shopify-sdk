<?php

namespace Thainph\ShopifySdk\Enums\Rest;

use MyCLabs\Enum\Enum;

class WeightUnit extends Enum
{
    const GRAMS = 'g';
    const KILOGRAMS = 'kg';
    const OUNCES = 'oz';
    const POUNDS = 'lb';
}
