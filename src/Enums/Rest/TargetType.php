<?php

namespace Thainph\ShopifySdk\Enums\Rest;

use MyCLabs\Enum\Enum;

class TargetType extends Enum
{
    const LINE_ITEM         = 'line_item';
    const SHIPPING_LINE     = 'shipping_line';
}
