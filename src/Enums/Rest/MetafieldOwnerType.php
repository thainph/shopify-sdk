<?php

namespace Thainph\ShopifySdk\Enums\Rest;

use MyCLabs\Enum\Enum;

class MetafieldOwnerType extends Enum
{
    const ARTICLE = 'articles';
    const BLOG = 'blogs';
    const COLLECTION = 'collections';
    const SMART_COLLECTION = 'smart_collections';
    const CUSTOMER = 'customers';
    const DRAFT_ORDER = 'draft_orders';
    const LOCATION = 'locations';
    const ORDER = 'orders';
    const PAGE = 'pages';
    const PRODUCT = 'products';
    const PRODUCT_IMAGE = 'product_images';
    const PRODUCT_VARIANT = 'variants';
    const SHOP = 'shop';
}
