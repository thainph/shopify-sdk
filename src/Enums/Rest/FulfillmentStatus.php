<?php

namespace Thainph\ShopifySdk\Enums\Rest;

use MyCLabs\Enum\Enum;

class FulfillmentStatus extends Enum
{
    const FULFILLED = 'fulfilled';
    const NULL = 'null';
    const PARTIAL = 'partial';
    const RESTOCKED = 'restocked';
}
