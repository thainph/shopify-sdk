<?php

namespace Thainph\ShopifySdk\Enums\Rest;

use MyCLabs\Enum\Enum;

class DisplayScope extends Enum
{
    const ONLINE_STORE = 'online_store';
    const ORDER_STATUS = 'order_status';
    const ALL          = 'all';
}
