<?php

namespace Thainph\ShopifySdk\Enums\Rest;

use MyCLabs\Enum\Enum;

class DiscountValueType extends Enum
{
    const FIXED_AMOUNT = 'fixed_amount';
    const PERCENTAGE = 'percentage';
    const SHIPPING = 'shipping';
}
