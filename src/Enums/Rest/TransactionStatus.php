<?php

namespace Thainph\ShopifySdk\Enums\Rest;

use MyCLabs\Enum\Enum;

class TransactionStatus extends Enum
{
    const PENDING = 'pending';
    const SUCCESS = 'success';
    const FAILURE = 'failure';
    const ERROR = 'error';
}
