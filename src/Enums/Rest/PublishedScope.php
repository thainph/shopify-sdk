<?php

namespace Thainph\ShopifySdk\Enums\Rest;

use MyCLabs\Enum\Enum;

class PublishedScope extends Enum
{
    const WEB = 'web';
    const GLOBAL = 'global';
}
