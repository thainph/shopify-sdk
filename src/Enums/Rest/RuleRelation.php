<?php

namespace Thainph\ShopifySdk\Enums\Rest;

use MyCLabs\Enum\Enum;

class RuleRelation extends Enum
{
    const GREATER_THAN = 'greater_than';
    const LESS_THAN = 'less_than';
    const EQUALS = 'equals';
    const NOT_EQUALS = 'not_equals';
    const STARTS_WITH = 'starts_with';
    const ENDS_WITH = 'ends_with';
    const CONTAINS = 'contains';
    const NOT_CONTAINS = 'not_contains';
}
