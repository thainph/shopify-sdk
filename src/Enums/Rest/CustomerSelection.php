<?php

namespace Thainph\ShopifySdk\Enums\Rest;

use MyCLabs\Enum\Enum;

class CustomerSelection extends Enum
{
    const ALL           = 'all';
    const PREREQUISITE  = 'prerequisite';
}
