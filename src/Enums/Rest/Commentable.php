<?php

namespace Thainph\ShopifySdk\Enums\Rest;

use MyCLabs\Enum\Enum;

class Commentable extends Enum
{
    const NO        = 'no';
    const MODERATE  = 'moderate';
    const YES       = 'yes';
}
