<?php

namespace Thainph\ShopifySdk\Enums\Rest;

use MyCLabs\Enum\Enum;

class InventoryPolicy extends Enum
{
    const CONTINUE = 'continue';
    const DENY = 'deny';
}
