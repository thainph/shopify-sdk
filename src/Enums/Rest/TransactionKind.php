<?php

namespace Thainph\ShopifySdk\Enums\Rest;

use MyCLabs\Enum\Enum;

class TransactionKind extends Enum
{
    const CAPTURE = 'capture';
    const VOID = 'void';
    const REFUND = 'refund';
}
