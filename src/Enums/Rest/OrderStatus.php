<?php

namespace Thainph\ShopifySdk\Enums\Rest;

use MyCLabs\Enum\Enum;

class OrderStatus extends Enum
{
    const OPEN          = 'open';
    const INVOICE_SENT  = 'invoice_sent';
    const COMPLETED     = 'completed';
}
