<?php

namespace Thainph\ShopifySdk\Enums\Rest;

use MyCLabs\Enum\Enum;

class RuleColumn extends Enum
{
    const TITLE = 'title';
    const TYPE = 'type';
    const VENDOR = 'vendor';
    const VARIANT_TITLE = 'variant_title';
    const VARIANT_COMPARE_AT_PRICE = 'variant_compare_at_price';
    const VARIANT_WEIGHT = 'variant_weight';
    const VARIANT_INVENTORY = 'variant_inventory';
    const VARIANT_PRICE = 'variant_price';
    const TAG = 'tag';
    const PRODUCT_METAFIELD_DEFINITION = 'product_metafield_definition';
    const VARIANT_METAFIELD_DEFINITION = 'variant_metafield_definition';
}
