<?php

namespace Thainph\ShopifySdk\Enums\Rest;

use MyCLabs\Enum\Enum;

class TargetSelection extends Enum
{
    const ALL         = 'all';
    const ENTITLED    = 'entitled';
}
