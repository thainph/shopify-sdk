<?php

namespace Thainph\ShopifySdk\Enums\Rest;

use MyCLabs\Enum\Enum;

class ThemeRole extends Enum
{
    const MAIN = 'main';
    const UNPUBLISHED = 'unpublished';
    const DEMO = 'demo';
    const DEVELOPMENT = 'development';
}
