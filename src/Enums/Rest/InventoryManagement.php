<?php

namespace Thainph\ShopifySdk\Enums\Rest;

use MyCLabs\Enum\Enum;

class InventoryManagement extends Enum
{
    const FULFILLMENT_SERVICE = 'fulfillment_service';
    const NOT_MANAGED = 'null';
    const SHOPIFY = 'shopify';
}
