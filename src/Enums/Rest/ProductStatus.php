<?php

namespace Thainph\ShopifySdk\Enums\Rest;

use MyCLabs\Enum\Enum;

class ProductStatus extends Enum
{
    const ACTIVE = 'active';
    const DRAFT = 'draft';
    const ARCHIVED = 'archived';
}
