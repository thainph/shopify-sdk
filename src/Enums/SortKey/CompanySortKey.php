<?php

namespace Thainph\ShopifySdk\Enums\SortKey;

use MyCLabs\Enum\Enum;

class CompanySortKey extends Enum
{
    const CREATED_AT = 'CREATED_AT';
    const ID = 'ID';
    const NAME = 'NAME';
    const ORDER_COUNT = 'ORDER_COUNT';
    const RELEVANCE = 'RELEVANCE';
    const SINCE_DATE = 'SINCE_DATE';
    const TOTAL_SPENT = 'TOTAL_SPENT';
    const UPDATED_AT = 'UPDATED_AT';
}
