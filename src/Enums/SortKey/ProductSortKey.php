<?php

namespace Thainph\ShopifySdk\Enums\SortKey;

use MyCLabs\Enum\Enum;

class ProductSortKey extends Enum
{
    const CREATED_AT = 'CREATED_AT';
    const ID = 'ID';
    const INVENTORY_TOTAL = 'INVENTORY_TOTAL';
    const PRODUCT_TYPE = 'PRODUCT_TYPE';
    const PUBLISHED_AT = 'PUBLISHED_AT';
    const RELEVANCE = 'RELEVANCE';
    const TITLE = 'TITLE';
    const UPDATED_AT = 'UPDATED_AT';
    const VENDOR = 'VENDOR';
}
