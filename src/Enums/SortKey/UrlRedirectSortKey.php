<?php

namespace Thainph\ShopifySdk\Enums\SortKey;

use MyCLabs\Enum\Enum;

class UrlRedirectSortKey extends Enum
{
    const ID = 'ID';
    const RELEVANCE = 'RELEVANCE';
    const PATH = 'PATH';
}
