<?php

namespace Thainph\ShopifySdk\Enums\SortKey;

use MyCLabs\Enum\Enum;

class AutomaticDiscountSortKey extends Enum
{
    const ID = 'ID';
    const RELEVANCE = 'RELEVANCE';
    const CREATED_AT = 'CREATED_AT';
}
