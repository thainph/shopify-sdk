<?php

namespace Thainph\ShopifySdk\Enums\SortKey;

use MyCLabs\Enum\Enum;

class DiscountCodeSortKey extends Enum
{
    const CREATED_AT = 'CREATED_AT';
    const ID = 'ID';
    const RELEVANCE = 'RELEVANCE';
    const CODE = 'CODE';
}
