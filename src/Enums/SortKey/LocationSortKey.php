<?php

namespace Thainph\ShopifySdk\Enums\SortKey;

use MyCLabs\Enum\Enum;

class LocationSortKey extends Enum
{
    const ID = 'ID';
    const RELEVANCE = 'RELEVANCE';
    const NAME = 'NAME';
}
