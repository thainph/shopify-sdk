<?php

namespace Thainph\ShopifySdk\Enums\SortKey;

use MyCLabs\Enum\Enum;

class CodeDiscountSortKey extends Enum
{
    const CREATED_AT = 'CREATED_AT';
    const ENDS_AT = 'ENDS_AT';
    const ID = 'ID';
    const RELEVANCE = 'RELEVANCE';
    const STARTS_AT = 'STARTS_AT';
    const TITLE = 'TITLE';
    const UPDATED_AT = 'UPDATED_AT';
}
