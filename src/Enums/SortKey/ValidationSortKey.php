<?php

namespace Thainph\ShopifySdk\Enums\SortKey;

use MyCLabs\Enum\Enum;

class ValidationSortKey extends Enum
{
    const ID = 'ID';
    const RELEVANCE = 'RELEVANCE';
}
