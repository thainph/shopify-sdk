<?php

namespace Thainph\ShopifySdk\Enums\SortKey;

use MyCLabs\Enum\Enum;

class CollectionSortKey extends Enum
{
    const ID = 'ID';
    const RELEVANCE = 'RELEVANCE';
    const TITLE = 'TITLE';
    const UPDATED_AT = 'UPDATED_AT';
}
