<?php

namespace Thainph\ShopifySdk\Enums\SortKey;

use MyCLabs\Enum\Enum;

class CustomerSortKey extends Enum
{
    const CREATE_AT = 'CREATED_AT';
    const NAME = 'NAME';
    const LOCATION = 'LOCATION';
    const ORDERS_COUNT = 'ORDERS_COUNT';
    const LAST_ORDER_DATE = 'LAST_ORDER_DATE';
    const TOTAL_SPENT = 'TOTAL_SPENT';
    const UPDATED_AT = 'UPDATED_AT';
    const ID = 'ID';
    const RELEVANCE = 'RELEVANCE';
}
