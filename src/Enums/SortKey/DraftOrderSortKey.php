<?php

namespace Thainph\ShopifySdk\Enums\SortKey;

use MyCLabs\Enum\Enum;

class DraftOrderSortKey extends Enum
{
    const CUSTOMER_NAME = 'CUSTOMER_NAME';
    const ID = 'ID';
    const NUMBER = 'NUMBER';
    const RELEVANCE = 'RELEVANCE';
    const STATUS = 'STATUS';
    const TOTAL_PRICE = 'TOTAL_PRICE';
    const UPDATED_AT = 'UPDATED_AT';
}
