<?php

namespace Thainph\ShopifySdk\Enums\SortKey;

use MyCLabs\Enum\Enum;

class ProductVariantSortKey extends Enum
{
    const FULL_TITLE = 'FULL_TITLE';
    const ID = 'ID';
    const INVENTORY_LEVELS_AVAILABLE = 'INVENTORY_LEVELS_AVAILABLE';
    const INVENTORY_MANAGEMENT = 'INVENTORY_MANAGEMENT';
    const INVENTORY_POLICY = 'INVENTORY_POLICY';
    const INVENTORY_QUANTITY = 'INVENTORY_QUANTITY';
    const NAME = 'NAME';
    const POPULAR = 'POPULAR';
    const POSITION = 'POSITION';
    const RELEVANCE = 'RELEVANCE';
    const SKU = 'SKU';
    const TITLE = 'TITLE';
}
