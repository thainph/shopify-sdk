<?php

namespace Thainph\ShopifySdk\Services\Rest;

use Thainph\ShopifySdk\Mockup\Rest\Theme;
use Thainph\ShopifySdk\Services\BaseService;
use Thainph\ShopifySdk\Clients\Rest\RestClient;
use Thainph\ShopifySdk\Helpers\ValidationHelper;
use Thainph\ShopifySdk\Exceptions\AttributeNotExists;
use Thainph\ShopifySdk\Validation\Validators\Rest\Theme\CreateValidator;
use Thainph\ShopifySdk\Validation\Validators\Rest\Theme\UpdateValidator;

class RestThemeService extends BaseService
{
    private RestClient $restClient;

    public function __construct(RestClient $restClient)
    {
        $this->restClient = $restClient;
    }

    public function get($params = null): array
    {
        return $this->restClient->get('themes.json', $params);
    }

    public function getAssets(int $themeId, $params = null): array
    {
        return $this->restClient->get("themes/$themeId/assets.json", $params);
    }

    public function getById(int $themeId, $params = null): array
    {
        return $this->restClient->get("themes/$themeId.json", $params);
    }

    /**
     * @throws AttributeNotExists
     */
    public function create(Theme $theme): array
    {
        $payload = ValidationHelper::validate($theme->bindingRestData(), CreateValidator::class);

        return $this->restClient->post('themes.json', [
            'theme' => $payload
        ]);
    }

    /**
     * @throws AttributeNotExists
     */
    public function update(Theme $theme): array
    {
        $payload = ValidationHelper::validate($theme->bindingRestData(), UpdateValidator::class);

        return $this->restClient->put("themes/{$payload['id']}.json", [
            'theme' => $payload
        ]);
    }

    public function remove(int $themeId): array
    {
        return $this->restClient->delete("themes/$themeId.json");
    }
}
