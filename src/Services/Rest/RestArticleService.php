<?php

namespace Thainph\ShopifySdk\Services\Rest;

use Thainph\ShopifySdk\Clients\Rest\RestClient;
use Thainph\ShopifySdk\Helpers\ValidationHelper;
use Thainph\ShopifySdk\Mockup\Rest\Article;
use Thainph\ShopifySdk\Services\BaseService;
use Thainph\ShopifySdk\Validation\Validators\Rest\Article\CreateValidator;
use Thainph\ShopifySdk\Validation\Validators\Rest\Article\UpdateValidator;

class RestArticleService extends BaseService
{
    private RestClient $restClient;

    public function __construct(RestClient $restClient)
    {
        $this->restClient = $restClient;
    }

    public function getAuthors()
    {
        return $this->restClient->get('articles/authors.json');
    }

    public function getTags()
    {
        return $this->restClient->get('articles/tags.json');
    }

    public function get(int $blogId, $params = null)
    {
        return $this->restClient->get("blogs/$blogId/articles.json", $params);
    }

    public function count(int $blogId, $params = null)
    {
        return $this->restClient->get("blogs/$blogId/articles/count.json", $params);
    }

    public function getById(int $blogId, int $articleId, $params = null)
    {
        return $this->restClient->get("blogs/$blogId/articles/$articleId.json", $params);
    }

    public function getTagsByBlogId(int $blogId, $params = null)
    {
        return $this->restClient->get("blogs/$blogId/articles/tags.json", $params);
    }

    public function create(int $blogId, Article $article)
    {
        $payload = ValidationHelper::validate($article->bindingRestData(), CreateValidator::class);

        return $this->restClient->post("blogs/$blogId/articles.json", [
            'article' => $payload
        ]);
    }

    public function update(int $blogId, Article $article)
    {
        $payload = ValidationHelper::validate($article->bindingRestData(), UpdateValidator::class);

        return $this->restClient->put("blogs/$blogId/articles/{$article->get('id')}.json", [
            'article' => $payload
        ]);
    }

    public function remove(int $blogId, int $articleId)
    {
        return $this->restClient->delete("blogs/$blogId/articles/$articleId.json");
    }
}
