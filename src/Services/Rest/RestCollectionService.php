<?php

namespace Thainph\ShopifySdk\Services\Rest;

use Thainph\ShopifySdk\Services\BaseService;
use Thainph\ShopifySdk\Mockup\Rest\Collection;
use Thainph\ShopifySdk\Clients\Rest\RestClient;
use Thainph\ShopifySdk\Exceptions\InvalidInput;
use Thainph\ShopifySdk\Helpers\ValidationHelper;
use Thainph\ShopifySdk\Validation\Validators\Rest\Collection\CreateSmartValidator;
use Thainph\ShopifySdk\Validation\Validators\Rest\Collection\UpdateSmartValidator;
use Thainph\ShopifySdk\Validation\Validators\Rest\Collection\CreateCustomValidator;
use Thainph\ShopifySdk\Validation\Validators\Rest\Collection\UpdateCustomValidator;

class RestCollectionService extends BaseService
{
    private RestClient $restClient;

    public function __construct(RestClient $restClient)
    {
        $this->restClient = $restClient;
    }

    public function get(bool $isCustom = false, array $query = null): array
    {
        $endpoint = $isCustom ? 'custom_collections.json' : 'smart_collections.json';

        return $this->restClient->get($endpoint, $query);
    }

    public function getById(int $collectionId, array $query = null): array
    {
        $endpoint = "collections/$collectionId.json";

        return $this->restClient->get($endpoint, $query);
    }

    public function getProducts(int $collectionId, array $query = null)
    {
        $endpoint = "collections/$collectionId/products.json";

        return $this->restClient->get($endpoint, $query);
    }

    public function countCustom(): array
    {
        $endpoint = 'custom_collections/count.json';

        return $this->restClient->get($endpoint);
    }

    /**
     * @throws InvalidInput
     * @throws AttributeNotExists
     */
    public function createCustom(Collection $collection): array
    {
        $endpoint = 'custom_collections.json';

        $payload = ValidationHelper::validate($collection->bindingRestData(), CreateCustomValidator::class);

        return $this->restClient->post($endpoint, [
            'custom_collection' => $payload
        ]);
    }

    /**
     * @throws InvalidInput
     * @throws AttributeNotExists
     */
    public function updateCustom(Collection $collection): array
    {
        $payload = ValidationHelper::validate($collection->bindingRestData(), UpdateCustomValidator::class);

        $endpoint = "custom_collections/{$collection->get('id')}.json";

        return $this->restClient->put($endpoint, [
            'custom_collection' => $payload
        ]);
    }

    public function removeCustom(int $collectionId): array
    {
        $endpoint = "custom_collections/$collectionId.json";

        return $this->restClient->delete($endpoint);
    }

    public function countSmart(): array
    {
        $endpoint = 'smart_collections/count.json';

        return $this->restClient->get($endpoint);
    }

    /**
     * @throws InvalidInput
     * @throws AttributeNotExists
     */
    public function createSmart(Collection $collection): array
    {
        $endpoint = 'smart_collections.json';

        $payload = ValidationHelper::validate($collection->bindingRestData(), CreateSmartValidator::class);

        return $this->restClient->post($endpoint, [
            'smart_collection' => $payload
        ]);
    }

    /**
     * @throws InvalidInput
     * @throws AttributeNotExists
     */
    public function updateSmart(Collection $collection): array
    {
        $payload = ValidationHelper::validate($collection->bindingRestData(), UpdateSmartValidator::class);

        $endpoint = "smart_collections/{$collection->get('id')}.json";

        return $this->restClient->put($endpoint, [
            'smart_collection' => $payload
        ]);
    }

    public function removeSmart(int $collectionId): array
    {
        $endpoint = "smart_collections/$collectionId.json";

        return $this->restClient->delete($endpoint);
    }

}
