<?php

namespace Thainph\ShopifySdk\Services\Rest;

use Thainph\ShopifySdk\Services\BaseService;
use Thainph\ShopifySdk\Clients\Rest\RestClient;
use Thainph\ShopifySdk\Helpers\ValidationHelper;
use Thainph\ShopifySdk\Validation\Validators\Rest\DiscountCode\BulkCreateValidator;

class RestDiscountService extends BaseService
{
    private RestClient $restClient;

    public function __construct(RestClient $restClient)
    {
        $this->restClient = $restClient;
    }

    public function countDiscountCode($params = null): array
    {
        return $this->restClient->get('discount_codes/count.json', $params);
    }

    public function getDiscountCodes(int $priceRules): array
    {
        return $this->restClient->get("price_rules/$priceRules/discount_codes.json");
    }

    public function lookupDiscountCode(string $code)
    {
        return $this->restClient->get("discount_codes/lookup.json?code=$code");
    }

    public function getDiscountCodeById(int $priceRules, int $discountCodeId): array
    {
        return $this->restClient->get("price_rules/$priceRules/discount_codes/$discountCodeId.json");
    }

    public function createDiscountCode(int $priceRules, string $code): array
    {
        return $this->restClient->post("price_rules/$priceRules/discount_codes.json", [
            'discount_code' => [
                'code'  => $code
            ]
        ]);
    }

    public function updateDiscountCode(int $priceRules, int $discountCodeId, string $code): array
    {
        return $this->restClient->put("price_rules/$priceRules/discount_codes/$discountCodeId.json", [
            'discount_code' => [
                'id'    => $discountCodeId,
                'code'  => $code
            ]
        ]);
    }

    public function removeDiscountCode(int $priceRules, int $discountCodeId): array
    {
        return $this->restClient->delete("price_rules/$priceRules/discount_codes/$discountCodeId.json");
    }

    public function createBatch(int $priceRules, array $discountCodes): array
    {
        $payload = ValidationHelper::validate([
            'discount_codes' => $discountCodes
        ], BulkCreateValidator::class);

        return $this->restClient->post("price_rules/$priceRules/batch.json", $payload);
    }

    public function getBatchById(int $priceRules, int $batchId): array
    {
        return $this->restClient->get("price_rules/$priceRules/batch/$batchId.json");
    }

    public function getBatchDiscountCodeById(int $priceRules, int $batchId): array
    {
        return $this->restClient->get("price_rules/$priceRules/batch/$batchId/discount_codes.json");
    }
}
