<?php

namespace Thainph\ShopifySdk\Services\Rest;

use Thainph\ShopifySdk\Clients\Rest\RestClient;
use Thainph\ShopifySdk\Helpers\ValidationHelper;
use Thainph\ShopifySdk\Mockup\Rest\Redirect;
use Thainph\ShopifySdk\Services\BaseService;
use Thainph\ShopifySdk\Validation\Validators\Rest\Redirect\CreateValidator;
use Thainph\ShopifySdk\Validation\Validators\Rest\Redirect\UpdateValidator;

class RestRedirectService extends BaseService
{
    private RestClient $restClient;

    public function __construct(RestClient $restClient)
    {
        $this->restClient = $restClient;
    }

    public function get($params = null)
    {
        return $this->restClient->get('redirects.json', $params);
    }

    public function count($params = null)
    {
        return $this->restClient->get('redirects/count.json', $params);
    }

    public function getById(int $redirectId, $params = null)
    {
        return $this->restClient->get("redirects/$redirectId.json", $params);
    }

    public function create(Redirect $redirect)
    {
        $payload = ValidationHelper::validate($redirect->bindingRestData(), CreateValidator::class);

        return $this->restClient->post('redirects.json', [
            'redirect' => $payload
        ]);
    }

    public function update(Redirect $redirect)
    {
        $payload = ValidationHelper::validate($redirect->bindingRestData(), UpdateValidator::class);

        return $this->restClient->put("redirects/{$redirect->get('id')}.json", [
            'redirect' => $payload
        ]);
    }

    public function remove(int $redirectId)
    {
        return $this->restClient->delete("redirects/$redirectId.json");
    }
}
