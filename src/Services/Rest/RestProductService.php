<?php

namespace Thainph\ShopifySdk\Services\Rest;

use Thainph\ShopifySdk\Mockup\Rest\Product;
use Thainph\ShopifySdk\Services\BaseService;
use Thainph\ShopifySdk\Clients\Rest\RestClient;
use Thainph\ShopifySdk\Helpers\ValidationHelper;
use Thainph\ShopifySdk\Exceptions\MissingIdentifier;
use Thainph\ShopifySdk\Exceptions\AttributeNotExists;
use Thainph\ShopifySdk\Validation\Validators\Rest\Product\CreateValidator;
use Thainph\ShopifySdk\Validation\Validators\Rest\Product\UpdateValidator;

class RestProductService extends BaseService
{
    private RestClient $restClient;

    public function __construct(RestClient $restClient)
    {
        $this->restClient = $restClient;
    }

    public function count(): array
    {
        return $this->restClient->get('products/count.json', null);
    }

    public function get(string $query = null): array
    {
        return $this->restClient->get('products.json', $query);
    }

    public function getById(int $productId): array
    {
        return $this->restClient->get('products/' . $productId . '.json');
    }

    /**
     * @throws AttributeNotExists
     */
    public function create(Product $product): array
    {
        $payload = ValidationHelper::validate($product->bindingRestData(), CreateValidator::class);

        return $this->restClient->post('products.json', [
            'product' => $payload
        ]);
    }

    /**
     * @throws MissingIdentifier
     * @throws AttributeNotExists
     */
    public function update(Product $product): array
    {
        $payload = ValidationHelper::validate($product->bindingRestData(), UpdateValidator::class);

        return $this->restClient->put('products/' . $product->get('id') . '.json', [
            'product' => $payload
        ]);
    }

    /**
     * @throws MissingIdentifier
     * @throws AttributeNotExists
     */
    public function remove(Product $product): array
    {
        if (empty($product->get('id'))) {
            throw new MissingIdentifier('Missing product id');
        }

        return $this->restClient->delete('products/' . $product->get('id') . '.json');
    }

}
