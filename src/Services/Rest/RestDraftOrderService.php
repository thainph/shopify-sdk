<?php

namespace Thainph\ShopifySdk\Services\Rest;

use Thainph\ShopifySdk\Clients\Rest\RestClient;
use Thainph\ShopifySdk\Helpers\ValidationHelper;
use Thainph\ShopifySdk\Mockup\Rest\DraftOrder;
use Thainph\ShopifySdk\Mockup\Rest\EmailInput;
use Thainph\ShopifySdk\Services\BaseService;
use Thainph\ShopifySdk\Validation\Validators\Rest\DraftOrder\CompleteValidator;
use Thainph\ShopifySdk\Validation\Validators\Rest\DraftOrder\CreateValidator;
use Thainph\ShopifySdk\Validation\Validators\Rest\DraftOrder\UpdateValidator;
use Thainph\ShopifySdk\Validation\Validators\Rest\DraftOrder\SendInvoiceValidator;

class RestDraftOrderService extends BaseService
{
    private RestClient $restClient;

    public function __construct(RestClient $restClient)
    {
        $this->restClient = $restClient;
    }

    public function get($params = null)
    {
        return $this->restClient->get('draft_orders.json', $params);
    }

    public function count($params = null)
    {
        return $this->restClient->get('draft_orders/count.json', $params);
    }

    public function getById(int $draftOrderId, $params = null)
    {
        return $this->restClient->get("draft_orders/$draftOrderId.json", $params);
    }

    public function create(DraftOrder $draftOrder)
    {
        $payload = ValidationHelper::validate($draftOrder->bindingRestData(), CreateValidator::class);

        return $this->restClient->post('draft_orders.json', [
            'draft_order' => $payload
        ]);
    }

    public function invoiceSend(int $draftOrderId, EmailInput $email)
    {
        $payload = ValidationHelper::validate($email->bindingRestData(), SendInvoiceValidator::class);

        return $this->restClient->post("draft_orders/$draftOrderId/send_invoice.json", [
            'draft_order_invoice' => $payload
        ]);
    }

    public function update(DraftOrder $draftOrder)
    {
        $payload = ValidationHelper::validate($draftOrder->bindingRestData(), UpdateValidator::class);

        return $this->restClient->put("draft_orders/{$draftOrder->get('id')}.json", [
            'draft_order' => $payload
        ]);
    }

    public function complete(DraftOrder $draftOrder)
    {
        $payload = ValidationHelper::validate($draftOrder->bindingRestData(), CompleteValidator::class);

        return $this->restClient->post("draft_orders/{$draftOrder->get('id')}/complete.json", $payload);
    }

    public function remove(int $draftOrderId)
    {
        return $this->restClient->delete("draft_orders/$draftOrderId.json");
    }
}
