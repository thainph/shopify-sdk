<?php

namespace Thainph\ShopifySdk\Services\Rest;

use Thainph\ShopifySdk\Clients\Rest\RestClient;
use Thainph\ShopifySdk\Exceptions\AttributeNotExists;
use Thainph\ShopifySdk\Exceptions\MissingIdentifier;
use Thainph\ShopifySdk\Mockup\Rest\Webhook;
use Thainph\ShopifySdk\Services\BaseService;

class RestWebhookService extends BaseService
{
    private RestClient $restClient;

    public function __construct(RestClient $restClient)
    {
        $this->restClient = $restClient;
    }

    /**
     * @throws AttributeNotExists
     */
    public function create(Webhook $webhook): array
    {
        return $this->restClient->post('webhooks.json', [
            'webhook' => $webhook->bindingRestData()
        ]);
    }

    /**
     * @throws MissingIdentifier
     * @throws AttributeNotExists
     */
    public function remove(Webhook $webhook): array
    {
        if (empty($webhook->get('id'))) {
            throw new MissingIdentifier('Missing webhook id');
        }

        return $this->restClient->delete('webhooks/' . $webhook->get('id') . '.json');
    }

    public function getByTopic($topic): array
    {
        return $this->restClient->get('webhooks.json', [
            'topic' => $topic
        ]);
    }

    public function get(): array
    {
        return $this->restClient->get('webhooks.json');
    }
}
