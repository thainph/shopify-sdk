<?php

namespace Thainph\ShopifySdk\Services\Rest;

use Thainph\ShopifySdk\Clients\Rest\RestClient;
use Thainph\ShopifySdk\Helpers\ValidationHelper;
use Thainph\ShopifySdk\Mockup\Rest\Transaction;
use Thainph\ShopifySdk\Services\BaseService;
use Thainph\ShopifySdk\Validation\Validators\Rest\Transaction\CreateValidator;

class RestTransactionService extends BaseService
{
    private RestClient $restClient;

    public function __construct(RestClient $restClient)
    {
        $this->restClient = $restClient;
    }

    public function get(int $orderId, $params = null)
    {
        return $this->restClient->get("orders/$orderId/transactions.json", $params);
    }

    public function count(int $orderId, $params = null)
    {
        return $this->restClient->get("orders/$orderId/transactions/count.json", $params);
    }

    public function getById(int $orderId, int $transactionId, $params = null)
    {
        return $this->restClient->get("orders/$orderId/transactions/$transactionId.json", $params);
    }

    public function create(int $orderId, Transaction $transaction)
    {
        $payload = ValidationHelper::validate($transaction->bindingRestData(), CreateValidator::class);

        return $this->restClient->post("orders/$orderId/transactions.json", [
            'transaction' => $payload
        ]);
    }
}
