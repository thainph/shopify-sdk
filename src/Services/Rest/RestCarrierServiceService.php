<?php

namespace Thainph\ShopifySdk\Services\Rest;

use Thainph\ShopifySdk\Services\BaseService;
use Thainph\ShopifySdk\Clients\Rest\RestClient;
use Thainph\ShopifySdk\Helpers\ValidationHelper;
use Thainph\ShopifySdk\Mockup\Rest\CarrierService;
use Thainph\ShopifySdk\Validation\Validators\Rest\CarrierService\CreateValidator;
use Thainph\ShopifySdk\Validation\Validators\Rest\CarrierService\UpdateValidator;

class RestCarrierServiceService extends BaseService
{
    private RestClient $restClient;

    public function __construct(RestClient $restClient)
    {
        $this->restClient = $restClient;
    }

    public function get(): array
    {
        $endpoint = 'carrier_services.json';

        return $this->restClient->get($endpoint, null);
    }

    public function getById(int $carrierServiceId): array
    {
        $endpoint = "carrier_services/$carrierServiceId.json";

        return $this->restClient->get($endpoint, null);
    }

    public function create(CarrierService $carrierService): array
    {
        $payload = ValidationHelper::validate($carrierService->bindingRestData(), CreateValidator::class);

        $endpoint = 'carrier_services.json';

        return $this->restClient->post($endpoint, [
            'carrier_service' => $payload
        ]);
    }

    public function update(CarrierService $carrierService): array
    {
        $payload = ValidationHelper::validate($carrierService->bindingRestData(), UpdateValidator::class);

        $endpoint = "carrier_services/{$carrierService->get('id')}.json";

        return $this->restClient->put($endpoint, [
            'carrier_service' => $payload
        ]);
    }

    public function remove(int $carrierServiceId): array
    {
        $endpoint = "carrier_services/$carrierServiceId.json";

        return $this->restClient->delete($endpoint);
    }
}
