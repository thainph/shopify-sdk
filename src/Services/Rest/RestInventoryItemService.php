<?php

namespace Thainph\ShopifySdk\Services\Rest;

use Thainph\ShopifySdk\Services\BaseService;
use Thainph\ShopifySdk\Clients\Rest\RestClient;
use Thainph\ShopifySdk\Helpers\ValidationHelper;
use Thainph\ShopifySdk\Mockup\Rest\InventoryItem;
use Thainph\ShopifySdk\Validation\Validators\Rest\InventoryItem\QueryValidator;
use Thainph\ShopifySdk\Validation\Validators\Rest\InventoryItem\UpdateValidator;

class RestInventoryItemService extends BaseService
{
    private RestClient $restClient;

    public function __construct(RestClient $restClient)
    {
        $this->restClient = $restClient;
    }

    public function get(array $params): array
    {
        $payload = ValidationHelper::validate($params, QueryValidator::class);
        
        return $this->restClient->get('inventory_items.json', $payload);
    }

    public function getById(int $id): array
    {
        return $this->restClient->get("inventory_items/{$id}.json");
    }

    public function update(InventoryItem $inventoryItem): array
    {
        $payload = ValidationHelper::validate($inventoryItem->bindingRestData(), UpdateValidator::class);

        return $this->restClient->put("inventory_items/{$inventoryItem->get('id')}.json", [
            'inventory_item' => $payload
        ]);
    }
}
