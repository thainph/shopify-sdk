<?php

namespace Thainph\ShopifySdk\Services\Rest;

use Thainph\ShopifySdk\Mockup\Rest\ProductImage;
use Thainph\ShopifySdk\Services\BaseService;
use Thainph\ShopifySdk\Clients\Rest\RestClient;
use Thainph\ShopifySdk\Helpers\ValidationHelper;
use Thainph\ShopifySdk\Validation\Validators\Rest\ProductImage\CreateValidator;
use Thainph\ShopifySdk\Validation\Validators\Rest\ProductImage\UpdateValidator;

class RestProductImageService extends BaseService
{
    private RestClient $restClient;

    public function __construct(RestClient $restClient)
    {
        $this->restClient = $restClient;
    }

    public function count(int $productId, $params = null): array
    {
        return $this->restClient->get("products/$productId/images/count.json", $params);
    }

    public function get(int $productId, $params = null): array
    {
        return $this->restClient->get("products/$productId/images.json", $params);
    }

    public function getById(int $productId, int $imageId, $params = null): array
    {
        return $this->restClient->get("products/$productId/images/$imageId.json", $params);
    }

    public function create(ProductImage $image): array
    {
        $payload = ValidationHelper::validate($image->bindingRestData(), CreateValidator::class);

        return $this->restClient->post("products/{$payload['product_id']}/images.json", [
            'image' => $payload
        ]);
    }

    public function update(ProductImage $image): array
    {
        $payload = ValidationHelper::validate($image->bindingRestData(), UpdateValidator::class);

        return $this->restClient->put("products/{$payload['product_id']}/images/{$payload['id']}.json", [
            'image' => $payload
        ]);
    }

    public function remove(int $productId, int $imageId): array
    {
        return $this->restClient->delete("products/$productId/images/$imageId.json");
    }
}
