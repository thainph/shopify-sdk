<?php

namespace Thainph\ShopifySdk\Services\Rest;

use Thainph\ShopifySdk\Mockup\Rest\Page;
use Thainph\ShopifySdk\Services\BaseService;
use Thainph\ShopifySdk\Clients\Rest\RestClient;
use Thainph\ShopifySdk\Helpers\ValidationHelper;
use Thainph\ShopifySdk\Validation\Validators\Rest\Page\CreateValidator;
use Thainph\ShopifySdk\Validation\Validators\Rest\Page\UpdateValidator;

class RestPageService extends BaseService
{
    private RestClient $restClient;

    public function __construct(RestClient $restClient)
    {
        $this->restClient = $restClient;
    }

    public function get($params = null)
    {
        return $this->restClient->get('pages.json', $params);
    }

    public function getById(int $id, $params = null)
    {
        return $this->restClient->get("pages/{$id}.json", $params);
    }

    public function count($params = null)
    {
        return $this->restClient->get('pages/count.json', $params);
    }

    public function create(Page $page): array
    {
        $payload = ValidationHelper::validate($page->bindingRestData(), CreateValidator::class);

        return $this->restClient->post('pages.json', [
            'page' => $payload
        ]);
    }

    public function update(Page $page): array
    {
        $payload = ValidationHelper::validate($page->bindingRestData(), UpdateValidator::class);

        return $this->restClient->put("pages/{$page->get('id')}.json", [
            'page' => $payload
        ]);
    }

    public function remove(int $pageId): array
    {
        return $this->restClient->delete("pages/$pageId.json");
    }
}
