<?php

namespace Thainph\ShopifySdk\Services\Rest;

use Thainph\ShopifySdk\Mockup\Rest\InventoryLevel;
use Thainph\ShopifySdk\Services\BaseService;
use Thainph\ShopifySdk\Clients\Rest\RestClient;
use Thainph\ShopifySdk\Helpers\ValidationHelper;
use Thainph\ShopifySdk\Validation\Validators\Rest\InventoryLevel\AdjustValidator;
use Thainph\ShopifySdk\Validation\Validators\Rest\InventoryLevel\ConnectValidator;
use Thainph\ShopifySdk\Validation\Validators\Rest\InventoryLevel\QueryValidator;
use Thainph\ShopifySdk\Validation\Validators\Rest\InventoryLevel\SetValidator;

class RestInventoryLevelService extends BaseService
{
    private RestClient $restClient;

    public function __construct(RestClient $restClient)
    {
        $this->restClient = $restClient;
    }

    public function get($params = null): array
    {
        $payload = ValidationHelper::validate($params, QueryValidator::class);

        return $this->restClient->get('inventory_levels.json', $payload);
    }

    public function adjust(InventoryLevel $blog): array
    {
        $payload = ValidationHelper::validate($blog->bindingRestData(), AdjustValidator::class);

        return $this->restClient->post('inventory_levels/adjust.json', $payload);
    }

    public function connect(InventoryLevel $blog): array
    {
        $payload = ValidationHelper::validate($blog->bindingRestData(), ConnectValidator::class);

        return $this->restClient->post('inventory_levels/connect.json', $payload);
    }

    public function set(InventoryLevel $blog): array
    {
        $payload = ValidationHelper::validate($blog->bindingRestData(), SetValidator::class);

        return $this->restClient->post('inventory_levels/set.json', $payload);
    }

    public function remove(int $inventoryItemId, int $locationId): array
    {
        return $this->restClient->delete('inventory_levels.json', [
            'inventory_item_id' => $inventoryItemId,
            'location_id' => $locationId
        ]);
    }
}
