<?php

namespace Thainph\ShopifySdk\Services\Rest;

use Thainph\ShopifySdk\Clients\Rest\RestClient;
use Thainph\ShopifySdk\Services\BaseService;

class RestPaymentService extends BaseService
{
    private RestClient $restClient;

    public function __construct(RestClient $restClient)
    {
        $this->restClient = $restClient;
    }


    public function getCheckoutObjectByToken($token)
    {
        return $this->restClient->get("checkouts/$token.json", [
            'Location' => $this->restClient->getUnstableEndpoint("checkouts/$token.json"),
            'Retry-After' => '1'
        ]);
    }

    public function getCartObjectByToken($token)
    {
        return $this->restClient->get("carts/$token.json", [
            'Location' => $this->restClient->getUnstableEndpoint("carts/$token.json"),
            'Retry-After' => '1'
        ]);
    }
}
