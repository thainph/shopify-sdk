<?php

namespace Thainph\ShopifySdk\Services\Rest;

use Thainph\ShopifySdk\Mockup\Rest\RecurringApplicationCharge;
use Thainph\ShopifySdk\Services\BaseService;
use Thainph\ShopifySdk\Clients\Rest\RestClient;
use Thainph\ShopifySdk\Helpers\ValidationHelper;
use Thainph\ShopifySdk\Validation\Validators\Rest\RecurringApplicationCharge\CreateValidator;
use Thainph\ShopifySdk\Validation\Validators\Rest\RecurringApplicationCharge\UpdateValidator;

class RestRecurringApplicationChargeService extends BaseService
{
    private RestClient $restClient;

    public function __construct(RestClient $restClient)
    {
        $this->restClient = $restClient;
    }

    public function get($params = null): array
    {
        return $this->restClient->get('recurring_application_charges.json', $params);
    }

    public function getById(int $recurringApplicationChargeId, $params = null): array
    {
        return $this->restClient->get("recurring_application_charges/$recurringApplicationChargeId.json", $params);
    }

    public function create(RecurringApplicationCharge $recurringApplicationCharge): array
    {
        $payload = ValidationHelper::validate($recurringApplicationCharge->bindingRestData(), CreateValidator::class);

        return $this->restClient->post('recurring_application_charges.json', [
            'recurring_application_charge' => $payload
        ]);
    }

    public function update(RecurringApplicationCharge $recurringApplicationCharge): array
    {
        $payload = ValidationHelper::validate($recurringApplicationCharge->bindingRestData(), UpdateValidator::class);

        return $this->restClient->put("recurring_application_charges/{$recurringApplicationCharge->get('id')}/customize.json", [
            'recurring_application_charge' => $payload
        ]);
    }

    public function cancel(int $recurringApplicationChargeId): array
    {
        return $this->restClient->delete("recurring_application_charges/$recurringApplicationChargeId.json");
    }
}
