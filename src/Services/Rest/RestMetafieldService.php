<?php

namespace Thainph\ShopifySdk\Services\Rest;

use Thainph\ShopifySdk\Clients\Rest\RestClient;
use Thainph\ShopifySdk\Enums\Rest\MetafieldOwnerType;
use Thainph\ShopifySdk\Exceptions\AttributeNotExists;
use Thainph\ShopifySdk\Exceptions\InvalidInput;
use Thainph\ShopifySdk\Exceptions\WrongMetafieldResource;
use Thainph\ShopifySdk\Helpers\ValidationHelper;
use Thainph\ShopifySdk\Mockup\Rest\Metafield;
use Thainph\ShopifySdk\Services\BaseService;
use Thainph\ShopifySdk\Validation\Validators\Rest\Metafield\CreateValidator;
use Thainph\ShopifySdk\Validation\Validators\Rest\Metafield\UpdateValidator;

class RestMetafieldService extends BaseService
{
    private RestClient $restClient;

    public function __construct(RestClient $restClient)
    {
        $this->restClient = $restClient;
    }

    /**
     * @throws WrongMetafieldResource
     */
    public function count(string $resource, int $resourceId): array
    {
        if (!MetafieldOwnerType::isValid($resource)) {
            throw new WrongMetafieldResource('Resource not found');
        }

        $endpoint = $resource === MetafieldOwnerType::SHOP
            ? 'metafields/count.json'
            : $resource . '/' . $resourceId . '/metafields/count.json';

        return $this->restClient->get($endpoint);
    }

    /**
     * @throws WrongMetafieldResource
     */
    public function get(string $resource, int $resourceId, string $query = null): array
    {
        if (!MetafieldOwnerType::isValid($resource)) {
            throw new WrongMetafieldResource('Resource not found');
        }

        $endpoint = $resource === MetafieldOwnerType::SHOP
            ? 'metafields.json'
            : $resource . '/' . $resourceId . '/metafields.json';

        return $this->restClient->get($endpoint, $query);
    }

    /**
     * @throws WrongMetafieldResource
     */
    public function getById(int $metafieldId, string $resource, int $resourceId): array
    {
        if (!MetafieldOwnerType::isValid($resource)) {
            throw new WrongMetafieldResource('Resource not found');
        }

        $endpoint = $resource === MetafieldOwnerType::SHOP
            ? 'metafields'
            : $resource . '/' . $resourceId . '/metafields';
        $endpoint .= '/' . $metafieldId . '.json';

        return $this->restClient->get($endpoint);
    }

    /**
     * @throws InvalidInput
     * @throws WrongMetafieldResource
     * @throws AttributeNotExists
     */
    public function create(Metafield $metafield, string $resource, int $resourceId): array
    {
        if (!MetafieldOwnerType::isValid($resource)) {
            throw new WrongMetafieldResource('Resource not found');
        }

        $payload  = ValidationHelper::validate($metafield->bindingRestData(), CreateValidator::class);
        $endpoint = $resource === MetafieldOwnerType::SHOP
            ? 'metafields.json'
            : $resource . '/' . $resourceId . '/metafields.json';

        return $this->restClient->post($endpoint, [
            'metafield' => $payload
        ]);
    }

    /**
     * @throws InvalidInput
     * @throws WrongMetafieldResource
     * @throws AttributeNotExists
     */
    public function update(Metafield $metafield, string $resource, int $resourceId): array
    {
        if (!MetafieldOwnerType::isValid($resource)) {
            throw new WrongMetafieldResource('Resource not found');
        }

        $payload  = ValidationHelper::validate($metafield->bindingRestData(), UpdateValidator::class);
        $endpoint = $resource === MetafieldOwnerType::SHOP
            ? 'metafields'
            : $resource . '/' . $resourceId . '/metafields';
        $endpoint .= '/' . $payload['id'] . '.json';

        return $this->restClient->put($endpoint, [
            'metafield' => $payload
        ]);
    }

    /**
     * @throws WrongMetafieldResource
     */
    public function remove(int $metafieldId, string $resource, int $resourceId): array
    {
        if (!MetafieldOwnerType::isValid($resource)) {
            throw new WrongMetafieldResource('Resource not found');
        }

        $endpoint = $resource === MetafieldOwnerType::SHOP
            ? 'metafields'
            : $resource . '/' . $resourceId . '/metafields';
        $endpoint .= '/' . $metafieldId . '.json';

        return $this->restClient->delete($endpoint);
    }
}
