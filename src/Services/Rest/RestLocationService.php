<?php

namespace Thainph\ShopifySdk\Services\Rest;

use Thainph\ShopifySdk\Clients\Rest\RestClient;
use Thainph\ShopifySdk\Services\BaseService;

class RestLocationService extends BaseService
{
    private RestClient $restClient;

    public function __construct(RestClient $restClient)
    {
        $this->restClient = $restClient;
    }

    public function get($params = null)
    {
        return $this->restClient->get('locations.json', $params);
    }

    public function count($params = null)
    {
        return $this->restClient->get('locations/count.json', $params);
    }

    public function getById(int $locationId, $params = null)
    {
        return $this->restClient->get("locations/$locationId.json", $params);
    }

    public function getInventoryLevels(int $locationId, $params = null)
    {
        return $this->restClient->get("locations/$locationId/inventory_levels.json", $params);
    }
}
