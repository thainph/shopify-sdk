<?php

namespace Thainph\ShopifySdk\Services\Rest;

use Thainph\ShopifySdk\Clients\Rest\RestClient;
use Thainph\ShopifySdk\Helpers\ValidationHelper;
use Thainph\ShopifySdk\Mockup\Rest\ScriptTag;
use Thainph\ShopifySdk\Services\BaseService;
use Thainph\ShopifySdk\Validation\Validators\Rest\ScriptTag\CreateValidator;
use Thainph\ShopifySdk\Validation\Validators\Rest\ScriptTag\UpdateValidator;

class RestScriptTagService extends BaseService
{
    private RestClient $restClient;

    public function __construct(RestClient $restClient)
    {
        $this->restClient = $restClient;
    }

    public function get($params = null)
    {
        return $this->restClient->get('script_tags.json', $params);
    }

    public function count($params = null)
    {
        return $this->restClient->get('script_tags/count.json', $params);
    }

    public function getById(int $scriptTagId, $params = null)
    {
        return $this->restClient->get("script_tags/$scriptTagId.json", $params);
    }

    public function create(ScriptTag $scriptTag)
    {
        $payload = ValidationHelper::validate($scriptTag->bindingRestData(), CreateValidator::class);

        return $this->restClient->post('script_tags.json', [
            'script_tag' => $payload
        ]);
    }

    public function update(ScriptTag $scriptTag)
    {
        $payload = ValidationHelper::validate($scriptTag->bindingRestData(), UpdateValidator::class);

        return $this->restClient->put("script_tags/{$scriptTag->get('id')}.json", [
            'script_tag' => $payload
        ]);
    }

    public function remove(int $scriptTagId)
    {
        return $this->restClient->delete("script_tags/$scriptTagId.json");
    }
}
