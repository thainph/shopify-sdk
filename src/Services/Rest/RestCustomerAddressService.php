<?php

namespace Thainph\ShopifySdk\Services\Rest;

use Thainph\ShopifySdk\Services\BaseService;
use Thainph\ShopifySdk\Clients\Rest\RestClient;
use Thainph\ShopifySdk\Helpers\ValidationHelper;
use Thainph\ShopifySdk\Mockup\Rest\CustomerAddress;
use Thainph\ShopifySdk\Validation\Validators\Rest\CustomerAddress\BulkRemoveValidator;
use Thainph\ShopifySdk\Validation\Validators\Rest\CustomerAddress\CreateValidator;
use Thainph\ShopifySdk\Validation\Validators\Rest\CustomerAddress\UpdateValidator;

class RestCustomerAddressService extends BaseService
{
    private RestClient $restClient;

    public function __construct(RestClient $restClient)
    {
        $this->restClient = $restClient;
    }

    public function get(int $customerId, $query = null): array
    {
        $endpoint = "customers/$customerId/addresses.json";

        return $this->restClient->get($endpoint, $query);
    }

    public function getById(int $customerId, int $addressId): array
    {
        $endpoint = "customers/$customerId/addresses/$addressId.json";

        return $this->restClient->get($endpoint, null);
    }

    public function create(int $customerId, CustomerAddress $address): array
    {
        $validator = ValidationHelper::validate($address->bindingRestData(), CreateValidator::class);
        
        return $this->restClient->post("customers/$customerId/addresses.json", [
            'address' => $validator
        ]);
    }

    public function update(int $customerId, CustomerAddress $address): array
    {
        $validator = ValidationHelper::validate($address->bindingRestData(), UpdateValidator::class);

        return $this->restClient->put("customers/$customerId/addresses/{$validator['id']}.json", [
            'address' => $validator
        ]);
    }

    public function setDefault(int $customerId, int $addressId): array
    {
        return $this->restClient->put("customers/$customerId/addresses/$addressId/default.json", null);
    }

    public function bulkRemove(int $customerId, array $addressIds): array
    {
        ValidationHelper::validate([
            'address_ids' => $addressIds
        ], BulkRemoveValidator::class);

        $endpoint = "customers/$customerId/addresses/set.json";

        return $this->restClient->put($endpoint, [
            'address_ids' => $addressIds,
            'operation'   => 'destroy'
        ]);
    }

    public function remove(int $customerId, int $addressId): array
    {
        return $this->restClient->delete("customers/$customerId/addresses/$addressId.json");
    }
}
