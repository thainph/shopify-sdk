<?php

namespace Thainph\ShopifySdk\Services\Rest;

use Thainph\ShopifySdk\Mockup\Rest\Blog;
use Thainph\ShopifySdk\Services\BaseService;
use Thainph\ShopifySdk\Clients\Rest\RestClient;
use Thainph\ShopifySdk\Helpers\ValidationHelper;
use Thainph\ShopifySdk\Validation\Validators\Rest\Blog\CreateValidator;
use Thainph\ShopifySdk\Validation\Validators\Rest\Blog\UpdateValidator;

class RestBlogService extends BaseService
{
    private RestClient $restClient;

    public function __construct(RestClient $restClient)
    {
        $this->restClient = $restClient;
    }

    public function count(): array
    {
        return $this->restClient->get('blogs/count.json');
    }

    public function get($params = null): array
    {
        return $this->restClient->get('blogs.json', $params);
    }

    public function getById(int $blogId, $params = null): array
    {
        return $this->restClient->get("blogs/$blogId.json", $params);
    }

    public function create(Blog $blog): array
    {
        $payload = ValidationHelper::validate($blog->bindingRestData(), CreateValidator::class);

        return $this->restClient->post('blogs.json', [
            'blog' => $payload
        ]);
    }

    public function update(Blog $blog): array
    {
        $payload = ValidationHelper::validate($blog->bindingRestData(), UpdateValidator::class);

        return $this->restClient->put("blogs/{$blog->get('id')}.json", [
            'blog' => $payload
        ]);
    }

    public function remove(int $blogId): array
    {
        return $this->restClient->delete("blogs/$blogId.json");
    }
}
