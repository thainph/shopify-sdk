<?php

namespace Thainph\ShopifySdk\Services\Rest;

use Thainph\ShopifySdk\Services\BaseService;
use Thainph\ShopifySdk\Clients\Rest\RestClient;

class RestCollectService extends BaseService
{
    private RestClient $restClient;

    public function __construct(RestClient $restClient)
    {
        $this->restClient = $restClient;
    }

    public function count(): array
    {
        return $this->restClient->get('collects/count.json');
    }

    public function get($params = null): array
    {
        return $this->restClient->get('collects.json', $params);
    }

    public function getById(int $collectId, $params = null): array
    {
        return $this->restClient->get("collects/$collectId.json", $params);
    }

    public function addProductToCustomCollection(int $productId, int $collectionId): array
    {
        return $this->restClient->post('collects.json', [
            'collect' => [
                'product_id' => $productId,
                'collection_id' => $collectionId
            ]
        ]);
    }

    public function remove(int $collectId): array
    {
        return $this->restClient->delete("collects/$collectId.json");
    }
}
