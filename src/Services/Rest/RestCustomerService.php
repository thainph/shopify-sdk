<?php

namespace Thainph\ShopifySdk\Services\Rest;

use Thainph\ShopifySdk\Clients\Rest\RestClient;
use Thainph\ShopifySdk\Exceptions\AttributeNotExists;
use Thainph\ShopifySdk\Exceptions\InvalidInput;
use Thainph\ShopifySdk\Helpers\ValidationHelper;
use Thainph\ShopifySdk\Mockup\Rest\Customer;
use Thainph\ShopifySdk\Services\BaseService;
use Thainph\ShopifySdk\Validation\Validators\Rest\Customer\CreateValidator;
use Thainph\ShopifySdk\Validation\Validators\Rest\Customer\UpdateValidator;

class RestCustomerService extends BaseService
{
    private RestClient $restClient;

    public function __construct(RestClient $restClient)
    {
        $this->restClient = $restClient;
    }

    public function get($query = null, $useSearchEndpoint = false): array
    {
        $endpoint = $useSearchEndpoint ? 'customers/search.json' : 'customers.json';

        return $this->restClient->get($endpoint, $query);
    }

    public function getById(int $customerId): array
    {
        $endpoint = "customers/$customerId.json";

        return $this->restClient->get($endpoint, null);
    }

    /**
     * @throws InvalidInput
     * @throws AttributeNotExists
     */
    public function create(Customer $customer): array
    {
        $payload = ValidationHelper::validate($customer->bindingRestData(), CreateValidator::class);

        return $this->restClient->post('customers.json', [
            'customer' => $payload
        ]);
    }

    /**
     * @throws AttributeNotExists
     * @throws InvalidInput
     */
    public function update(Customer $customer): array
    {
        $payload = ValidationHelper::validate($customer->bindingRestData(), UpdateValidator::class);

        return $this->restClient->put('customers/' . $customer->get('id') . '.json', [
            'customer' => $payload
        ]);
    }

    public function remove(int $customerId): array
    {
        return $this->restClient->delete('customers/' . $customerId . '.json');
    }

    public function getOrders(int $customerId, $query = null): array
    {
        return $this->restClient->get('customers/' . $customerId . '/orders.json', $query);
    }

    public function sendInviteEmail(int $customerId): array
    {
        return $this->restClient->post('customers/' . $customerId . '/send_invite.json');
    }

    public function createActivationUrl(int $customerId): array
    {
        return $this->restClient->post('customers/' . $customerId . '/account_activation_url.json');
    }
}
