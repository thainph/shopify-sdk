<?php

namespace Thainph\ShopifySdk\Services\Rest;

use Thainph\ShopifySdk\Services\BaseService;
use Thainph\ShopifySdk\Mockup\Rest\PriceRule;
use Thainph\ShopifySdk\Clients\Rest\RestClient;
use Thainph\ShopifySdk\Helpers\ValidationHelper;
use Thainph\ShopifySdk\Validation\Validators\Rest\PriceRule\CreateValidator;
use Thainph\ShopifySdk\Validation\Validators\Rest\PriceRule\UpdateValidator;

class RestPriceRuleService extends BaseService
{
    private RestClient $restClient;

    public function __construct(RestClient $restClient)
    {
        $this->restClient = $restClient;
    }

    public function count(): array
    {
        return $this->restClient->get('price_rules/count.json');
    }

    public function get($query = null): array
    {
        return $this->restClient->get('price_rules.json', $query);
    }

    public function getById(int $priceRuleId): array
    {
        $endpoint = "price_rules/$priceRuleId.json";

        return $this->restClient->get($endpoint, null);
    }

    /**
     * @throws InvalidInput
     * @throws AttributeNotExists
     */
    public function create(PriceRule $priceRule): array
    {
        $payload = ValidationHelper::validate($priceRule->bindingRestData(), CreateValidator::class);

        return $this->restClient->post('price_rules.json', [
            'price_rule' => $payload
        ]);
    }

    /**
     * @throws InvalidInput
     * @throws AttributeNotExists
     */
    public function update(PriceRule $priceRule): array
    {
        $payload = ValidationHelper::validate($priceRule->bindingRestData(), UpdateValidator::class);

        $endpoint = "price_rules/{$priceRule->get('id')}.json";

        return $this->restClient->put($endpoint, [
            'price_rule' => $payload
        ]);
    }

    public function remove(int $priceRuleId): array
    {
        $endpoint = "price_rules/$priceRuleId.json";

        return $this->restClient->delete($endpoint);
    }
}
