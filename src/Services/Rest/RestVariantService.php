<?php

namespace Thainph\ShopifySdk\Services\Rest;

use Thainph\ShopifySdk\Clients\Rest\RestClient;
use Thainph\ShopifySdk\Exceptions\AttributeNotExists;
use Thainph\ShopifySdk\Exceptions\InvalidInput;
use Thainph\ShopifySdk\Helpers\ValidationHelper;
use Thainph\ShopifySdk\Mockup\Rest\Variant;
use Thainph\ShopifySdk\Services\BaseService;
use Thainph\ShopifySdk\Validation\Validators\Rest\Variant\CreateValidator;
use Thainph\ShopifySdk\Validation\Validators\Rest\Variant\UpdateValidator;

class RestVariantService extends BaseService
{
    private RestClient $restClient;

    public function __construct(RestClient $restClient)
    {
        $this->restClient = $restClient;
    }

    public function count(int $productId): array
    {
        return $this->restClient->get("products/$productId/variants/count.json");
    }

    public function get(int $productId, string $query = null): array
    {
        return $this->restClient->get("products/$productId/variants.json", $query);
    }

    public function getById(int $variantId): array
    {
        return $this->restClient->get("variants/$variantId.json");
    }

    /**
     * @throws InvalidInput
     * @throws AttributeNotExists
     */
    public function create(Variant $variant): array
    {
        $payload = ValidationHelper::validate($variant->bindingRestData(), CreateValidator::class);

        return $this->restClient->post('products/' . $variant->get('product_id') . '/variants.json', [
            'variant' => $payload
        ]);
    }

    /**
     * @throws AttributeNotExists
     * @throws InvalidInput
     */
    public function update(Variant $variant): array
    {
        $payload = ValidationHelper::validate($variant->bindingRestData(), UpdateValidator::class);

        return $this->restClient->put('variants/' . $variant->get('id') . '.json', [
            'variant' => $payload
        ]);
    }

    public function remove(int $variantId): array
    {
        return $this->restClient->delete("variants/$variantId.json");
    }
}
