<?php

namespace Thainph\ShopifySdk\Services\Rest;

use Thainph\ShopifySdk\Mockup\Rest\Order;
use Thainph\ShopifySdk\Services\BaseService;
use Thainph\ShopifySdk\Clients\Rest\RestClient;
use Thainph\ShopifySdk\Helpers\ValidationHelper;
use Thainph\ShopifySdk\Validation\Validators\Rest\Order\CreateValidator;
use Thainph\ShopifySdk\Validation\Validators\Rest\Order\UpdateValidator;

class RestOrderService extends BaseService
{
    private RestClient $restClient;

    public function __construct(RestClient $restClient)
    {
        $this->restClient = $restClient;
    }

    public function count($params = null): array
    {
        return $this->restClient->get('orders/count.json', $params);
    }

    public function get($params = null): array
    {
        return $this->restClient->get('orders.json', $params);
    }

    public function getById(int $orderId, $params = null): array
    {
        return $this->restClient->get("orders/$orderId.json", $params);
    }

    public function create(Order $order): array
    {
        $payload = ValidationHelper::validate($order->bindingRestData(), CreateValidator::class);

        return $this->restClient->post('orders.json', [
            'order' => $payload
        ]);
    }

    public function cancelById(int $orderId): array
    {
        return $this->restClient->post("orders/$orderId/cancel.json");
    }

    public function closeById(int $orderId): array
    {
        return $this->restClient->post("orders/$orderId/close.json");
    }

    public function openById(int $orderId): array
    {
        return $this->restClient->post("orders/$orderId/open.json");
    }

    public function update(Order $order): array
    {
        $payload = ValidationHelper::validate($order->bindingRestData(), UpdateValidator::class);

        return $this->restClient->put("orders/{$payload['id']}.json", [
            'order' => $payload
        ]);
    }

    public function remove(int $orderId): array
    {
        return $this->restClient->delete("orders/$orderId.json");
    }
}
