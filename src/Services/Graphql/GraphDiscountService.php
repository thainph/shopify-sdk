<?php

namespace Thainph\ShopifySdk\Services\Graphql;

use Thainph\ShopifySdk\Services\BaseService;
use Thainph\ShopifySdk\Exceptions\InvalidInput;
use Thainph\ShopifySdk\Helpers\ValidationHelper;
use Thainph\ShopifySdk\Clients\Graphql\GraphClient;
use Thainph\ShopifySdk\Mockup\Graphql\DiscountCode;
use Thainph\ShopifySdk\Enums\Graphql\DiscountService;
use Thainph\ShopifySdk\Exceptions\ServiceNotSupported;
use Thainph\ShopifySdk\Mockup\Graphql\DiscountAutomatic;
use Thainph\ShopifySdk\Validation\Validators\Graphql\Discount\IsDiscountId;
use Thainph\ShopifySdk\Validation\Validators\Graphql\Discount\QueryValidator;
use Thainph\ShopifySdk\Validation\Validators\Graphql\DiscountCode\BulkValidator as CodeBulkValidator;
use Thainph\ShopifySdk\Validation\Validators\Graphql\DiscountCode\QueryValidator as CodeQueryValidator;
use Thainph\ShopifySdk\Validation\Validators\Graphql\DiscountCode\IsCodeDiscountId;
use Thainph\ShopifySdk\Validation\Validators\Graphql\DiscountCode\AppInputValidator as CodeAppInputValidator;
use Thainph\ShopifySdk\Validation\Validators\Graphql\DiscountCode\BxgyInputValidator as CodeBxgyInputValidator;
use Thainph\ShopifySdk\Validation\Validators\Graphql\DiscountCode\BasicInputValidator as CodeBasicInputValidator;
use Thainph\ShopifySdk\Validation\Validators\Graphql\DiscountCode\BulkRedeemCreateValidator as CodeBulkRedeemCreateValidator;
use Thainph\ShopifySdk\Validation\Validators\Graphql\DiscountCode\BulkRedeemRemoveValidator as CodeBulkRedeemRemoveValidator;
use Thainph\ShopifySdk\Validation\Validators\Graphql\DiscountCode\QuerySavedSearchValidator as CodeQuerySavedSearchValidator;
use Thainph\ShopifySdk\Validation\Validators\Graphql\DiscountCode\FreeShippingInputValidator as CodeFreeShippingInputValidator;
use Thainph\ShopifySdk\Validation\Validators\Graphql\DiscountCode\IsDiscountRedeemCodeBulkCreationId;
use Thainph\ShopifySdk\Validation\Validators\Graphql\DiscountAutomatic\QueryValidator as AutomaticQueryValidator;
use Thainph\ShopifySdk\Validation\Validators\Graphql\DiscountAutomatic\QuerySavedSearchValidator as AutomaticQuerySavedSearchValidator;
use Thainph\ShopifySdk\Validation\Validators\Graphql\DiscountAutomatic\AppInputValidator as AutomaticAppInputValidator;
use Thainph\ShopifySdk\Validation\Validators\Graphql\DiscountAutomatic\BxgyInputValidator as AutomaticBxgyInputValidator;
use Thainph\ShopifySdk\Validation\Validators\Graphql\DiscountAutomatic\BasicInputValidator as AutomaticBasicInputValidator;
use Thainph\ShopifySdk\Validation\Validators\Graphql\DiscountAutomatic\BulkValidator as AutomaticBulkValidator;
use Thainph\ShopifySdk\Validation\Validators\Graphql\DiscountAutomatic\IsAutomaticDiscountId;
use Thainph\ShopifySdk\Validation\Validators\Graphql\DiscountAutomatic\FreeShippingInputValidator as AutomaticFreeShippingInputValidator;

class GraphDiscountService extends BaseService
{
    protected GraphClient $graphClient;
    protected string $service = DiscountService::ALL;

    public function __construct(GraphClient $graphClient)
    {
        $this->graphClient = $graphClient;
    }

    public function setDiscountService(string $service): self
    {
        if (!in_array($service, DiscountService::toArray())) {
            throw new ServiceNotSupported($service);
        }

        $this->service = $service;

        return $this;
    }

    public function getDiscountService(): string
    {
        return $this->service;
    }

    private function getDiscountQueryFields(): string
    {
        return 'id discount{
            ... on DiscountCodeFreeShipping { title status startsAt endsAt }
            ... on DiscountCodeBxgy { title status startsAt endsAt }
            ... on DiscountCodeBasic { title status startsAt endsAt }
            ... on DiscountCodeApp { title status startsAt endsAt }
            ... on DiscountAutomaticFreeShipping { title status startsAt endsAt }
            ... on DiscountAutomaticBxgy { title status startsAt endsAt }
            ... on DiscountAutomaticBasic { title status startsAt endsAt }
            ... on DiscountAutomaticApp { title status startsAt endsAt }
            __typename
        }';
    }

    #region Discount Automatic
    private function getAutomaticDiscountQueryFields(): string
    {
        return 'id automaticDiscount{
            ... on DiscountAutomaticFreeShipping { title status startsAt endsAt }
            ... on DiscountAutomaticBxgy { title status startsAt endsAt }
            ... on DiscountAutomaticBasic { title status startsAt endsAt }
            ... on DiscountAutomaticApp { title status startsAt endsAt }
            __typename
        }';
    }

    /**
     * @throws InvalidInput
     */
    public function getDiscountAutomatics(array $params = [], ?string $queryFields = null): array
    {
        $payload = ValidationHelper::validate($params, AutomaticQueryValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'edges { node { '
                . $this->getAutomaticDiscountQueryFields() .
            ' } }';
        }

        $query = '
            query ($after: String, $before: String, $first: Int, $last: Int, $query: String, $reverse: Boolean, $savedSearchId: ID, $sortKey: AutomaticDiscountSortKeys)  {
                automaticDiscountNodes(after: $after, before: $before, first: $first, last: $last, query: $query, reverse: $reverse, savedSearchId: $savedSearchId, sortKey: $sortKey) {
                    ' . $queryFields . '
                    pageInfo{
                        endCursor
                        hasNextPage
                        startCursor
                        hasPreviousPage
                    }
                }
            }';

        return $this->graphClient->request($query, $payload);
    }

    public function getDiscountAutomaticSavedSearches(array $params = [], ?string $queryFields = null): array
    {
        $payload = ValidationHelper::validate($params, AutomaticQuerySavedSearchValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'edges { node { id name query resourceType } }';
        }

        $query = '
            query ($after: String, $before: String, $first: Int, $last: Int, $reverse: Boolean)  {
                automaticDiscountSavedSearches(after: $after, before: $before, first: $first, last: $last, reverse: $reverse) {
                    ' . $queryFields . '
                    pageInfo{
                        endCursor
                        hasNextPage
                        startCursor
                        hasPreviousPage
                    }
                }
            }';
        
        return $this->graphClient->request($query, $payload);
    }

    public function getDiscountAutomaticById(string $automaticDiscountId, ?string $queryFields = null): array
    {
        $payloadId = ValidationHelper::validate(['id' => $automaticDiscountId], IsAutomaticDiscountId::class);

        if (empty($queryFields)) {
            $queryFields = $this->getAutomaticDiscountQueryFields();
        }

        $query = '
            query ($id: ID!) {
                automaticDiscountNode(id: $id) {
                    ' . $queryFields . '
                }
            }';

        return $this->graphClient->request($query, $payloadId);
    }

    /**
     * @throws InvalidInput
     */
    public function createDiscountAutomaticApp(DiscountAutomatic $discountAutomatic, ?string $queryFields = null): array
    {
        $payload = ValidationHelper::validate($discountAutomatic->bindingGraphData(), AutomaticAppInputValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'automaticAppDiscount { discountId startsAt endsAt status }';
        }

        $query = '
            mutation ($automaticAppDiscount: DiscountAutomaticAppInput!) {
                discountAutomaticAppCreate(automaticAppDiscount: $automaticAppDiscount) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, [
            'automaticAppDiscount' => $payload
        ]);
    }

    /**
     * @throws InvalidInput
     */
    public function updateDiscountAutomaticApp(string $discountId, DiscountAutomatic $discountAutomatic, ?string $queryFields = null): array
    {
        ValidationHelper::validate(['id' => $discountId], IsAutomaticDiscountId::class);

        $payload = ValidationHelper::validate($discountAutomatic->bindingGraphData(), AutomaticAppInputValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'automaticAppDiscount { discountId startsAt endsAt status }';
        }

        $query = '
            mutation ($automaticAppDiscount: DiscountAutomaticAppInput!, $id: ID!) {
                discountAutomaticAppUpdate(automaticAppDiscount: $automaticAppDiscount, id: $id) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, [
            'id' => $discountId,
            'automaticAppDiscount' => $payload
        ]);
    }

    /**
     * @throws InvalidInput
     */
    public function createDiscountAutomaticBasic(DiscountAutomatic $discountAutomatic, ?string $queryFields = null): array
    {
        $payload = ValidationHelper::validate($discountAutomatic->bindingGraphData(), AutomaticBasicInputValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'automaticDiscountNode { id automaticDiscount{
                ... on DiscountAutomaticBasic { title status startsAt endsAt } 
            } }';
        }

        $query = '
            mutation ($automaticBasicDiscount: DiscountAutomaticBasicInput!) {
                discountAutomaticBasicCreate(automaticBasicDiscount: $automaticBasicDiscount) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, [
            'automaticBasicDiscount' => $payload
        ]);
    }

    /**
     * @throws InvalidInput
     */
    public function updateDiscountAutomaticBasic(string $discountId, DiscountAutomatic $discountAutomatic, ?string $queryFields = null): array
    {
        ValidationHelper::validate(['id' => $discountId], IsAutomaticDiscountId::class);

        $payload = ValidationHelper::validate($discountAutomatic->bindingGraphData(), AutomaticBasicInputValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'automaticDiscountNode { id automaticDiscount{
                ... on DiscountAutomaticBasic { title status startsAt endsAt } 
            } }';
        }

        $query = '
            mutation ($automaticBasicDiscount: DiscountAutomaticBasicInput!, $id: ID!) {
                discountAutomaticBasicUpdate(automaticBasicDiscount: $automaticBasicDiscount, id: $id) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, [
            'id' => $discountId,
            'automaticBasicDiscount' => $payload
        ]);
    }

    /**
     * @throws InvalidInput
     */
    public function createDiscountAutomaticBxgy(DiscountAutomatic $discountAutomatic, ?string $queryFields = null): array
    {
        $payload = ValidationHelper::validate($discountAutomatic->bindingGraphData(), AutomaticBxgyInputValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'automaticDiscountNode { id automaticDiscount{
                ... on DiscountAutomaticBxgy { title status startsAt endsAt }
            } }';
        }

        $query = '
            mutation ($automaticBxgyDiscount: DiscountAutomaticBxgyInput!) {
                discountAutomaticBxgyCreate(automaticBxgyDiscount: $automaticBxgyDiscount) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, [
            'automaticBxgyDiscount' => $payload
        ]);
    }

    /**
     * @throws InvalidInput
     */
    public function updateDiscountAutomaticBxgy(string $discountId, DiscountAutomatic $discountAutomatic, ?string $queryFields = null): array
    {
        ValidationHelper::validate(['id' => $discountId], IsAutomaticDiscountId::class);

        $payload = ValidationHelper::validate($discountAutomatic->bindingGraphData(), AutomaticBxgyInputValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'automaticDiscountNode { id automaticDiscount{
                ... on DiscountAutomaticBxgy { title status startsAt endsAt }
            } }';
        }

        $query = '
            mutation ($automaticBxgyDiscount: DiscountAutomaticBxgyInput!, $id: ID!) {
                discountAutomaticBxgyUpdate(automaticBxgyDiscount: $automaticBxgyDiscount, id: $id) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, [
            'id' => $discountId,
            'automaticBxgyDiscount' => $payload
        ]);
    }

    /**
     * @throws InvalidInput
     */
    public function createDiscountAutomaticFreeShipping(DiscountAutomatic $discountAutomatic, ?string $queryFields = null): array
    {
        $payload = ValidationHelper::validate($discountAutomatic->bindingGraphData(), AutomaticFreeShippingInputValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'automaticDiscountNode { id automaticDiscount{
                ... on DiscountAutomaticFreeShipping { title status startsAt endsAt }
            } }';
        }

        $query = '
            mutation ($freeShippingAutomaticDiscount: DiscountAutomaticFreeShippingInput!) {
                    discountAutomaticFreeShippingCreate(freeShippingAutomaticDiscount: $freeShippingAutomaticDiscount) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, [
            'freeShippingAutomaticDiscount' => $payload
        ]);
    }

    /**
     * @throws InvalidInput
     */
    public function updateDiscountAutomaticFreeShipping(string $discountId, DiscountAutomatic $discountAutomatic, ?string $queryFields = null): array
    {
        ValidationHelper::validate(['id' => $discountId], IsAutomaticDiscountId::class);

        $payload = ValidationHelper::validate($discountAutomatic->bindingGraphData(), AutomaticFreeShippingInputValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'automaticDiscountNode { id automaticDiscount{
                ... on DiscountAutomaticFreeShipping { title status startsAt endsAt }
            } }';
        }

        $query = '
            mutation ($freeShippingAutomaticDiscount: DiscountAutomaticFreeShippingInput!, $id: ID!) {
                discountAutomaticFreeShippingUpdate(freeShippingAutomaticDiscount: $freeShippingAutomaticDiscount, id: $id) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, [
            'id' => $discountId,
            'freeShippingAutomaticDiscount' => $payload
        ]);
    }

    /**
     * @throws InvalidInput
     */
    public function discountAutomaticActivate(string $discountId, ?string $queryFields = null): array
    {
        $payloadId = ValidationHelper::validate(['id' => $discountId], IsAutomaticDiscountId::class);

        if (empty($queryFields)) {
            $queryFields = 'automaticDiscountNode { '
                . $this->getAutomaticDiscountQueryFields() .
            ' }';
        }

        $query = '
            mutation ($id: ID!) {
                discountAutomaticActivate(id: $id) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, $payloadId);
    }

    /**
     * @throws InvalidInput
     */
    public function discountAutomaticDeactivate(string $discountId, ?string $queryFields = null): array
    {
        $payloadId = ValidationHelper::validate(['id' => $discountId], IsAutomaticDiscountId::class);

        if (empty($queryFields)) {
            $queryFields = 'automaticDiscountNode { '
                . $this->getAutomaticDiscountQueryFields() .
            ' }';
        }

        $query = '
            mutation ($id: ID!) {
                discountAutomaticDeactivate(id: $id) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, $payloadId);
    }

    /**
     * @throws InvalidInput
     */
    public function removeDiscountAutomatic(string $discountId): array
    {
        $payloadId = ValidationHelper::validate(['id' => $discountId], IsAutomaticDiscountId::class);

        $query = '
            mutation ($id: ID!) {
                discountAutomaticDelete(id: $id) {
                    deletedAutomaticDiscountId
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, $payloadId);
    }

    /**
     * @throws InvalidInput
     */
    public function discountAutomaticBulkRemove(array $params, ?string $queryFields = null): array
    {
        $payload = ValidationHelper::validate($params, AutomaticBulkValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'job { id done }';
        }

        $query = '
            mutation ($ids: [ID!], $search: String, $savedSearchId: ID) {
                discountAutomaticBulkDelete(ids: $ids, search: $search, savedSearchId: $savedSearchId) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, $payload);
    }
    #endregion

    #region Discount Code
    private function getCodeDiscountQueryFields(): string
    {
        return 'id codeDiscount{
            ... on DiscountCodeFreeShipping { title status startsAt endsAt codes(first: 10){ nodes { id code asyncUsageCount } } }
            ... on DiscountCodeBxgy { title status startsAt endsAt codes(first: 10){ nodes { id code asyncUsageCount } } }
            ... on DiscountCodeBasic { title status startsAt endsAt codes(first: 10){ nodes { id code asyncUsageCount } } }
            ... on DiscountCodeApp { title status startsAt endsAt codes(first: 10){ nodes { id code asyncUsageCount } } }
            __typename
        }';
    }

    /**
     * @throws InvalidInput
     */
    public function getDiscountCodes(array $params = [], ?string $queryFields = null): array
    {
        $payload = ValidationHelper::validate($params, CodeQueryValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'edges { node { '
                . $this->getCodeDiscountQueryFields() .
                ' } }';
        }

        $query = '
            query ($after: String, $before: String, $first: Int, $last: Int, $query: String, $reverse: Boolean, $savedSearchId: ID, $sortKey: CodeDiscountSortKeys)  {
                codeDiscountNodes(after: $after, before: $before, first: $first, last: $last, query: $query, reverse: $reverse, savedSearchId: $savedSearchId, sortKey: $sortKey) {
                    ' . $queryFields . '
                    pageInfo{
                        endCursor
                        hasNextPage
                        startCursor
                        hasPreviousPage
                    }
                }
            }';

        return $this->graphClient->request($query, $payload);
    }

    public function countDiscountCode(string $query = null): array
    {
        return $this->graphClient->request(
            'query discountCodeCount($query: String) {
                discountCodeCount(query: $query)
            }',
            [
                'query' => $query
            ]
        );
    }

    /**
     * @throws InvalidInput
     */
    public function getDiscountCodeSavedSearches(array $params = [], ?string $queryFields = null): array
    {
        $payload = ValidationHelper::validate($params, CodeQuerySavedSearchValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'edges { node { id name query resourceType } }';
        }

        $query = '
            query ($after: String, $before: String, $first: Int, $last: Int, $reverse: Boolean)  {
                codeDiscountSavedSearches(after: $after, before: $before, first: $first, last: $last, reverse: $reverse) {
                    ' . $queryFields . '
                    pageInfo{
                        endCursor
                        hasNextPage
                        startCursor
                        hasPreviousPage
                    }
                }
            }';

        return $this->graphClient->request($query, $payload);
    }

    /**
     * @throws InvalidInput
     */
    public function getDiscountCodeById(string $discountId, ?string $queryFields = null): array
    {
        $payloadId = ValidationHelper::validate(['id' => $discountId], IsCodeDiscountId::class);

        if (empty($queryFields)) {
            $queryFields = $this->getCodeDiscountQueryFields();
        }

        $query = '
            query ($id: ID!) {
                codeDiscountNode(id: $id) {
                    ' . $queryFields . '
                }
            }';

        return $this->graphClient->request($query, $payloadId);
    }

    public function getDiscountCodeByCode(string $code, ?string $queryFields = null): array
    {
        if (empty($queryFields)) {
            $queryFields = $this->getCodeDiscountQueryFields();
        }

        $query = '
            query ($code: String!) {
                codeDiscountNodeByCode(code: $code) {
                    ' . $queryFields . '
                }
            }';

        return $this->graphClient->request($query, ['code' => $code]);
    }

    /**
     * @throws InvalidInput
     */
    public function createDiscountCodeApp(DiscountCode $discountCode, ?string $queryFields = null): array
    {
        $payload = ValidationHelper::validate($discountCode->bindingGraphData(), CodeAppInputValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'codeAppDiscount { discountId startsAt endsAt status }';
        }

        $query = '
            mutation ($codeAppDiscount: DiscountCodeAppInput!) {
                discountCodeAppCreate(codeAppDiscount: $codeAppDiscount) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, ['codeAppDiscount' => $payload]);
    }

    /**
     * @throws InvalidInput
     */
    public function updateDiscountCodeApp(string $discountId, DiscountCode $discountCode, ?string $queryFields = null): array
    {
        ValidationHelper::validate(['id' => $discountId], IsCodeDiscountId::class);

        $payload = ValidationHelper::validate($discountCode->bindingGraphData(), CodeAppInputValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'codeAppDiscount { discountId startsAt endsAt status }';
        }

        $query = '
            mutation ($codeAppDiscount: DiscountCodeAppInput!, $id: ID!) {
                discountCodeAppUpdate(codeAppDiscount: $codeAppDiscount, id: $id) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, [
            'id' => $discountId,
            'codeAppDiscount' => $payload
        ]);
    }

    /**
     * @throws InvalidInput
     */
    public function createDiscountCodeBasic(DiscountCode $discountCode, ?string $queryFields = null): array
    {
        $payload = ValidationHelper::validate($discountCode->bindingGraphData(), CodeBasicInputValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'codeDiscountNode { id codeDiscount{ 
                ... on DiscountCodeBasic { title status startsAt endsAt } 
            } }';
        }

        $query = '
            mutation ($basicCodeDiscount: DiscountCodeBasicInput!) {
                discountCodeBasicCreate(basicCodeDiscount: $basicCodeDiscount) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, ['basicCodeDiscount' => $payload]);
    }

    /**
     * @throws InvalidInput
     */
    public function updateDiscountCodeBasic(string $discountId, DiscountCode $discountCode, ?string $queryFields = null): array
    {
        ValidationHelper::validate(['id' => $discountId], IsCodeDiscountId::class);

        $payload = ValidationHelper::validate($discountCode->bindingGraphData(), CodeBasicInputValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'codeDiscountNode { id codeDiscount{ 
                ... on DiscountCodeBasic { title status startsAt endsAt }
            } }';
        }

        $query = '
            mutation ($basicCodeDiscount: DiscountCodeBasicInput!, $id: ID!) {
                discountCodeBasicUpdate(basicCodeDiscount: $basicCodeDiscount, id: $id) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, [
            'id' => $discountId,
            'basicCodeDiscount' => $payload
        ]);
    }

    /**
     * @throws InvalidInput
     */
    public function createDiscountCodeBxgy(DiscountCode $discountCode, ?string $queryFields = null): array
    {
        $payload = ValidationHelper::validate($discountCode->bindingGraphData(), CodeBxgyInputValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'codeDiscountNode { id codeDiscount{ 
                ... on DiscountCodeBxgy { title status startsAt endsAt } 
            } }';
        }

        $query = '
            mutation ($bxgyCodeDiscount: DiscountCodeBxgyInput!) {
                discountCodeBxgyCreate(bxgyCodeDiscount: $bxgyCodeDiscount) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, ['bxgyCodeDiscount' => $payload]);
    }

    /**
     * @throws InvalidInput
     */
    public function updateDiscountCodeBxgy(string $discountId, DiscountCode $discountCode, ?string $queryFields = null): array
    {
        ValidationHelper::validate(['id' => $discountId], IsCodeDiscountId::class);

        $payload = ValidationHelper::validate($discountCode->bindingGraphData(), CodeBxgyInputValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'codeDiscountNode { id codeDiscount{ 
                ... on DiscountCodeBxgy { title status startsAt endsAt }
            } }';
        }

        $query = '
            mutation ($bxgyCodeDiscount: DiscountCodeBxgyInput!, $id: ID!) {
                discountCodeBxgyUpdate(bxgyCodeDiscount: $bxgyCodeDiscount, id: $id) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, [
            'id' => $discountId,
            'bxgyCodeDiscount' => $payload
        ]);
    }

    /**
     * @throws InvalidInput
     */
    public function createDiscountCodeFreeShipping(DiscountCode $discountCode, ?string $queryFields = null): array
    {
        $payload = ValidationHelper::validate($discountCode->bindingGraphData(), CodeFreeShippingInputValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'codeDiscountNode { id codeDiscount{ 
                ... on DiscountCodeFreeShipping { title status startsAt endsAt }
            } }';
        }

        $query = '
            mutation ($freeShippingCodeDiscount: DiscountCodeFreeShippingInput!) {
                discountCodeFreeShippingCreate(freeShippingCodeDiscount: $freeShippingCodeDiscount) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, ['freeShippingCodeDiscount' => $payload]);
    }

    /**
     * @throws InvalidInput
     */
    public function updateDiscountCodeFreeShipping(string $discountId, DiscountCode $discountCode, ?string $queryFields = null): array
    {
        ValidationHelper::validate(['id' => $discountId], IsCodeDiscountId::class);

        $payload = ValidationHelper::validate($discountCode->bindingGraphData(), CodeFreeShippingInputValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'codeDiscountNode { id codeDiscount { 
                ... on DiscountCodeFreeShipping { title status startsAt endsAt }
            } }';
        }

        $query = '
            mutation ($freeShippingCodeDiscount: DiscountCodeFreeShippingInput!, $id: ID!) {
                discountCodeFreeShippingUpdate(freeShippingCodeDiscount: $freeShippingCodeDiscount, id: $id) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, [
            'id' => $discountId,
            'freeShippingCodeDiscount' => $payload
        ]);
    }

    /**
     * @throws InvalidInput
     */
    public function discountCodeActivate(string $discountId, ?string $queryFields = null): array
    {
        $payloadId = ValidationHelper::validate(['id' => $discountId], IsCodeDiscountId::class);

        if (empty($queryFields)) {
            $queryFields = 'codeDiscountNode { '
                . $this->getCodeDiscountQueryFields() .
                ' }';
        }

        $query = '
            mutation ($id: ID!) {
                discountCodeActivate(id: $id) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, $payloadId);
    }

    /**
     * @throws InvalidInput
     */
    public function discountCodeDeactivate(string $discountId, ?string $queryFields = null): array
    {
        $payloadId = ValidationHelper::validate(['id' => $discountId], IsCodeDiscountId::class);

        if (empty($queryFields)) {
            $queryFields = 'codeDiscountNode { '
                . $this->getCodeDiscountQueryFields() .
                ' }';
        }

        $query = '
            mutation ($id: ID!) {
                discountCodeDeactivate(id: $id) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, $payloadId);
    }

    /**
     * @throws InvalidInput
     */
    public function removeDiscountCode(string $discountId): array
    {
        $payloadId = ValidationHelper::validate(['id' => $discountId], IsCodeDiscountId::class);

        $query = '
            mutation ($id: ID!) {
                discountCodeDelete(id: $id) {
                    deletedCodeDiscountId
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, $payloadId);
    }

    /**
     * @throws InvalidInput
     */
    public function discountCodeBulkActivate(array $params, ?string $queryFields = null): array
    {
        $payload = ValidationHelper::validate($params, CodeBulkValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'job { id done }';
        }

        $query = '
            mutation ($ids: [ID!], $search: String, $savedSearchId: ID) {
                discountCodeBulkActivate(ids: $ids, search: $search, savedSearchId: $savedSearchId) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, $payload);
    }

    /**
     * @throws InvalidInput
     */
    public function discountCodeBulkDeactivate(array $params, ?string $queryFields = null): array
    {
        $payload = ValidationHelper::validate($params, CodeBulkValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'job { id done }';
        }

        $query = '
            mutation ($ids: [ID!], $search: String, $savedSearchId: ID) {
                discountCodeBulkDeactivate(ids: $ids, search: $search, savedSearchId: $savedSearchId) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, $payload);
    }

    /**
     * @throws InvalidInput
     */
    public function discountCodeBulkRemove(array $params, ?string $queryFields = null): array
    {
        $payload = ValidationHelper::validate($params, CodeBulkValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'job { id done }';
        }

        $query = '
            mutation ($ids: [ID!], $search: String, $savedSearchId: ID) {
                discountCodeBulkDelete(ids: $ids, search: $search, savedSearchId: $savedSearchId) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, $payload);
    }

    /**
     * @throws InvalidInput
     */
    public function getRedeemCodeBulkCreationById(string $discountRedeemCodeBulkCreationId, ?string $queryFields = null)
    {
        $payloadId = ValidationHelper::validate(['id' => $discountRedeemCodeBulkCreationId], IsDiscountRedeemCodeBulkCreationId::class);

        if (empty($queryFields)) {
            $queryFields = 'id done codesCount';
        }

        $query = '
            query ($id: ID!) {
                discountRedeemCodeBulkCreation(id: $id) {
                    ' . $queryFields . '
                }
            }';

        return $this->graphClient->request($query, $payloadId);
    }

    /**
     * @throws InvalidInput
     */
    public function getRedeemCodeBulkCreationSavedSearch(array $params, ?string $queryFields = null)
    {
        $payload = ValidationHelper::validate($params, CodeQuerySavedSearchValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'edges { node { id name query resourceType } }';
        }

        $query = '
            query ($after: String, $before: String, $first: Int, $last: Int, $reverse: Boolean, $query: String, $sortKey: DiscountCodeSortKeys)  {
                discountRedeemCodeSavedSearches(after: $after, before: $before, first: $first, last: $last, reverse: $reverse, query: $query, sortKey: $sortKey) {
                    ' . $queryFields . '
                    pageInfo{
                        endCursor
                        hasNextPage
                        startCursor
                        hasPreviousPage
                    }
                }
            }';

        return $this->graphClient->request($query, $payload);
    }

    /**
     * @throws InvalidInput
     */
    public function bulkCreateRedeemCode(string $discountId, array $codes, ?string $queryFields = null)
    {
        ValidationHelper::validate(['id' => $discountId], IsCodeDiscountId::class);

        $payload = ValidationHelper::validate([
            'codes' => $codes
        ], CodeBulkRedeemCreateValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'bulkCreation { id done }';
        }

        $query = '
            mutation ($discountId: ID!, $codes: [DiscountRedeemCodeInput!]!) {
                discountRedeemCodeBulkAdd(discountId: $discountId, codes: $codes) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, [
            'discountId' => $discountId,
            'codes' => $payload['codes']
        ]);
    }

    /**
     * @throws InvalidInput
     */
    public function bulkRemoveRedeemCode(array $params, ?string $queryFields = null)
    {
        $payload = ValidationHelper::validate($params, CodeBulkRedeemRemoveValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'job { id done }';
        }

        $query = '
            mutation ($discountId: ID!, $search: String, $savedSearchId: ID, $ids: [ID!]) {
                discountCodeRedeemCodeBulkDelete(discountId: $discountId, search: $search, savedSearchId: $savedSearchId, ids: $ids) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, $payload);
    }
    #endregion

    #region Get
    /**
     * @throws InvalidInput
     */
    public function getDiscounts(array $params = [], ?string $queryFields = null)
    {
        $payload = ValidationHelper::validate($params, QueryValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'edges { node { '
                . $this->getDiscountQueryFields() .
                ' } }';
        }

        $query = '
            query ($after: String, $before: String, $first: Int, $last: Int, $reverse: Boolean, $query: String, $savedSearchId: ID, $sortKey: DiscountSortKeys) {
                discountNodes(first: $first, last: $last, after: $after, before: $before, reverse: $reverse, query: $query, savedSearchId: $savedSearchId, sortKey: $sortKey) {
                    ' . $queryFields . '
                }
            }';

        return $this->graphClient->request($query, $payload);
    }

    /**
     * @throws InvalidInput
     * @throws ServiceNotSupported
     */
    public function get(array $params = [], ?string $queryFields = null)
    {
        switch ($this->service) {
            case DiscountService::ALL:
                return $this->getDiscounts($params, $queryFields);
            case DiscountService::AUTOMATIC:
                return $this->getDiscountAutomatics($params, $queryFields);
            case DiscountService::CODE:
                return $this->getDiscountCodes($params, $queryFields);
            default:
                throw new ServiceNotSupported($this->service);
        }
    }
    #endregion

    /**
     * @throws InvalidInput
     * @throws ServiceNotSupported
     */
    public function getSavedSearches(array $params = [], ?string $queryFields = null): array
    {
        switch($this->service) {
            case DiscountService::CODE:
                return $this->getDiscountCodeSavedSearches($params, $queryFields);
            case DiscountService::AUTOMATIC:
                return $this->getDiscountAutomaticSavedSearches($params, $queryFields);
            default:
                throw new ServiceNotSupported('Invalid discount service');
        }
    }

    #region Get By Id
    /**
     * @throws InvalidInput
     */
    private function getDiscountById(string $discountId, ?string $queryFields = null)
    {
        $payloadId = ValidationHelper::validate(['id' => $discountId], IsDiscountId::class);

        if (empty($queryFields)) {
            $queryFields = $this->getDiscountQueryFields();
        }

        $query = '
            query ($id: ID!) {
                discountNode(id: $id) {
                    ' . $queryFields . '
                }
            }';

        return $this->graphClient->request($query, $payloadId);
    }

    /**
     * @throws InvalidInput
     * @throws ServiceNotSupported
     */
    public function getById(string $discountId, ?string $queryFields = null)
    {
        switch ($this->service) {
            case DiscountService::ALL:
                return $this->getDiscountById($discountId, $queryFields);
            case DiscountService::AUTOMATIC:
                return $this->getDiscountAutomaticById($discountId, $queryFields);
            case DiscountService::CODE:
                return $this->getDiscountCodeById($discountId, $queryFields);
            default:
                throw new ServiceNotSupported($this->service);
        }
    }
    #endregion

    /**
     * @throws InvalidInput
     * @throws ServiceNotSupported
     */
    public function createApp(DiscountAutomatic|DiscountCode|null $discount, ?string $queryFields = null)
    {
        switch($this->service) {
            case DiscountService::CODE:
                if (!($discount instanceof DiscountCode)) {
                    throw new InvalidInput('Invalid discount object');
                }
                return $this->createDiscountCodeApp($discount, $queryFields);
            case DiscountService::AUTOMATIC:
                if (!($discount instanceof DiscountAutomatic)) {
                    throw new InvalidInput('Invalid discount object');
                }
                return $this->createDiscountAutomaticApp($discount, $queryFields);
            default:
                throw new ServiceNotSupported('Invalid discount service');
        }
    }

    /**
     * @throws InvalidInput
     * @throws ServiceNotSupported
     */
    public function createBasic(DiscountAutomatic|DiscountCode|null $discount, ?string $queryFields = null)
    {
        switch($this->service) {
            case DiscountService::CODE:
                if (!($discount instanceof DiscountCode)) {
                    throw new InvalidInput('Invalid discount object');
                }
                return $this->createDiscountCodeBasic($discount, $queryFields);
            case DiscountService::AUTOMATIC:
                if (!($discount instanceof DiscountAutomatic)) {
                    throw new InvalidInput('Invalid discount object');
                }
                return $this->createDiscountAutomaticBasic($discount, $queryFields);
            default:
                throw new ServiceNotSupported('Invalid discount service');
        }
    }

    /**
     * @throws InvalidInput
     * @throws ServiceNotSupported
     */
    public function createBxgy(DiscountAutomatic|DiscountCode|null $discount, ?string $queryFields = null)
    {
        switch($this->service) {
            case DiscountService::CODE:
                if (!($discount instanceof DiscountCode)) {
                    throw new InvalidInput('Invalid discount object');
                }
                return $this->createDiscountCodeBxgy($discount, $queryFields);
            case DiscountService::AUTOMATIC:
                if (!($discount instanceof DiscountAutomatic)) {
                    throw new InvalidInput('Invalid discount object');
                }
                return $this->createDiscountAutomaticBxgy($discount, $queryFields);
            default:
                throw new ServiceNotSupported('Invalid discount service');
        }
    }

    /**
     * @throws InvalidInput
     * @throws ServiceNotSupported
     */
    public function createFreeShipping(DiscountAutomatic|DiscountCode|null $discount, ?string $queryFields = null)
    {
        switch($this->service) {
            case DiscountService::CODE:
                if (!($discount instanceof DiscountCode)) {
                    throw new InvalidInput('Invalid discount object');
                }
                return $this->createDiscountCodeFreeShipping($discount, $queryFields);
            case DiscountService::AUTOMATIC:
                if (!($discount instanceof DiscountAutomatic)) {
                    throw new InvalidInput('Invalid discount object');
                }
                return $this->createDiscountAutomaticFreeShipping($discount, $queryFields);
            default:
                throw new ServiceNotSupported('Invalid discount service');
        }
    }

    /**
     * @throws InvalidInput
     * @throws ServiceNotSupported
     */
    public function updateApp(string $discountId, DiscountAutomatic|DiscountCode|null $discount, ?string $queryFields = null)
    {
        switch($this->service) {
            case DiscountService::CODE:
                if (!($discount instanceof DiscountCode)) {
                    throw new InvalidInput('Invalid discount object');
                }
                return $this->updateDiscountCodeApp($discountId, $discount, $queryFields);
            case DiscountService::AUTOMATIC:
                if (!($discount instanceof DiscountAutomatic)) {
                    throw new InvalidInput('Invalid discount object');
                }
                return $this->updateDiscountAutomaticApp($discountId, $discount, $queryFields);
            default:
                throw new ServiceNotSupported('Invalid discount service');
        }
    }

    /**
     * @throws InvalidInput
     * @throws ServiceNotSupported
     */
    public function updateBasic(string $discountId, DiscountAutomatic|DiscountCode|null $discount, ?string $queryFields = null)
    {
        switch($this->service) {
            case DiscountService::CODE:
                if (!($discount instanceof DiscountCode)) {
                    throw new InvalidInput('Invalid discount object');
                }
                return $this->updateDiscountCodeBasic($discountId, $discount, $queryFields);
            case DiscountService::AUTOMATIC:
                if (!($discount instanceof DiscountAutomatic)) {
                    throw new InvalidInput('Invalid discount object');
                }
                return $this->updateDiscountAutomaticBasic($discountId, $discount, $queryFields);
            default:
                throw new ServiceNotSupported('Invalid discount service');
        }
    }

    /**
     * @throws InvalidInput
     * @throws ServiceNotSupported
     */
    public function updateBxgy(string $discountId, DiscountAutomatic|DiscountCode|null $discount, ?string $queryFields = null)
    {
        switch($this->service) {
            case DiscountService::CODE:
                if (!($discount instanceof DiscountCode)) {
                    throw new InvalidInput('Invalid discount object');
                }
                return $this->updateDiscountCodeBxgy($discountId, $discount, $queryFields);
            case DiscountService::AUTOMATIC:
                if (!($discount instanceof DiscountAutomatic)) {
                    throw new InvalidInput('Invalid discount object');
                }
                return $this->updateDiscountAutomaticBxgy($discountId, $discount, $queryFields);
            default:
                throw new ServiceNotSupported('Invalid discount service');
        }
    }

    /**
     * @throws InvalidInput
     * @throws ServiceNotSupported
     */
    public function updateFreeShipping(string $discountId, DiscountAutomatic|DiscountCode|null $discount, ?string $queryFields = null)
    {
        switch($this->service) {
            case DiscountService::CODE:
                if (!($discount instanceof DiscountCode)) {
                    throw new InvalidInput('Invalid discount object');
                }
                return $this->updateDiscountCodeFreeShipping($discountId, $discount, $queryFields);
            case DiscountService::AUTOMATIC:
                if (!($discount instanceof DiscountAutomatic)) {
                    throw new InvalidInput('Invalid discount object');
                }
                return $this->updateDiscountAutomaticFreeShipping($discountId, $discount, $queryFields);
            default:
                throw new ServiceNotSupported('Invalid discount service');
        }
    }

    /**
     * @throws InvalidInput
     * @throws ServiceNotSupported
     */
    public function activate(string $discountId, ?string $queryFields = null): array
    {
        switch($this->service) {
            case DiscountService::CODE:
                return $this->discountCodeActivate($discountId, $queryFields);
            case DiscountService::AUTOMATIC:
                return $this->discountAutomaticActivate($discountId, $queryFields);
            default:
                throw new ServiceNotSupported('Invalid discount service');
        }
    }

    /**
     * @throws InvalidInput
     * @throws ServiceNotSupported
     */
    public function deactivate(string $discountId, ?string $queryFields = null): array
    {
        switch($this->service) {
            case DiscountService::CODE:
                return $this->discountCodeDeactivate($discountId, $queryFields);
            case DiscountService::AUTOMATIC:
                return $this->discountAutomaticDeactivate($discountId, $queryFields);
            default:
                throw new ServiceNotSupported('Invalid discount service');
        }
    }

    /**
     * @throws InvalidInput
     * @throws ServiceNotSupported
     */
    public function remove(string $discountId, ?string $queryFields = null): array
    {
        switch($this->service) {
            case DiscountService::CODE:
                return $this->removeDiscountCode($discountId, $queryFields);
            case DiscountService::AUTOMATIC:
                return $this->removeDiscountAutomatic($discountId, $queryFields);
            default:
                throw new ServiceNotSupported('Invalid discount service');
        }
    }

    /**
     * @throws InvalidInput
     * @throws ServiceNotSupported
     */
    public function bulkRemove(array $params, ?string $queryFields = null): array
    {
        switch($this->service) {
            case DiscountService::CODE:
                return $this->discountCodeBulkRemove($params, $queryFields);
            case DiscountService::AUTOMATIC:
                return $this->discountAutomaticBulkRemove($params, $queryFields);
            default:
                throw new ServiceNotSupported('Invalid discount service');
        }
    }
}
