<?php

namespace Thainph\ShopifySdk\Services\Graphql;

use Thainph\ShopifySdk\Services\BaseService;
use Thainph\ShopifySdk\Helpers\ValidationHelper;
use Thainph\ShopifySdk\Clients\Graphql\GraphClient;
use Thainph\ShopifySdk\Validation\Validators\Graphql\StagedUpload\CreateValidator;
use Thainph\ShopifySdk\Validation\Validators\Graphql\StagedUpload\IsArrayOfStagedUpload;

class GraphStagedUploadService extends BaseService
{
    protected GraphClient $graphClient;

    public function __construct(GraphClient $graphClient)
    {
        $this->graphClient = $graphClient;
    }

    public function create(array $params = [], ?string $queryFields = null)
    {
        $data = ValidationHelper::validate([
            'stagedUpload' => $params
        ], IsArrayOfStagedUpload::class);

        $payload = ValidationHelper::validate($data, CreateValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'stagedTargets { url resourceUrl parameters{ name value } }';
        }

        $query = '
            mutation stagedUploadsCreate($input: [StagedUploadInput!]!) {
                stagedUploadsCreate(input: $input) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, [
            'input' => $payload['stagedUpload']
        ]);
    }
}
