<?php

namespace Thainph\ShopifySdk\Services\Graphql;

use Thainph\ShopifySdk\Services\BaseService;
use Thainph\ShopifySdk\Exceptions\InvalidInput;
use Thainph\ShopifySdk\Helpers\ValidationHelper;
use Thainph\ShopifySdk\Mockup\Graphql\DraftOrder;
use Thainph\ShopifySdk\Mockup\Graphql\EmailInput;
use Thainph\ShopifySdk\Clients\Graphql\GraphClient;
use Thainph\ShopifySdk\Validation\Validators\Graphql\Order\IsOrderId;
use Thainph\ShopifySdk\Validation\Validators\Graphql\DraftOrder\BulkValidator;
use Thainph\ShopifySdk\Validation\Validators\Graphql\DraftOrder\InputValidator;
use Thainph\ShopifySdk\Validation\Validators\Graphql\DraftOrder\IsDraftOrderId;
use Thainph\ShopifySdk\Validation\Validators\Graphql\DraftOrder\QueryValidator;
use Thainph\ShopifySdk\Validation\Validators\Graphql\DraftOrder\BulkTagValidator;
use Thainph\ShopifySdk\Validation\Validators\Graphql\DraftOrder\CompleteValidator;
use Thainph\ShopifySdk\Validation\Validators\Graphql\DraftOrder\SendInvoiceValidator;

class GraphDraftOrderService extends BaseService
{
    protected GraphClient $graphClient;

    public function __construct(GraphClient $graphClient)
    {
        $this->graphClient = $graphClient;
    }

    /**
     * @throws InvalidInput
     */
    public function get(array $params = [], ?string $queryFields = null)
    {
        $payload = ValidationHelper::validate($params, QueryValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'edges { node { id email totalPrice status } }';
        }

        $query = '
            query ($after: String, $before: String, $first: Int, $last: Int, $query: String, $reverse: Boolean, $sortKey: DraftOrderSortKeys, $savedSearchId: ID)  {
                draftOrders(after: $after, before: $before, first: $first, last: $last, query: $query, reverse: $reverse, sortKey: $sortKey, savedSearchId: $savedSearchId) {
                    ' . $queryFields . '
                    pageInfo{
                        endCursor
                        hasNextPage
                        startCursor
                        hasPreviousPage
                    }
                }
            }';

        return $this->graphClient->request($query, $payload);
    }

    /**
     * @throws InvalidInput
     */
    public function getById(string $draftOrderId, ?string $queryFields = null)
    {
        $payloadId = ValidationHelper::validate(['id' => $draftOrderId], IsDraftOrderId::class);

        $query = '
            query ($id: ID!) {
                draftOrder(id: $id) {
                    ' . ($queryFields ?? 'id email totalPrice status') . '
                }
            }';

        return $this->graphClient->request($query, $payloadId);
    }

    /**
     * @throws InvalidInput
     */
    public function create(DraftOrder $draftOrder, ?string $queryFields = null)
    {
        $payload = ValidationHelper::validate($draftOrder->bindingGraphData(), InputValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'draftOrder { id email totalPrice status }';
        }

        $query = '
            mutation ($input: DraftOrderInput!) {
                draftOrderCreate(input: $input) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, ['input' => $payload]);
    }

    /**
     * @throws InvalidInput
     */
    public function duplicate(string $draftOrderId, ?string $queryFields = null)
    {
        $payloadId = ValidationHelper::validate(['id' => $draftOrderId], IsDraftOrderId::class);

        if (empty($queryFields)) {
            $queryFields = 'draftOrder { id email totalPrice status }';
        }

        $query = '
            mutation ($id: ID!) {
                draftOrderDuplicate(id: $id) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, $payloadId);
    }

    /**
     * @throws InvalidInput
     */
    public function createFromOrder(string $orderId, ?string $queryFields = null)
    {
        $payloadId = ValidationHelper::validate(['id' => $orderId], IsOrderId::class);

        if (empty($queryFields)) {
            $queryFields = 'draftOrder { id email totalPrice status }';
        }

        $query = '
            mutation ($id: ID!) {
                draftOrderCreateFromOrder(orderId: $id) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, $payloadId);
    }

    /**
     * @throws InvalidInput
     */
    public function calculate(DraftOrder $draftOrder, ?string $queryFields = null)
    {
        $payload = ValidationHelper::validate($draftOrder->bindingGraphData(), InputValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'calculatedDraftOrder { lineItems {custom name quantity appliedDiscount { value } originalTotal { amount currencyCode } } }';
        }

        $query = '
            mutation ($input: DraftOrderInput!) {
                draftOrderCalculate(input: $input) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, ['input' => $payload]);
    }

    /**
     * @throws InvalidInput
     */
    public function invoiceSend(string $draftOrderId, EmailInput $email, ?string $queryFields = null)
    {
        ValidationHelper::validate(['id' => $draftOrderId], IsDraftOrderId::class);

        $payload = ValidationHelper::validate($email->bindingGraphData(), SendInvoiceValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'draftOrder { id email totalPrice status }';
        }

        $query = '
            mutation ($id: ID!, $email: EmailInput) {
                draftOrderInvoiceSend(id: $id, email: $email) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, ['id' => $draftOrderId, 'email' => $payload]);
    }

    /**
     * @throws InvalidInput
     */
    public function invoicePreview(string $draftOrderId, EmailInput $email, ?string $queryFields = null)
    {
        ValidationHelper::validate(['id' => $draftOrderId], IsDraftOrderId::class);

        $payload = ValidationHelper::validate($email->bindingGraphData(), SendInvoiceValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'previewHtml previewSubject';
        }

        $query = '
            mutation ($id: ID!, $email: EmailInput) {
                draftOrderInvoicePreview(id: $id, email: $email) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, ['id' => $draftOrderId, 'email' => $payload]);
    }

    /**
     * @throws InvalidInput
     */
    public function update(string $draftOrderId, DraftOrder $draftOrder, ?string $queryFields = null)
    {
        ValidationHelper::validate(['id' => $draftOrderId], IsDraftOrderId::class);

        $payload = ValidationHelper::validate($draftOrder->bindingGraphData(), InputValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'draftOrder { id email totalPrice status }';
        }

        $query = '
            mutation ($id: ID!, $input: DraftOrderInput!) {
                draftOrderUpdate(id: $id, input: $input) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, [
            'id' => $draftOrderId,
            'input' => $payload
        ]);
    }

    /**
     * @throws InvalidInput
     */
    public function complete(string $draftOrderId, DraftOrder $draftOrder, ?string $queryFields = null)
    {
        $payloadId = ValidationHelper::validate(['id' => $draftOrderId], IsDraftOrderId::class);

        $payload = ValidationHelper::validate($draftOrder->bindingGraphData(), CompleteValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'draftOrder { id email totalPrice status }';
        }

        $query = '
            mutation ($id: ID!, $paymentGatewayId: ID, $paymentPending: Boolean, $sourceName: String) {
                draftOrderComplete(id: $id, paymentGatewayId: $paymentGatewayId, paymentPending: $paymentPending, sourceName: $sourceName) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, array_merge(
            $payloadId,
            $payload
        ));
    }

    /**
     * @throws InvalidInput
     */
    public function remove(string $draftOrderId)
    {
        $payloadId = ValidationHelper::validate(['id' => $draftOrderId], IsDraftOrderId::class);

        $query = '
            mutation draftOrderDelete($input: DraftOrderDeleteInput!) {
                draftOrderDelete(input: $input) {
                    deletedId
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, [
            'input' => $payloadId
        ]);
    }


    /**
     * @throws InvalidInput
     */
    public function bulkAddTags(array $data, ?string $queryFields = null)
    {
        $payload = ValidationHelper::validate($data, BulkTagValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'job { id done }';
        }

        $query = '
            mutation draftOrderBulkAddTags($tags: [String!]!, $ids: [ID!], $search: String, $savedSearchId: ID) {
                draftOrderBulkAddTags(tags: $tags, ids: $ids, search: $search, savedSearchId: $savedSearchId) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, $payload);
    }

    /**
     * @throws InvalidInput
     */
    public function bulkRemoveTags(array $data, ?string $queryFields = null)
    {
        $payload = ValidationHelper::validate($data, BulkTagValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'job { id done }';
        }

        $query = '
            mutation draftOrderBulkRemoveTags($tags: [String!]!, $ids: [ID!], $search: String, $savedSearchId: ID) {
                draftOrderBulkRemoveTags(tags: $tags, ids: $ids, search: $search, savedSearchId: $savedSearchId) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, $payload);
    }

    /**
     * @throws InvalidInput
     */
    public function bulkRemove(array $data, ?string $queryFields = null)
    {
        $payload = ValidationHelper::validate($data, BulkValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'job { id done }';
        }

        $query = '
            mutation draftOrderBulkDelete($ids: [ID!], $search: String, $savedSearchId: ID) {
                draftOrderBulkDelete(ids: $ids, search: $search, savedSearchId: $savedSearchId) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, $payload);
    }
}
