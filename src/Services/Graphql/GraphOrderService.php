<?php

namespace Thainph\ShopifySdk\Services\Graphql;

use Thainph\ShopifySdk\Services\BaseService;
use Thainph\ShopifySdk\Helpers\ValidationHelper;
use Thainph\ShopifySdk\Mockup\Graphql\Transaction;
use Thainph\ShopifySdk\Clients\Graphql\GraphClient;
use Thainph\ShopifySdk\Validation\Validators\Graphql\Order\IsOrderId;
use Thainph\ShopifySdk\Validation\Validators\Graphql\Order\IsAnyOrderId;
use Thainph\ShopifySdk\Validation\Validators\Graphql\Variant\IsVariantId;
use Thainph\ShopifySdk\Validation\Validators\Graphql\Order\CaptureValidator;
use Thainph\ShopifySdk\Validation\Validators\Graphql\Order\IsCalculatedOrderId;

class GraphOrderService extends BaseService
{
    protected GraphClient $graphClient;

    public function __construct(GraphClient $graphClient)
    {
        $this->graphClient = $graphClient;
    }

    public function getById($id, ?string $queryFields = null)
    {
        ValidationHelper::validate(['id' => $id], IsOrderId::class);

        if (empty($queryFields)) {
            $queryFields = implode(' ', [
                'id',
                'name',
                'createdAt'
            ]);
        }

        $query = <<<GQL
            query {
                order(id: "$id") {
                    $queryFields
                }
            }
            GQL;

        return $this->graphClient->request($query, null);
    }

    public function mutationBeginEdit($id, ?string $queryFields = null)
    {
        ValidationHelper::validate(['id' => $id], IsOrderId::class);

        if (empty($queryFields)) {
            $queryFields = <<<GQL
                calculatedOrder {
                    id
                }
            GQL;
        }

        $query = <<<GQL
            mutation beginEdit {
                orderEditBegin(id: "$id"){
                    $queryFields
                    userErrors {
                        field
                        message
                    }
                }
            }
        GQL;

        return $this->graphClient->request($query, null);
    }
    public function mutationCommitEdit($id, ?string $queryFields = null)
    {
        ValidationHelper::validate(['id' => $id], IsCalculatedOrderId::class);

        if (empty($queryFields)) {
            $queryFields = <<<GQL
                order {
                    id
                }
            GQL;
        }

        $query = <<<GQL
            mutation commitEdit {
                orderEditCommit(
                    id: "$id",
                    notifyCustomer: false,
                    staffNote: "決済手数料を追加"
                ) {
                    $queryFields
                    userErrors {
                        field
                        message
                    }
                }
            }
        GQL;

        return $this->graphClient->request($query, null);
    }
    public function mutationOrderMarkAsPaid($id, ?string $queryFields = null)
    {
        ValidationHelper::validate(['id' => $id], IsOrderId::class);

        if (empty($queryFields)) {
            $queryFields = <<<GQL
                order {
                    id
                }
            GQL;
        }

        $query = '
            mutation orderMarkAsPaid($input: OrderMarkAsPaidInput!) {
                orderMarkAsPaid(input: $input) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, [
            'input' => [
                'id' => $id
            ]
        ]);
    }
    public function mutationAddVariantToOrder(string $orderId, string $variantId, int $quantity = 1, ?string $queryFields = null)
    {
        ValidationHelper::validate(['id' => $orderId], IsAnyOrderId::class);
        ValidationHelper::validate(['id' => $variantId], IsVariantId::class);

        if (empty($queryFields)) {
            $queryFields = <<<GQL
                calculatedOrder {
                    id
                    addedLineItems(first:5) {
                        edges {
                            node {
                                id
                                quantity
                            }
                        }
                    }
                }
            GQL;
        }


        $query = <<<GQL
            mutation addVariantToOrder{
                orderEditAddVariant(
                    id: "$orderId",
                    variantId: "$variantId",
                    quantity: $quantity
                ) {
                    $queryFields
                    userErrors {
                      field
                      message
                    }
              }
            }
        GQL;

        return $this->graphClient->request($query, null);
    }

    /**
     * @throws InvalidInput
     */
    public function capture(Transaction $transaction, ?string $queryFields = null)
    {
        $payload = ValidationHelper::validate($transaction->bindingGraphData(), CaptureValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'transaction { id kind status }';
        }

        $query = '
            mutation ($input: OrderCaptureInput!) {
                orderCapture(input: $input) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, [
            'input' => $payload
        ]);
    }
}
