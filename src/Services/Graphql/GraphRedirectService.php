<?php

namespace Thainph\ShopifySdk\Services\Graphql;

use Thainph\ShopifySdk\Services\BaseService;
use Thainph\ShopifySdk\Exceptions\InvalidInput;
use Thainph\ShopifySdk\Mockup\Graphql\Redirect;
use Thainph\ShopifySdk\Helpers\ValidationHelper;
use Thainph\ShopifySdk\Clients\Graphql\GraphClient;
use Thainph\ShopifySdk\Validation\Validators\Graphql\Redirect\IsRedirectId;
use Thainph\ShopifySdk\Validation\Validators\Graphql\Redirect\UpdateValidator;
use Thainph\ShopifySdk\Validation\Validators\Graphql\Redirect\QueryValidator;
use Thainph\ShopifySdk\Validation\Validators\Graphql\Redirect\CreateValidator;

class GraphRedirectService extends BaseService
{
    protected GraphClient $graphClient;

    public function __construct(GraphClient $graphClient)
    {
        $this->graphClient = $graphClient;
    }

    /**
     * @throws InvalidInput
     */
    public function get(array $params = [], ?string $queryFields = null)
    {
        $payload = ValidationHelper::validate($params, QueryValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'edges { node { id path target } }';
        }

        $query = '
            query ($after: String, $before: String, $first: Int, $last: Int, $query: String, $reverse: Boolean, $sortKey: UrlRedirectSortKeys, $savedSearchId: ID)  {
                urlRedirects(after: $after, before: $before, first: $first, last: $last, query: $query, reverse: $reverse, sortKey: $sortKey, savedSearchId: $savedSearchId) {
                    ' . $queryFields . '
                    pageInfo{
                        endCursor
                        hasNextPage
                        startCursor
                        hasPreviousPage
                    }
                }
            }';

        return $this->graphClient->request($query, $payload);
    }

    /**
     * @throws InvalidInput
     */
    public function getbyId(string $redirectId, ?string $queryFields = null)
    {
        $payloadId = ValidationHelper::validate([
            'id' => $redirectId
        ], IsRedirectId::class);

        if (empty($queryFields)) {
            $queryFields = 'id path target';
        }

        $query = '
            query ($id: ID!)  {
                urlRedirect(id: $id) {
                    ' . $queryFields . '
                }
            }';

        return $this->graphClient->request($query, $payloadId);
    }

    /**
     * @throws InvalidInput
     */
    public function create(Redirect $redirect, ?string $queryFields = null)
    {
        $payload = ValidationHelper::validate($redirect->bindingGraphData(), CreateValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'urlRedirect { id path target }';
        }

        $query = '
            mutation ($input: UrlRedirectInput!) {
                urlRedirectCreate (urlRedirect: $input) {
                    ' . $queryFields . '
                    userErrors { field message } 
                }
            }';

        return $this->graphClient->request($query, [
            'input' => $payload
        ]);
    }

    /**
     * @throws InvalidInput
     */
    public function update(Redirect $redirect, ?string $queryFields = null)
    {
        $payload = ValidationHelper::validate($redirect->bindingGraphData(), UpdateValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'urlRedirect { id path target }';
        }

        $query = '
            mutation ($id: ID!, $input: UrlRedirectInput!) {
                urlRedirectUpdate (id: $id, urlRedirect: $input) {
                    ' . $queryFields . '
                    userErrors { field message } 
                }
            }';

        return $this->graphClient->request($query, [
            'input' => [
                'path' => $payload['path'],
                'target' => $payload['target']
            ],
            'id' => $payload['id']
        ]);
    }

    /**
     * @throws InvalidInput
     */
    public function remove(string $redirectId, ?string $queryFields = null)
    {
        $payloadId = ValidationHelper::validate([
            'id' => $redirectId
        ], IsRedirectId::class);

        if (empty($queryFields)) {
            $queryFields = 'deletedUrlRedirectId';
        }

        $query = '
            mutation ($id: ID!) {
                urlRedirectDelete (id: $id) {
                    ' . $queryFields . '
                    userErrors { field message } 
                }
            }';

        return $this->graphClient->request($query, $payloadId);
    }
}
