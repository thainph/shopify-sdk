<?php

namespace Thainph\ShopifySdk\Services\Graphql;

use Thainph\ShopifySdk\Exceptions\InvalidInput;
use Thainph\ShopifySdk\Services\BaseService;
use Thainph\ShopifySdk\Mockup\Graphql\Product;
use Thainph\ShopifySdk\Helpers\ValidationHelper;
use Thainph\ShopifySdk\Clients\Graphql\GraphClient;
use Thainph\ShopifySdk\Exceptions\AttributeNotExists;
use Thainph\ShopifySdk\Validation\Validators\Graphql\Product\IsProductId;
use Thainph\ShopifySdk\Validation\Validators\Graphql\Media\IsArrayOfMediaId;
use Thainph\ShopifySdk\Validation\Validators\Graphql\Media\IsArrayOfMedia;
use Thainph\ShopifySdk\Validation\Validators\Graphql\Product\QueryValidator;
use Thainph\ShopifySdk\Validation\Validators\Graphql\Product\CreateValidator;
use Thainph\ShopifySdk\Validation\Validators\Graphql\Product\UpdateValidator;
use Thainph\ShopifySdk\Validation\Validators\Graphql\Media\BulkCreateValidator;
use Thainph\ShopifySdk\Validation\Validators\Graphql\Media\BulkUpdateValidator;

class GraphProductService extends BaseService
{
    protected GraphClient $graphClient;

    public function __construct(GraphClient $graphClient)
    {
        $this->graphClient = $graphClient;
    }

    /**
     * @throws InvalidInput
     */
    public function get(array $params = [], ?string $queryFields = null)
    {
        $payload = ValidationHelper::validate($params, QueryValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'nodes{ handle title description onlineStoreUrl }';
        }

        $query = '
            query ($after: String, $before: String, $first: Int, $last: Int, $query: String, $reverse: Boolean, $sortKey: ProductSortKeys, $savedSearchId: ID){
                products(after: $after, before: $before, first: $first, last: $last, query: $query, reverse: $reverse, sortKey: $sortKey, savedSearchId: $savedSearchId) {
                    ' . $queryFields . '
                    pageInfo {
                        endCursor
                        hasNextPage
                        startCursor
                        hasPreviousPage
                    }
                }
            }';

        return $this->graphClient->request($query, $payload);
    }

    /**
     * @throws InvalidInput
     */
    public function getById(string $productId, ?string $queryFields = null)
    {
        $payloadId = ValidationHelper::validate([
            'id' => $productId
        ], IsProductId::class);

        if (empty($queryFields)) {
            $queryFields = 'handle title description onlineStoreUrl';
        }

        $query = '
            query ($id: ID!){
                product(id: $id) {
                    ' . $queryFields . '
                }
            }';

        return $this->graphClient->request($query, $payloadId);
    }

    /**
     * @throws AttributeNotExists
     * @throws InvalidInput
     */
    public function create(Product $product, array $medias = [], ?string $queryFields = null)
    {
        $payloadProduct = ValidationHelper::validate($product->bindingGraphData(), CreateValidator::class);

        $graphParams = ValidationHelper::validate([
            'medias' => $medias
        ], IsArrayOfMedia::class);
        $payloadMedia   = ValidationHelper::validate($graphParams, BulkCreateValidator::class);

        if (empty($queryFields)) {
            $queryFields = '
            product { 
                id title handle 
                metafields(first: 20){ nodes{ id namespace key type value } }
                media(first: 10) { nodes { alt mediaContentType preview { status } } }
            }';
        }

        $query = '
            mutation productCreate($input: ProductInput!, $media: [CreateMediaInput!]) {
                productCreate(input: $input, media: $media) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, [
            'input' => $payloadProduct,
            'media' => $payloadMedia['medias']
        ]);
    }

    /**
     * @throws InvalidInput
     */
    public function createMedias(string $productId, array $medias, ?string $queryFields = null)
    {
        ValidationHelper::validate([
            'id' => $productId
        ], IsProductId::class);

        $graphParams = ValidationHelper::validate([
            'medias' => $medias
        ], IsArrayOfMedia::class);
        $payloadMedia   = ValidationHelper::validate($graphParams, BulkCreateValidator::class);

        if (empty($queryFields)) {
            $queryFields = '
            media { id alt mediaContentType status }
            product { id title }';
        }

        $query = '
            mutation productCreateMedia($media: [CreateMediaInput!]!, $productId: ID!) {
                productCreateMedia(media: $media, productId: $productId) {
                    ' . $queryFields . '
                    mediaUserErrors { field  message }
                }
            }';

        return $this->graphClient->request($query, [
            'productId' => $productId,
            'media'     => $payloadMedia['medias']
        ]);
    }

    /**
     * @throws AttributeNotExists
     * @throws InvalidInput
     */
    public function update(Product $product, array $medias = [], ?string $queryFields = null)
    {
        $payloadProduct = ValidationHelper::validate($product->bindingGraphData(), UpdateValidator::class);

        $graphParams = ValidationHelper::validate([
            'medias' => $medias
        ], IsArrayOfMedia::class);
        $payloadMedia   = ValidationHelper::validate($graphParams, BulkCreateValidator::class);

        if (empty($queryFields)) {
            $queryFields = '
            product { 
                id title handle 
                metafields(first: 20){ nodes{ id namespace key type value } }
                media(first: 10) { nodes { alt mediaContentType preview { status } } }
            }';
        }

        $query = '
            mutation productUpdate($input: ProductInput!, $media: [CreateMediaInput!]) {
                productUpdate(input: $input, media: $media) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, [
            'input' => $payloadProduct,
            'media' => $payloadMedia['medias']
        ]);
    }

    /**
     * @throws InvalidInput
     */
    public function updateMedias(string $productId, array $medias, ?string $queryFields = null)
    {
        ValidationHelper::validate([
            'id' => $productId
        ], IsProductId::class);

        $graphParams = ValidationHelper::validate([
            'medias' => $medias
        ], IsArrayOfMedia::class);
        $payloadMedia   = ValidationHelper::validate($graphParams, BulkUpdateValidator::class);

        if (empty($queryFields)) {
            $queryFields = '
            media { alt mediaContentType status }
            product { id title }';
        }

        $query = '
            mutation productUpdateMedia($media: [UpdateMediaInput!]!, $productId: ID!) {
                productUpdateMedia(media: $media, productId: $productId) {
                    ' . $queryFields . '
                    mediaUserErrors { field  message }
                }
            }';

        return $this->graphClient->request($query, [
            'productId' => $productId,
            'media'     => $payloadMedia['medias']
        ]);
    }

    /**
     * @throws InvalidInput
     */
    public function remove(string $productId, ?string $queryFields = null)
    {
        $payloadId = ValidationHelper::validate([
            'id' => $productId
        ], IsProductId::class);

        if (empty($queryFields)) {
            $queryFields = 'deletedProductId';
        }

        $query = '
            mutation productDelete($input: ProductDeleteInput!) {
                productDelete(input: $input) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, [
            'input' => $payloadId
        ]);
    }

    /**
     * @throws InvalidInput
     */
    public function removeMedias(string $productId, array $mediaIds, ?string $queryFields = null)
    {
        ValidationHelper::validate([
            'id' => $productId
        ], IsProductId::class);

        $payload = ValidationHelper::validate([
            'ids' => $mediaIds
        ], IsArrayOfMediaId::class);

        if (empty($queryFields)) {
            $queryFields = 'deletedMediaIds deletedProductImageIds';
        }

        $query = '
            mutation productDeleteMedia($mediaIds: [ID!]!, $productId: ID!) {
                productDeleteMedia(mediaIds: $mediaIds, productId: $productId) {
                    ' . $queryFields . '
                    mediaUserErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, [
            'mediaIds'  => $payload['ids'],
            'productId' => $productId
        ]);
    }
}
