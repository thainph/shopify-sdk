<?php

namespace Thainph\ShopifySdk\Services\Graphql;

use Thainph\ShopifySdk\Services\BaseService;
use Thainph\ShopifySdk\Exceptions\InvalidInput;
use Thainph\ShopifySdk\Helpers\ValidationHelper;
use Thainph\ShopifySdk\Clients\Graphql\GraphClient;
use Thainph\ShopifySdk\Mockup\Graphql\FulfillmentConstraint;
use Thainph\ShopifySdk\Validation\Validators\Graphql\FulfillmentConstraint\CreateValidator;
use Thainph\ShopifySdk\Validation\Validators\Graphql\FulfillmentConstraint\IsFulfillmentConstraintId;

class GraphFulfillmentConstraintService extends BaseService
{
    protected GraphClient $graphClient;

    public function __construct(GraphClient $graphClient)
    {
        $this->graphClient = $graphClient;
    }

    /**
     * @throws InvalidInput
     */
    public function get(?string $queryFields = null)
    {
        if (empty($queryFields)) {
            $queryFields = 'id
            function {id title }
            metafields(first: 10) { nodes { namespace key value } }';
        }

        $query = '
            query {
                fulfillmentConstraintRules {
                    ' . $queryFields . '
                }
            }';

        return $this->graphClient->request($query, null);
    }

    /**
     * @throws InvalidInput
     */
    public function create(FulfillmentConstraint $fulfillmentConstraint, ?string $queryFields = null)
    {
        $payload = ValidationHelper::validate($fulfillmentConstraint->bindingGraphData(), CreateValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'fulfillmentConstraintRule { id
                function {id title }
                metafields(first: 10) { nodes { namespace key value } }
            }';
        }

        $query = '
            mutation ($functionId: String!, $metafields: [MetafieldInput!]){
                fulfillmentConstraintRuleCreate(functionId: $functionId, metafields: $metafields) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, $payload);
    }

    /**
     * @throws InvalidInput
     */
    public function remove(string $id)
    {
        ValidationHelper::validate(['id' => $id], IsFulfillmentConstraintId::class);

        $query = '
            mutation ($id: ID!){
                fulfillmentConstraintRuleDelete(id: $id) {
                    success
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, ['id' => $id]);
    }
}
