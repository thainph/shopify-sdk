<?php

namespace Thainph\ShopifySdk\Services\Graphql;

use Thainph\ShopifySdk\Services\BaseService;
use Thainph\ShopifySdk\Exceptions\InvalidInput;
use Thainph\ShopifySdk\Helpers\ValidationHelper;
use Thainph\ShopifySdk\Clients\Graphql\GraphClient;
use Thainph\ShopifySdk\Mockup\Graphql\InventoryItem;
use Thainph\ShopifySdk\Validation\Validators\Graphql\InventoryItem\QueryValidator;
use Thainph\ShopifySdk\Validation\Validators\Graphql\InventoryItem\UpdateValidator;
use Thainph\ShopifySdk\Validation\Validators\Graphql\InventoryItem\IsInventoryItemId;
use Thainph\ShopifySdk\Validation\Validators\Graphql\InventoryItem\BulkActivationValidator;

class GraphInventoryItemService extends BaseService
{
    protected GraphClient $graphClient;

    public function __construct(GraphClient $graphClient)
    {
        $this->graphClient = $graphClient;
    }

    /**
     * @throws InvalidInput
     */
    public function get(array $params = [], ?string $queryFields = null)
    {
        $payload = ValidationHelper::validate($params, QueryValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'edges { node { id tracked sku } }';
        }

        $query = '
            query ($after: String, $before: String, $first: Int, $last: Int, $query: String, $reverse: Boolean)  {
                inventoryItems(after: $after, before: $before, first: $first, last: $last, query: $query, reverse: $reverse) {
                    ' . $queryFields . '
                    pageInfo{
                        endCursor
                        hasNextPage
                        startCursor
                        hasPreviousPage
                    }
                }
            }';

        return $this->graphClient->request($query, $payload);
    }

    /**
     * @throws InvalidInput
     */
    public function getById(string $inventoryItemId, ?string $queryFields = null)
    {
        $payloadId = ValidationHelper::validate([
            'id' => $inventoryItemId
        ], IsInventoryItemId::class);

        if (empty($queryFields)) {
            $queryFields = 'id tracked sku';
        }

        $query = '
            query inventoryItem($id: ID!) {
                inventoryItem(id: $id) {
                    ' . $queryFields . '
                }
            }';

        return $this->graphClient->request($query, $payloadId);
    }

    /**
     * @throws InvalidInput
     */
    public function update(string $inventoryItemId, InventoryItem $inventoryItem, ?string $queryFields = null)
    {
        ValidationHelper::validate([
            'id' => $inventoryItemId
        ], IsInventoryItemId::class);

        $payload = ValidationHelper::validate($inventoryItem->toArray(), UpdateValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'inventoryItem { id tracked sku }';
        }

        $query = '
            mutation inventoryItemUpdate($id: ID!, $input: InventoryItemUpdateInput!) {
                inventoryItemUpdate(id: $id, input: $input) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, [
            'id' => $inventoryItemId,
            'input' => $payload
        ]);
    }

    /**
     * @throws InvalidInput
     */
    public function bulkToggleActivation(string $inventoryItemId, array $inventoryItemUpdates, ?string $queryFields = null)
    {
        ValidationHelper::validate([
            'id' => $inventoryItemId
        ], IsInventoryItemId::class);

        $payload = ValidationHelper::validate([
            'inventoryItemUpdates' => $inventoryItemUpdates
        ], BulkActivationValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'inventoryItem { id }
            inventoryLevels { id location { id } }';
        }

        $query = '
            mutation inventoryBulkToggleActivation($inventoryItemId: ID!, $inventoryItemUpdates: [InventoryBulkToggleActivationInput!]!) {
                inventoryBulkToggleActivation(inventoryItemId: $inventoryItemId, inventoryItemUpdates: $inventoryItemUpdates) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                        code
                    }
                }
            }';

        return $this->graphClient->request($query, [
            'inventoryItemId' => $inventoryItemId,
            'inventoryItemUpdates' => $payload['inventoryItemUpdates']
        ]);
    }
}
