<?php

namespace Thainph\ShopifySdk\Services\Graphql;

use Thainph\ShopifySdk\Services\BaseService;
use Thainph\ShopifySdk\Exceptions\InvalidInput;
use Thainph\ShopifySdk\Helpers\ValidationHelper;
use Thainph\ShopifySdk\Clients\Graphql\GraphClient;
use Thainph\ShopifySdk\Mockup\Graphql\CartTransform;
use Thainph\ShopifySdk\Validation\Validators\Graphql\CartTransform\CreateValidator;
use Thainph\ShopifySdk\Validation\Validators\Graphql\CartTransform\IsCartTransformId;
use Thainph\ShopifySdk\Validation\Validators\Graphql\CartTransform\QueryValidator;

class GraphCartTransformService extends BaseService
{
    protected GraphClient $graphClient;

    public function __construct(GraphClient $graphClient)
    {
        $this->graphClient = $graphClient;
    }

    /**
     * @throws InvalidInput
     */
    public function get(array $params = [], ?string $queryFields = null): array
    {
        $payload = ValidationHelper::validate($params, QueryValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'edges { node { id functionId blockOnFailure } }';
        }

        $query = '
            query ($after: String, $before: String, $first: Int, $last: Int, $reverse: Boolean)  {
                cartTransforms(after: $after, before: $before, first: $first, last: $last, reverse: $reverse) {
                    ' . $queryFields . '
                    pageInfo{
                        endCursor
                        hasNextPage
                        startCursor
                        hasPreviousPage
                    }
                }
            }';

        return $this->graphClient->request($query, $payload);
    }

    /**
     * @throws InvalidInput
     */
    public function create(CartTransform $cartTransform, ?string $queryFields = null): array
    {
        $payload = ValidationHelper::validate($cartTransform->bindingGraphData(), CreateValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'cartTransform { id functionId blockOnFailure }';
        }

        $query = '
            mutation ($functionId: String!, $blockOnFailure: Boolean) {
                cartTransformCreate(functionId: $functionId, blockOnFailure: $blockOnFailure) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, $payload);
    }

    /**
     * @throws InvalidInput
     */
    public function remove(string $id): array
    {
        $payloadId = ValidationHelper::validate(['id' => $id], IsCartTransformId::class);

        $query = '
            mutation ($id: ID!) {
                cartTransformDelete(id: $id) {
                    deletedId
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, $payloadId);
    }
}
