<?php

namespace Thainph\ShopifySdk\Services\Graphql;

use Thainph\ShopifySdk\Clients\Graphql\GraphClient;
use Thainph\ShopifySdk\Exceptions\AttributeNotExists;
use Thainph\ShopifySdk\Exceptions\InvalidInput;
use Thainph\ShopifySdk\Helpers\ValidationHelper;
use Thainph\ShopifySdk\Mockup\Graphql\Variant;
use Thainph\ShopifySdk\Services\BaseService;
use Thainph\ShopifySdk\Validation\Validators\Graphql\Product\IsProductId;
use Thainph\ShopifySdk\Validation\Validators\Graphql\Variant\BulkCreateValidator;
use Thainph\ShopifySdk\Validation\Validators\Graphql\Variant\BulkUpdateValidator;
use Thainph\ShopifySdk\Validation\Validators\Graphql\Variant\CreateValidator;
use Thainph\ShopifySdk\Validation\Validators\Graphql\Variant\IsArrayOfVariant;
use Thainph\ShopifySdk\Validation\Validators\Graphql\Variant\IsArrayOfVariantId;
use Thainph\ShopifySdk\Validation\Validators\Graphql\Variant\IsVariantId;
use Thainph\ShopifySdk\Validation\Validators\Graphql\Variant\UpdateValidator;
use Thainph\ShopifySdk\Validation\Validators\Graphql\Variant\QueryValidator;

class GraphVariantService extends BaseService
{
    protected GraphClient $graphClient;

    public function __construct(GraphClient $graphClient)
    {
        $this->graphClient = $graphClient;
    }

    /**
     * @throws InvalidInput
     */
    public function getById(string $variantId, ?string $queryFields = null)
    {
        $payloadId = ValidationHelper::validate([
            'id' => $variantId
        ], IsVariantId::class);

        if (empty($queryFields)) {
            $queryFields = 'title displayName price compareAtPrice inventoryQuantity availableForSale';
        }

        $query = '
            query ($id: ID!){
                productVariant(id: $id) {
                    ' . $queryFields . '
                }
            }';

        return $this->graphClient->request($query, $payloadId);
    }

    /**
     * @throws InvalidInput
     */
    public function get(array $params = [], ?string $queryFields = null)
    {
        $payload = ValidationHelper::validate($params, QueryValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'edges { node { id title displayName sku price } }';
        }

        $query = '
            query ($after: String, $before: String, $first: Int, $last: Int, $query: String, $reverse: Boolean, $sortKey: ProductVariantSortKeys, $savedSearchId: ID)  {
                productVariants(after: $after, before: $before, first: $first, last: $last, query: $query, reverse: $reverse, sortKey: $sortKey, savedSearchId: $savedSearchId) {
                    ' . $queryFields . '
                    pageInfo {
                        endCursor
                        hasNextPage
                        startCursor
                        hasPreviousPage
                    }
                }
            }';

        return $this->graphClient->request($query, $payload);
    }

    /**
     * @throws InvalidInput
     * @throws AttributeNotExists
     */
    public function create(Variant $variant, ?string $queryFields = null)
    {
        $payload = ValidationHelper::validate($variant->bindingGraphData(), CreateValidator::class);

        if (empty($queryFields)) {
            $queryFields = '
                product {
                    id title handle
                }
                productVariant {
                    id title sku price
                }';
        }

        $query = '
            mutation productVariantCreate($input: ProductVariantInput!) {
                productVariantCreate(input: $input) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, [
            'input' => $payload
        ]);
    }

    /**
     * @throws InvalidInput
     * @throws AttributeNotExists
     */
    public function update(Variant $variant, ?string $queryFields = null)
    {
        $payload = ValidationHelper::validate($variant->bindingGraphData(), UpdateValidator::class);

        if (empty($queryFields)) {
            $queryFields = '
                product {
                    id title handle
                }
                productVariant {
                    id title sku price
                }';
        }

        $query = '
            mutation productVariantUpdate($input: ProductVariantInput!) {
                productVariantUpdate(input: $input) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, [
            'input' => $payload
        ]);
    }

    /**
     * @throws InvalidInput
     */
    public function remove(string $variantId, ?string $queryFields = null)
    {
        $payloadId = ValidationHelper::validate([
            'id' => $variantId
        ], IsVariantId::class);

        if (empty($queryFields)) {
            $queryFields = '
                deletedProductVariantId
                product {
                    id title handle
                }';
        }

        $query = '
            mutation productVariantDelete($id: ID!) {
                productVariantDelete(id: $id) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, $payloadId);
    }

    /**
     * @throws InvalidInput
     */
    public function bulkCreate(string $productId, array $variants, ?string $queryFields = null)
    {
        ValidationHelper::validate([
            'id' => $productId
        ], IsProductId::class);

        $graphParams = ValidationHelper::validate([
            'variants' => $variants
        ], IsArrayOfVariant::class);

        $payload = ValidationHelper::validate($graphParams, BulkCreateValidator::class);

        if (empty($queryFields)) {
            $queryFields = '
                product {
                    id title handle
                }
                productVariants {
                    id title sku price
                }';
        }

        $query = '
            mutation productVariantsBulkCreate($productId: ID!, $variants: [ProductVariantsBulkInput!]!) {
                productVariantsBulkCreate(productId: $productId, variants: $variants) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, [
            'productId' => $productId,
            'variants' => $payload
        ]);
    }

    /**
     * @throws InvalidInput
     */
    public function bulkUpdate(string $productId, array $variants, ?string $queryFields = null)
    {
        ValidationHelper::validate([
            'id' => $productId
        ], IsProductId::class);

        $graphParams = ValidationHelper::validate([
            'variants' => $variants
        ], IsArrayOfVariant::class);

        $payload = ValidationHelper::validate($graphParams, BulkUpdateValidator::class);

        if (empty($queryFields)) {
            $queryFields = '
                product {
                    id title handle
                }
                productVariants {
                    id title sku price
                }';
        }

        $query = '
            mutation productVariantsBulkUpdate($productId: ID!, $variants: [ProductVariantsBulkInput!]!) {
                productVariantsBulkUpdate(productId: $productId, variants: $variants) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, [
            'productId' => $productId,
            'variants' => $payload
        ]);
    }

    /**
     * @throws InvalidInput
     */
    public function bulkRemove(string $productId, array $variantIds, ?string $queryFields = null)
    {
        ValidationHelper::validate([
            'id' => $productId
        ], IsProductId::class);

        ValidationHelper::validate([
            'ids' => $variantIds
        ], IsArrayOfVariantId::class);

        if (empty($queryFields)) {
            $queryFields = '
                product {
                    id title handle
                }';
        }

        $query = '
            mutation productVariantsBulkDelete($productId: ID!, $variantsIds: [ID!]!) {
                productVariantsBulkDelete(productId: $productId, variantsIds: $variantsIds) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, [
            'productId' => $productId,
            'variantsIds' => $variantIds
        ]);
    }
}
