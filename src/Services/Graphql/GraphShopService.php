<?php

namespace Thainph\ShopifySdk\Services\Graphql;

use Thainph\ShopifySdk\Services\BaseService;
use Thainph\ShopifySdk\Clients\Graphql\GraphClient;

class GraphShopService extends BaseService
{
    protected GraphClient $graphClient;

    public function __construct(GraphClient $graphClient)
    {
        $this->graphClient = $graphClient;
    }

    public function get(?string $queryFields = null)
    {
        if (empty($queryFields)) {
            $queryFields = 'id name email primaryDomain { url } plan { shopifyPlus partnerDevelopment displayName }';
        }

        $query = '
            query {
                shop {
                ' . $queryFields . '
                }
            }';

        return $this->graphClient->request($query, null);
    }

    public function getCustomerTags(int $perPage = 250)
    {
        $query = '
            query ($first: Int!){
                shop {
                    customerTags(first: $first) {
                        edges {
                            cursor
                            node
                        }
                        pageInfo {
                            endCursor
                            hasNextPage
                            hasPreviousPage
                            startCursor
                        }
                    }
                }
            }';

        return $this->graphClient->request($query, [
            'first' => $perPage
        ]);
    }

    public function getDraftOrderTags(int $perPage = 250)
    {
        $query = '
            query ($first: Int!){
                shop {
                    draftOrderTags(first: $first) {
                        edges {
                            cursor
                            node
                        }
                        pageInfo {
                            endCursor
                            hasNextPage
                            hasPreviousPage
                            startCursor
                        }
                    }
                }
            }';

        return $this->graphClient->request($query, [
            'first' => $perPage
        ]);
    }

    public function getOrderTags(int $perPage = 250)
    {
        $query = '
            query ($first: Int!){
                shop {
                    orderTags(first: $first) {
                        edges {
                            cursor
                            node
                        }
                        pageInfo {
                            endCursor
                            hasNextPage
                            hasPreviousPage
                            startCursor
                        }
                    }
                }
            }';

        return $this->graphClient->request($query, [
            'first' => $perPage
        ]);
    }

    public function getProductTags(int $perPage = 250)
    {
        $query = '
            query ($first: Int!){
                shop {
                    productTags(first: $first) {
                        edges {
                            cursor
                            node
                        }
                        pageInfo {
                            endCursor
                            hasNextPage
                            hasPreviousPage
                            startCursor
                        }
                    }
                }
            }';

        return $this->graphClient->request($query, [
            'first' => $perPage
        ]);
    }

    public function getProductTypes(int $perPage = 250)
    {
        $query = '
            query ($first: Int!){
                shop {
                    productTypes(first: $first) {
                        edges {
                            cursor
                            node
                        }
                        pageInfo {
                            endCursor
                            hasNextPage
                            hasPreviousPage
                            startCursor
                        }
                    }
                }
            }';

        return $this->graphClient->request($query, [
            'first' => $perPage
        ]);
    }

    public function getProductVendors(int $perPage = 250)
    {
        $query = '
            query ($first: Int!){
                shop {
                    productVendors(first: $first) {
                        edges {
                            cursor
                            node
                        }
                        pageInfo {
                            endCursor
                            hasNextPage
                            hasPreviousPage
                            startCursor
                        }
                    }
                }
            }';

        return $this->graphClient->request($query, [
            'first' => $perPage
        ]);
    }
}
