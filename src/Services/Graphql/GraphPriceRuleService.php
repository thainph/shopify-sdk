<?php

namespace Thainph\ShopifySdk\Services\Graphql;

use Thainph\ShopifySdk\Services\BaseService;
use Thainph\ShopifySdk\Helpers\ValidationHelper;
use Thainph\ShopifySdk\Clients\Graphql\GraphClient;
use Thainph\ShopifySdk\Validation\Validators\Graphql\PriceRule\IsPriceRuleId;
use Thainph\ShopifySdk\Validation\Validators\Graphql\PriceRule\QueryValidator;

class GraphPriceRuleService extends BaseService
{
    protected GraphClient $graphClient;

    public function __construct(GraphClient $graphClient)
    {
        $this->graphClient = $graphClient;
    }

    public function get(array $params = [], ?string $queryFields = null)
    {
        $payload = ValidationHelper::validate($params, QueryValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'nodes { id title status }';
        }

        $query = '
        query ($after: String, $before: String, $first: Int, $last: Int, $query: String, $reverse: Boolean, $sortKey: PriceRuleSortKeys, $savedSearchId: ID){
            priceRules(after: $after, before: $before, first: $first, last: $last, query: $query, reverse: $reverse, sortKey: $sortKey, savedSearchId: $savedSearchId) {
                ' . $queryFields . '
                pageInfo {
                    endCursor
                    hasNextPage
                    startCursor
                    hasPreviousPage
                }
            }
        }';

        return $this->graphClient->request($query, $payload);
    }

    public function getById(string $priceRuleId, ?string $queryFields = null)
    {
        $payloadId = ValidationHelper::validate([
          'id' => $priceRuleId
        ], IsPriceRuleId::class);

        if (empty($queryFields)) {
            $queryFields = 'id title status';
        }

        $query = '
        query ($id: ID!){
            priceRule(id: $id) {
                ' . $queryFields . '
            }
        }';

        return $this->graphClient->request($query, $payloadId);
    }
}
