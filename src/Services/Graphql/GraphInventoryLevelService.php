<?php

namespace Thainph\ShopifySdk\Services\Graphql;

use Thainph\ShopifySdk\Services\BaseService;
use Thainph\ShopifySdk\Exceptions\InvalidInput;
use Thainph\ShopifySdk\Helpers\ValidationHelper;
use Thainph\ShopifySdk\Clients\Graphql\GraphClient;
use Thainph\ShopifySdk\Mockup\Graphql\InventoryLevel;
use Thainph\ShopifySdk\Validation\Validators\Graphql\InventoryLevel\ActivateValidator;
use Thainph\ShopifySdk\Validation\Validators\Graphql\InventoryLevel\IsInventoryLevelId;

class GraphInventoryLevelService extends BaseService
{
    protected GraphClient $graphClient;

    public function __construct(GraphClient $graphClient)
    {
        $this->graphClient = $graphClient;
    }

    /**
     * @throws InvalidInput
     */
    public function getById(string $inventoryLevelId, ?string $queryFields = null)
    {
        $payloadId = ValidationHelper::validate([
            'id' => $inventoryLevelId
        ], IsInventoryLevelId::class);

        if (empty($queryFields)) {
            $queryFields = 'id available incoming item { id sku } location { id name }';
        }

        $query = '
            query inventoryLevel($id: ID!) {
                inventoryLevel(id: $id) {
                    ' . $queryFields . '
                }
            }';

        return $this->graphClient->request($query, $payloadId);
    }

    /**
     * @throws InvalidInput
     */
    public function activate(InventoryLevel $inventoryLevel, ?string $queryFields = null)
    {
        $payload = ValidationHelper::validate($inventoryLevel->bindingGraphData(), ActivateValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'inventoryLevel { id available incoming item { id sku } location { id name } }';
        }

        $query = '
            mutation inventoryActivate($inventoryItemId: ID!, $locationId: ID!) {
                inventoryActivate(inventoryItemId: $inventoryItemId, locationId: $locationId) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, $payload);
    }
}
