<?php

namespace Thainph\ShopifySdk\Services\Graphql;

use Thainph\ShopifySdk\Services\BaseService;
use Thainph\ShopifySdk\Exceptions\InvalidInput;
use Thainph\ShopifySdk\Helpers\ValidationHelper;
use Thainph\ShopifySdk\Clients\Graphql\GraphClient;
use Thainph\ShopifySdk\Validation\Validators\Graphql\BulkOperation\QueryValidator;
use Thainph\ShopifySdk\Validation\Validators\Graphql\BulkOperation\IsBulkOperationId;

class GraphBulkOperationService extends BaseService
{
    protected GraphClient $graphClient;

    public function __construct(GraphClient $graphClient)
    {
        $this->graphClient = $graphClient;
    }

    /**
     * @throws InvalidInput
     */
    public function getCurrent(array $params = [], ?string $queryFields = null)
    {
        $payload = ValidationHelper::validate($params, QueryValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'id type status';
        }

        $query = '
            query ($type: BulkOperationType){
                currentBulkOperation(type: $type) {
                    ' . $queryFields . '
                }
            }';

        return $this->graphClient->request($query, $payload);
    }

    /**
     * @throws InvalidInput
     */
    public function runMutation(string $mutation, string $stagedUploadPath, string $clientIdentifier = '', ?string $queryFields = null)
    {
        if (empty($queryFields)) {
            $queryFields = 'bulkOperation{ id type status }';
        }

        $query = '
            mutation bulkOperationRunMutation($mutation: String!, $stagedUploadPath: String!, $clientIdentifier: String) {
                bulkOperationRunMutation(mutation: $mutation, stagedUploadPath: $stagedUploadPath, clientIdentifier: $clientIdentifier) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, [
            'mutation' => $mutation,
            'stagedUploadPath' => $stagedUploadPath,
            'clientIdentifier' => $clientIdentifier
        ]);
    }

    /**
     * @throws InvalidInput
     */
    public function runQuery(string $query, ?string $queryFields = null)
    {
        if (empty($queryFields)) {
            $queryFields = 'bulkOperation{ id type status }';
        }

        $query = '
            mutation bulkOperationRunQuery {
                bulkOperationRunQuery(query: "' . $query . '") {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';


        return $this->graphClient->request($query, [
            'query' => $query
        ]);
    }

    /**
     * @throws InvalidInput
     */
    public function cancel(string $id, ?string $queryFields = null)
    {
        $payloadId = ValidationHelper::validate([
            'id' => $id
        ], IsBulkOperationId::class);

        if (empty($queryFields)) {
            $queryFields = 'bulkOperation{ id type status }';
        }

        $query = '
            mutation bulkOperationCancel($id: ID!) {
                bulkOperationCancel(id: $id) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, $payloadId);
    }
}
