<?php

namespace Thainph\ShopifySdk\Services\Graphql;

use Thainph\ShopifySdk\Clients\Graphql\GraphClient;
use Thainph\ShopifySdk\Exceptions\InvalidInput;
use Thainph\ShopifySdk\Helpers\ValidationHelper;
use Thainph\ShopifySdk\Services\BaseService;
use Thainph\ShopifySdk\Validation\Validators\Graphql\Metafield\BulkCreateValidator;
use Thainph\ShopifySdk\Validation\Validators\Graphql\Metafield\IsArrayOfMetafield;
use Thainph\ShopifySdk\Validation\Validators\Graphql\Metafield\IsMetafieldId;

class GraphMetafieldService extends BaseService
{
    protected GraphClient $graphClient;

    public function __construct(GraphClient $graphClient)
    {
        $this->graphClient = $graphClient;
    }

    /**
     * @throws InvalidInput
     */
    public function remove(string $metafieldId)
    {
        $payloadId = ValidationHelper::validate([
            'id' => $metafieldId
        ], IsMetafieldId::class);

        $query = '
            mutation metafieldDelete($input: MetafieldDeleteInput!) {
                metafieldDelete(input: $input) {
                    deletedId
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, [
            'input' => $payloadId
        ]);
    }

    /**
     * Limit 25 metafields per request
     * @throws InvalidInput
     */
    public function bulkCreateOrUpdate(array $metafields, ?string $queryFields = null)
    {
        $graphParams = ValidationHelper::validate([
            'metafields' => $metafields
        ], IsArrayOfMetafield::class);
        $payload = ValidationHelper::validate($graphParams, BulkCreateValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'metafields { id namespace key value createdAt updatedAt }';
        }

        $query = '
            mutation metafieldsSet($metafields: [MetafieldsSetInput!]!) {
                metafieldsSet(metafields: $metafields) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, [
            'metafields' => $payload['metafields']
        ]);
    }
}
