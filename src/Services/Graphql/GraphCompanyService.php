<?php

namespace Thainph\ShopifySdk\Services\Graphql;

use Thainph\ShopifySdk\Services\BaseService;
use Thainph\ShopifySdk\Exceptions\InvalidInput;
use Thainph\ShopifySdk\Helpers\ValidationHelper;
use Thainph\ShopifySdk\Clients\Graphql\GraphClient;
use Thainph\ShopifySdk\Mockup\Graphql\Company;
use Thainph\ShopifySdk\Mockup\Graphql\CompanyContact;
use Thainph\ShopifySdk\Mockup\Graphql\CompanyLocation;
use Thainph\ShopifySdk\Validation\Validators\Graphql\Company\CreateValidator;
use Thainph\ShopifySdk\Validation\Validators\Graphql\Company\IsCompanyId;
use Thainph\ShopifySdk\Validation\Validators\Graphql\Company\QueryValidator;
use Thainph\ShopifySdk\Validation\Validators\Graphql\Company\UpdateValidator;

class GraphCompanyService extends BaseService
{
    protected GraphClient $graphClient;

    public function __construct(GraphClient $graphClient)
    {
        $this->graphClient = $graphClient;
    }

    /**
     * @throws InvalidInput
     */
    public function get(array $params = [], ?string $queryFields = null)
    {
        $payload = ValidationHelper::validate($params, QueryValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'edges { node { id name contactCount } }';
        }

        $query = '
            query ($after: String, $before: String, $first: Int, $last: Int, $query: String, $reverse: Boolean, $sortKey: CompanySortKeys)  {
                companies(after: $after, before: $before, first: $first, last: $last, query: $query, reverse: $reverse, sortKey: $sortKey) {
                ' . $queryFields . '
                pageInfo{
                    endCursor
                    hasNextPage
                    startCursor
                    hasPreviousPage
                }
              }
            }';

        return $this->graphClient->request($query, $payload);
    }

    /**
     * @throws InvalidInput
     */
    public function getById(string $companyId, ?string $queryFields = null)
    {
        $payloadId = ValidationHelper::validate([
            'id' => $companyId
        ], IsCompanyId::class);

        if (empty($queryFields)) {
            $queryFields = 'id name contactCount';
        }

        $query = '
            query companyById($id: ID!) {
                company(id: $id) {
                    ' . $queryFields . '
                }
            }';

        return $this->graphClient->request($query, $payloadId);
    }

    /**
     * @throws InvalidInput
     */
    public function create(Company $company, ?CompanyContact $companyContact = null, ?CompanyLocation $companyLocation = null, ?string $queryFields = null)
    {
        $payload = ValidationHelper::validate([
            'company'           => $company->bindingGraphData(),
            'companyContact'    => $companyContact ? $companyContact->bindingGraphData() : null,
            'companyLocation'   => $companyLocation ? $companyLocation->bindingGraphData() : null,
        ], CreateValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'company{ id name contactCount }';
        }

        $query = '
            mutation companyCreate($input: CompanyCreateInput!) {
                companyCreate(input: $input) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, ['input' => $payload]);
    }

    /**
     * @throws InvalidInput
     */
    public function update(string $companyId, Company $company, ?string $queryFields = null)
    {
        ValidationHelper::validate([
            'id' => $companyId
        ], IsCompanyId::class);

        $payload = ValidationHelper::validate($company->bindingGraphData(), UpdateValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'company{ id name contactCount }';
        }

        $query = '
            mutation companyUpdate($companyId: ID!, $company: CompanyInput!) {
                companyUpdate(companyId: $companyId, input: $company) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, [
            'companyId' => $companyId,
            'company' => $payload
        ]);
    }

    /**
     * @throws InvalidInput
     */
    public function delete(string $companyId)
    {
        $payloadId = ValidationHelper::validate([
            'id' => $companyId
        ], IsCompanyId::class);

        $query = '
            mutation companyDelete($id: ID!) {
                companyDelete(id: $id) {
                    deletedCompanyId
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, $payloadId);
    }
}
