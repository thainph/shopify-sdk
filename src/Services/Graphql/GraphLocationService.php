<?php

namespace Thainph\ShopifySdk\Services\Graphql;

use Thainph\ShopifySdk\Services\BaseService;
use Thainph\ShopifySdk\Helpers\ValidationHelper;
use Thainph\ShopifySdk\Clients\Graphql\GraphClient;
use Thainph\ShopifySdk\Mockup\Graphql\Location;
use Thainph\ShopifySdk\Validation\Validators\Graphql\Location\CreateValidator;
use Thainph\ShopifySdk\Validation\Validators\Graphql\Location\IsLocationId;
use Thainph\ShopifySdk\Validation\Validators\Graphql\Location\QueryValidator;
use Thainph\ShopifySdk\Validation\Validators\Graphql\Location\UpdateValidator;

class GraphLocationService extends BaseService
{
    protected GraphClient $graphClient;

    public function __construct(GraphClient $graphClient)
    {
        $this->graphClient = $graphClient;
    }

    /**
     * @throws InvalidInput
     */
    public function get(array $params = [], ?string $queryFields = null)
    {
        $payload = ValidationHelper::validate($params, QueryValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'nodes{ id name address { address1 address2 city country zip } }';
        }

        $query = '
            query ($after: String, $before: String, $first: Int, $last: Int, $query: String, $reverse: Boolean, $sortKey: LocationSortKeys, $includeInactive: Boolean, $includeLegacy: Boolean)  {
                locations (after: $after, before: $before, first: $first, last: $last, query: $query, reverse: $reverse, sortKey: $sortKey, includeInactive: $includeInactive, includeLegacy: $includeLegacy) {
                    ' . $queryFields . '
                    pageInfo{
                        endCursor
                        hasNextPage
                        startCursor
                        hasPreviousPage
                    }
                }
            }';

        return $this->graphClient->request($query, $payload);
    }

    /**
     * @throws InvalidInput
     */
    public function getById(string $locationId, ?string $queryFields = null)
    {
        $payloadId = ValidationHelper::validate([
            'id' => $locationId
        ], IsLocationId::class);

        if (empty($queryFields)) {
            $queryFields = 'id name address { address1 address2 city country zip }';
        }

        $query = '
            query locationById($id: ID!) {
                location(id: $id) {
                    ' . $queryFields . '
                }
            }
            ';

        return $this->graphClient->request($query, $payloadId);
    }

    /**
     * @throws InvalidInput
     */
    public function create(Location $location, ?string $queryFields = null)
    {
        $payload = ValidationHelper::validate($location->bindingGraphData(), CreateValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'location { id name address { address1 address2 city country zip } }';
        }

        $query = '
            mutation ($input: LocationAddInput!) {
                locationAdd(input: $input) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }
            ';

        return $this->graphClient->request($query, [
            'input' => $payload
        ]);
    }

    /**
     * @throws InvalidInput
     */
    public function update(string $locationId, Location $location, ?string $queryFields = null)
    {
        ValidationHelper::validate([
            'id' => $locationId
        ], IsLocationId::class);

        $payload = ValidationHelper::validate($location->bindingGraphData(), UpdateValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'location { id name address { address1 address2 city country zip } }';
        }

        $query = '
            mutation locationEdit($id: ID!, $input: LocationEditInput!) {
                locationEdit(id: $id, input: $input) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }
            ';

        return $this->graphClient->request($query, [
            'id' => $locationId,
            'input' => $payload
        ]);
    }

    /**
     * @throws InvalidInput
     */
    public function activate(string $locationId, ?string $queryFields = null)
    {
        $payloadId = ValidationHelper::validate([
            'id' => $locationId
        ], IsLocationId::class);

        if (empty($queryFields)) {
            $queryFields = 'location { id name address { address1 address2 city country zip } }';
        }

        $query = '
            mutation locationActivate($id: ID!) {
                locationActivate(locationId: $id) {
                    ' . $queryFields . '
                    locationActivateUserErrors {
                        field
                        message
                    }
                }
            }
            ';

        return $this->graphClient->request($query, $payloadId);
    }

    /**
     * @throws InvalidInput
     */
    public function deactivate(string $locationId, ?string $queryFields = null)
    {
        $payloadId = ValidationHelper::validate([
            'id' => $locationId
        ], IsLocationId::class);

        if (empty($queryFields)) {
            $queryFields = 'location { id name address { address1 address2 city country zip } }';
        }

        $query = '
            mutation locationDeactivate($id: ID!) {
                locationDeactivate(locationId: $id) {
                    ' . $queryFields . '
                    locationDeactivateUserErrors {
                        field
                        message
                    }
                }
            }
            ';

        return $this->graphClient->request($query, $payloadId);
    }

    /**
     * @throws InvalidInput
     */
    public function remove(string $locationId, ?string $queryFields = null)
    {
        $payloadId = ValidationHelper::validate([
            'id' => $locationId
        ], IsLocationId::class);

        if (empty($queryFields)) {
            $queryFields = 'deletedLocationId';
        }

        $query = '
            mutation locationDelete($id: ID!) {
                locationDelete(locationId: $id) {
                    ' . $queryFields . '
                    locationDeleteUserErrors {
                        field
                        message
                    }
                }
            }
            ';

        return $this->graphClient->request($query, $payloadId);
    }
}
