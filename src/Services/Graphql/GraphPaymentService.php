<?php

namespace Thainph\ShopifySdk\Services\Graphql;

use Thainph\ShopifySdk\Services\BaseService;
use Thainph\ShopifySdk\Helpers\ValidationHelper;
use Thainph\ShopifySdk\Clients\Graphql\GraphClient;
use Thainph\ShopifySdk\Mockup\Graphql\PaymentSession;
use Thainph\ShopifySdk\Validation\Validators\Graphql\Payment\IsPaymentSessionId;
use Thainph\ShopifySdk\Validation\Validators\Graphql\Payment\CaptureRejectValidator;
use Thainph\ShopifySdk\Validation\Validators\Graphql\Payment\PaymentAppConfigureValidator;
use Thainph\ShopifySdk\Validation\Validators\Graphql\Payment\RefundSessionRejectValidator;
use Thainph\ShopifySdk\Validation\Validators\Graphql\Payment\RejectPaymentSessionValidator;
use Thainph\ShopifySdk\Validation\Validators\Graphql\Payment\PendingPaymentSessionValidator;
use Thainph\ShopifySdk\Validation\Validators\Graphql\Payment\ResolvePaymentSessionValidator;
use Thainph\ShopifySdk\Validation\Validators\Graphql\Payment\RedirectPaymentSessionValidator;
use Thainph\ShopifySdk\Validation\Validators\Graphql\Payment\VerificationSessionRejectValidator;
use Thainph\ShopifySdk\Validation\Validators\Graphql\Payment\VerificationSessionResolveValidator;

class GraphPaymentService extends BaseService
{
    protected GraphClient $graphClient;

    public function __construct(GraphClient $graphClient)
    {
        $this->graphClient = $graphClient;
    }

    public function paymentsAppConfigure(bool $isReady, ?string $externalHandle = null)
    {
        $payload = ValidationHelper::validate([
            'externalHandle' => $externalHandle,
            'ready' => $isReady,
        ], PaymentAppConfigureValidator::class);

        $query = '
          mutation PaymentsAppConfigure($externalHandle: String, $ready: Boolean!) {
              paymentsAppConfigure(externalHandle: $externalHandle, ready: $ready) {
                  userErrors {
                      field
                      message
                  }
              }
          }';

        return $this->graphClient->request($query, $payload, $this->graphClient->getPaymentEndpoint());
    }

    public function captureSessionReject(PaymentSession $paymentSession, ?string $queryFields = null)
    {
        $payload = ValidationHelper::validate($paymentSession->bindingGraphData(), CaptureRejectValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'captureSession { id state { ... on CaptureSessionStateRejected { code reason merchantMessage } } }';
        }

        $query = '
          mutation captureSessionReject($id: ID!, $reason: CaptureSessionRejectionReasonInput!) {
              captureSessionReject(id: $id, reason: $reason) {
                  ' . $queryFields . '
                  userErrors {
                      field
                      message
                  }
              }
          }';

        return $this->graphClient->request($query, $payload, $this->graphClient->getPaymentEndpoint());
    }

    public function captureSessionResolve(string $paymentSessionId, ?string $queryFields = null)
    {
        $payload = ValidationHelper::validate([
          'id' => $paymentSessionId,
        ], IsPaymentSessionId::class);

        if (empty($queryFields)) {
            $queryFields = 'captureSession { id state { ... on CaptureSessionStateResolved { code } } }';
        }

        $query = '
          mutation captureSessionResolve($id: ID!) {
              captureSessionResolve(id: $id) {
                  ' . $queryFields . '
                  userErrors {
                      field
                      message
                  }
              }
          }';

        return $this->graphClient->request($query, $payload, $this->graphClient->getPaymentEndpoint());
    }

    public function confirmPaymentSession(string $paymentSessionId, ?string $queryFields = null)
    {
        $payload = ValidationHelper::validate([
          'id' => $paymentSessionId,
        ], IsPaymentSessionId::class);

        if (empty($queryFields)) {
            $queryFields = 'paymentSession { 
              id pendingExpiresAt authorizationExpiresAt
              state { ... on PaymentSessionStateConfirmed { code } } 
              nextAction { action context { ... on PaymentSessionActionsRedirect { redirectUrl } } }
            }';
        }

        $query = '
          mutation paymentSessionConfirm($id: ID!) {
              paymentSessionConfirm(id: $id) {
                  ' . $queryFields . '
                  userErrors {
                      field
                      message
                  }
              }
          }';

        return $this->graphClient->request($query, $payload, $this->graphClient->getPaymentEndpoint());
    }

    public function pendingPaymentSession(PaymentSession $paymentSession, ?string $queryFields = null)
    {
        $payload = ValidationHelper::validate($paymentSession->bindingGraphData(), PendingPaymentSessionValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'paymentSession { 
              id pendingExpiresAt authorizationExpiresAt
              state { ... on PaymentSessionStatePending { code } } 
              nextAction { action context { ... on PaymentSessionActionsRedirect { redirectUrl } } }
            }';
        }

        $query = '
          mutation paymentSessionPending($id: ID!, $pendingExpiresAt: DateTime!, $reason: PaymentSessionStatePendingReason!) {
              paymentSessionPending(id: $id, pendingExpiresAt: $pendingExpiresAt, reason: $reason) {
                  ' . $queryFields . '
                  userErrors {
                      field
                      message
                  }
              }
          }';

        return $this->graphClient->request($query, $payload, $this->graphClient->getPaymentEndpoint());
    }

    public function redirectPaymentSession(PaymentSession $paymentSession, ?string $queryFields = null)
    {
        $payload = ValidationHelper::validate($paymentSession->bindingGraphData(), RedirectPaymentSessionValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'paymentSession { 
              id pendingExpiresAt authorizationExpiresAt
              state { ... on PaymentSessionStateRedirecting { code } } 
              nextAction { action context { ... on PaymentSessionActionsRedirect { redirectUrl } } }
            }';
        }

        $query = '
          mutation paymentSessionRedirect($id: ID!, $redirectUrl: String!) {
              paymentSessionRedirect(id: $id, redirectUrl: $redirectUrl) {
                  ' . $queryFields . '
                  userErrors {
                      field
                      message
                  }
              }
          }';

        return $this->graphClient->request($query, $payload, $this->graphClient->getPaymentEndpoint());
    }

    public function rejectPaymentSession(PaymentSession $paymentSession, ?string $queryFields = null)
    {
        $payload = ValidationHelper::validate($paymentSession->bindingGraphData(), RejectPaymentSessionValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'paymentSession { 
              id pendingExpiresAt authorizationExpiresAt
              state { ... on PaymentSessionStateRejected { code reason merchantMessage } } 
              nextAction { action context { ... on PaymentSessionActionsRedirect { redirectUrl } } }
            }';
        }

        $query = '
          mutation paymentSessionReject($id: ID!, $reason: PaymentSessionRejectionReasonInput!, $authentication: PaymentSessionThreeDSecureAuthentication) {
              paymentSessionReject(id: $id, reason: $reason, authentication: $authentication) {
                  ' . $queryFields . '
                  userErrors {
                      field
                      message
                  }
              }
          }';

        return $this->graphClient->request($query, $payload, $this->graphClient->getPaymentEndpoint());
    }

    public function resolvePaymentSession(PaymentSession $paymentSession, ?string $queryFields = null)
    {
        $payload = ValidationHelper::validate($paymentSession->bindingGraphData(), ResolvePaymentSessionValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'paymentSession { 
              id pendingExpiresAt authorizationExpiresAt
              state { ... on PaymentSessionStateResolved { code } } 
              nextAction { action context { ... on PaymentSessionActionsRedirect { redirectUrl } } }
            }';
        }

        $query = '
          mutation paymentSessionResolve($id: ID!, $authentication: PaymentSessionThreeDSecureAuthentication, $authorizationExpiresAt: DateTime, $networkTransactionId: String) {
              paymentSessionResolve(id: $id, authentication: $authentication, authorizationExpiresAt: $authorizationExpiresAt, networkTransactionId: $networkTransactionId) {
                  ' . $queryFields . '
                  userErrors {
                      field
                      message
                  }
              }
          }';

        return $this->graphClient->request($query, $payload, $this->graphClient->getPaymentEndpoint());
    }

    public function refundSessionReject(PaymentSession $paymentSession, ?string $queryFields = null)
    {
        $payload = ValidationHelper::validate($paymentSession->bindingGraphData(), RefundSessionRejectValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'refundSession { id state { ... on RefundSessionStateRejected { code reason merchantMessage } } }';
        }

        $query = '
          mutation refundSessionReject($id: ID!, $reason: RefundSessionRejectionReasonInput!)
              refundSessionReject(id: $id, reason: $reason) {
                  ' . $queryFields . '
                  userErrors {
                      field
                      message
                  }
              }
          }';

        return $this->graphClient->request($query, $payload, $this->graphClient->getPaymentEndpoint());
    }

    public function refundSessionResolve(string $paymentSessionId, ?string $queryFields = null)
    {
        $payload = ValidationHelper::validate([
          'id' => $paymentSessionId,
        ], IsPaymentSessionId::class);

        if (empty($queryFields)) {
            $queryFields = 'refundSession { id state { ... on RefundSessionStateResolved { code } } }';
        }

        $query = '
          mutation refundSessionResolve($id: ID!) {
              refundSessionResolve(id: $id) {
                  ' . $queryFields . '
                  userErrors {
                      field
                      message
                  }
              }
          }';

        return $this->graphClient->request($query, $payload, $this->graphClient->getPaymentEndpoint());
    }

    public function verificationSessionReject(PaymentSession $paymentSession, ?string $queryFields = null)
    {
        $payload = ValidationHelper::validate($paymentSession->bindingGraphData(), VerificationSessionRejectValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'verificationSession { id state { ... on VerificationSessionStateRejected { code reason merchantMessage } } }';
        }

        $query = '
          mutation verificationSessionReject($id: ID!, $reason: VerificationSessionRejectionReasonInput!)
              verificationSessionReject(id: $id, reason: $reason) {
                  ' . $queryFields . '
                  userErrors {
                      field
                      message
                  }
              }
          }';

        return $this->graphClient->request($query, $payload, $this->graphClient->getPaymentEndpoint());
    }

    public function verificationSessionResolve(PaymentSession $paymentSession, ?string $queryFields = null)
    {
        $payload = ValidationHelper::validate($paymentSession->bindingGraphData(), VerificationSessionResolveValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'verificationSession { id state { ... on VerificationSessionStateResolved { code } } }';
        }

        $query = '
          mutation verificationSessionResolve($id: ID!, $networkTransactionId: String) {
              verificationSessionResolve(id: $id, networkTransactionId: $networkTransactionId) {
                  ' . $queryFields . '
                  userErrors {
                      field
                      message
                  }
              }
          }';

        return $this->graphClient->request($query, $payload, $this->graphClient->getPaymentEndpoint());
    }

    public function voidSessionReject(PaymentSession $paymentSession, ?string $queryFields = null)
    {
        $payload = ValidationHelper::validate($paymentSession->bindingGraphData(), VerificationSessionRejectValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'voidSession { id state { ... on VoidSessionStateRejected { code reason merchantMessage } } }';
        }

        $query = '
          mutation voidSessionReject($id: ID!, $reason: VoidSessionRejectionReasonInput!)
              voidSessionReject(id: $id, reason: $reason) {
                  ' . $queryFields . '
                  userErrors {
                      field
                      message
                  }
              }
          }';

        return $this->graphClient->request($query, $payload, $this->graphClient->getPaymentEndpoint());
    }

    public function voidSessionResolve(string $paymentSessionId, ?string $queryFields = null)
    {
        $payload = ValidationHelper::validate([
          'id' => $paymentSessionId,
        ], IsPaymentSessionId::class);

        if (empty($queryFields)) {
            $queryFields = 'voidSession { id state { ... on VoidSessionStateResolved { code } } }';
        }

        $query = '
          mutation voidSessionResolve($id: ID!) {
              voidSessionResolve(id: $id) {
                  ' . $queryFields . '
                  userErrors {
                      field
                      message
                  }
              }
          }';

        return $this->graphClient->request($query, $payload, $this->graphClient->getPaymentEndpoint());
    }
}
