<?php

namespace Thainph\ShopifySdk\Services\Graphql;

use Thainph\ShopifySdk\Services\BaseService;
use Thainph\ShopifySdk\Exceptions\InvalidInput;
use Thainph\ShopifySdk\Helpers\ValidationHelper;
use Thainph\ShopifySdk\Clients\Graphql\GraphClient;
use Thainph\ShopifySdk\Validation\Validators\Graphql\Node\IsGraphqlId;
use Thainph\ShopifySdk\Validation\Validators\Graphql\Node\IsArrayGraphqlId;

class GraphNodeService extends BaseService
{
    const DEFAULT_FIELDS = '
      ... on App { title handle }
      ... on AppSubscription { name status trialDays }
      ... on BulkOperation { status completedAt url }
      ... on Channel { name }
      ... on CheckoutProfile { name isPublished }
      ... on Collection { title productsCount publicationCount }
      ... on Company { name note orderCount }
      ... on CompanyAddress { address1 address2 city country }
      ... on CompanyContact { title isMainContact }
      ... on CompanyLocation { name externalId }
      ... on Customer { email firstName lastName }
      ... on DeliveryCarrierService { name formattedName }
      ... on DeliveryCondition { field operator }
      ... on DeliveryMethod { methodType }
      ... on Product {productType title }
    ';

    protected GraphClient $graphClient;

    public function __construct(GraphClient $graphClient)
    {
        $this->graphClient = $graphClient;
    }

    /**
     * @throws InvalidInput
     */
    public function getByIds(array $ids = [], ?string $queryFields = null)
    {
        $payload = ValidationHelper::validate([
          'ids' => $ids,
        ], IsArrayGraphqlId::class);

        if (empty($queryFields)) {
            $queryFields = self::DEFAULT_FIELDS;
        }

        $query = '
          query ($ids: [ID!]!){
              nodes(ids: $ids) {
                  id
                  ' . $queryFields . '
              }
          }';

        return $this->graphClient->request($query, $payload);
    }

    public function getById(string $id, ?string $queryFields = null)
    {
        $payload = ValidationHelper::validate([
          'id' => $id,
        ], IsGraphqlId::class);

        if (empty($queryFields)) {
            $queryFields = self::DEFAULT_FIELDS;
        }
    
        $query = '
          query ($id: ID!){
              node(id: $id) {
                  id
                  ' . $queryFields . '
              }
          }';

        return $this->graphClient->request($query, $payload);
    }
}
