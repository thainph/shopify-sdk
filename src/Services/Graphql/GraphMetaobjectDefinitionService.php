<?php

namespace Thainph\ShopifySdk\Services\Graphql;

use Thainph\ShopifySdk\Services\BaseService;
use Thainph\ShopifySdk\Helpers\ValidationHelper;
use Thainph\ShopifySdk\Clients\Graphql\GraphClient;
use Thainph\ShopifySdk\Mockup\Graphql\MetaobjectDefinition;
use Thainph\ShopifySdk\Validation\Validators\Graphql\MetaobjectDefinition\CreateValidator;
use Thainph\ShopifySdk\Validation\Validators\Graphql\MetaobjectDefinition\QueryValidator;
use Thainph\ShopifySdk\Validation\Validators\Graphql\MetaobjectDefinition\IsMetaobjectDefinitionId;
use Thainph\ShopifySdk\Validation\Validators\Graphql\MetaobjectDefinition\UpdateValidator;

class GraphMetaobjectDefinitionService extends BaseService
{
    protected GraphClient $graphClient;

    public function __construct(GraphClient $graphClient)
    {
        $this->graphClient = $graphClient;
    }

    /**
     * @throws InvalidInput
     */
    public function get(array $params = [], ?string $queryFields = null)
    {
        $payload = ValidationHelper::validate($params, QueryValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'nodes{ id name type metaobjectsCount }';
        }

        $query = '
            query ($after: String, $before: String, $first: Int, $last: Int, $reverse: Boolean){
                metaobjectDefinitions(after: $after, before: $before, first: $first, last: $last, reverse: $reverse) {
                    ' . $queryFields . '
                    pageInfo {
                        endCursor
                        hasNextPage
                        startCursor
                        hasPreviousPage
                    }
                }
            }';

        return $this->graphClient->request($query, $payload);
    }

    /**
     * @throws InvalidInput
     */
    public function getById(string $metaobjectDefinitionId, ?string $queryFields = null)
    {
        $payloadId = ValidationHelper::validate([
            'id' => $metaobjectDefinitionId
        ], IsMetaobjectDefinitionId::class);

        if (empty($queryFields)) {
            $queryFields = 'id name type metaobjectsCount';
        }

        $query = '
            query ($id: ID!){
                metaobjectDefinition(id: $id) {
                    ' . $queryFields . '
                }
            }';

        return $this->graphClient->request($query, $payloadId);
    }

    /**
     * @throws InvalidInput
     */
    public function getByType(string $type, ?string $queryFields = null)
    {
        if (empty($queryFields)) {
            $queryFields = 'id name type metaobjectsCount';
        }

        $query = '
            query {
                metaobjectDefinitionByType(type: "' . $type . '") {
                    ' . $queryFields . '
                }
            }';

        return $this->graphClient->request($query, null);
    }

    /**
     * @throws InvalidInput
     */
    public function create(MetaobjectDefinition $metaobjectDefinition, ?string $queryFields = null)
    {
        $payload = ValidationHelper::validate($metaobjectDefinition->bindingGraphData(), CreateValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'metaobjectDefinition { id name type metaobjectsCount }';
        }

        $query = '
            mutation metaobjectDefinitionCreate($definition: MetaobjectDefinitionCreateInput!) {
                metaobjectDefinitionCreate(definition: $definition) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, [
            'definition' => $payload
        ]);
    }

    /**
     * @throws InvalidInput
     */
    public function update(string $metaobjectDefinitionId, MetaobjectDefinition $metaobjectDefinition, ?string $queryFields = null)
    {
        ValidationHelper::validate([
            'id' => $metaobjectDefinitionId
        ], IsMetaobjectDefinitionId::class);

        $payload = ValidationHelper::validate($metaobjectDefinition->bindingGraphData(), UpdateValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'metaobjectDefinition { id name type metaobjectsCount }';
        }

        $query = '
            mutation metaobjectDefinitionUpdate($id: ID!, $definition: MetaobjectDefinitionUpdateInput!) {
                metaobjectDefinitionUpdate(id: $id, definition: $definition) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, [
            'id' => $metaobjectDefinitionId,
            'definition' => $payload
        ]);
    }

    public function standardEnable(string $type, ?string $queryFields = null)
    {
        if (empty($queryFields)) {
            $queryFields = ' metaobjectDefinition { id name type metaobjectsCount }';
        }

        $query = '
            mutation standardMetaobjectDefinitionEnable($type: String!) {
                standardMetaobjectDefinitionEnable(type: $type) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, [
            'type' => $type
        ]);
    }

    /**
     * @throws InvalidInput
     */
    public function remove(string $metaobjectDefinitionId, ?string $queryFields = null)
    {
        $payloadId = ValidationHelper::validate([
            'id' => $metaobjectDefinitionId
        ], IsMetaobjectDefinitionId::class);

        if (empty($queryFields)) {
            $queryFields = 'deletedId';
        }

        $query = '
            mutation metaobjectDefinitionDelete($id: ID!) {
                metaobjectDefinitionDelete(id: $id) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, $payloadId);
    }
}
