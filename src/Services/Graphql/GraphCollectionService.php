<?php

namespace Thainph\ShopifySdk\Services\Graphql;

use Thainph\ShopifySdk\Services\BaseService;
use Thainph\ShopifySdk\Exceptions\InvalidInput;
use Thainph\ShopifySdk\Helpers\ValidationHelper;
use Thainph\ShopifySdk\Clients\Graphql\GraphClient;
use Thainph\ShopifySdk\Validation\Validators\Graphql\Collection\QueryValidator;
use Thainph\ShopifySdk\Validation\Validators\Graphql\Collection\AddProductValidator;
use Thainph\ShopifySdk\Validation\Validators\Graphql\Collection\RemoveProductValidator;

class GraphCollectionService extends BaseService
{
    protected GraphClient $graphClient;

    public function __construct(GraphClient $graphClient)
    {
        $this->graphClient = $graphClient;
    }

    /**
     * @throws InvalidInput
     */
    public function get(array $params = [], ?string $queryFields = null)
    {
        $payload = ValidationHelper::validate($params, QueryValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'edges { node { id title handle } }';
        }

        $query = '
            query ($after: String, $before: String, $first: Int, $last: Int, $query: String, $reverse: Boolean, $sortKey: CollectionSortKeys, $savedSearchId: ID)  {
              collections(after: $after, before: $before, first: $first, last: $last, query: $query, reverse: $reverse, sortKey: $sortKey, savedSearchId: $savedSearchId) {
                ' . $queryFields . '
                pageInfo{
                    endCursor
                    hasNextPage
                    startCursor
                    hasPreviousPage
                }
              }
            }';

        return $this->graphClient->request($query, $payload);
    }

    /**
     * @throws InvalidInput
     */
    public function getByHandle(string $handle, ?string $queryFields = null)
    {
        if (empty($queryFields)) {
            $queryFields = 'id title handle';
        }

        $query = '
            query collectionByHandle($handle: String!) {
                collectionByHandle(handle: $handle) {
                    ' . $queryFields . '
                }
            }';

        return $this->graphClient->request($query, [
            'handle' => $handle
        ]);
    }

    /**
     * @throws InvalidInput
     */
    public function addProducts(string $collectionId, array $productIds, ?string $queryFields = null)
    {
        $payload = ValidationHelper::validate([
            'collectionId' => $collectionId,
            'productIds' => $productIds
        ], AddProductValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'job { done id }';
        }

        $query = '
            mutation collectionAddProductsV2($collectionId: ID!, $productIds: [ID!]!) {
                collectionAddProductsV2(id: $collectionId, productIds: $productIds) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, $payload);
    }

    /**
     * @throws InvalidInput
     */
    public function removeProducts(string $collectionId, array $productIds, ?string $queryFields = null)
    {
        $payload = ValidationHelper::validate([
            'collectionId' => $collectionId,
            'productIds' => $productIds
        ], RemoveProductValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'job { done id }';
        }

        $query = '
            mutation collectionRemoveProducts($collectionId: ID!, $productIds: [ID!]!) {
                collectionRemoveProducts(id: $collectionId, productIds: $productIds) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, $payload);
    }
}
