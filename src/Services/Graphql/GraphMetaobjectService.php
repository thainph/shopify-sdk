<?php

namespace Thainph\ShopifySdk\Services\Graphql;

use Thainph\ShopifySdk\Services\BaseService;
use Thainph\ShopifySdk\Exceptions\InvalidInput;
use Thainph\ShopifySdk\Helpers\ValidationHelper;
use Thainph\ShopifySdk\Mockup\Graphql\Metaobject;
use Thainph\ShopifySdk\Clients\Graphql\GraphClient;
use Thainph\ShopifySdk\Validation\Validators\Graphql\Metaobject\BulkRemoveValidator;
use Thainph\ShopifySdk\Validation\Validators\Graphql\Metaobject\CreateOrUpdateValidator;
use Thainph\ShopifySdk\Validation\Validators\Graphql\Metaobject\IsMetaobjectId;
use Thainph\ShopifySdk\Validation\Validators\Graphql\Metaobject\QueryValidator;
use Thainph\ShopifySdk\Validation\Validators\Graphql\Metaobject\CreateValidator;
use Thainph\ShopifySdk\Validation\Validators\Graphql\Metaobject\UpdateValidator;

class GraphMetaobjectService extends BaseService
{
    protected GraphClient $graphClient;

    public function __construct(GraphClient $graphClient)
    {
        $this->graphClient = $graphClient;
    }

    /**
     * @throws InvalidInput
     */
    public function get(array $params = [], ?string $queryFields = null)
    {
        $payload = ValidationHelper::validate($params, QueryValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'nodes{ id handle type fields{ key value } }';
        }

        $query = '
            query ($after: String, $before: String, $first: Int, $last: Int, $query: String, $reverse: Boolean, $sortKey: String, $type: String!){
                metaobjects(after: $after, before: $before, first: $first, last: $last, query: $query, reverse: $reverse, sortKey: $sortKey, type: $type) {
                    ' . $queryFields . '
                    pageInfo {
                        endCursor
                        hasNextPage
                        startCursor
                        hasPreviousPage
                    }
                }
            }';

        return $this->graphClient->request($query, $payload);
    }

    /**
     * @throws InvalidInput
     */
    public function getById(string $id, ?string $queryFields = null)
    {
        $payloadId = ValidationHelper::validate(['id' => $id], IsMetaobjectId::class);

        if (empty($queryFields)) {
            $queryFields = 'id handle type fields{ key value }';
        }

        $query = '
            query ($id: ID!){
                metaobject(id: $id) {
                    ' . $queryFields . '
                }
            }';

        return $this->graphClient->request($query, $payloadId);
    }

    /**
     * @throws InvalidInput
     */
    public function getByHandle(string $handle, string $type, ?string $queryFields = null)
    {
        if (empty($queryFields)) {
            $queryFields = 'id handle type fields{ key value }';
        }

        $query = '
            query ($handle: MetaobjectHandleInput!){
                metaobjectByHandle(handle: $handle) {
                    ' . $queryFields . '
                }
            }';

        return $this->graphClient->request($query, ['handle' => [
            'handle' => $handle,
            'type' => $type
        ]]);
    }

    /**
     * @throws InvalidInput
     */
    public function create(Metaobject $metaobject, ?string $queryFields = null)
    {
        $payload = ValidationHelper::validate($metaobject->bindingGraphData(), CreateValidator::class);

        if (empty($queryFields)) {
            $queryFields = ' metaobject { id handle type fields{ key value } }';
        }

        $query = '
            mutation metaobjectCreate($metaobject: MetaobjectCreateInput!) {
                metaobjectCreate(metaobject: $metaobject) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, [
            'metaobject' => $payload
        ]);
    }

    /**
     * @throws InvalidInput
     */
    public function update(string $id, Metaobject $metaobject, ?string $queryFields = null)
    {
        ValidationHelper::validate(['id' => $id], IsMetaobjectId::class);

        $payload = ValidationHelper::validate($metaobject->bindingGraphData(), UpdateValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'metaobject { id handle type fields{ key value } }';
        }

        $query = '
            mutation metaobjectUpdate($id: ID!, $metaobject: MetaobjectUpdateInput!) {
                metaobjectUpdate(id: $id, metaobject: $metaobject) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, [
            'id' => $id,
            'metaobject' => $payload
        ]);
    }

    /**
     * @throws InvalidInput
     */
    public function createOrUpdate(Metaobject $metaobject, ?string $queryFields = null)
    {
        $payload = ValidationHelper::validate($metaobject->bindingGraphData(), CreateOrUpdateValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'metaobject { id handle type fields{ key value } }';
        }

        $query = '
            mutation metaobjectUpsert($handle: MetaobjectHandleInput!, $metaobject: MetaobjectUpsertInput!) {
                    metaobjectUpsert(handle: $handle, metaobject: $metaobject) {
                        ' . $queryFields . '
                        userErrors {
                            field
                            message
                        }
                }
            }';

        return $this->graphClient->request($query, $payload);
    }

    /**
     * @throws InvalidInput
     */
    public function remove(string $id)
    {
        $payloadId = ValidationHelper::validate(['id' => $id], IsMetaobjectId::class);

        $query = '
            mutation metaobjectDelete($id: ID!) {
                metaobjectDelete(id: $id) {
                    deletedId
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, $payloadId);
    }

    /**
     * @throws InvalidInput
     */
    public function bulkRemove(array $where)
    {
        ValidationHelper::validate($where, BulkRemoveValidator::class);

        $query = '
            mutation metaobjectBulkDelete($where: MetaobjectBulkDeleteWhereCondition!) {
                metaobjectBulkDelete(where: $where) {
                    job {
                        id
                        done
                    }
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, [
            'where' => $where
        ]);
    }
}
