<?php

namespace Thainph\ShopifySdk\Services\Graphql;

use Thainph\ShopifySdk\Services\BaseService;
use Thainph\ShopifySdk\Helpers\ValidationHelper;
use Thainph\ShopifySdk\Clients\Graphql\GraphClient;
use Thainph\ShopifySdk\Mockup\Graphql\DeliveryCustomization;
use Thainph\ShopifySdk\Validation\Validators\Graphql\DeliveryCustomization\QueryValidator;
use Thainph\ShopifySdk\Validation\Validators\Graphql\DeliveryCustomization\InputValidator;
use Thainph\ShopifySdk\Validation\Validators\Graphql\DeliveryCustomization\IsDeliveryCustomizationId;
use Thainph\ShopifySdk\Validation\Validators\Graphql\DeliveryCustomization\IsArrayDeliveryCustomizationId;

class GraphDeliveryCustomizationService extends BaseService
{
    protected GraphClient $graphClient;

    public function __construct(GraphClient $graphClient)
    {
        $this->graphClient = $graphClient;
    }

    /**
     * @throws InvalidInput
     */
    public function get(array $params = [], ?string $queryFields = null)
    {
        $payload = ValidationHelper::validate($params, QueryValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'edges { node { id functionId enabled } }';
        }

        $query = '
            query ($after: String, $before: String, $first: Int, $last: Int, $query: String, $reverse: Boolean)  {
                deliveryCustomizations(after: $after, before: $before, first: $first, last: $last, query: $query, reverse: $reverse) {
                    ' . $queryFields . '
                    pageInfo{
                        endCursor
                        hasNextPage
                        startCursor
                        hasPreviousPage
                    }
                }
            }';

        return $this->graphClient->request($query, $payload);
    }

    /**
     * @throws InvalidInput
     */
    public function getById(string $deliveryCustomizationId, ?string $queryFields = null)
    {
        $payloadId = ValidationHelper::validate([
            'id' => $deliveryCustomizationId
        ], IsDeliveryCustomizationId::class);

        if (empty($queryFields)) {
            $queryFields = 'id functionId enabled';
        }

        $query = '
            query deliveryCustomization($id: ID!) {
                deliveryCustomization(id: $id) {
                    ' . $queryFields . '
                }
            }';

        return $this->graphClient->request($query, $payloadId);
    }

    /**
     * @throws InvalidInput
     */
    public function create(DeliveryCustomization $deliveryCustomization, ?string $queryFields = null)
    {
        $payload = ValidationHelper::validate($deliveryCustomization->bindingGraphData(), InputValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'deliveryCustomization { id functionId enabled }';
        }

        $query = '
            mutation deliveryCustomizationCreate($deliveryCustomization: DeliveryCustomizationInput!) {
                deliveryCustomizationCreate(deliveryCustomization: $deliveryCustomization) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, [
            'deliveryCustomization' => $payload
        ]);
    }

    /**
     * @throws InvalidInput
     */
    public function update(string $deliveryCustomizationId, DeliveryCustomization $deliveryCustomization, ?string $queryFields = null)
    {
        ValidationHelper::validate([
            'id' => $deliveryCustomizationId
        ], IsDeliveryCustomizationId::class);

        $payload = ValidationHelper::validate($deliveryCustomization->bindingGraphData(), InputValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'deliveryCustomization { id functionId enabled }';
        }

        $query = '
            mutation deliveryCustomizationUpdate($deliveryCustomization: DeliveryCustomizationInput!, $id: ID!) {
                deliveryCustomizationUpdate(deliveryCustomization: $deliveryCustomization, id: $id) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, [
            'deliveryCustomization' => $payload,
            'id' => $deliveryCustomizationId
        ]);
    }

    /**
     * @throws InvalidInput
     */
    public function bulkActivation(array $deliveryCustomizationIds, bool $enabled)
    {
        ValidationHelper::validate([
            'ids' => $deliveryCustomizationIds
        ], IsArrayDeliveryCustomizationId::class);
        
        $query = '
            mutation deliveryCustomizationActivation($enabled: Boolean!, $ids: [ID!]!) {
                deliveryCustomizationActivation(enabled: $enabled, ids: $ids) {
                    ids
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, ['ids' => $deliveryCustomizationIds, 'enabled' => $enabled]);
    }

    /**
     * @throws InvalidInput
     */
    public function remove(string $deliveryCustomizationId)
    {
        $payloadId = ValidationHelper::validate([
            'id' => $deliveryCustomizationId
        ], IsDeliveryCustomizationId::class);

        $query = '
            mutation deliveryCustomizationDelete($id: ID!) {
                deliveryCustomizationDelete(id: $id) {
                    deletedId
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, $payloadId);
    }

}
