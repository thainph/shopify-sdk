<?php

namespace Thainph\ShopifySdk\Services\Graphql;

use Thainph\ShopifySdk\Services\BaseService;
use Thainph\ShopifySdk\Exceptions\InvalidInput;
use Thainph\ShopifySdk\Helpers\ValidationHelper;
use Thainph\ShopifySdk\Clients\Graphql\GraphClient;
use Thainph\ShopifySdk\Mockup\Graphql\ScriptTag;
use Thainph\ShopifySdk\Validation\Validators\Graphql\Collection\QueryValidator;
use Thainph\ShopifySdk\Validation\Validators\Graphql\ScriptTag\CreateValidator;
use Thainph\ShopifySdk\Validation\Validators\Graphql\ScriptTag\IsScriptTagId;
use Thainph\ShopifySdk\Validation\Validators\Graphql\ScriptTag\UpdateValidator;

class GraphScriptTagService extends BaseService
{
    protected GraphClient $graphClient;

    public function __construct(GraphClient $graphClient)
    {
        $this->graphClient = $graphClient;
    }

    /**
     * @throws InvalidInput
     */
    public function get(array $params = [], ?string $queryFields = null)
    {
        $payload = ValidationHelper::validate($params, QueryValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'edges { node { id src displayScope } }';
        }

        $query = '
            query ($after: String, $before: String, $first: Int, $last: Int, $reverse: Boolean)  {
                scriptTags(after: $after, before: $before, first: $first, last: $last, reverse: $reverse) {
                    ' . $queryFields . '
                    pageInfo{
                        endCursor
                        hasNextPage
                        startCursor
                        hasPreviousPage
                    }
                }
            }';

        return $this->graphClient->request($query, $payload);
    }

    /**
     * @throws InvalidInput
     */
    public function getById(string $scriptTagId, ?string $queryFields = null)
    {
        $payloadId = ValidationHelper::validate([
            'id' => $scriptTagId
        ], IsScriptTagId::class);

        if (empty($queryFields)) {
            $queryFields = 'id src displayScope';
        }

        $query = '
            query scriptTag($id: ID!) {
                scriptTag(id: $id) {
                    ' . $queryFields . '
                }
            }';

        return $this->graphClient->request($query, $payloadId);
    }

    /**
     * @throws InvalidInput
     */
    public function create(ScriptTag $scriptTag, ?string $queryFields = null)
    {
        $payload = ValidationHelper::validate($scriptTag->bindingGraphData(), CreateValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'scriptTag{ id src displayScope }';
        }

        $query = '
            mutation scriptTagCreate($input: ScriptTagInput!) {
                scriptTagCreate(input: $input) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, [
            'input' => $payload
        ]);
    }

    /**
     * @throws InvalidInput
     */
    public function update(string $scriptTagId, ScriptTag $scriptTag, ?string $queryFields = null)
    {
        ValidationHelper::validate([
            'id' => $scriptTagId
        ], IsScriptTagId::class);

        $payload = ValidationHelper::validate($scriptTag->bindingGraphData(), UpdateValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'scriptTag{ id src displayScope }';
        }

        $query = '
            mutation scriptTagUpdate($id: ID!, $input: ScriptTagInput!) {
                scriptTagUpdate(id: $id, input: $input) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, [
            'id' => $scriptTagId,
            'input' => $payload
        ]);
    }

    /**
     * @throws InvalidInput
     */
    public function remove(string $scriptTagId, ?string $queryFields = null)
    {
        $payloadId = ValidationHelper::validate([
            'id' => $scriptTagId
        ], IsScriptTagId::class);

        if (empty($queryFields)) {
            $queryFields = 'deletedScriptTagId';
        }

        $query = '
            mutation scriptTagDelete($id: ID!) {
                scriptTagDelete(id: $id) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, $payloadId);
    }
}
