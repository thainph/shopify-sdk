<?php

namespace Thainph\ShopifySdk\Services\Graphql;

use Thainph\ShopifySdk\Services\BaseService;
use Thainph\ShopifySdk\Helpers\ValidationHelper;
use Thainph\ShopifySdk\Clients\Graphql\GraphClient;
use Thainph\ShopifySdk\Mockup\Graphql\MetafieldDefinition;
use Thainph\ShopifySdk\Validation\Validators\Graphql\MetafieldDefinition\QueryValidator;
use Thainph\ShopifySdk\Validation\Validators\Graphql\MetafieldDefinition\CreateValidator;
use Thainph\ShopifySdk\Validation\Validators\Graphql\MetafieldDefinition\IsMetafieldDefinitionId;
use Thainph\ShopifySdk\Validation\Validators\Graphql\MetafieldDefinition\UpdateValidator;

class GraphMetafieldDefinitionService extends BaseService
{
    protected GraphClient $graphClient;

    public function __construct(GraphClient $graphClient)
    {
        $this->graphClient = $graphClient;
    }

    public function get(array $params = [], ?string $queryFields = null)
    {
        $payload = ValidationHelper::validate($params, QueryValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'nodes{ id name namespace key }';
        }

        $query = '
            query ( $after: String, $before: String, 
                    $first: Int, $last: Int, 
                    $query: String, $reverse: Boolean, $sortKey: MetafieldDefinitionSortKeys,
                    $namespace: String, $key: String,
                    $ownerType: MetafieldOwnerType!, $pinnedStatus: MetafieldDefinitionPinnedStatus){
                metafieldDefinitions(after: $after, before: $before, 
                                    first: $first, last: $last, 
                                    query: $query, reverse: $reverse, sortKey: $sortKey,
                                    namespace: $namespace, key: $key,
                                    ownerType: $ownerType, pinnedStatus: $pinnedStatus) {
                ' . $queryFields . '
                pageInfo {
                    endCursor
                    hasNextPage
                    startCursor
                    hasPreviousPage
                }
              }
            }';

        return $this->graphClient->request($query, $payload);
    }

    public function getById(string $metafieldDefinitionId, ?string $queryFields = null)
    {
        $payloadId = ValidationHelper::validate([
            'id' => $metafieldDefinitionId
        ], IsMetafieldDefinitionId::class);

        if (empty($queryFields)) {
            $queryFields = 'id name namespace key';
        }

        $query = '
            query ($id: ID!){
                metafieldDefinition(id: $id) {
                    ' . $queryFields . '
                }
            }';

        return $this->graphClient->request($query, $payloadId);
    }

    public function create(MetafieldDefinition $definition, ?string $queryFields = null)
    {
        $payload = ValidationHelper::validate($definition->bindingGraphData(), CreateValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'createdDefinition { id name namespace key }';
        }

        $query = '
            mutation ($definition: MetafieldDefinitionInput!) {
                metafieldDefinitionCreate(definition: $definition) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, [
            'definition' => $payload
        ]);
    }

    public function update(MetafieldDefinition $definition, ?string $queryFields = null)
    {
        $payload = ValidationHelper::validate($definition->bindingGraphData(), UpdateValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'updatedDefinition { id name namespace key }';
        }

        $query = '
            mutation ($definition: MetafieldDefinitionUpdateInput!) {
                metafieldDefinitionUpdate(definition: $definition) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, [
            'definition' => $payload
        ]);
    }

    public function pin(string $metafieldDefinitionId, ?string $queryFields = null)
    {
        $payloadId = ValidationHelper::validate([
            'id' => $metafieldDefinitionId
        ], IsMetafieldDefinitionId::class);

        if (empty($queryFields)) {
            $queryFields = 'pinnedDefinition { name key namespace pinnedPosition }';
        }

        $query = '
            mutation ($id: ID!){
                metafieldDefinitionPin(definitionId: $id) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, $payloadId);
    }

    public function unpin(string $metafieldDefinitionId, ?string $queryFields = null)
    {
        $payloadId = ValidationHelper::validate([
            'id' => $metafieldDefinitionId
        ], IsMetafieldDefinitionId::class);

        if (empty($queryFields)) {
            $queryFields = 'unpinnedDefinition { name key namespace pinnedPosition }';
        }

        $query = '
            mutation ($id: ID!){
                metafieldDefinitionUnpin(definitionId: $id) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, $payloadId);
    }

    public function remove(string $metafieldDefinitionId)
    {
        $payloadId = ValidationHelper::validate([
            'id' => $metafieldDefinitionId
        ], IsMetafieldDefinitionId::class);

        $query = '
            mutation ($id: ID!){
                metafieldDefinitionDelete(id: $id) {
                    deletedDefinitionId
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, $payloadId);
    }
}
