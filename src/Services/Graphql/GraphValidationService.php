<?php

namespace Thainph\ShopifySdk\Services\Graphql;

use Thainph\ShopifySdk\Services\BaseService;
use Thainph\ShopifySdk\Exceptions\InvalidInput;
use Thainph\ShopifySdk\Helpers\ValidationHelper;
use Thainph\ShopifySdk\Clients\Graphql\GraphClient;
use Thainph\ShopifySdk\Mockup\Graphql\Validation;
use Thainph\ShopifySdk\Validation\Validators\Graphql\Validation\CreateValidator;
use Thainph\ShopifySdk\Validation\Validators\Graphql\Validation\QueryValidator;
use Thainph\ShopifySdk\Validation\Validators\Graphql\Validation\UpdateValidator;
use Thainph\ShopifySdk\Validation\Validators\Graphql\Validation\IsValidationId;

class GraphValidationService extends BaseService
{
    protected GraphClient $graphClient;

    public function __construct(GraphClient $graphClient)
    {
        $this->graphClient = $graphClient;
    }

    /**
     * @throws InvalidInput
     */
    public function get(array $params = [], ?string $queryFields = null): array
    {
        $payload = ValidationHelper::validate($params, QueryValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'edges { node { id title enabled blockOnFailure } }';
        }

        $query = '
            query ($after: String, $before: String, $first: Int, $last: Int, $reverse: Boolean, $sortKey: ValidationSortKeys)  {
                validations(after: $after, before: $before, first: $first, last: $last, reverse: $reverse, sortKey: $sortKey) {
                    ' . $queryFields . '
                    pageInfo{
                        endCursor
                        hasNextPage
                        startCursor
                        hasPreviousPage
                    }
                }
            }';

        return $this->graphClient->request($query, $payload);
    }

    /**
     * @throws InvalidInput
     */
    public function getById(string $validationId, ?string $queryFields = null): array
    {
        $payloadId = ValidationHelper::validate([
            'id' => $validationId
        ], IsValidationId::class);

        if (empty($queryFields)) {
            $queryFields = 'id title enabled blockOnFailure';
        }

        $query = '
            query ($id: ID!)  {
                validation(id: $id) {
                    ' . $queryFields . '
                }
            }';

        return $this->graphClient->request($query, $payloadId);
    }

    /**
     * @throws InvalidInput
     */
    public function create(Validation $params, ?string $queryFields = null): array
    {
        $payload = ValidationHelper::validate($params->bindingGraphData(), CreateValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'validation { id title enabled blockOnFailure }';
        }

        $query = '
            mutation ($validation: ValidationCreateInput!) {
                validationCreate(validation: $validation) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, [
            'validation' => $payload
        ]);
    }

    /**
     * @throws InvalidInput
     */
    public function update(string $validationId, Validation $params, ?string $queryFields = null): array
    {
        ValidationHelper::validate([
            'id' => $validationId
        ], IsValidationId::class);

        $payload = ValidationHelper::validate($params->bindingGraphData(), UpdateValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'validation { id title enabled blockOnFailure }';
        }

        $query = '
            mutation ($id: ID!, $validation: ValidationUpdateInput!) {
                validationUpdate(id: $id, validation: $validation) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, [
            'id' => $validationId,
            'validation' => $payload
        ]);
    }

    /**
     * @throws InvalidInput
     */
    public function remove(string $validationId): array
    {
        $payloadId = ValidationHelper::validate([
            'id' => $validationId
        ], IsValidationId::class);

        $query = '
            mutation ($id: ID!) {
                validationDelete(id: $id) {
                    deletedId
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, $payloadId);
    }
}
