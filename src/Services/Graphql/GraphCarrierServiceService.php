<?php

namespace Thainph\ShopifySdk\Services\Graphql;

use Thainph\ShopifySdk\Services\BaseService;
use Thainph\ShopifySdk\Helpers\ValidationHelper;
use Thainph\ShopifySdk\Clients\Graphql\GraphClient;
use Thainph\ShopifySdk\Validation\Validators\Graphql\CarrierService\IsCarrierServiceId;

class GraphCarrierServiceService extends BaseService
{
    protected GraphClient $graphClient;

    public function __construct(GraphClient $graphClient)
    {
        $this->graphClient = $graphClient;
    }

    public function getById(string $serviceId, ?string $queryFields = null)
    {
        ValidationHelper::validate([
            'id' => $serviceId
        ], IsCarrierServiceId::class);

        if (empty($queryFields)) {
            $queryFields = 'id name formattedName';
        }

        $query = <<<GQL
            query {
                carrierService(id: "$serviceId") {
                    $queryFields
                }
            }
            GQL;

        return $this->graphClient->request($query, null);
    }

}
