<?php

namespace Thainph\ShopifySdk\Services\Graphql;

use Thainph\ShopifySdk\Services\BaseService;
use Thainph\ShopifySdk\Exceptions\InvalidInput;
use Thainph\ShopifySdk\Helpers\ValidationHelper;
use Thainph\ShopifySdk\Clients\Graphql\GraphClient;
use Thainph\ShopifySdk\Mockup\Graphql\PaymentCustomization;
use Thainph\ShopifySdk\Validation\Validators\Graphql\PaymentCustomization\InputValidator;
use Thainph\ShopifySdk\Validation\Validators\Graphql\PaymentCustomization\QueryValidator;
use Thainph\ShopifySdk\Validation\Validators\Graphql\PaymentCustomization\IsPaymentCustomizationId;
use Thainph\ShopifySdk\Validation\Validators\Graphql\PaymentCustomization\IsArrayPaymentCustomizationId;

class GraphPaymentCustomizationService extends BaseService
{
    protected GraphClient $graphClient;

    public function __construct(GraphClient $graphClient)
    {
        $this->graphClient = $graphClient;
    }

    /**
     * @throws InvalidInput
     */
    public function get(array $params = [], ?string $queryFields = null)
    {
        $payload = ValidationHelper::validate($params, QueryValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'edges { node { 
                id title enabled functionId
                metafields(first: 10) { nodes { namespace key value } }
            } }';
        }

        $query = '
            query ($after: String, $before: String, $first: Int, $last: Int, $query: String, $reverse: Boolean)  {
                paymentCustomizations(after: $after, before: $before, first: $first, last: $last, query: $query, reverse: $reverse) {
                ' . $queryFields . '
                pageInfo{
                    endCursor
                    hasNextPage
                    startCursor
                    hasPreviousPage
                }
              }
            }';

        return $this->graphClient->request($query, $payload);
    }

    /**
     * @throws InvalidInput
     */
    public function getById(string $paymentCustomizationId, ?string $queryFields = null)
    {
        $payloadId = ValidationHelper::validate(
            ['id' => $paymentCustomizationId],
            IsPaymentCustomizationId::class
        );

        if (empty($queryFields)) {
            $queryFields = '
                id title enabled functionId
                metafields(first: 10) { nodes { namespace key value } }
            ';
        }

        $query = '
            query paymentCustomization($id: ID!) {
                paymentCustomization(id: $id) {
                    ' . $queryFields . '
                }
            }';

        return $this->graphClient->request($query, $payloadId);
    }

    /**
     * @throws InvalidInput
     */
    public function create(PaymentCustomization $paymentCustomization, ?string $queryFields = null)
    {
        $payload = ValidationHelper::validate($paymentCustomization->bindingGraphData(), InputValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'paymentCustomization {
                id title enabled functionId
                metafields(first: 10) { nodes { namespace key value } }
            }';
        }

        $query = '
            mutation paymentCustomizationCreate($paymentCustomization: PaymentCustomizationInput!) {
                paymentCustomizationCreate(paymentCustomization: $paymentCustomization) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, [
            'paymentCustomization' => $payload
        ]);
    }

    /**
     * @throws InvalidInput
     */
    public function update(string $paymentCustomizationId, PaymentCustomization $paymentCustomization, ?string $queryFields = null)
    {
        ValidationHelper::validate(
            ['id' => $paymentCustomizationId],
            IsPaymentCustomizationId::class
        );

        $payload = ValidationHelper::validate($paymentCustomization->bindingGraphData(), InputValidator::class);

        if (empty($queryFields)) {
            $queryFields = 'paymentCustomization { 
                id title enabled functionId
                metafields(first: 10) { nodes { namespace key value } }
            }';
        }

        $query = '
            mutation paymentCustomizationUpdate($id: ID!, $input: PaymentCustomizationInput!) {
                paymentCustomizationUpdate(id: $id, paymentCustomization: $input) {
                    ' . $queryFields . '
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, [
            'id' => $paymentCustomizationId,
            'input' => $payload
        ]);
    }

    public function bulkActivation(array $ids, bool $enabled)
    {
        ValidationHelper::validate(
            ['ids' => $ids],
            IsArrayPaymentCustomizationId::class
        );

        $query = '
            mutation paymentCustomizationActivation($ids: [ID!]!, $enabled: Boolean!) {
                paymentCustomizationActivation(ids: $ids, enabled: $enabled) {
                    ids
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, [
            'ids' => $ids,
            'enabled' => $enabled
        ]);
    }

    /**
     * @throws InvalidInput
     */
    public function remove(string $paymentCustomizationId)
    {
        $payloadId = ValidationHelper::validate(
            ['id' => $paymentCustomizationId],
            IsPaymentCustomizationId::class
        );

        $query = '
            mutation paymentCustomizationDelete($id: ID!) {
                paymentCustomizationDelete(id: $id) {
                    deletedId
                    userErrors {
                        field
                        message
                    }
                }
            }';

        return $this->graphClient->request($query, $payloadId);
    }
}
