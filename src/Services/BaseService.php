<?php
namespace Thainph\ShopifySdk\Services;

class BaseService
{
    private static BaseService $instance;

    public static function getInstance(): BaseService
    {
        if (empty(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

}
