<?php
namespace Thainph\ShopifySdk;

use Illuminate\Support\Facades\Http;

class ShopifyApp
{
    protected string $appKey;
    protected string $appSecret;
    protected string $appScopes;
    protected string $appProxy;
    protected string $appName;

    public function __construct(string $appKey, string $appSecret, string $appScopes, string $appProxy, string $appName)
    {
        $this->appKey = $appKey;
        $this->appSecret = $appSecret;
        $this->appScopes = $appScopes;
        $this->appProxy = $appProxy;
        $this->appName = strtolower($appName);

    }

    public function getInstallUrl(string $storeUrl, string $redirectUrl): string
    {
        $params = [
            'client_id' => $this->appKey,
            'scope' => $this->appScopes,
            'redirect_uri' => $redirectUrl,
        ];

        return "https://$storeUrl/admin/oauth/authorize?". http_build_query($params);
    }

    /**
     * @param string $storeUrl
     * @param string $code
     * @return string|null
     */
    public function getAccessToken(string $storeUrl, string $code): ?string
    {
        $endpoint = "https://$storeUrl/admin/oauth/access_token";
        $response = Http::post($endpoint, [
            'client_id' => $this->appKey,
            'client_secret' => $this->appSecret,
            'code' => $code,
        ]);
        $result = $response->json();

        return $result['access_token'] ?? null;
    }

    public function getAppDashboardUrl($storeName): string
    {
        return "https://admin.shopify.com/store/$storeName/apps/$this->appName";
    }
}
