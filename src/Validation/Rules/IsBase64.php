<?php

namespace Thainph\ShopifySdk\Validation\Rules;

use Illuminate\Contracts\Validation\Rule;

class IsBase64 implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value): bool
    {
        return base64_decode($value, true) !== false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        return 'The :attribute must be a valid Base64 string.';
    }
}
