<?php

namespace Thainph\ShopifySdk\Validation\Rules;

use Illuminate\Contracts\Validation\Rule;
use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Exceptions\ObjectNotFound;

class IsShopifyGraphId implements Rule
{
    protected string $shopifyObject;
    protected array $requiredParameters = [];

    /**
     * Create a new rule instance.
     *
     * @param string $shopifyObject
     * @return void
     * @throws ObjectNotFound
     */
    public function __construct(string $shopifyObject, array $requiredParameters = [])
    {
        if (!in_array($shopifyObject, ShopifyObject::toArray())) {
            throw new ObjectNotFound('Invalid or unsupported Shopify object.');
        }

        $this->shopifyObject = $shopifyObject;

        foreach ($requiredParameters as $key => $value) {
            if (is_numeric($key)) {
                $this->requiredParameters[$value] = null;
            } else {
                $this->requiredParameters[$key] = $value;
            }
        }
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value): bool
    {
        $path = explode('?', $value);

        $regexPattern = '/^gid:\/\/shopify\/' . $this->shopifyObject . '\/\w+$/';
        return preg_match($regexPattern, $path[0]) && $this->validateParameters($path[1] ?? '');
    }

    /**
     * @param string $parameters
     * @return bool
     */
    protected function validateParameters(string $parameters): bool
    {
        if (empty($this->requiredParameters)) {
            return true;
        }

        $parameters = explode('&', $parameters);
        $parameters = array_map(function ($parameter) {
            return explode('=', $parameter);
        }, $parameters);

        $parameters = array_filter($parameters, function ($parameter) {
            return array_key_exists($parameter[0], $this->requiredParameters) && (
                $this->requiredParameters[$parameter[0]] === null || $this->requiredParameters[$parameter[0]] === $parameter[1]
            );
        });

        return count($parameters) === count($this->requiredParameters);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        return ':attribute is not a valid Shopify Graph ID.';
    }
}
