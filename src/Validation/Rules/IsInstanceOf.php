<?php

namespace Thainph\ShopifySdk\Validation\Rules;

use Illuminate\Contracts\Validation\Rule;

class IsInstanceOf implements Rule
{
    protected string $className;

    /**
     * Create a new rule instance.
     *
     * @param string $className
     * @return void
     */
    public function __construct(string $className)
    {
        $this->className = $className;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value): bool
    {
        return $value instanceof $this->className;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        return ":attribute must be an instance of {$this->className}.";
    }
}
