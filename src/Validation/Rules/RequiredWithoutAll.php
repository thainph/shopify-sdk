<?php

namespace Thainph\ShopifySdk\Validation\Rules;

use Thainph\ShopifySdk\Exceptions\ObjectNotFound;

class RequiredWithoutAll
{
    protected array $fields;

    /**
     * Create a new rule instance.
     *
     * @param array $fields
     * @return void
     * @throws ObjectNotFound
     */
    public function __construct(array $fields, array|string $except = [])
    {
        if (is_string($except)) {
            $except = [$except];
        }

        $this->fields = array_diff($fields, $except);
    }

    /**
     * Convert the rule to a validation string.
     *
     * @return string
     */
    public function __toString()
    {
        return 'required_without_all:' . implode(',', $this->fields);
    }
}
