<?php

namespace Thainph\ShopifySdk\Validation\Rules;

use Illuminate\Contracts\Validation\Rule;
use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Exceptions\ObjectNotFound;

class IsOneShopifyGraphIdOff implements Rule
{
    protected array $shopifyObjects;

    /**
     * Create a new rule instance.
     *
     * @param array $shopifyObjects
     * @return void
     * @throws ObjectNotFound
     */
    public function __construct(array $shopifyObjects)
    {
        $invalidObjects = array_diff($shopifyObjects, ShopifyObject::toArray());

        if (count($invalidObjects) > 0) {
            throw new ObjectNotFound('Invalid or unsupported Shopify object(s): ' . implode(', ', $invalidObjects));
        }

        $this->shopifyObjects = $shopifyObjects;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value): bool
    {
        foreach ($this->shopifyObjects as $shopifyObject) {
            $path = explode('?', $value);

            $regexPattern = '/^gid:\/\/shopify\/' . $shopifyObject . '\/\w+$/';
            if (preg_match($regexPattern, $path[0])) {
                return true;
            }
        }

        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return ':attribute is invalid or unsupported Shopify Graph ID.';
    }
}
