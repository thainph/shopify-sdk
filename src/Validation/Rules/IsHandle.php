<?php

namespace Thainph\ShopifySdk\Validation\Rules;

use Illuminate\Contracts\Validation\Rule;

class IsHandle implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value): bool
    {
        return preg_match('/^[a-zA-Z0-9_%\-]+$/', $value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        return ':attribute is not a valid handle.';
    }
}
