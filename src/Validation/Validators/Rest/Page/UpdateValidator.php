<?php

namespace Thainph\ShopifySdk\Validation\Validators\Rest\Page;

use Illuminate\Validation\Rule;
use Thainph\ShopifySdk\Enums\MetafieldType;
use Thainph\ShopifySdk\Validation\Rules\IsHandle;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class UpdateValidator extends BaseValidator
{
    public function rules(): array
    {
        return [
            'id'                    => 'required|integer',
            'title'                 => 'required|string',
            'author'                => 'string',
            'body_html'             => 'string',
            'handle'                => new IsHandle(),
            'published_at'          => 'date',
            'template_suffix'       => 'string',

            'metafields'                => 'array',
            'metafields.*.namespace'    => 'required|string',
            'metafields.*.key'          => 'required|string',
            'metafields.*.value'        => 'required|string',
            'metafields.*.type'         => [
                'required',
                Rule::in(MetafieldType::toArray())
            ],
        ];
    }
}
