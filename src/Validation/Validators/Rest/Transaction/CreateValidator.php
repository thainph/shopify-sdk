<?php

namespace Thainph\ShopifySdk\Validation\Validators\Rest\Transaction;

use Illuminate\Validation\Rule;
use Thainph\ShopifySdk\Enums\Rest\TransactionStatus;
use Thainph\ShopifySdk\Enums\Rest\TransactionKind;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class CreateValidator extends BaseValidator
{
    public function rules(): array
    {
        return [
            'authorization'             => 'string',
            'authorization_expires_at'  => 'date',
            'currency'                  => 'string|size:3',
            'amount'                    => 'numeric|gt:0|required_if:kind,' . TransactionKind::REFUND,
            'gateway'                   => 'string',
            'kind'                      => [
                'required',
                Rule::in(TransactionKind::toArray())
            ],
            'order_id'                  => 'integer',
            'parent_id'                 => 'integer|required',
            'user_id'                   => 'integer',
            'processed_at'              => 'date',
            'status'                    => Rule::in(TransactionStatus::toArray()),
            'test'                      => 'boolean',

            'extended_authorization_attributes.standard_authorization_expires_at'   => 'date',
            'extended_authorization_attributes.extended_authorization_expires_at'   => 'date',

            'payments_refund_attributes.status'                                     => Rule::in(TransactionStatus::toArray()),
            'payments_refund_attributes.acquirer_reference_number'                  => 'numeric',

            'currency_exchange_adjustment.id'                                       => 'integer',
            'currency_exchange_adjustment.adjustment'                               => 'numeric',
            'currency_exchange_adjustment.original_amount'                          => 'string',
            'currency_exchange_adjustment.final_amount'                             => 'numeric',
            'currency_exchange_adjustment.currency'                                 => 'string|size:3',
        ];
    }
}
