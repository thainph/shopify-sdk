<?php

namespace Thainph\ShopifySdk\Validation\Validators\Rest\CarrierService;

use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class UpdateValidator extends BaseValidator
{
    public function rules(): array
    {
        return [
            'id'                    => 'required|integer',
            'name'                  => 'required|string',
            'callback_url'          => 'required|url',
            'active'                => 'boolean',
            'format'                => 'string',
            'carrier_service_type'  => 'string',
            'service_discovery'     => 'boolean',
        ];
    }
}
