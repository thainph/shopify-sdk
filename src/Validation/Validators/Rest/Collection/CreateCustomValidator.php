<?php

namespace Thainph\ShopifySdk\Validation\Validators\Rest\Collection;

use Illuminate\Validation\Rule;
use Thainph\ShopifySdk\Enums\MetafieldType;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;
use Thainph\ShopifySdk\Enums\Rest\SortOrder;
use Thainph\ShopifySdk\Validation\Rules\IsBase64;
use Thainph\ShopifySdk\Validation\Rules\IsHandle;

class CreateCustomValidator extends BaseValidator
{
    public function rules(): array
    {
        return [
            'body_html'             => 'string',
            'handle'                => new IsHandle(),
            'image.attachment'      => new IsBase64(),
            'image.src'             => 'url',
            'image.alt'             => 'string',
            'image.width'           => 'integer|min:1',
            'image.height'          => 'integer|min:1',
            'published'             => 'boolean',
            'sort_order'            => Rule::in(SortOrder::toArray()),
            'template_suffix'       => 'string',
            'title'                 => 'required|string',
            'metafields'            => 'array',
            'metafields.*.namespace' => 'required|string',
            'metafields.*.key'      => 'required|string',
            'metafields.*.value'    => 'required|string',
            'metafields.*.type'     => [
                'required',
                Rule::in(MetafieldType::toArray())
            ],
            'collects'              => 'array',
            'collects.*.product_id' => 'required|integer',
        ];
    }
}
