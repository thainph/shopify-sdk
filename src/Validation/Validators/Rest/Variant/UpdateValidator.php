<?php

namespace Thainph\ShopifySdk\Validation\Validators\Rest\Variant;

use Illuminate\Validation\Rule;
use Thainph\ShopifySdk\Enums\MetafieldType;
use Thainph\ShopifySdk\Enums\Rest\WeightUnit;
use Thainph\ShopifySdk\Enums\Rest\InventoryPolicy;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class UpdateValidator extends BaseValidator
{
    public function rules(): array
    {
        return [
            'barcode'                                   => 'string',
            'grams'                                     => 'integer',
            'compare_at_price'                          => 'numeric',
            'fulfillment_service'                       => 'string',
            'image_id'                                  => 'integer',
            'inventory_item_id'                         => 'integer',
            'inventory_management'                      => 'string',
            'inventory_policy'                          => Rule::in(InventoryPolicy::toArray()),
            'option1'                                   => 'required|string',
            'option2'                                   => 'string',
            'option3'                                   => 'string',
            'price'                                     => 'numeric',
            'sku'                                       => 'string',
            'taxable'                                   => 'boolean',
            'title'                                     => 'string',
            'weight'                                    => 'numeric',
            'weight_unit'                               => Rule::in(WeightUnit::toArray()),

            'metafields'                    => 'array',
            'metafields.*.namespace'        => 'required|string',
            'metafields.*.key'              => 'required|string',
            'metafields.*.value'            => 'required|string',
            'metafields.*.type'             => [
                'required',
                Rule::in(MetafieldType::toArray())
            ],
        ];
    }
}
