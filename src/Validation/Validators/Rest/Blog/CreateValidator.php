<?php

namespace Thainph\ShopifySdk\Validation\Validators\Rest\Blog;

use Illuminate\Validation\Rule;
use Thainph\ShopifySdk\Enums\MetafieldType;
use Thainph\ShopifySdk\Enums\Rest\Commentable;
use Thainph\ShopifySdk\Validation\Rules\IsHandle;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class CreateValidator extends BaseValidator
{
    public function rules(): array
    {
        return [
            'title'                         => 'required|string',
            'commentable'                   => Rule::in(Commentable::toArray()),
            'handle'                        => new IsHandle(),
            'template_suffix'               => 'string',

            'metafields'                    => 'array',
            'metafields.*.namespace'        => 'required|string',
            'metafields.*.key'              => 'required|string',
            'metafields.*.value'            => 'required|string',
            'metafields.*.type'             => [
                'required',
                Rule::in(MetafieldType::toArray())
            ],

        ];
    }
}
