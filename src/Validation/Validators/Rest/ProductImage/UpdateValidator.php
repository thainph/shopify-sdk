<?php

namespace Thainph\ShopifySdk\Validation\Validators\Rest\ProductImage;

use Illuminate\Validation\Rule;
use Thainph\ShopifySdk\Enums\MetafieldType;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class UpdateValidator extends BaseValidator
{
    public function rules(): array
    {
        return [
            'id'                            => 'required|integer',
            'product_id'                    => 'required|integer',
            'position'                      => 'integer',
            'variant_ids'                   => 'array',
            'variant_ids.*'                 => 'integer',
            'filename'                      => 'string',
            'alt'                           => 'string',

            'metafields'                    => 'array',
            'metafields.*.namespace'        => 'required|string',
            'metafields.*.key'              => 'required|string',
            'metafields.*.value'            => 'required|string',
            'metafields.*.type'             => [
                'required',
                Rule::in(MetafieldType::toArray())
            ],
        ];
    }
}
