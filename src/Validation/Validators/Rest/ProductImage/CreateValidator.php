<?php

namespace Thainph\ShopifySdk\Validation\Validators\Rest\ProductImage;

use Illuminate\Validation\Rule;
use Thainph\ShopifySdk\Enums\MetafieldType;
use Thainph\ShopifySdk\Validation\Rules\IsBase64;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class CreateValidator extends BaseValidator
{
    public function rules(): array
    {
        return [
            'product_id'                    => 'required|integer',
            'position'                      => 'integer',
            'variant_ids'                   => 'array',
            'variant_ids.*'                 => 'integer',
            'attachment'                    => [
                'required_without:src',
                new IsBase64()
            ],
            'src'                           => 'url|required_without:attachment',
            'filename'                      => 'string',
            'alt'                           => 'string',

            'metafields'                    => 'array',
            'metafields.*.namespace'        => 'required|string',
            'metafields.*.key'              => 'required|string',
            'metafields.*.value'            => 'required|string',
            'metafields.*.type'             => [
                'required',
                Rule::in(MetafieldType::toArray())
            ],
        ];
    }
}
