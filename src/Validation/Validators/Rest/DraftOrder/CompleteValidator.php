<?php

namespace Thainph\ShopifySdk\Validation\Validators\Rest\DraftOrder;

use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class CompleteValidator extends BaseValidator
{
    public function rules(): array
    {
        return [
            'payment_gateway_id'        => 'numeric',
            'payment_pending'           => 'boolean',
        ];
    }

}
