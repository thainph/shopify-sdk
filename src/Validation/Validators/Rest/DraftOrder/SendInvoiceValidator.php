<?php

namespace Thainph\ShopifySdk\Validation\Validators\Rest\DraftOrder;

use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class SendInvoiceValidator extends BaseValidator
{
    public function rules(): array
    {
        return [
            'to'                => 'email|required',
            'from'              => 'email',
            'subject'           => 'string',
            'custom_message'    => 'string',
            'bcc'               => 'array',
            'bcc.*'             => 'email',
        ];
    }
}
