<?php

namespace Thainph\ShopifySdk\Validation\Validators\Rest\DraftOrder;

use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Thainph\ShopifySdk\Enums\Rest\OrderStatus;
use Thainph\ShopifySdk\Validation\Rules\IsHandle;
use Thainph\ShopifySdk\Enums\TaxExemption;
use Thainph\ShopifySdk\Enums\Rest\DiscountValueType;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class CreateValidator extends BaseValidator
{
    public function rules(): array
    {
        return array_merge(
            [
            'name'                  => 'string',
            'customer.id'           => 'integer',
            'note'                  => 'string',
            'email'                 => 'email',
            'currency'              => 'string',
            'invoice_sent_at'       => 'date',
            'invoice_url'           => 'string|url',
            'tags'                  => 'string',
            'tax_exempt'            => 'boolean',
            'tax_exemptions'        => 'array',
            'tax_exemptions.*'      => Rule::in($this->canadianTax()),
            'taxes_included'        => 'boolean',
            'total_tax'             => 'numeric',
            'subtotal_price'        => 'numeric',
            'total_price'           => 'numeric',
            'completed_at'          => 'date',
            'status'                => Rule::in(OrderStatus::toArray()),

            'note_attributes'           => 'array',
            'note_attributes.*.name'    => 'string',
            'note_attributes.*.value'   => 'string',

            'line_items'                        => 'required|array',
            'line_items.*.variant_id'           => 'required_without:line_items.*.price|numeric',
            'line_items.*.title'                => 'required_without:line_items.*.variant_id|string',
            'line_items.*.price'                => 'required_without:line_items.*.variant_id|numeric',
            'line_items.*.quantity'             => 'required|integer',
            'lime_items.*.gift_card'            => 'boolean',
            'line_items.*.fulfillment_service'  => 'string',
            'line_items.*.properties'           => 'array',
            'line_items.*.properties.*.name'    => 'string',
            'line_items.*.properties.*.value'   => 'string',
            'line_items.*.taxable'              => 'boolean',
            'line_items.*.vendor'               => 'string',
            'line_items.*.grams'                => 'numeric',
            'line_items.*.requires_shipping'    => 'boolean',

            'shipping_line.title'       => 'string',
            'shipping_line.price'       => 'numeric',
            'shipping_line.handle'      => new IsHandle(),
            ],
            $this->getMailingAddressRules('shipping_address'),
            $this->getMailingAddressRules('billing_address'),
            $this->getDiscountRules('line_items.*.applied_discount'),
            $this->getDiscountRules('applied_discount'),
        );
    }

    private function getMailingAddressRules($base)
    {
        return [
            $base . '.address1'         => 'string',
            $base . '.address2'         => 'string',
            $base . '.city'             => 'string',
            $base . '.company'          => 'string',
            $base . '.country'          => 'string',
            $base . '.country_code'     => 'string|size:2',
            $base . '.first_name'       => 'string',
            $base . '.last_name'        => 'string',
            $base . '.latitude'         => 'numeric',
            $base . '.longitude'        => 'numeric',
            $base . '.name'             => 'string',
            $base . '.phone'            => 'string',
            $base . '.province'         => 'string',
            $base . '.province_code'    => 'string|size:2',
            $base . '.zip'              => 'string',

        ];
    }

    private function getDiscountRules($base)
    {
        return [
            $base . '.title'        => 'string',
            $base . '.description'  => 'string',
            $base . '.value'        => 'numeric',
            $base . '.amount'       => 'numeric',
            $base . '.value_type'   => Rule::in([DiscountValueType::FIXED_AMOUNT, DiscountValueType::PERCENTAGE]),
        ];
    }

    private function canadianTax()
    {
        $taxes = TaxExemption::toArray();

        $canadianTax = array_filter($taxes, function ($tax) {
            return Str::startsWith($tax, 'CA');
        });

        return [
            ...$canadianTax,
            'EXEMPT_ALL'
        ];
    }
}
