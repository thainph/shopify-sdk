<?php

namespace Thainph\ShopifySdk\Validation\Validators\Rest\Redirect;

use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class UpdateValidator extends BaseValidator
{
    public function rules(): array
    {
        return [
            'id'                            => 'required|integer',
            'path'                          => 'required|regex:/^(https?:\/\/[a-zA-Z0-9-_%]+)?(\/[a-zA-Z0-9-_%]+)*$/',
            'target'                        => 'required|regex:/^(https?:\/\/[a-zA-Z0-9-_%]+)?(\/[a-zA-Z0-9-_%]+)*$/',
        ];
    }
}
