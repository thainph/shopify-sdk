<?php

namespace Thainph\ShopifySdk\Validation\Validators\Rest\RecurringApplicationCharge;

use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class CreateValidator extends BaseValidator
{
    public function rules(): array
    {
        return [
            'activated_on'          => 'date',
            'billing_on'            => 'date',
            'cancelled_on'          => 'date',
            'capped_amount'         => 'numeric',
            'confirmation_url'      => 'url',
            'created_at'            => 'date',
            'name'                  => 'required|string',
            'price'                 => 'required|numeric|gt:0',
            'return_url'            => 'required|url',
            'terms'                 => 'string',
            'test'                  => 'boolean|nullable',
            'trial_days'            => 'integer',
            'trial_ends_on'         => 'date',
            'updated_at'            => 'date',
            'currency'              => 'string|size:3'
        ];
    }
}
