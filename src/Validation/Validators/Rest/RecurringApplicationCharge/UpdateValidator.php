<?php

namespace Thainph\ShopifySdk\Validation\Validators\Rest\RecurringApplicationCharge;

use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class UpdateValidator extends BaseValidator
{
    public function rules(): array
    {
        return [
            'id'                    => 'required|integer',
            'capped_amount'         => 'numeric',
        ];
    }
}
