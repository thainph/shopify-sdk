<?php

namespace Thainph\ShopifySdk\Validation\Validators\Rest\DiscountCode;

use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class BulkCreateValidator extends BaseValidator
{
    public function rules(): array
    {
        return [
            'discount_codes' => 'required|array|max:100',
            'discount_codes.*.code' => 'required',
        ];
    }
}
