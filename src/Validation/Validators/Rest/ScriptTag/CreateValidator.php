<?php

namespace Thainph\ShopifySdk\Validation\Validators\Rest\ScriptTag;

use Illuminate\Validation\Rule;
use Thainph\ShopifySdk\Enums\Rest\DisplayScope;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class CreateValidator extends BaseValidator
{
    public function rules(): array
    {
        return [
            'event'         => 'required|in:onload',
            'src'           => 'required|url|regex:/^https:/',
            'display_scope' => [
                Rule::in(DisplayScope::toArray())
            ],
            'cache'         => 'boolean'
        ];
    }
}
