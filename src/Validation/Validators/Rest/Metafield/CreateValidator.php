<?php

namespace Thainph\ShopifySdk\Validation\Validators\Rest\Metafield;

use Illuminate\Validation\Rule;
use Thainph\ShopifySdk\Enums\MetafieldType;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class CreateValidator extends BaseValidator
{
    public function rules(): array
    {
        return [
            'namespace' => 'required',
            'key' => 'required',
            'value' => 'required',
            'type' => [
                'required',
                Rule::in(MetafieldType::toArray())
            ]
        ];
    }
}
