<?php

namespace Thainph\ShopifySdk\Validation\Validators\Rest\Order;

use Illuminate\Validation\Rule;
use Thainph\ShopifySdk\Enums\MetafieldType;
use Thainph\ShopifySdk\Enums\Rest\FinancialStatus;
use Thainph\ShopifySdk\Enums\Rest\DiscountValueType;
use Thainph\ShopifySdk\Enums\Rest\FulfillmentStatus;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class UpdateValidator extends BaseValidator
{
    public function rules(): array
    {
        return array_merge(
            [
                'id'                        => 'required|integer',
                'total_tax'                 => 'numeric',
                'currency'                  => 'string|size:3',
                'email'                     => 'email',
                'phone'                     => 'string',
                'financial_status'          => Rule::in(FinancialStatus::toArray()),
                'fulfillment_status'        => Rule::in(FulfillmentStatus::toArray()),
                'fulfillments'              => 'array',
                'fulfillments.*.id'         => 'required_without:fulfillments.*.location_id|integer',
                'fulfillments.*.location_id'=> 'required_without:fulfillments.*.id|integer',
                'send_receipt'              => 'boolean',
                'send_fulfillment_receipt'  => 'boolean',

                'line_items'                => 'required|array',
                'line_items.*.variant_id'   => 'required_without:line_items.*.price|integer',
                'line_items.*.title'        => 'required_without:line_items.*.variant_id|string',
                'line_items.*.price'        => 'required_without:line_items.*.variant_id|numeric',
                'line_items.*.quantity'     => 'required|numeric',
                'line_items.*.grams'        => 'numeric',

                'line_items.*.tax_lines'            => 'array',
                'line_items.*.tax_lines.*.price'    => 'numeric',
                'line_items.*.tax_lines.*.rate'     => 'numeric',
                'line_items.*.tax_lines.*.title'    => 'string',

                'transactions'              => 'array',
                'transactions.*.amount'     => 'numeric',
                'transactions.*.kind'       => 'string',
                'transactions.*.status'     => 'string',

                'customer.id'               => 'required_without:customer.email|integer',
                'customer.email'            => 'required_without:customer.id|email',
                'customer.phone'            => 'string',
                'customer.first_name'       => 'required_without:customer.id|string',
                'customer.last_name'        => 'required_without:customer.id|string',
                'customer.tags'             => 'array',
                'customer.tags.*'           => 'string',
                'customer.note'             => 'string',
                'customer.locale'           => 'string',
                'customer.tax_exempt'       => 'boolean',

                'discount_codes'            => 'array',
                'discount_codes.*.code'     => 'string',
                'discount_codes.*.amount'   => 'numeric',
                'discount_codes.*.type'     => Rule::in(DiscountValueType::toArray()),

                'tax_lines'                 => 'array',
                'tax_lines.*.price'         => 'numeric',
                'tax_lines.*.rate'          => 'numeric',
                'tax_lines.*.title'         => 'string',
                
                

                'metafields'                => 'array',
                'metafields.*.key'          => 'string',
                'metafields.*.value'        => 'string',
                'metafields.*.type'         => Rule::in(MetafieldType::toArray()),
                'metafields.*.namespace'    => 'string',
            ],
            $this->getCustomerAddressRules('billing_address'),
            $this->getCustomerAddressRules('shipping_address'),
        );
    }

    private function getCustomerAddressRules(string $base): array
    {
        return [
            $base . '.address1'         => 'string|required',
            $base . '.address2'         => 'string',
            $base . '.city'             => 'string|required',
            $base . '.company'          => 'string',
            $base . '.country'          => 'string|required',
            $base . '.country_code'     => 'string|size:2',
            $base . '.first_name'       => 'string|required',
            $base . '.last_name'        => 'string|required',
            $base . '.latitude'         => 'numeric',
            $base . '.longitude'        => 'numeric',
            $base . '.name'             => 'string',
            $base . '.phone'            => 'string',
            $base . '.province'         => 'string|required',
            $base . '.province_code'    => 'string',
            $base . '.zip'              => 'string|required',
        ];
    }
}
