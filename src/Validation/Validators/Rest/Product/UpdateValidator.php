<?php

namespace Thainph\ShopifySdk\Validation\Validators\Rest\Product;

use Illuminate\Validation\Rule;
use Thainph\ShopifySdk\Enums\Rest\WeightUnit;
use Thainph\ShopifySdk\Enums\MetafieldType;
use Thainph\ShopifySdk\Enums\Rest\PublishedScope;
use Thainph\ShopifySdk\Enums\Rest\ProductStatus;
use Thainph\ShopifySdk\Enums\Rest\InventoryPolicy;
use Thainph\ShopifySdk\Validation\Rules\IsBase64;
use Thainph\ShopifySdk\Validation\Rules\IsHandle;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class UpdateValidator extends BaseValidator
{
    public function rules(): array
    {
        return [
            'id'                            => 'required|integer',
            'title'                         => 'required|string',
            'handle'                        => new IsHandle(),
            'body_html'                     => 'string',
            'vendor'                        => 'string',
            'product_type'                  => 'string',
            'status'                        => Rule::in(ProductStatus::toArray()),
            'published_at'                  => 'date',
            'published_scope'               => Rule::in(PublishedScope::toArray()),
            'template_suffix'               => 'string',

            'tags'                          => 'array',
            'tags.*'                        => 'string',

            'images'                        => 'array',
            'images.*.attachment'           => [
                new IsBase64(),
                'required_without:images.*.src'
            ],
            'images.*.src'                  => 'url|required_without:images.*.attachment',
            'images.*.alt'                  => 'string',
            'images.*.width'                => 'integer|min:1',
            'images.*.height'               => 'integer|min:1',

            'options'                       => 'array',
            'options.*.name'                => 'required|string',
            'options.*.values'              => 'required|array',
            'options.*.values.*'            => 'required|string',

            'metafields'                    => 'array',
            'metafields.*.namespace'        => 'required|string',
            'metafields.*.key'              => 'required|string',
            'metafields.*.value'            => 'required|string',
            'metafields.*.type'             => [
                'required',
                Rule::in(MetafieldType::toArray())
            ],

            'variants'                          => 'array',
            'variants.*.barcode'                => 'string',
            'variants.*.grams'                  => 'integer',
            'variants.*.compare_at_price'       => 'numeric',
            'variants.*.fulfillment_service'    => 'string',
            'variants.*.image_id'               => 'integer',
            'variants.*.inventory_item_id'      => 'integer',
            'variants.*.inventory_management'   => 'string',
            'variants.*.inventory_policy'       => Rule::in(InventoryPolicy::toArray()),
            'variants.*.option1'                => 'required|string',
            'variants.*.option2'                => 'string',
            'variants.*.option3'                => 'string',
            'variants.*.price'                  => 'numeric',
            'variants.*.product_id'             => 'required|integer',
            'variants.*.sku'                    => 'string',
            'variants.*.taxable'                => 'boolean',
            'variants.*.title'                  => 'string',
            'variants.*.weight'                 => 'numeric',
            'variants.*.weight_unit'            => Rule::in(WeightUnit::toArray()),

            'variants.*.metafields'                    => 'array',
            'variants.*.metafields.*.namespace'        => 'required|string',
            'variants.*.metafields.*.key'              => 'required|string',
            'variants.*.metafields.*.value'            => 'required|string',
            'variants.*.metafields.*.type'             => [
                'required',
                Rule::in(MetafieldType::toArray())
            ],
        ];
    }
}
