<?php

namespace Thainph\ShopifySdk\Validation\Validators\Rest\Article;

use Illuminate\Validation\Rule;
use Thainph\ShopifySdk\Enums\MetafieldType;
use Thainph\ShopifySdk\Validation\Rules\IsBase64;
use Thainph\ShopifySdk\Validation\Rules\IsHandle;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class CreateValidator extends BaseValidator
{
    public function rules(): array
    {
        return [
            'title'             => 'string|required',
            'author'            => 'string',
            'tags'              => 'string',
            'body_html'         => 'string',
            'summary_html'      => 'string',
            'handle'            => new IsHandle(),
            'published'         => 'boolean',
            'published_at'      => 'date',
            'template_suffix'   => 'string',

            'image.src'         => 'string|url',
            'image.attachment'  => new IsBase64(),
            'image.alt'         => 'string',

            'metafields'                    => 'array',
            'metafields.*.namespace'        => 'required|string',
            'metafields.*.key'              => 'required|string',
            'metafields.*.value'            => 'required|string',
            'metafields.*.type'             => [
                'required',
                Rule::in(MetafieldType::toArray())
            ],
        ];
    }
}
