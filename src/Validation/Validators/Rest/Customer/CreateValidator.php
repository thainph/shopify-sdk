<?php

namespace Thainph\ShopifySdk\Validation\Validators\Rest\Customer;

use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class CreateValidator extends BaseValidator
{
    public function rules(): array
    {
        return [
            'first_name'    => 'string',
            'last_name'     => 'string',
            'email'         => 'string',
            'phone'         => 'string',
            'tags'          => 'array',
            'tags.*'        => 'string',
            'note'          => 'string',
            'locale'        => 'string',
            'tax_exempt'    => 'boolean',
        ];
    }
}
