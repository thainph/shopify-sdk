<?php

namespace Thainph\ShopifySdk\Validation\Validators\Rest\Theme;

use Illuminate\Validation\Rule;
use Thainph\ShopifySdk\Enums\Rest\ThemeRole;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class UpdateValidator extends BaseValidator
{
    public function rules(): array
    {
        return [
            'id'            => 'required|integer',
            'name'          => 'string',
            'src'           => 'string|url',
            'role'          => Rule::in(ThemeRole::toArray()),
        ];
    }
}
