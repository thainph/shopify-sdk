<?php

namespace Thainph\ShopifySdk\Validation\Validators\Rest\Theme;

use Illuminate\Validation\Rule;
use Thainph\ShopifySdk\Enums\Rest\ThemeRole;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class CreateValidator extends BaseValidator
{
    public function rules(): array
    {
        return [
            'name'          => 'string|required',
            'src'           => 'string|url',
            'role'          => Rule::in(ThemeRole::toArray()),
        ];
    }
}
