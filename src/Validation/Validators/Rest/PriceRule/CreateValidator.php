<?php

namespace Thainph\ShopifySdk\Validation\Validators\Rest\PriceRule;

use Illuminate\Validation\Rule;
use Thainph\ShopifySdk\Enums\Rest\TargetType;
use Thainph\ShopifySdk\Enums\Rest\AllocationMethod;
use Thainph\ShopifySdk\Enums\Rest\CustomerSelection;
use Thainph\ShopifySdk\Enums\Rest\DiscountValueType;
use Thainph\ShopifySdk\Enums\Rest\TargetSelection;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class CreateValidator extends BaseValidator
{
    public function rules(): array
    {
        return [
            'title'                         => 'required|string',
            'value_type'                    => [
                'required',
                Rule::in([DiscountValueType::FIXED_AMOUNT, DiscountValueType::PERCENTAGE])
            ],
            'value'                         => 'required|numeric',
            'customer_selection'            => [
                'required',
                Rule::in(CustomerSelection::toArray())
            ],
            'target_type'                   => [
                'required',
                Rule::in(TargetType::toArray())
            ],
            'target_selection'              => [
                'required',
                Rule::in(TargetSelection::toArray())
            ],
            'allocation_method'             => [
                'required',
                Rule::in(AllocationMethod::toArray())
            ],
            'starts_at'                     => 'required|date',
            'ends_at'                       => 'date|after:starts_at',
            'prerequisite_collection_ids'   => 'array',
            'prerequisite_collection_ids.*' => 'integer',
            'entitled_collection_ids'       => 'array',
            'entitled_collection_ids.*'     => 'integer',
            'entitled_country_ids'          => 'array',
            'entitled_country_ids.*'        => 'integer',
            'entitled_product_ids'          => 'array',
            'entitled_product_ids.*'        => 'integer',
            'prerequisite_product_ids'      => 'array',
            'prerequisite_product_ids.*'    => 'integer',
            'entitled_variant_ids'          => 'array',
            'entitled_variant_ids.*'        => 'integer',
            'prerequisite_variant_ids'      => 'array',
            'prerequisite_variant_ids.*'    => 'integer',
            'once_per_customer'             => 'boolean',
            'prerequisite_customer_ids'     => 'array',
            'prerequisite_customer_ids.*'   => 'integer',
            'customer_segment_prerequisite_ids'                                 => 'array',
            'customer_segment_prerequisite_ids.*'                               => 'integer',
            'prerequisite_to_entitlement_quantity_ratio.prerequisite_quantity'  => 'integer',
            'prerequisite_to_entitlement_quantity_ratio.entitled_quantity'      => 'integer',
            'allocation_limit'                                                  => 'integer',
            'prerequisite_quantity_range.greater_than_or_equal_to'              => 'integer',
            'prerequisite_shipping_price_range.less_than_or_equal_to'           => 'numeric',
            'prerequisite_subtotal_range.greater_than_or_equal_to'              => 'numeric',
            'prerequisite_to_entitlement_purchase.prerequisite_amount'          => 'numeric',
            'prerequisite_subtotal_range.greater_than_or_equal_to'              => 'numeric',
            'prerequisite_to_entitlement_purchase.prerequisite_amount'          => 'numeric',
            'usage_limit'                                                       => 'integer',
        ];
    }
}
