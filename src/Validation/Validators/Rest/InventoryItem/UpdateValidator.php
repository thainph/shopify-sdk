<?php

namespace Thainph\ShopifySdk\Validation\Validators\Rest\InventoryItem;

use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class UpdateValidator extends BaseValidator
{
    public function rules(): array
    {
        return [
            'id'                        => 'required|integer',
            'cost'                      => 'numeric',
            'country_code_of_origin'    => 'string|size:2',
            'harmonized_system_code'    => 'string|min:6|max:13',
            'province_code_of_origin'   => 'string|size:2',
            'sku'                       => 'string',
            'tracked'                   => 'boolean',
            'requires_shipping'         => 'boolean',
        ];
    }
}
