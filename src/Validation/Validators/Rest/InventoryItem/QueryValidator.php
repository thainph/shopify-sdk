<?php

namespace Thainph\ShopifySdk\Validation\Validators\Rest\InventoryItem;

use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class QueryValidator extends BaseValidator
{
    public function rules(): array
    {
        return [
            'ids'                       => 'string|required',
            'limit'                     => 'integer|gt:0|lte:250',
        ];
    }
}
