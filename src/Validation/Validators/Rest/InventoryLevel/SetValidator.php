<?php

namespace Thainph\ShopifySdk\Validation\Validators\Rest\InventoryLevel;

use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class SetValidator extends BaseValidator
{
    public function rules(): array
    {
        return [
            'inventory_item_id'             => 'required|integer',
            'location_id'                   => 'required|integer',
            'available'                     => 'required|integer',
            'disconnect_if_necessary'       => 'boolean',
        ];
    }
}
