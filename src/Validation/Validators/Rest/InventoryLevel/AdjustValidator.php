<?php

namespace Thainph\ShopifySdk\Validation\Validators\Rest\InventoryLevel;

use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class AdjustValidator extends BaseValidator
{
    public function rules(): array
    {
        return [
            'available_adjustment'  => 'required|integer',
            'location_id'           => 'required|integer',
            'inventory_item_id'     => 'required|integer',
        ];
    }
}
