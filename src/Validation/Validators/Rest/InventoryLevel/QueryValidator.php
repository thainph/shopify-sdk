<?php

namespace Thainph\ShopifySdk\Validation\Validators\Rest\InventoryLevel;

use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class QueryValidator extends BaseValidator
{
    public function rules(): array
    {
        return [
            'inventory_item_ids'    => 'required_without:location_ids|string',
            'location_ids'          => 'required_without:inventory_item_ids|string',
        ];
    }
}
