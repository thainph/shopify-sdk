<?php

namespace Thainph\ShopifySdk\Validation\Validators\Rest\CustomerAddress;

use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class BulkRemoveValidator extends BaseValidator
{
    public function rules(): array
    {
        return [
            'address_ids'   => 'array|required',
            'address_ids.*' => 'numeric|required'
        ];
    }
}
