<?php

namespace Thainph\ShopifySdk\Validation\Validators\Rest\CustomerAddress;

use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class UpdateValidator extends BaseValidator
{
    public function rules(): array
    {
        return [
            'id'               => 'numeric|required',
            'address1'         => 'string|required',
            'address2'         => 'string',
            'city'             => 'string|required',
            'company'          => 'string',
            'country'          => 'string|required',
            'country_name'     => 'string',
            'country_code'     => 'string|size:2',
            'first_name'       => 'string|required',
            'last_name'        => 'string|required',
            'latitude'         => 'numeric',
            'longitude'        => 'numeric',
            'name'             => 'string',
            'phone'            => 'string',
            'province'         => 'string|required',
            'province_code'    => 'string',
            'zip'              => 'string|required',
        ];
    }
}
