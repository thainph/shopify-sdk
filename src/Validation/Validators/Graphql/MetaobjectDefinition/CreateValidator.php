<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\MetaobjectDefinition;

use Illuminate\Validation\Rule;
use Thainph\ShopifySdk\Enums\Graphql\MetaobjectAdminAccess;
use Thainph\ShopifySdk\Enums\Graphql\MetaobjectStorefrontAccess;
use Thainph\ShopifySdk\Enums\MetafieldType;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class CreateValidator extends BaseValidator
{
    public function rules(): array
    {
        return [
            'access.admin'              => Rule::in(MetaobjectAdminAccess::toArray()),
            'access.storefront'         => Rule::in(MetaobjectStorefrontAccess::toArray()),
            'description'               => 'string',
            'displayNameKey'            => 'string',
            'name'                      => 'string',
            'type'                      => 'required|regex:/^[a-zA-Z0-9_-]+$/',

            'fieldDefinitions'              => 'required|array',
            'fieldDefinitions.*.name'       => 'string',
            'fieldDefinitions.*.type'       => [
                'required',
                Rule::in(MetafieldType::toArray())
            ],
            'fieldDefinitions.*.description' => 'string',
            'fieldDefinitions.*.key'        => 'required|string',
            'fieldDefinitions.*.required'   => 'boolean',
            'fieldDefinitions.*.validations'            => 'array',
            'fieldDefinitions.*.validations.*.name'     => 'string',
            'fieldDefinitions.*.validations.*.value'    => 'string',

            'capabilities.onlineStore.data.createRedirects'     => 'boolean',
            'capabilities.onlineStore.data.urlHandle'           => 'string|url',
            'capabilities.onlineStore.enabled'                  => 'boolean',
            'capabilities.publishable.enabled'                  => 'boolean',
            'capabilities.renderable.data.metaDescriptionKey'   => 'string',
            'capabilities.renderable.data.metaTitleKey'         => 'string',
            'capabilities.renderable.enabled'                   => 'boolean',
            'capabilities.translatable.enabled'                 => 'boolean',
        ];
    }
}
