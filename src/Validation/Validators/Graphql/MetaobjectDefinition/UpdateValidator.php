<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\MetaobjectDefinition;

use Illuminate\Validation\Rule;
use Thainph\ShopifySdk\Exceptions\ObjectNotFound;
use Thainph\ShopifySdk\Enums\Graphql\MetaobjectAdminAccess;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;
use Thainph\ShopifySdk\Enums\Graphql\MetaobjectStorefrontAccess;

class UpdateValidator extends BaseValidator
{
    /**
     * @throws ObjectNotFound
     */
    public function rules(): array
    {
        return [
            'access.admin'              => Rule::in(MetaobjectAdminAccess::toArray()),
            'access.storefront'         => Rule::in(MetaobjectStorefrontAccess::toArray()),
            'description'               => 'string',
            'displayNameKey'            => 'string',
            'name'                      => 'string',
            'resetFieldOrder'           => 'boolean',

            'capabilities.onlineStore.data.createRedirects'     => 'boolean',
            'capabilities.onlineStore.data.urlHandle'           => 'string|url',
            'capabilities.onlineStore.enabled'                  => 'boolean',
            'capabilities.publishable.enabled'                  => 'boolean',
            'capabilities.renderable.data.metaDescriptionKey'   => 'string',
            'capabilities.renderable.data.metaTitleKey'         => 'string',
            'capabilities.renderable.enabled'                   => 'boolean',
            'capabilities.translatable.enabled'                 => 'boolean',
        ];
    }
}
