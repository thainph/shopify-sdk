<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\MetaobjectDefinition;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Validation\Rules\IsShopifyGraphId;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class IsMetaobjectDefinitionId extends BaseValidator
{
    public function rules(): array
    {
        return [
            'id' => [
                'required',
                new IsShopifyGraphId(ShopifyObject::METAOBJECT_DEFINITION)
            ],
        ];
    }
}
