<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\Discount;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Validation\Rules\IsOneShopifyGraphIdOff;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class IsDiscountId extends BaseValidator
{
    public function rules(): array
    {
        return [
            'id' => [
                'required',
                new IsOneShopifyGraphIdOff([
                    ShopifyObject::DISCOUNT_AUTOMATIC,
                    ShopifyObject::DISCOUNT_CODE
                ])
            ]
        ];
    }
}
