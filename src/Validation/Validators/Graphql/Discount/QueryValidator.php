<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\Discount;

use Illuminate\Validation\Rule;
use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Enums\SortKey\DiscountSortKey;
use Thainph\ShopifySdk\Validation\Rules\IsShopifyGraphId;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class QueryValidator extends BaseValidator
{
    public function rules(): array
    {
        return [
            'after' => 'string',
            'before' => 'string|required_with:last',
            'first' => 'integer|gt:0|lte:250|required_without:last',
            'last' => 'integer|gt:0|lte:250|required_without:first',
            'reverse' => 'boolean',
            'query' => 'string',
            'savedSearchId' => new IsShopifyGraphId(ShopifyObject::SAVED_SEARCH),
            'sortKey' => [
                'string',
                Rule::in(DiscountSortKey::toArray())
            ],
        ];
    }
}
