<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\DraftOrder;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;
use Thainph\ShopifySdk\Validation\Rules\IsShopifyGraphId;

class IsDraftOrderId extends BaseValidator
{
    public function rules() : array
    {
        return [
            'id' => [
                'required',
                new IsShopifyGraphId(ShopifyObject::DRAFT_ORDER)
            ]
        ];
    }
}
