<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\DraftOrder;

use Illuminate\Validation\Rule;
use Thainph\ShopifySdk\Enums\MetafieldType;
use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Enums\Graphql\WeightUnit;
use Thainph\ShopifySdk\Enums\Graphql\DiscountValueType;
use Thainph\ShopifySdk\Validation\Rules\IsShopifyGraphId;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;
use Thainph\ShopifySdk\Enums\Graphql\LocalizationExtensionKey;

class InputValidator extends BaseValidator
{
    public function rules(): array
    {
        return array_merge(
            [
                'email'                         => 'email',
                'marketRegionCountryCode'       => 'string|size:2',
                'note'                          => 'string',
                'phone'                         => 'string',
                'poNumber'                      => 'string',
                'presentmentCurrencyCode'       => 'string|size:3',
                'customAttributes'              => 'array',
                'customAttributes.*.key'        => 'required|string',
                'customAttributes.*.value'      => 'required|string',
                'reserveInventoryUntil'         => 'date',
                'shippingLine.price'                => 'numeric',
                'shippingLine.title'                => 'string',
                'shippingLine.shippingRateHandle'   => 'string',
                'sourceName'                        => 'string',
                'tags'                              => 'array',
                'tags.*'                            => 'string',
                'taxExempt'                         => 'boolean',
                'useCustomerDefaultAddress'         => 'boolean',
                'visibleToCustomer'                 => 'boolean',

                'lineItems'                             => 'array',
                'lineItems.*.customAttributes'          => 'array',
                'lineItems.*.customAttributes.*.key'    => 'required|string',
                'lineItems.*.customAttributes.*.value'  => 'required|string',
                'lineItems.*.grams'                     => 'integer',
                'lineItems.*.originalUnitPrice'         => 'numeric',
                'lineItems.*.quantity'                  => 'integer|required',
                'lineItems.*.requiresShipping'          => 'boolean',
                'lineItems.*.sku'                       => 'string',
                'lineItems.*.title'                     => 'string',
                'lineItems.*.taxable'                   => 'boolean',
                'lineItems.*.variantId'                 => new IsShopifyGraphId(ShopifyObject::PRODUCT_VARIANT),
                'lineItems.*.weight.unit'               => Rule::in(WeightUnit::toArray()),
                'lineItems.*.weight.value'              => 'numeric',

                'localizationExtensions'                => 'array',
                'localizationExtensions.*.key'          => [
                    'required',
                    Rule::in(LocalizationExtensionKey::toArray()),
                ],
                'localizationExtensions.*.value'        => 'required|string',

                'metafields'                            => 'array',
                'metafields.*.namespace'                => 'required',
                'metafields.*.key'                      => 'required',
                'metafields.*.value'                    => 'required',
                'metafields.*.type'                     => [
                    'required',
                    Rule::in(MetafieldType::toArray())
                ],

                'paymentTerms.paymentSchedules'             => 'array',
                'paymentTerms.paymentSchedules.*.dueAt'     => 'date',
                'paymentTerms.paymentSchedules.*.issuedAt'  => 'numeric',

                'purchasingEntity.customerId'                           => new IsShopifyGraphId(ShopifyObject::CUSTOMER),
                'purchasingEntity.purchasingCompany.companyContactId'   => new IsShopifyGraphId(ShopifyObject::COMPANY_CONTACT),
                'purchasingEntity.purchasingCompany.companyId'          => new IsShopifyGraphId(ShopifyObject::COMPANY),
                'purchasingEntity.purchasingCompany.companyLocationId'  => new IsShopifyGraphId(ShopifyObject::COMPANY_LOCATION),
            ],
            $this->getDiscountRules('appliedDiscount'),
            $this->getMailingAddressRules('shippingAddress'),
            $this->getMailingAddressRules('billingAddress'),
            $this->getDiscountRules('lineItems.*.appliedDiscount'),
        );
    }

    private function getMailingAddressRules($base)
    {
        return [
            $base . '.address1'         => 'string',
            $base . '.address2'         => 'string',
            $base . '.city'             => 'string',
            $base . '.company'          => 'string',
            $base . '.country'          => 'string',
            $base . '.countryCode'      => 'string|size:2',
            $base . '.firstName'        => 'string',
            $base . '.lastName'         => 'string',
            $base . '.phone'            => 'string',
            $base . '.province'         => 'string',
            $base . '.provinceCode'     => 'string|size:2',
            $base . '.zip'              => 'string',
        ];
    }

    private function getDiscountRules($base)
    {
        return [
            $base . '.title'        => 'string',
            $base . '.description'  => 'string',
            $base . '.value'        => 'numeric',
            $base . '.amount'       => 'numeric',
            $base . '.valueType'    => Rule::in([DiscountValueType::FIXED_AMOUNT, DiscountValueType::PERCENTAGE]),
        ];
    }
}
