<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\DraftOrder;

use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class SendInvoiceValidator extends BaseValidator
{
    public function rules() : array
    {
        return [
            'bcc'           => 'array',
            'bcc.*'         => 'email',
            'body'          => 'string',
            'customMessage' => 'string',
            'from'          => 'email',
            'subject'       => 'string',
            'to'            => 'email',
        ];
    }
}
