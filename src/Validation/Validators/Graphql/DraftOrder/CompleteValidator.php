<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\DraftOrder;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Validation\Rules\IsShopifyGraphId;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class CompleteValidator extends BaseValidator
{
    public function rules() : array
    {
        return [
            'paymentGatewayId'  => new IsShopifyGraphId(ShopifyObject::PAYMENT_GATEWAY),
            'paymentPending'    => 'boolean',
            'sourceName'        => 'string',
        ];
    }
}
