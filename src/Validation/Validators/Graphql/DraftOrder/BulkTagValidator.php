<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\DraftOrder;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;
use Thainph\ShopifySdk\Validation\Rules\IsShopifyGraphId;

class BulkTagValidator extends BaseValidator
{
    public function rules(): array
    {
        return [
            'savedSearchId'     => [
                'required_without_all:search,ids',
                new IsShopifyGraphId(ShopifyObject::SAVED_SEARCH)
            ],
            'search'            => 'string|required_without_all:savedSearchId,ids',
            'ids'               => 'array|required_without_all:savedSearchId,search',
            'ids.*'             => new IsShopifyGraphId(ShopifyObject::DRAFT_ORDER),
            'tags'              => 'array',
            'tags.*'            => 'string'
        ];
    }
}
