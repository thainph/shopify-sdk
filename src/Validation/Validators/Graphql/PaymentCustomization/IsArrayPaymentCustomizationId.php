<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\PaymentCustomization;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Validation\Rules\IsShopifyGraphId;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class IsArrayPaymentCustomizationId extends BaseValidator
{
    public function rules(): array
    {
        return [
            'ids'       => 'array|required',
            'ids.*'     => [
                'required',
                new IsShopifyGraphId(ShopifyObject::PAYMENT_CUSTOMIZATION)
            ],
        ];
    }
}
