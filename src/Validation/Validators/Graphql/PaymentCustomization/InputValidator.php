<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\PaymentCustomization;

use Illuminate\Validation\Rule;
use Thainph\ShopifySdk\Enums\MetafieldType;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class InputValidator extends BaseValidator
{
    public function rules(): array
    {
        return [
            'enabled'       => 'boolean|required',
            'functionId'    => 'string',
            'title'         => 'string|required',

            'metafields'                => 'array',
            'metafields.*.namespace'    => 'required|string',
            'metafields.*.key'          => 'required|string',
            'metafields.*.value'        => 'required|string',
            'metafields.*.type'         => [
                'required',
                Rule::in(MetafieldType::toArray())
            ],
        ];
    }
}
