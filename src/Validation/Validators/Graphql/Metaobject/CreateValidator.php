<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\Metaobject;

use Illuminate\Validation\Rule;
use Thainph\ShopifySdk\Exceptions\ObjectNotFound;
use Thainph\ShopifySdk\Validation\Rules\IsHandle;
use Thainph\ShopifySdk\Enums\Graphql\MetaobjectStatus;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class CreateValidator extends BaseValidator
{
    /**
     * @throws ObjectNotFound
     */
    public function rules(): array
    {
        return [
            'handle'        => [
                'string',
                new IsHandle()
            ],
            'type'          => [
                'required',
                'string',
                new IsHandle()
            ],

            'capabilities.onlineStore.templateSuffix'   => 'string',
            'capabilities.publishable.status'           => Rule::in(MetaobjectStatus::toArray()),

            'fields'        => 'array',
            'fields.*.name' => 'required|string',
            'fields.*.type' => 'required|string',
        ];
    }
}
