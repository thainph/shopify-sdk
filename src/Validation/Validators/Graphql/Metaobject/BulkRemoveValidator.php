<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\Metaobject;

use Thainph\ShopifySdk\Exceptions\ObjectNotFound;
use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Validation\Rules\IsShopifyGraphId;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class BulkRemoveValidator extends BaseValidator
{
    /**
     * @throws ObjectNotFound
     */
    public function rules(): array
    {
        return [
            'type'          => 'required_without:ids|regex:/^[a-zA-Z0-9_-]+$/',
            'ids'           => 'required_without:type|array',
            'ids.*'         => new IsShopifyGraphId(ShopifyObject::METAOBJECT),
        ];
    }
}
