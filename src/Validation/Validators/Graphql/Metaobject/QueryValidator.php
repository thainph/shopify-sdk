<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\Metaobject;

use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class QueryValidator extends BaseValidator
{
    public function rules(): array
    {
        return [
            'after' => 'string',
            'before' => 'string|required_with:last',
            'first' => 'integer|gt:0|lte:250|required_without:last',
            'last' => 'integer|gt:0|lte:250|required_without:first',
            'reverse' => 'boolean',
            'query' => 'string',
            'sortKey' => 'string',
            'type' => 'string|required'
        ];
    }
}
