<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\Order;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;
use Thainph\ShopifySdk\Validation\Rules\IsShopifyGraphId;

class IsCalculatedOrderId extends BaseValidator
{
    public function rules(): array
    {
        return [
            'id' => [
                'required',
                new IsShopifyGraphId(ShopifyObject::CALCULATED_ORDER)
            ]
        ];
    }
}
