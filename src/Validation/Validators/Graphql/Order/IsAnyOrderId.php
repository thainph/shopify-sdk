<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\Order;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Validation\Rules\IsOneShopifyGraphIdOff;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class IsAnyOrderId extends BaseValidator
{
    public function rules(): array
    {
        return [
            'id' => [
                'required',
                new IsOneShopifyGraphIdOff([
                    ShopifyObject::ORDER,
                    ShopifyObject::CALCULATED_ORDER
                ])
            ]
        ];
    }
}
