<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\Order;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Validation\Rules\IsShopifyGraphId;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class CaptureValidator extends BaseValidator
{
    public function rules(): array
    {
        return [
            'id'        => [
                'required',
                new IsShopifyGraphId(ShopifyObject::ORDER)
            ],
            'amount'    => 'required|numeric',
            'currency'  => 'string|size:3',
            'parentTransactionId' => [
                // 'required',
                new IsShopifyGraphId(ShopifyObject::TRANSACTION)
            ],
        ];
    }
}
