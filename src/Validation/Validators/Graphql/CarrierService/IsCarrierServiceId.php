<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\CarrierService;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Validation\Rules\IsShopifyGraphId;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class IsCarrierServiceId extends BaseValidator
{
    public function rules(): array
    {
        return [
            'id' => [
                'required',
                new IsShopifyGraphId(ShopifyObject::CARRIER_SERVICE)
            ]
        ];
    }
}
