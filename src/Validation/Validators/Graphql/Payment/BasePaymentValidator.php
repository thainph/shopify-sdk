<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\Payment;

use Illuminate\Validation\Rule;
use Thainph\ShopifySdk\Enums\Graphql\Payment\Version;
use Thainph\ShopifySdk\Enums\Graphql\Payment\TransStatus;
use Thainph\ShopifySdk\Enums\Graphql\Payment\PartnerError;
use Thainph\ShopifySdk\Enums\Graphql\Payment\AuthenticationFlow;
use Thainph\ShopifySdk\Enums\Graphql\Payment\ChargebackLiability;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class BasePaymentValidator extends BaseValidator
{
    protected function authenticationRules(): array
    {
        return [
            'authentication.partnerError'           => Rule::in(PartnerError::toArray()),
            'authentication.authenticationData.authenticationFlow'  => [
                'required_with:authentication.authenticationData',
                Rule::in(AuthenticationFlow::toArray()),
            ],
            'authentication.authenticationData.chargebackLiability' => [
                'required_with:authentication.authenticationData',
                Rule::in(ChargebackLiability::toArray())
            ],
            'authentication.authenticationData.dsTransactionId'     => 'string',
            'authentication.authenticationData.transStatus'         => [
                'required_with:authentication.authenticationData',
                Rule::in(TransStatus::toArray())
            ],
            'authentication.authenticationData.transStatusReason'   => [
                'string',
                'required_if:authentication.authenticationData.transStatus,N,U,R',
                'regex:/^A(0[1-9]|1[0-9]|2[0-6]|[89][0-9])z$/'
            ],
            'authentication.authenticationData.version' => [
                'required_with:authentication.authenticationData',
                Rule::in(Version::toArray())
            ]
        ];
    }
}
