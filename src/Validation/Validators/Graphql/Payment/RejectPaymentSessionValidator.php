<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\Payment;

use Illuminate\Validation\Rule;
use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Validation\Rules\IsShopifyGraphId;
use Thainph\ShopifySdk\Enums\Graphql\Payment\PaymentReasonCode;

class RejectPaymentSessionValidator extends BasePaymentValidator
{
    public function rules(): array
    {
        return array_merge(
            $this->authenticationRules(),
            [
                'id'                        => [
                    'required',
                    new IsShopifyGraphId(ShopifyObject::PAYMENT_SESSION)
                ],
                'reason'                    => 'required',
                'reason.code'               => [
                    'required',
                    Rule::in(PaymentReasonCode::paymentSessionReject())
                ],
                'reason.merchantMessage'    => 'string',
            ]
        );
    }
}
