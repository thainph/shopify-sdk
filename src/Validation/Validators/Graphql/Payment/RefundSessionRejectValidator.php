<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\Payment;

use Illuminate\Validation\Rule;
use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Validation\Rules\IsShopifyGraphId;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;
use Thainph\ShopifySdk\Enums\Graphql\Payment\PaymentReasonCode;

class RefundSessionRejectValidator extends BaseValidator
{
    public function rules(): array
    {
        return [
            'id'                       => [
                'required',
                new IsShopifyGraphId(ShopifyObject::PAYMENT_SESSION)
            ],
            'reason.code'              => 'string',
            'reason.merchantMessage'   => Rule::in(PaymentReasonCode::refundSessionReject()),
        ];
    }
}
