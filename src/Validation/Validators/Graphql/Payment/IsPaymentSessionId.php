<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\Payment;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Validation\Rules\IsShopifyGraphId;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class IsPaymentSessionId extends BaseValidator
{
    public function rules(): array
    {
        return [
            'id' => [
                'required',
                new IsShopifyGraphId(ShopifyObject::PAYMENT_SESSION)
            ],
        ];
    }
}
