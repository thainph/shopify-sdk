<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\Payment;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Validation\Rules\IsShopifyGraphId;

class ResolvePaymentSessionValidator extends BasePaymentValidator
{
    public function rules(): array
    {
        return array_merge(
            $this->authenticationRules(),
            [
                'id'                        => [
                    'required',
                    new IsShopifyGraphId(ShopifyObject::PAYMENT_SESSION)
                ],
                'authorizationExpiresAt'    => 'date',
                'networkTransactionId'      => 'string',
            ]
        );
    }
}
