<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\Payment;

use Illuminate\Validation\Rule;
use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Validation\Rules\IsShopifyGraphId;
use Thainph\ShopifySdk\Enums\Graphql\Payment\PaymentReasonCode;

class CaptureRejectValidator extends BasePaymentValidator
{
    public function rules(): array
    {
        return [
            'id'                        => [
                'required',
                new IsShopifyGraphId(ShopifyObject::PAYMENT_SESSION)
            ],
            'reason'                    => 'required',
            'reason.code'               => [
                'required',
                Rule::in(PaymentReasonCode::captureSessionReject())
            ],
            'reason.merchantMessage'    => 'string',
        ];
    }
}
