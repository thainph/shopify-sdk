<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\Payment;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Validation\Rules\IsShopifyGraphId;

class VerificationSessionResolveValidator extends BasePaymentValidator
{
    public function rules(): array
    {
        return [
            'id'                        => [
                'required',
                new IsShopifyGraphId(ShopifyObject::PAYMENT_SESSION)
            ],
            'networkTransactionId'      => 'string',
        ];
    }
}
