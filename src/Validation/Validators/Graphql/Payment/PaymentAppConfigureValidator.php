<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\Payment;

class PaymentAppConfigureValidator extends BasePaymentValidator
{
    public function rules(): array
    {
        return [
            'ready' => 'required|boolean',
            'externalHandle' => [
                'required_if:ready,true',
                'string',
            ],
        ];
    }
}
