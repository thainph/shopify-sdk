<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\Payment;

use Illuminate\Validation\Rule;
use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Validation\Rules\IsShopifyGraphId;
use Thainph\ShopifySdk\Enums\Graphql\Payment\PaymentReasonCode;

class PendingPaymentSessionValidator extends BasePaymentValidator
{
    public function rules(): array
    {
        return [
            'id'                        => [
                'required',
                new IsShopifyGraphId(ShopifyObject::PAYMENT_SESSION)
            ],
            'pendingExpiresAt'          => 'required|date',
            'reason'               => [
                'required',
                Rule::in(PaymentReasonCode::paymentSessionPending())
            ]
        ];
    }
}
