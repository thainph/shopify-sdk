<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\Variant;

use Illuminate\Validation\Rule;
use Thainph\ShopifySdk\Enums\Graphql\InventoryManagement;
use Thainph\ShopifySdk\Enums\Graphql\InventoryPolicy;
use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Enums\Graphql\WeightUnit;
use Thainph\ShopifySdk\Validation\Rules\IsOneShopifyGraphIdOff;
use Thainph\ShopifySdk\Validation\Rules\IsShopifyGraphId;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class CreateValidator extends BaseValidator
{
    public function rules(): array
    {
        return [
            'barcode'                                   => 'string',
            'compareAtPrice'                            => 'numeric',
            'fulfillmentServiceId'                      => new IsShopifyGraphId(ShopifyObject::FULFILL_SERVICE),
            'mediaId'                                   => new IsOneShopifyGraphIdOff([
                ShopifyObject::MEDIA_IMAGE,
                ShopifyObject::MEDIA_VIDEO,
                ShopifyObject::MEDIA_EXTERNAL_VIDEO,
                ShopifyObject::MEDIA_MODEL_3D
            ]),
            'inventoryItem.cost'                        => 'numeric',
            'inventoryItem.tracked'                     => 'boolean',
            'inventoryManagement'                       => Rule::in(InventoryManagement::toArray()),
            'inventoryPolicy'                           => Rule::in(InventoryPolicy::toArray()),
            'inventoryQuantities'                       => 'array',
            'inventoryQuantities.*.availableQuantity'   => 'integer',
            'inventoryQuantities.*.locationId'          => new IsShopifyGraphId(ShopifyObject::LOCATION),
            'options'                                   => 'required|array',
            'options.*'                                 => 'required|string',
            'price'                                     => 'numeric',
            'productId'                                 => [
                'required',
                new IsShopifyGraphId(ShopifyObject::PRODUCT)
            ],
            'sku'                                       => 'string',
            'taxable'                                   => 'boolean',
            'title'                                     => 'string',
            'weight'                                    => 'numeric',
            'weightUnit'                                => Rule::in(WeightUnit::toArray())
        ];
    }
}
