<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\Variant;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Validation\Rules\IsShopifyGraphId;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class IsArrayOfVariantId extends BaseValidator
{
    public function rules(): array
    {
        return [
            'ids' => 'required|array',
            'ids.*' => [
                'required',
                new IsShopifyGraphId(ShopifyObject::PRODUCT_VARIANT)
            ]
        ];
    }
}
