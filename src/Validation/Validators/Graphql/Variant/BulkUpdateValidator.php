<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\Variant;

use Illuminate\Validation\Rule;
use Thainph\ShopifySdk\Enums\Graphql\InventoryPolicy;
use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Enums\Graphql\WeightUnit;
use Thainph\ShopifySdk\Validation\Rules\IsShopifyGraphId;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class BulkUpdateValidator extends BaseValidator
{
    public function rules(): array
    {
        return [
            'variants'                                             => 'required|array',
            'variants.*.id'                                        => [
                'required',
                new IsShopifyGraphId(ShopifyObject::PRODUCT_VARIANT)
            ],
            'variants.*.barcode'                                   => 'string',
            'variants.*.compareAtPrice'                            => 'numeric',
            'variants.*.fulfillmentServiceId'                      => new IsShopifyGraphId(ShopifyObject::FULFILL_SERVICE),
            'variants.*.inventoryItem.cost'                        => 'numeric',
            'variants.*.inventoryItem.tracked'                     => 'boolean',
            'variants.*.inventoryPolicy'                           => Rule::in(InventoryPolicy::toArray()),
            'variants.*.inventoryQuantities'                       => 'array',
            'variants.*.inventoryQuantities.*.availableQuantity'   => 'integer',
            'variants.*.inventoryQuantities.*.locationId'          => new IsShopifyGraphId(ShopifyObject::LOCATION),
            'variants.*.options'                                   => 'required|array',
            'variants.*.options.*'                                 => 'required|string',
            'variants.*.price'                                     => 'numeric',
            'variants.*.sku'                                       => 'string',
            'variants.*.taxable'                                   => 'boolean',
            'variants.*.weight'                                    => 'numeric',
            'variants.*.weightUnit'                                => Rule::in(WeightUnit::toArray())
        ];
    }
}
