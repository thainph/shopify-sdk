<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\Variant;

use Thainph\ShopifySdk\Mockup\Graphql\Variant;
use Thainph\ShopifySdk\Validation\Rules\IsInstanceOf;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class IsArrayOfVariant extends BaseValidator
{
    public function rules(): array
    {
        return [
            'variants' => 'required|array',
            'variants.*' => [
                'required',
                new IsInstanceOf(Variant::class)
            ],
        ];
    }

    public function validated(): array
    {
        /** @var Variant $variant */
        $variants = array_map(function ($variant) {
            return $variant->bindingGraphData();
        }, $this->params['variants']);

        return ['variants' => $variants];
    }
}
