<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\ScriptTag;

use Illuminate\Validation\Rule;
use Thainph\ShopifySdk\Enums\Graphql\DisplayScope;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class UpdateValidator extends BaseValidator
{
    public function rules(): array
    {
        return [
            'cache'         => 'boolean',
            'src'           => 'required|url|regex:/^https:/',
            'displayScope' => [
                Rule::in(DisplayScope::toArray())
            ],
        ];
    }
}
