<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\ScriptTag;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Validation\Rules\IsShopifyGraphId;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class IsScriptTagId extends BaseValidator
{
    public function rules(): array
    {
        return [
            'id' => [
                'required',
                new IsShopifyGraphId(ShopifyObject::SCRIPT_TAG)
            ]
        ];
    }
}
