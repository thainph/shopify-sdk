<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\Node;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Validation\Rules\IsOneShopifyGraphIdOff;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class IsArrayGraphqlId extends BaseValidator
{
    public function rules(): array
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => [
                'required',
                new IsOneShopifyGraphIdOff(ShopifyObject::toArray())
            ],
        ];
    }
}
