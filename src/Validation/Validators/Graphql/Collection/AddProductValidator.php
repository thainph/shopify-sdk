<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\Collection;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;
use Thainph\ShopifySdk\Validation\Rules\IsShopifyGraphId;

class AddProductValidator extends BaseValidator
{
    public function rules() : array
    {
        return [
            'collectionId'  => [
                'required',
                new IsShopifyGraphId(ShopifyObject::COLLECTION)
            ],
            'productIds'    => 'required|array',
            'productIds.*'  => [
                'required',
                new IsShopifyGraphId(ShopifyObject::PRODUCT)
            ]
        ];
    }
}
