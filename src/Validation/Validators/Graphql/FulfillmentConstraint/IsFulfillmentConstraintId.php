<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\FulfillmentConstraint;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Validation\Rules\IsShopifyGraphId;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class IsFulfillmentConstraintId extends BaseValidator
{
    public function rules(): array
    {
        return [
            'id' => [
                'required',
                new IsShopifyGraphId(ShopifyObject::FULFILLMENT_CONSTRAINT)
            ],
        ];
    }
}
