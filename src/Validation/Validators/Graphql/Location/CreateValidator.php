<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\Location;

use Illuminate\Validation\Rule;
use Thainph\ShopifySdk\Enums\MetafieldType;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class CreateValidator extends BaseValidator
{
    public function rules(): array
    {
        return [
            'name'                  => 'required|string',
            'fulfillsOnlineOrders'  => 'boolean',

            'address.address1'      => 'required|string',
            'address.address2'      => 'string',
            'address.city'          => 'string',
            'address.countryCode'   => 'required|string|size:2',
            'address.phone'         => 'string',
            'address.provinceCode'  => 'string',
            'address.zip'           => 'string',

            'metafields'                => 'array',
            'metafields.*.namespace'    => 'required|string',
            'metafields.*.key'          => 'required|string',
            'metafields.*.value'        => 'required|string',
            'metafields.*.type'         => [
                'required',
                Rule::in(MetafieldType::toArray())
            ],

        ];
    }
}
