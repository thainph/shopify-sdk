<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\Location;

use Illuminate\Validation\Rule;
use Thainph\ShopifySdk\Enums\SortKey\LocationSortKey;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class QueryValidator extends BaseValidator
{
    public function rules(): array
    {
        return [
            'after' => 'string',
            'before' => 'string|required_with:last',
            'first' => 'integer|gt:0|lte:250|required_without:last',
            'last' => 'integer|gt:0|lte:250|required_without:first',
            'includeInactive' => 'boolean',
            'includeLegacy' => 'boolean',
            'reverse' => 'boolean',
            'query' => 'string',
            'sortKey' => [
                'string',
                Rule::in(LocationSortKey::toArray())
            ],
        ];
    }
}
