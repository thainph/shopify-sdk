<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\Location;

use Illuminate\Validation\Rule;
use Thainph\ShopifySdk\Enums\MetafieldType;
use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Validation\Rules\IsShopifyGraphId;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class UpdateValidator extends BaseValidator
{
    public function rules(): array
    {
        return [
            'name'                  => 'required|string',
            'fulfillsOnlineOrders'  => 'boolean',

            'address.address1'      => 'string|required',
            'address.address2'      => 'string',
            'address.city'          => 'string',
            'address.countryCode'   => 'string|required|size:2',
            'address.phone'         => 'string',
            'address.provinceCode'  => 'string',
            'address.zip'           => 'string',

            'metafields'                => 'array',
            'metafields.*.id'           => [
                'required_without:metafields.*.namespace,metafields.*.key,metafields.*.type',
                new IsShopifyGraphId(ShopifyObject::METAFIELD)
            ],
            'metafields.*.namespace'    => 'required_without:metafields.*.id|string',
            'metafields.*.key'          => 'required_without:metafields.*.id|string',
            'metafields.*.value'        => 'required|string',
            'metafields.*.type'         => [
                'required_without:metafields.*.id',
                Rule::in(MetafieldType::toArray())
            ],

        ];
    }
}
