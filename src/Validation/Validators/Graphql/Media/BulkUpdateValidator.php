<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\Media;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Validation\Rules\IsOneShopifyGraphIdOff;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class BulkUpdateValidator extends BaseValidator
{
    public function rules(): array
    {
        return [
            'medias'                        => 'array',
            'medias.*.id'                   => [
                'required',
                new IsOneShopifyGraphIdOff([
                    ShopifyObject::MEDIA_IMAGE,
                    ShopifyObject::MEDIA_VIDEO,
                    ShopifyObject::MEDIA_MODEL_3D,
                    ShopifyObject::MEDIA_EXTERNAL_VIDEO,
                ])
            ],
            'medias.*.alt'                  => 'string',
            'medias.*.previewImageSource'   => 'string',
        ];
    }
}
