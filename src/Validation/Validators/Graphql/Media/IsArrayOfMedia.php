<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\Media;

use Thainph\ShopifySdk\Mockup\Graphql\Media;
use Thainph\ShopifySdk\Validation\Rules\IsInstanceOf;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class IsArrayOfMedia extends BaseValidator
{
    public function rules(): array
    {
        return [
            'medias'                     => 'array',
            'medias.*'                   => [
                'required',
                new IsInstanceOf(Media::class)
            ]
        ];
    }

    public function validated(): array
    {
        /** @var Media $mocker */
        $medias = array_map(function ($mocker) {
            return $mocker->bindingGraphData();
        }, $this->params['medias']);

        return [
            'medias' => $medias,
        ];
    }
}
