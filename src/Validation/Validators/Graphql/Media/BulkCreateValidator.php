<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\Media;

use Illuminate\Validation\Rule;
use Thainph\ShopifySdk\Enums\Graphql\MediaContentType;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class BulkCreateValidator extends BaseValidator
{
    public function rules(): array
    {
        return [
            'medias'                     => 'array',
            'medias.*.alt'               => 'string',
            'medias.*.mediaContentType'  => Rule::in(MediaContentType::toArray()),
            'medias.*.originalSource'    => 'string',
        ];
    }
}
