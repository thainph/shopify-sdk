<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\Media;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;
use Thainph\ShopifySdk\Validation\Rules\IsOneShopifyGraphIdOff;

class IsArrayOfMediaId extends BaseValidator
{
    public function rules(): array
    {
        return [
            'ids'                       => 'array|required',
            'ids.*'                     => [
                'required',
                new IsOneShopifyGraphIdOff([
                    ShopifyObject::MEDIA_IMAGE,
                    ShopifyObject::MEDIA_VIDEO,
                    ShopifyObject::MEDIA_MODEL_3D,
                    ShopifyObject::MEDIA_EXTERNAL_VIDEO,
                ])
            ]
        ];
    }
}
