<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\Metafield;

use Thainph\ShopifySdk\Mockup\Graphql\Metafield;
use Thainph\ShopifySdk\Validation\Rules\IsInstanceOf;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class IsArrayOfMetafield extends BaseValidator
{
    public function rules(): array
    {
        return [
            'metafields' => 'required|array',
            'metafields.*' => [
                'required',
                new IsInstanceOf(Metafield::class)
            ],
        ];
    }

    public function validated(): array
    {
        /** @var Metafield $mocker */
        $metafields = array_map(function ($mocker) {
            return $mocker->bindingGraphData();
        }, $this->params['metafields']);

        return [
            'metafields' => $metafields,
        ];
    }
}
