<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\Metafield;

use Illuminate\Validation\Rule;
use Thainph\ShopifySdk\Enums\MetafieldType;
use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Exceptions\ObjectNotFound;
use Thainph\ShopifySdk\Validation\Rules\IsOneShopifyGraphIdOff;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class CreateValidator extends BaseValidator
{
    /**
     * @throws ObjectNotFound
     */
    public function rules(): array
    {
        return [
            'namespace' => 'required',
            'key' => 'required',
            'value' => 'required',
            'type' => [
                'required',
                Rule::in(MetafieldType::toArray())
            ],
            'ownerId' => [
                'required',
                new IsOneShopifyGraphIdOff([
                    ShopifyObject::ARTICLE,
                    ShopifyObject::BLOG,
                    ShopifyObject::COLLECTION,
                    ShopifyObject::CUSTOMER,
                    ShopifyObject::DRAFT_ORDER,
                    ShopifyObject::LOCATION,
                    ShopifyObject::ORDER,
                    ShopifyObject::PAGE,
                    ShopifyObject::PRODUCT,
                    ShopifyObject::PRODUCT_IMAGE,
                    ShopifyObject::PRODUCT_VARIANT,
                    ShopifyObject::SHOP,
                ])
            ],
        ];
    }
}
