<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\Metafield;

use Illuminate\Validation\Rule;
use Thainph\ShopifySdk\Enums\MetafieldType;
use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Exceptions\ObjectNotFound;
use Thainph\ShopifySdk\Validation\Rules\IsOneShopifyGraphIdOff;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class BulkCreateValidator extends BaseValidator
{
    /**
     * @throws ObjectNotFound
     */
    public function rules(): array
    {
        return [
            'metafields' => 'required|array|limit:25',
            'metafields.*.namespace' => 'required',
            'metafields.*.key' => 'required',
            'metafields.*.value' => 'required',
            'metafields.*.type' => [
                'required',
                Rule::in(MetafieldType::toArray())
            ],
            'metafields.*.ownerId' => [
                'required',
                new IsOneShopifyGraphIdOff([
                    ShopifyObject::ARTICLE,
                    ShopifyObject::BLOG,
                    ShopifyObject::COLLECTION,
                    ShopifyObject::CUSTOMER,
                    ShopifyObject::DRAFT_ORDER,
                    ShopifyObject::LOCATION,
                    ShopifyObject::ORDER,
                    ShopifyObject::PAGE,
                    ShopifyObject::PRODUCT,
                    ShopifyObject::PRODUCT_IMAGE,
                    ShopifyObject::PRODUCT_VARIANT,
                    ShopifyObject::SHOP,
                ])
            ],
        ];
    }
}
