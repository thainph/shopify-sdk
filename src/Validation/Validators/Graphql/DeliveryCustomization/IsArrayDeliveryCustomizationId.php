<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\DeliveryCustomization;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Validation\Rules\IsShopifyGraphId;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class IsArrayDeliveryCustomizationId extends BaseValidator
{
    public function rules(): array
    {
        return [
            'ids'      => 'required|array',
            'ids.*'    => [
                'required',
                new IsShopifyGraphId(ShopifyObject::DELIVERY_CUSTOMIZATION)
            ]
        ];
    }
}
