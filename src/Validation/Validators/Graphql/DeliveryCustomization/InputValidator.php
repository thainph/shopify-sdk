<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\DeliveryCustomization;

use Illuminate\Validation\Rule;
use Thainph\ShopifySdk\Enums\MetafieldType;
use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Validation\Rules\IsShopifyGraphId;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class InputValidator extends BaseValidator
{
    public function rules(): array
    {
        return [
            'enabled'       => 'required|boolean',
            'functionId'    => 'required|string',
            'title'         => 'required|string',

            'metafields'        => 'array',
            'metafields.*.id' => [
                'required_without:metafields.*.namespace',
                new IsShopifyGraphId(ShopifyObject::METAFIELD)
            ],
            'metafields.*.key' => 'required_without:metafields.*.id',
            'metafields.*.namespace' => 'required_without:metafields.*.id',
            'metafields.*.value' => 'required|string',
            'metafields.*.type' => [
                'required_without:metafields.*.id',
                Rule::in(MetafieldType::toArray())
            ],
        ];
    }
}
