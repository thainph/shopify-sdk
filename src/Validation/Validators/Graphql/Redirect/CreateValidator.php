<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\Redirect;

use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class CreateValidator extends BaseValidator
{
    public function rules(): array
    {
        return [
            'path'                          => 'required|regex:/^(https?:\/\/[a-zA-Z0-9-_%]+)?(\/[a-zA-Z0-9-_%]+)*$/',
            'target'                        => 'required|regex:/^(https?:\/\/[a-zA-Z0-9-_%]+)?(\/[a-zA-Z0-9-_%]+)*$/',
        ];
    }
}
