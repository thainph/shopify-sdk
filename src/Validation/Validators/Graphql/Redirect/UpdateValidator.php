<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\Redirect;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;
use Thainph\ShopifySdk\Validation\Rules\IsShopifyGraphId;

class UpdateValidator extends BaseValidator
{
    public function rules(): array
    {
        return [
            'id'                            => [
                'required',
                new IsShopifyGraphId(ShopifyObject::REDIRECT)
            ],
            'path'                          => 'required|regex:/^(https?:\/\/[a-zA-Z0-9-_%]+)?(\/[a-zA-Z0-9-_%]+)*$/',
            'target'                        => 'required|regex:/^(https?:\/\/[a-zA-Z0-9-_%]+)?(\/[a-zA-Z0-9-_%]+)*$/',
        ];
    }
}
