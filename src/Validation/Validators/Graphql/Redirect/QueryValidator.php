<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\Redirect;

use Illuminate\Validation\Rule;
use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;
use Thainph\ShopifySdk\Enums\SortKey\UrlRedirectSortKey;
use Thainph\ShopifySdk\Validation\Rules\IsShopifyGraphId;

class QueryValidator extends BaseValidator
{
    public function rules(): array
    {
        return [
            'after' => 'string',
            'before' => 'string|required_with:last',
            'first' => 'integer|gt:0|lte:250|required_without:last',
            'last' => 'integer|gt:0|lte:250|required_without:first',
            'reverse' => 'boolean',
            'sortKey' => [
                'string',
                Rule::in(UrlRedirectSortKey::toArray())
            ],
            'savedSearchId' => new IsShopifyGraphId(ShopifyObject::SAVED_SEARCH),
        ];
    }
}
