<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\Redirect;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;
use Thainph\ShopifySdk\Validation\Rules\IsShopifyGraphId;

class IsRedirectId extends BaseValidator
{
    public function rules(): array
    {
        return [
            'id' => [
                'required',
                new IsShopifyGraphId(ShopifyObject::REDIRECT)
            ],
        ];
    }
}
