<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\DiscountAutomatic;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Validation\Rules\IsShopifyGraphId;
use Thainph\ShopifySdk\Validation\Rules\RequiredWithoutAll;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class BasicInputValidator extends BaseValidator
{
    private array $customerGetItems = [
        'customerGets.items.products.productVariantsToAdd',
        'customerGets.items.products.productVariantsToRemove',
        'customerGets.items.products.productsToAdd',
        'customerGets.items.products.productsToRemove',
        'customerGets.items.collections.add',
        'customerGets.items.collections.remove',
        'customerGets.items.all'
    ];
    
    public function rules(): array
    {
        return [
            'startsAt'                              => 'date|required',
            'endsAt'                                => 'date',
            'title'                                 => 'required|string',
            'recurringCycleLimit'                   => 'integer',

            'combinesWith.orderDiscounts'           => 'boolean',
            'combinesWith.productDiscounts'         => 'boolean',
            'combinesWith.shippingDiscounts'        => 'boolean',

            'minimumRequirement.quantity.greaterThanOrEqualToQuantity'  => 'string|numeric|gt:0|required_without_all:minimumRequirement.subtotal.greaterThanOrEqualToQuantity',
            'minimumRequirement.subtotal.greaterThanOrEqualToQuantity'  => 'string|numeric|gt:0|required_without_all:minimumRequirement.quantity.greaterThanOrEqualToQuantity',

            'customerGets.appliesOnOneTimePurchase'                     => 'boolean',
            'customerGets.appliesOnSubscription'                        => 'boolean',
            'customerGets.items.all'                                    => [
                'boolean',
                new RequiredWithoutAll($this->customerGetItems)
            ],
            'customerGets.items.products.productVariantsToAdd'          => [
                'array',
                new RequiredWithoutAll($this->customerGetItems)
            ],
            'customerGets.items.products.productVariantsToAdd.*'        => new IsShopifyGraphId(ShopifyObject::PRODUCT_VARIANT),
            'customerGets.items.products.productVariantsToRemove'       => [
                'array',
                new RequiredWithoutAll($this->customerGetItems)
            ],
            'customerGets.items.products.productVariantsToRemove.*'     => new IsShopifyGraphId(ShopifyObject::PRODUCT_VARIANT),
            'customerGets.items.products.productsToAdd'                 => [
                'array',
                new RequiredWithoutAll($this->customerGetItems)
            ],
            'customerGets.items.products.productsToAdd.*'               => new IsShopifyGraphId(ShopifyObject::PRODUCT),
            'customerGets.items.products.productsToRemove'              => [
                'array',
                new RequiredWithoutAll($this->customerGetItems)
            ],
            'customerGets.items.products.productsToRemove.*'            => new IsShopifyGraphId(ShopifyObject::PRODUCT),
            'customerGets.items.collections.add'                        => [
                'array',
                new RequiredWithoutAll($this->customerGetItems)
            ],
            'customerGets.items.collections.add.*'                      => new IsShopifyGraphId(ShopifyObject::COLLECTION),
            'customerGets.items.collections.remove'                     => [
                'array',
                new RequiredWithoutAll($this->customerGetItems)
            ],
            'customerGets.items.collections.remove.*'                   => new IsShopifyGraphId(ShopifyObject::COLLECTION),
            'customerGets.value.percentage'                             => 'numeric',
            'customerGets.value.discountAmount.amount'                  => 'numeric',
            'customerGets.value.discountAmount.appliesOnEachItem'       => 'boolean',
        ];
    }
}
