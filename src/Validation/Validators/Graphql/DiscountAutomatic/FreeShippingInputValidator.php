<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\DiscountAutomatic;

use Thainph\ShopifySdk\Validation\Rules\RequiredWithoutAll;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class FreeShippingInputValidator extends BaseValidator
{
    private array $destination = [
        'destination.all',
        'destination.countries.add',
        'destination.countries.remove',
        'destination.countries.includeRestOfWorld'
    ];

    public function rules(): array
    {
        return [
            'startsAt'                              => 'date|required',
            'endsAt'                                => 'date',
            'title'                                 => 'required|string',
            'appliesOnOneTimePurchase'              => 'boolean',
            'appliesOnSubscription'                 => 'boolean',
            'maximumShippingPrice'                  => 'numeric',
            'recurringCycleLimit'                   => 'integer',

            'combinesWith.orderDiscounts'           => 'boolean',
            'combinesWith.productDiscounts'         => 'boolean',
            'combinesWith.shippingDiscounts'        => 'boolean',

            'minimumRequirement.quantity.greaterThanOrEqualToQuantity'  => 'numeric|string|gte:0|required_without:minimumRequirement.subtotal.greaterThanOrEqualToSubtotal',
            'minimumRequirement.subtotal.greaterThanOrEqualToSubtotal'  => 'numeric|gte:0|required_without:minimumRequirement.quantity.greaterThanOrEqualToQuantity',

            'destination.all'                           => [
                'boolean',
                new RequiredWithoutAll($this->destination)
            ],
            'destination.countries.add'                 => [
                'array',
                new RequiredWithoutAll($this->destination)
            ],
            'destination.countries.add.*'               => 'string|size:2',
            'destination.countries.remove'              => [
                'array',
                new RequiredWithoutAll($this->destination)
            ],
            'destination.countries.remove.*'            => 'string|size:2',
            'destination.countries.includeRestOfWorld'  => [
                'boolean',
                new RequiredWithoutAll($this->destination)
            ],
        ];
    }
}
