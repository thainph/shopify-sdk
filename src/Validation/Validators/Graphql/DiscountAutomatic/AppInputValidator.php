<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\DiscountAutomatic;

use Illuminate\Validation\Rule;
use Thainph\ShopifySdk\Enums\MetafieldType;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class AppInputValidator extends BaseValidator
{
    public function rules(): array
    {
        return [
            'startsAt'      => 'date|required',
            'endsAt'        => 'date',
            'functionId'    => 'required|string',
            'title'         => 'required|string',

            'combinesWith.orderDiscounts'       => 'boolean',
            'combinesWith.productDiscounts'     => 'boolean',
            'combinesWith.shippingDiscounts'    => 'boolean',

            'metafields'                => 'array',
            'metafields.*.namespace'    => 'required|string',
            'metafields.*.key'          => 'required|string',
            'metafields.*.value'        => 'required|string',
            'metafields.*.type'         => [
                'required',
                Rule::in(MetafieldType::toArray())
            ],
        ];
    }
}
