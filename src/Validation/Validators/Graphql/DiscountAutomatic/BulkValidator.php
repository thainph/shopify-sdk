<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\DiscountAutomatic;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Validation\Rules\IsShopifyGraphId;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class BulkValidator extends BaseValidator
{
    public function rules(): array
    {
        return [
            'search'            => 'string|required_without_all:savedSearchId,ids',
            'savedSearchId'     => [
                new IsShopifyGraphId(ShopifyObject::SAVED_SEARCH),
                'required_without_all:search,ids'
            ],
            'ids'               => 'array|required_without_all:search,savedSearchId',
            'ids.*'             => [
                'required',
                new IsShopifyGraphId(ShopifyObject::DISCOUNT_AUTOMATIC)
            ]
        ];
    }
}
