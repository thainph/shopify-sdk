<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\Company;

use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class UpdateValidator extends BaseValidator
{
    public function rules(): array
    {
        return [
            'customerSince'     => 'date',
            'externalId'        => 'string',
            'name'              => 'required|string',
            'note'              => 'string',
        ];
    }
}
