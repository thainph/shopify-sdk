<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\Company;

use Illuminate\Validation\Rule;
use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Enums\TaxExemption;
use Thainph\ShopifySdk\Validation\Rules\IsShopifyGraphId;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class CreateValidator extends BaseValidator
{
    public function rules(): array
    {
        return array_merge(
            [
                'company.customerSince'     => 'date',
                'company.externalId'        => 'string',
                'company.name'              => 'required|string',
                'company.note'              => 'string',

                'companyContact.email'      => 'email',
                'companyContact.firstName'  => 'string',
                'companyContact.lastName'   => 'string',
                'companyContact.phone'      => 'string',
                'companyContact.title'      => 'string',
                'companyContact.locale'     => 'string',

                'companyLocation.billingSameAsShipping'                                 => 'boolean',
                'companyLocation.buyerExperienceConfiguration.checkoutToDraft'          => 'boolean',
                'companyLocation.buyerExperienceConfiguration.editableShippingAddress'  => 'boolean',
                'companyLocation.buyerExperienceConfiguration.paymentTermsTemplateId'   => new IsShopifyGraphId(ShopifyObject::PAYMENT_TERMS_TEMPLATE),
                'companyLocation.externalId'                                            => 'string',
                'companyLocation.locale'                                                => 'string',
                'companyLocation.name'                                                  => 'string',
                'companyLocation.note'                                                  => 'string',
                'companyLocation.phone'                                                 => 'string',
                'companyLocation.taxExemptions'                                         => Rule::in(TaxExemption::toArray()),
                'companyLocation.taxRegistrationId'                                     => 'string',
            ],
            $this->getCompanyAddressRules('companyLocation.billingAddress'),
            $this->getCompanyAddressRules('companyLocation.shippingAddress')
        );
    }

    private function getCompanyAddressRules($base): array
    {
        return [
            $base . '.address1' => 'string',
            $base . '.address2' => 'string',
            $base . '.city' => 'string',
            $base . '.countryCode' => 'string|size:2',
            $base . '.phone' => 'string',
            $base . '.recipient' => 'string',
            $base . '.zip' => 'string',
            $base . '.zoneCode' => 'string',
        ];
    }
}
