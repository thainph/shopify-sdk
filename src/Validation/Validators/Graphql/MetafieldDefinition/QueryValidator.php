<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\MetafieldDefinition;

use Illuminate\Validation\Rule;
use Thainph\ShopifySdk\Enums\Graphql\MetafieldDefinitionPinnedStatus;
use Thainph\ShopifySdk\Enums\Graphql\MetafieldOwnerType;
use Thainph\ShopifySdk\Enums\SortKey\ProductSortKey;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class QueryValidator extends BaseValidator
{
    public function rules(): array
    {
        return [
            'after' => 'string',
            'before' => 'string|required_with:last',
            'first' => 'integer|gt:0|lte:250|required_without:last',
            'last' => 'integer|gt:0|lte:250|required_without:first',
            'reverse' => 'boolean',
            'query' => 'string',
            'sortKey' => [
                'string',
                Rule::in(ProductSortKey::toArray())
            ],
            'namespace' => 'string',
            'key' => 'string',
            'ownerType' => [
                'required',
                Rule::in(MetafieldOwnerType::toArray())
            ],
            'pinnedStatus' => Rule::in(MetafieldDefinitionPinnedStatus::toArray()),
        ];
    }
}
