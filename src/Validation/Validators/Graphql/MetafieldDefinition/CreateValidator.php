<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\MetafieldDefinition;

use Illuminate\Validation\Rule;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;
use Thainph\ShopifySdk\Enums\Graphql\MetafieldOwnerType;
use Thainph\ShopifySdk\Enums\Graphql\MetafieldAdminAccess;
use Thainph\ShopifySdk\Enums\Graphql\MetafieldGrantAccessLevel;
use Thainph\ShopifySdk\Enums\Graphql\MetafieldStorefrontAccess;
use Thainph\ShopifySdk\Enums\MetafieldType;

class CreateValidator extends BaseValidator
{
    public function rules(): array
    {
        return [
            'access.admin'              => Rule::in(MetafieldAdminAccess::toArray()),
            'access.storefront'         => Rule::in(MetafieldStorefrontAccess::toArray()),
            'access.grants'             => 'array',
            'access.grants.*.access'    => Rule::in(MetafieldGrantAccessLevel::toArray()),
            'access.grants.*.grantee'   => 'string',

            'description'               => 'string',
            'key'                       => 'required|string|min:3|max:64|regex:/^[a-zA-Z0-9_-]+$/',
            'name'                      => 'required|string',
            'namespace'                 => 'required|string|min:3|max:255|regex:/^[a-zA-Z0-9_-]+$/',
            'ownerType'                 => [
                'required',
                Rule::in(MetafieldOwnerType::toArray())
            ],
            'pin'                       => 'boolean',
            'type'                      => [
                'required',
                Rule::in(MetafieldType::toArray())
            ],
            'useAsCollectionCondition'  => 'boolean',

            'validations'               => 'array',
            'validations.*.name'        => 'required|string',
            'validations.*.value'       => 'required|string',
        ];
    }
}
