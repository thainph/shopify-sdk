<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\MetafieldDefinition;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Validation\Rules\IsShopifyGraphId;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class IsMetafieldDefinitionId extends BaseValidator
{
    public function rules(): array
    {
        return [
            'id' => [
                'required',
                new IsShopifyGraphId(ShopifyObject::METAFIELD_DEFINITION)
            ]
        ];
    }
}
