<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\StagedUpload;

use Thainph\ShopifySdk\Mockup\Graphql\StagedUpload;
use Thainph\ShopifySdk\Validation\Rules\IsInstanceOf;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class IsArrayOfStagedUpload extends BaseValidator
{
    public function rules(): array
    {
        return [
            'stagedUpload' => 'required|array',
            'stagedUpload.*' => [
                'required',
                new IsInstanceOf(StagedUpload::class)
            ],
        ];
    }

    public function validated(): array
    {
        /** @var StagedUpload $mocker */
        $stagedUpload = array_map(function ($mocker) {
            return $mocker->bindingGraphData();
        }, $this->params['stagedUpload']);

        return [
            'stagedUpload' => $stagedUpload,
        ];
    }
}
