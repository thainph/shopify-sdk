<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\StagedUpload;

use Illuminate\Validation\Rule;
use Thainph\ShopifySdk\Enums\Graphql\StagedUploadResource;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class CreateValidator extends BaseValidator
{
    public function rules(): array
    {
        return [
            'stagedUpload'              => 'required|array',
            'stagedUpload.*.fileSize'   => 'integer|required_if:stagedUpload.*.resource,VIDEO,MODEL_3D',
            'stagedUpload.*.filename'   => 'string|required',
            'stagedUpload.*.httpMethod' => 'string|required|in:POST,PUT',
            'stagedUpload.*.mimeType'   => 'string|required',
            'stagedUpload.*.resource'   => Rule::in(StagedUploadResource::toArray()),
        ];
    }
}
