<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\CartTransform;

use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class CreateValidator extends BaseValidator
{
    public function rules(): array
    {
        return [
            'blockOnFailure'    => 'boolean',
            'functionId'        => 'string|required',
        ];
    }
}
