<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\CartTransform;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Validation\Rules\IsShopifyGraphId;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class IsCartTransformId extends BaseValidator
{
    public function rules(): array
    {
        return [
            'id' => [
                'required',
                new IsShopifyGraphId(ShopifyObject::CART_TRANSFORM)
            ]
        ];
    }
}
