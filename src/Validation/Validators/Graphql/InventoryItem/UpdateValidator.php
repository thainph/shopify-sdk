<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\InventoryItem;

use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class UpdateValidator extends BaseValidator
{
    public function rules(): array
    {
        return [
            'cost'                  => 'numeric',
            'countryCodeOfOrigin'   => 'string|size:2',
            'harmonizedSystemCode'  => 'string|min:6|max:13',
            'provinceCodeOfOrigin'  => 'string|size:2',
            'tracked'               => 'boolean',

            'countryHarmonizedSystemCodes'                          => 'array',
            'countryHarmonizedSystemCodes.*.countryCode'            => 'required|string|size:2',
            'countryHarmonizedSystemCodes.*.harmonizedSystemCode'   => 'required|string',
        ];
    }
}
