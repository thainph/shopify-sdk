<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\InventoryItem;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Validation\Rules\IsShopifyGraphId;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class BulkActivationValidator extends BaseValidator
{
    public function rules(): array
    {
        return [
            'inventoryItemUpdates'                  => 'required|array',
            'inventoryItemUpdates.*.activate'       => 'required|boolean',
            'inventoryItemUpdates.*.locationId'     => [
                'required',
                new IsShopifyGraphId(ShopifyObject::LOCATION)
            ]
        ];
    }
}
