<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\InventoryItem;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Validation\Rules\IsShopifyGraphId;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class IsInventoryItemId extends BaseValidator
{
    public function rules(): array
    {
        return [
            'id' => [
                'required',
                new IsShopifyGraphId(ShopifyObject::INVENTORY_ITEM)
            ]
        ];
    }
}
