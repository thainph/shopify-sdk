<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\InventoryLevel;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Validation\Rules\IsShopifyGraphId;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class IsInventoryLevelId extends BaseValidator
{
    public function rules(): array
    {
        return [
            'id' => [
                'required',
                new IsShopifyGraphId(ShopifyObject::INVENTORY_LEVEL, ['inventory_item_id'])
            ],
        ];
    }
}
