<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\InventoryLevel;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Validation\Rules\IsShopifyGraphId;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class ActivateValidator extends BaseValidator
{
    public function rules(): array
    {
        return [
            'available'             => 'integer',
            'inventoryItemId'       => [
                'required',
                new IsShopifyGraphId(ShopifyObject::INVENTORY_ITEM)
            ],
            'locationId'            => [
                'required',
                new IsShopifyGraphId(ShopifyObject::LOCATION)
            ],
            'onHand'                => 'integer',
        ];
    }
}
