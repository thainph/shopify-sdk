<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\Product;

use Illuminate\Validation\Rule;
use Thainph\ShopifySdk\Enums\Graphql\WeightUnit;
use Thainph\ShopifySdk\Enums\MetafieldType;
use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Exceptions\ObjectNotFound;
use Thainph\ShopifySdk\Enums\Graphql\ProductStatus;
use Thainph\ShopifySdk\Enums\Graphql\InventoryPolicy;
use Thainph\ShopifySdk\Enums\Graphql\InventoryManagement;
use Thainph\ShopifySdk\Validation\Rules\IsHandle;
use Thainph\ShopifySdk\Validation\Rules\IsOneShopifyGraphIdOff;
use Thainph\ShopifySdk\Validation\Rules\IsShopifyGraphId;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class UpdateValidator extends BaseValidator
{
    /**
     * @throws ObjectNotFound
     */
    public function rules(): array
    {
        return [
            'id'                        => new IsShopifyGraphId(ShopifyObject::PRODUCT),
            'bodyHtml'                  => 'string',
            'claimOwnership.bundles'    => 'boolean',
            'collectionsToJoin'         => 'array',
            'collectionsToJoin.*'       => new IsShopifyGraphId(ShopifyObject::COLLECTION),
            'collectionsToLeave'        => 'array',
            'collectionsToLeave.*'      => new IsShopifyGraphId(ShopifyObject::COLLECTION),
            'customProductType'         => 'string',
            'descriptionHtml'           => 'string',
            'giftCard'                  => 'boolean',
            'giftCardTemplateSuffix'    => 'string',
            'handle'                    => new IsHandle(),

            'metafields'                => 'array',
            'metafields.*.namespace'    => 'required|string',
            'metafields.*.key'          => 'required|string',
            'metafields.*.value'        => 'required|string',
            'metafields.*.type'         => [
                'required',
                Rule::in(MetafieldType::toArray())
            ],

            'options'                   => 'array',
            'options.*'                 => 'required|string',

            'productType'               => 'string',
            'publishDate'               => 'date',
            'publishOn'                 => 'date',
            'published'                 => 'boolean',
            'status'                    => Rule::in(ProductStatus::toArray()),

            'tags'                      => 'array',
            'tags.*'                    => 'string',

            'templateSuffix'            => 'string',
            'title'                     => 'required|string',
            'vendor'                    => 'string',

            'variants'                                             => 'array',
            'variants.*.barcode'                                   => 'string',
            'variants.*.compareAtPrice'                            => 'numeric',
            'variants.*.fulfillmentServiceId'                      => new IsShopifyGraphId(ShopifyObject::FULFILL_SERVICE),
            'variants.*.harmonizedSystemCode'                      => 'string',
            'variants.*.imageId'                                   => new IsShopifyGraphId(ShopifyObject::PRODUCT_IMAGE),
            'variants.*.mediaId'                                   => new IsOneShopifyGraphIdOff([
                ShopifyObject::MEDIA_IMAGE,
                ShopifyObject::MEDIA_MODEL_3D,
                ShopifyObject::MEDIA_VIDEO,
                ShopifyObject::MEDIA_EXTERNAL_VIDEO
            ]),
            'variants.*.mediaSrc'                                  => 'array',
            'variants.*.mediaSrc.*'                                => 'string',
            'variants.*.inventoryItem.cost'                        => 'numeric',
            'variants.*.inventoryItem.tracked'                     => 'boolean',
            'variants.*.inventoryManagement'                       => Rule::in(InventoryManagement::toArray()),
            'variants.*.inventoryPolicy'                           => Rule::in(InventoryPolicy::toArray()),
            'variants.*.inventoryQuantities'                       => 'array',
            'variants.*.inventoryQuantities.*.availableQuantity'   => 'integer',
            'variants.*.inventoryQuantities.*.locationId'          => new IsShopifyGraphId(ShopifyObject::LOCATION),
            'variants.*.options'                                   => 'required|array',
            'variants.*.options.*'                                 => 'required|string',
            'variants.*.position'                                  => 'integer',
            'variants.*.price'                                     => 'numeric',
            'variants.*.sku'                                       => 'string',
            'variants.*.taxable'                                   => 'boolean',
            'variants.*.taxCode'                                   => 'string',
            'variants.*.requiresShipping'                          => 'boolean',
            'variants.*.title'                                     => 'string',
            'variants.*.weight'                                    => 'numeric',
            'variants.*.weightUnit'                                => Rule::in(WeightUnit::toArray()),
            'variants.*.metafields'                                => 'array',
            'variants.*.metafields.*.namespace'                    => 'required|string',
            'variants.*.metafields.*.key'                          => 'required|string',
            'variants.*.metafields.*.value'                        => 'required|string',
            'variants.*.metafields.*.type'                         => [
                'required',
                Rule::in(MetafieldType::toArray())
            ],
        ];
    }
}
