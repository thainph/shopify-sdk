<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\Validation;

use Illuminate\Validation\Rule;
use Thainph\ShopifySdk\Enums\MetafieldType;
use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Validation\Rules\IsShopifyGraphId;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class CreateValidator extends BaseValidator
{
    public function rules(): array
    {
        return [
            'blockOnFailure'    => 'boolean',
            'enable'           => 'boolean',
            'functionId'        => 'required|string',

            'metafields'        => 'array',
            'metafields.*.id' => [
                'required_without:metafields.*.namespace',
                new IsShopifyGraphId(ShopifyObject::METAFIELD)
            ],
            'metafields.*.key' => 'required_without:metafields.*.id',
            'metafields.*.namespace' => 'required_without:metafields.*.id',
            'metafields.*.value' => 'required|string',
            'metafields.*.type' => [
                'required_without:metafields.*.id',
                Rule::in(MetafieldType::toArray())
            ],
        ];
    }
}
