<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\DiscountCode;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Validation\Rules\IsShopifyGraphId;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class IsCodeDiscountId extends BaseValidator
{
    public function rules(): array
    {
        return [
            'id' => [
                'required',
                new IsShopifyGraphId(ShopifyObject::DISCOUNT_CODE)
            ]
        ];
    }
}
