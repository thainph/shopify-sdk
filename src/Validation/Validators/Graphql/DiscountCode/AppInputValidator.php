<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\DiscountCode;

use Illuminate\Validation\Rule;
use Thainph\ShopifySdk\Enums\MetafieldType;
use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Validation\Rules\IsShopifyGraphId;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class AppInputValidator extends BaseValidator
{
    public function rules(): array
    {
        return [
            'startsAt'                  => 'date|required',
            'endsAt'                    => 'date',
            'functionId'                => 'required|string',
            'title'                     => 'required|string',
            'appliesOncePerCustomer'    => 'boolean',
            'code'                      => 'required|string',
            'usageLimit'                => 'integer',

            'combinesWith.orderDiscounts'       => 'boolean',
            'combinesWith.productDiscounts'     => 'boolean',
            'combinesWith.shippingDiscounts'    => 'boolean',

            'metafields'                => 'array',
            'metafields.*.namespace'    => 'required|string',
            'metafields.*.key'          => 'required|string',
            'metafields.*.value'        => 'required|string',
            'metafields.*.type'         => [
                'required',
                Rule::in(MetafieldType::toArray())
            ],

            'customerSelection.all'                         => 'boolean',
            'customerSelection.customers.add'               => 'array',
            'customerSelection.customers.add.*'             => new IsShopifyGraphId(ShopifyObject::CUSTOMER),
            'customerSelection.customers.remove'            => 'array',
            'customerSelection.customers.remove.*'          => new IsShopifyGraphId(ShopifyObject::CUSTOMER),
            'customerSelection.customerSegments.add'        => 'array',
            'customerSelection.customerSegments.add.*'      => new IsShopifyGraphId(ShopifyObject::CUSTOMER_SEGMENT),
            'customerSelection.customerSegments.remove'     => 'array',
            'customerSelection.customerSegments.remove.*'   => new IsShopifyGraphId(ShopifyObject::CUSTOMER_SEGMENT),
        ];
    }
}
