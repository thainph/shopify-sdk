<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\DiscountCode;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Validation\Rules\IsShopifyGraphId;
use Thainph\ShopifySdk\Validation\Rules\RequiredWithoutAll;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class BasicInputValidator extends BaseValidator
{
    private array $customerGetItems = [
        'customerGets.items.products.productVariantsToAdd',
        'customerGets.items.products.productVariantsToRemove',
        'customerGets.items.products.productsToAdd',
        'customerGets.items.products.productsToRemove',
        'customerGets.items.collections.add',
        'customerGets.items.collections.remove',
        'customerGets.items.all'
    ];

    private array $customerSelectionItems = [
        'customerSelection.customers.add',
        'customerSelection.customers.remove',
        'customerSelection.customerSegments.add',
        'customerSelection.customerSegments.remove',
        'customerSelection.all'
    ];
    
    public function rules(): array
    {
        return [
            'startsAt'                              => 'date|required',
            'endsAt'                                => 'date',
            'title'                                 => 'required|string',
            'appliesOncePerCustomer'                => 'boolean',
            'code'                                  => 'required|string',
            'recurringCycleLimit'                   => 'integer',
            'usageLimit'                            => 'integer',

            'combinesWith.orderDiscounts'           => 'boolean',
            'combinesWith.productDiscounts'         => 'boolean',
            'combinesWith.shippingDiscounts'        => 'boolean',

            'minimumRequirement.quantity.greaterThanOrEqualToQuantity'  => 'integer|gte:0',
            'minimumRequirement.subtotal.greaterThanOrEqualToQuantity'  => 'numeric|gte:0',

            'customerGets.appliesOnOneTimePurchase'                     => 'boolean',
            'customerGets.appliesOnSubscription'                        => 'boolean',
            'customerGets.items.all'                                    => [
                'boolean',
                new RequiredWithoutAll($this->customerGetItems)
            ],
            'customerGets.items.products.productVariantsToAdd'          => [
                'array',
                new RequiredWithoutAll($this->customerGetItems)
            ],
            'customerGets.items.products.productVariantsToAdd.*'        => new IsShopifyGraphId(ShopifyObject::PRODUCT_VARIANT),
            'customerGets.items.products.productVariantsToRemove'       => [
                'array',
                new RequiredWithoutAll($this->customerGetItems)
            ],
            'customerGets.items.products.productVariantsToRemove.*'     => new IsShopifyGraphId(ShopifyObject::PRODUCT_VARIANT),
            'customerGets.items.products.productsToAdd'                 => [
                'array',
                new RequiredWithoutAll($this->customerGetItems)
            ],
            'customerGets.items.products.productsToAdd.*'               => new IsShopifyGraphId(ShopifyObject::PRODUCT),
            'customerGets.items.products.productsToRemove'              => [
                'array',
                new RequiredWithoutAll($this->customerGetItems)
            ],
            'customerGets.items.products.productsToRemove.*'            => new IsShopifyGraphId(ShopifyObject::PRODUCT),
            'customerGets.items.collections.add'                        => [
                'array',
                new RequiredWithoutAll($this->customerGetItems)
            ],
            'customerGets.items.collections.add.*'                      => new IsShopifyGraphId(ShopifyObject::COLLECTION),
            'customerGets.items.collections.remove'                     => [
                'array',
                new RequiredWithoutAll($this->customerGetItems)
            ],
            'customerGets.items.collections.remove.*'                   => new IsShopifyGraphId(ShopifyObject::COLLECTION),
            'customerGets.value.percentage'                             => 'numeric|between:0,1|required_without:customerGets.value.discountAmount',
            'customerGets.value.discountAmount'                         => 'required_without:customerGets.value.percentage',
            'customerGets.value.discountAmount.amount'                  => 'required_with:customerGets.value.discountAmount|numeric',
            'customerGets.value.discountAmount.appliesOnEachItem'       => 'required_with:customerGets.value.discountAmount|boolean',
            
            'customerSelection.all'                         => [
                'boolean',
                new RequiredWithoutAll($this->customerSelectionItems)
            ],
            'customerSelection.customers.add'               => [
                'array',
                new RequiredWithoutAll($this->customerSelectionItems)
            ],
            'customerSelection.customers.add.*'             => new IsShopifyGraphId(ShopifyObject::CUSTOMER),
            'customerSelection.customers.remove'            => [
                'array',
                new RequiredWithoutAll($this->customerSelectionItems)
            ],
            'customerSelection.customers.remove.*'          => new IsShopifyGraphId(ShopifyObject::CUSTOMER),
            'customerSelection.customerSegments.add'        => [
                'array',
                new RequiredWithoutAll($this->customerSelectionItems)
            ],
            'customerSelection.customerSegments.add.*'      => new IsShopifyGraphId(ShopifyObject::CUSTOMER_SEGMENT),
            'customerSelection.customerSegments.remove'     => [
                'array',
                new RequiredWithoutAll($this->customerSelectionItems)
            ],
            'customerSelection.customerSegments.remove.*'   => new IsShopifyGraphId(ShopifyObject::CUSTOMER_SEGMENT),
        ];
    }
}
