<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\DiscountCode;

use Illuminate\Validation\Rule;
use Thainph\ShopifySdk\Enums\SortKey\DiscountCodeSortKey;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class QuerySavedSearchValidator extends BaseValidator
{
    public function rules(): array
    {
        return [
            'after' => 'string',
            'before' => 'string|required_with:last',
            'first' => 'integer|gt:0|lte:250|required_without:last',
            'last' => 'integer|gt:0|lte:250|required_without:first',
            'reverse' => 'boolean',
            'query' => 'string',
            'sortKey' => Rule::in(DiscountCodeSortKey::toArray())
        ];
    }
}
