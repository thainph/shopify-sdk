<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\DiscountCode;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Validation\Rules\IsShopifyGraphId;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class IsDiscountRedeemCodeBulkCreationId extends BaseValidator
{
    public function rules(): array
    {
        return [
            'id' => [
                'required',
                new IsShopifyGraphId(ShopifyObject::DISCOUNT_REDEEM_CODE_BULK_CREATION)
            ]
        ];
    }
}
