<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\DiscountCode;

use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class BulkRedeemCreateValidator extends BaseValidator
{
    public function rules(): array
    {
        return [
            'codes'         => 'array|required',
            'codes.*.code'  => 'string|required',
        ];
    }
}
