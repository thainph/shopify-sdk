<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\DiscountCode;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Validation\Rules\IsShopifyGraphId;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class BulkRedeemRemoveValidator extends BaseValidator
{
    public function rules(): array
    {
        return [
            'discountId'        => [
                new IsShopifyGraphId(ShopifyObject::DISCOUNT_CODE),
                'required'
            ],
            'search'            => 'string|required_without_all:savedSearchId,ids',
            'savedSearchId'     => [
                new IsShopifyGraphId(ShopifyObject::SAVED_SEARCH),
                'required_without_all:search,ids'
            ],
            'ids'               => 'array|required_without_all:search,savedSearchId',
            'ids.*'             => [
                'required',
                new IsShopifyGraphId(ShopifyObject::DISCOUNT_REDEEM_CODE)
            ]
        ];
    }
}
