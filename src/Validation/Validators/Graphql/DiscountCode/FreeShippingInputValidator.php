<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\DiscountCode;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Validation\Rules\IsShopifyGraphId;
use Thainph\ShopifySdk\Validation\Rules\RequiredWithoutAll;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class FreeShippingInputValidator extends BaseValidator
{
    private array $customerSelection = [
        'customerSelection.customers.add',
        'customerSelection.customers.remove',
        'customerSelection.customerSegments.add',
        'customerSelection.customerSegments.remove',
        'customerSelection.all'
    ];

    public function rules(): array
    {
        return [
            'startsAt'                              => 'date|required',
            'endsAt'                                => 'date',
            'title'                                 => 'required|string',
            'appliesOnOneTimePurchase'              => 'boolean',
            'appliesOnSubscription'                 => 'boolean',
            'maximumShippingPrice'                  => 'numeric',
            'recurringCycleLimit'                   => 'integer',
            'appliesOncePerCustomer'                => 'boolean',
            'code'                                  => 'required|string',
            'usageLimit'                            => 'integer',

            'combinesWith.orderDiscounts'           => 'boolean',
            'combinesWith.productDiscounts'         => 'boolean',
            'combinesWith.shippingDiscounts'        => 'boolean',

            'minimumRequirement.quantity.greaterThanOrEqualToQuantity'  => 'integer|gte:0',
            'minimumRequirement.subtotal.greaterThanOrEqualToQuantity'  => 'numeric|gte:0',

            'destination.all'                           => 'boolean',
            'destination.countries.add'                 => 'array',
            'destination.countries.add.*'               => 'string|size:2',
            'destination.countries.remove'              => 'array',
            'destination.countries.remove.*'            => 'string|size:2',
            'destination.countries.includeRestOfWorld'  => 'boolean',

            'customerSelection.all'                         => [
                'boolean',
                new RequiredWithoutAll($this->customerSelection)
            ],
            'customerSelection.customers.add'               => [
                'array',
                new RequiredWithoutAll($this->customerSelection)
            ],
            'customerSelection.customers.add.*'             => new IsShopifyGraphId(ShopifyObject::CUSTOMER),
            'customerSelection.customers.remove'            => [
                'array',
                new RequiredWithoutAll($this->customerSelection)
            ],
            'customerSelection.customers.remove.*'          => new IsShopifyGraphId(ShopifyObject::CUSTOMER),
            'customerSelection.customerSegments.add'        => [
                'array',
                new RequiredWithoutAll($this->customerSelection)
            ],
            'customerSelection.customerSegments.add.*'      => new IsShopifyGraphId(ShopifyObject::CUSTOMER_SEGMENT),
            'customerSelection.customerSegments.remove'     => [
                'array',
                new RequiredWithoutAll($this->customerSelection)
            ],
            'customerSelection.customerSegments.remove.*'   => new IsShopifyGraphId(ShopifyObject::CUSTOMER_SEGMENT),
        ];
    }
}
