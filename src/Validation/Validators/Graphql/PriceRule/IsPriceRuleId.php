<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\PriceRule;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Validation\Rules\IsShopifyGraphId;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class IsPriceRuleId extends BaseValidator
{
    public function rules(): array
    {
        return [
            'id' => [
                'required',
                new IsShopifyGraphId(ShopifyObject::PRICE_RULE)
            ],
        ];
    }
}
