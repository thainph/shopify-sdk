<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\BulkOperation;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Validation\Rules\IsShopifyGraphId;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class IsBulkOperationId extends BaseValidator
{
    public function rules(): array
    {
        return [
            'id' => [
                'required',
                new IsShopifyGraphId(ShopifyObject::BULK_OPERATION)
            ],
        ];
    }
}
