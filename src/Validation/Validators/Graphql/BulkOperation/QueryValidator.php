<?php

namespace Thainph\ShopifySdk\Validation\Validators\Graphql\BulkOperation;

use Illuminate\Validation\Rule;
use Thainph\ShopifySdk\Enums\Graphql\BulkOperationType;
use Thainph\ShopifySdk\Validation\Validators\BaseValidator;

class QueryValidator extends BaseValidator
{
    public function rules(): array
    {
        return [
            'type' => [
                'required',
                Rule::in(BulkOperationType::toArray())
            ],
        ];
    }
}
