<?php

namespace Thainph\ShopifySdk\Validation\Validators;

use Illuminate\Support\Facades\Validator;

class BaseValidator
{
    protected $validator;
    protected array $params;

    public function __construct(array $params)
    {
        $this->params = $params;
        $this->validator = Validator::make(
            $params,
            $this->rules(),
            $this->messages(),
            $this->attributes()
        );
    }

    public function rules(): array
    {
        return [];
    }

    public function messages(): array
    {
        return [];
    }

    public function attributes(): array
    {
        return [];
    }

    public function fails()
    {
        return $this->validator->fails();
    }

    public function errors()
    {
        return $this->validator->errors();
    }

    public function validated()
    {
        return $this->validator->validated();
    }
}
