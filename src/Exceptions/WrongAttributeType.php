<?php

namespace Thainph\ShopifySdk\Exceptions;

use Exception;

class WrongAttributeType extends Exception
{
}
