<?php

namespace Thainph\ShopifySdk\Exceptions;

use Exception;

class MissingIdentifier extends Exception
{
}
