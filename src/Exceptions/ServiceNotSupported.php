<?php

namespace Thainph\ShopifySdk\Exceptions;

use Exception;

class ServiceNotSupported extends Exception
{
}
