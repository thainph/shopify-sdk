<?php

namespace Thainph\ShopifySdk\Exceptions;

use Exception;

class AttributeNotExists extends Exception
{
}
