<?php

namespace Thainph\ShopifySdk\Exceptions;

use Exception;

class ObjectNotFound extends Exception
{
}
