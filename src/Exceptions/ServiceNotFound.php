<?php

namespace Thainph\ShopifySdk\Exceptions;

use Exception;

class ServiceNotFound extends Exception
{
}
