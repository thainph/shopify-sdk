<?php
namespace Thainph\ShopifySdk\Helpers;

use Thainph\ShopifySdk\Enums\ShopifyObject;
use Thainph\ShopifySdk\Exceptions\InvalidInput;

class MappingHelper
{
    public static function getGraphIdById(string $object, string $id): string
    {
        if(!in_array($object, ShopifyObject::toArray())) {
            throw new InvalidInput('Invalid object');
        }

        return "gid://shopify/$object/$id";
    }

    public static function getIdByGraphId(string $graphId): string
    {
        $parts = explode('/', $graphId);
        return end($parts);
    }
}
