<?php
namespace Thainph\ShopifySdk\Helpers;

use Thainph\ShopifySdk\Exceptions\InvalidInput;

class ValidationHelper
{
    /**
     * @throws InvalidInput
     */
    public static function validate($params, $validatorClass): array
    {
        $validator = new $validatorClass($params);

        if ($validator->fails()) {
            throw new InvalidInput($validator->errors()->first());
        }

        return $validator->validated();
    }

}
