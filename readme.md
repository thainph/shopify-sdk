# Shopify SDK
 Shopify SDK for Laravel

## Convention
List files need to format

```
./vendor/bin/php-cs-fixer fix --dry-run
```

Format files

```
./vendor/bin/php-cs-fixer fix
```

Install automation formatter
```
cp pre-commit .git/hooks/ && chmod +x .git/hooks/pre-commit
```
